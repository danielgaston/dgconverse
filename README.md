![alt text](https://imgur.com/M55s6NF.png)

# DGConverse

[![Build Status](https://gitlab.com/danielgaston/dgconverse/badges/master/pipeline.svg?style=flat)](https://gitlab.com/danielgaston/dgconverse)
[![Version](https://img.shields.io/cocoapods/v/DGConverse.svg?style=flat)](https://cocoapods.org/pods/DGConverse)
[![License](https://img.shields.io/cocoapods/l/DGConverse.svg?style=flat)](https://cocoapods.org/pods/DGConverse)
[![Platform](https://img.shields.io/cocoapods/p/DGConverse.svg?style=flat)](https://cocoapods.org/pods/DGConverse)
[![Coverage](https://gitlab.com/danielgaston/dgconverse/badges/master/coverage.svg?style=flat)](https://gitlab.com/danielgaston/dgconverse)


- [x] 📱 iOS 11.0+

## Requirements

## Installation :inbox_tray:

### CocoaPods
We are indexed on [CocoaPods](http://cocoapods.org), which can be installed using [Ruby gems](https://rubygems.org/):
```shell
$ gem install cocoapods
```
Then simply add `DGConverse` to your `Podfile`.
```
pod 'DGConverse'
```
Lastly let CocoaPods do its thing by running:
```shell
$ pod install
```

## Dependencies :open_file_folder:

DGConverse has been designed not to directly depend on any particular framework or dependency, however due to its open nature, and to make it ready-to-use, it uses:

* [SPTPersistentCache](https://github.com/spotify/SPTPersistentCache): Used to cache data.
* [JSONModel](https://github.com/jsonmodel/jsonmodel): Used to map objects.

## Motivation :book:

The motivation behind creating this library is lighten a [UIViewController](https://developer.apple.com/documentation/uikit/uiviewcontroller) as much as possible by decoupling and isolating tasks that are not intrinsically related with it. 

This implementation heavily relies on dependency injection (injecting [DGConverseAPIOperationManager](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Manager/API/DGConverseAPIOperationManager.h)), notably reducing the coupling between components, getting rid of singletons or [UIAppDelegate](https://developer.apple.com/documentation/uikit/uiapplicationdelegate) calls. It’s a simple concept available in pure Objective-C language, so that third-party dependencies are no more required.

Each component logic (e.g. network) is totally isolated from any other component, following both SOLID and Clean Architecture principles. Thus, [UIViewControllers](https://developer.apple.com/documentation/uikit/uiviewcontroller) are only responsible for updating and displaying data, and injecting the operation manager to the next [UIViewController](https://developer.apple.com/documentation/uikit/uiviewcontroller) if using simple MVP architecture; or Interactors instead UIViewControllers in other architectures as VIPER or RIBs. The main concept is that in general terms, and continuing with the network example, network tasks are not accessible any more from the UIViewController, separating clearly the network layer from the UI layer. This comes with a great set of advantages in terms of testing. As the components are more decoupled and modularized, it is easier to test our application, or replace parts of them if necessary.

The library makes extensive use of [NSOperations](https://developer.apple.com/documentation/foundation/operation) as a wrapper for each particular network tasks (data, upload or download). But do not think it is limited only to networking tasks, because similar operation wrapping occurs in other tasks nature as caching and data conversion, which now they all are defined as a completely independent unit of work (operations).

This operation-based approach is quite helpful because, on the one hand, it defines and creates a repository of *producer operations* that can be easily reused in different projects as *consumer operations*. On the other hand it also facilitates the extension to an infinite number of operations (e.g. storage operations). For that reason this library, **is not a closed component**, I's been created as a **live** component that **comes with a predefined operations**, but **being also open to be extended**.

Let's drill down on how it works.

Each described operation is added to the corresponding [NSOperationQueue](https://developer.apple.com/documentation/foundation/nsoperationqueue) to be processed. Additionally, each operation has its own [priority](https://developer.apple.com/documentation/foundation/operation/queuepriority) so that it can be reordered in the queue while it is being waiting for the turn to be executed.

Additionally, not only operations have priority, queues have it as well defined as [QoS](https://developer.apple.com/documentation/foundation/operationqueue/1417919-qualityofservice)). QoS helps to manage and assign the available iOS resources to each particular queue, depending on the task nature it holds (e.g. high priority networking tasks, low priority networking tasks, generic tasks...).

These operation & queue design gets complemented for the case of network operations, by an internal network module architecture which allows complex requests to be created in a very flexible way.

The main benefit of having everything wrapped around operations is, firstly, the flexibility and power we get from them. By adding dependencies between operations it is possible to control its executing order and race conditions.
Secondly, operations provide a lot of execution information from themselves and from the queue where they are executed. For the case of network operations and as an example, they not only report connection status or connection speed, but also real time concurrent and maximum queue operations.

All this feedback is used as queue input for dynamically modifying the queue behaviour. In other words, this makes possible to reduce the number of concurrent operations so as not to saturate the available network bandwidth with additional requests; cancel requests halfway, suspend them, or simply stop all network interactions to be further resumed. And this can be applied not only on networking operations, but on any other type of operation you can imagine.

I hope you have enjoyed as much as I've done developing this personal project.

## Usage :eyes:

You can use DGConverse as it is ([Basic usage](#basic-usage)), or you can extend it to fulfil your project's requirements ([Advanced usage](#advanced-usage)).

### Basic Usage

The framework provides the basic operations to fetch data, cache it, parse it, and convert it to any appropriate format. Each operation acts as an independent working unit and could be accessed individually, however the most recommendable way to work with them is via [DGConverseAPIOperationManager](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Manager/API/DGConverseAPIOperationManager.h).

We can instantiate and hold an [DGConverseAPIOperationManager](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Manager/API/DGConverseAPIOperationManager.h). A recommended way to do it is via dependency injection on each ViewController. AppDelegate may instantiate it and pass it to the first ViewController, which in turn injects it to a second one and so on. Additionally we can also hold the **groupOp** in case we want to cancel in cascade all the operations.

Following, a few simplified examples about how to access the API.

#### GET
```objc
// manager instantiation
DGConverseAPIOperationManager *manager = [[DGConverseAPIOperationManager alloc] init];

// GET request
id<DGConverseAPIGroupBaseOperation> groupOp = [manager GET:@"https://jsonplaceholder.typicode.com/posts" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
	if (!result.error) {
    	NSLog(@"Fetched Obj: %@", result.responseObj);
    }
}];
```

#### POST
```objc
// manager instantiation
DGConverseAPIOperationManager *manager = [[DGConverseAPIOperationManager alloc] init];

// parameters object creation
DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
[params addKey:@"title" value:@"foo"];
[params addKey:@"body" value:@"bar"];
[params addKey:@"userId" value:@"1"];
    
// POST request
id<DGConverseAPIGroupBaseOperation> groupOp = [manager POST:@"https://jsonplaceholder.typicode.com/posts" parameters:params  progress:nil  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
	if (!result.error) {
		NSLog(@"Fetched Obj: %@", result.responseObj);
	}
}];
```

#### Download JSON
```objc
// manager instantiation
DGConverseAPIOperationManager *manager = [[DGConverseAPIOperationManager alloc] init];

// JSON download
id<DGConverseAPIGroupBaseOperation> groupOp = [manager downloadJSON:@"https://api.ipify.org?format=json" useCache:NO progress:nil completion:^(DGConverseAPIJSONResult *result) {
	if (!result.error) {
    	NSLog(@"JSON Obj: %@", result.responseObj);
	}
}];
```

#### Download Image
```objc
// manager instantiation
DGConverseAPIOperationManager *manager = [[DGConverseAPIOperationManager alloc] init];

// image download
id<DGConverseAPIGroupBaseOperation> groupOp = [manager downloadImage:@"https://en.wikipedia.org/wiki/Lenna#/media/File:Lenna_(test_image).png" useCache:YES progress:nil completion:^(DGConverseImageResult *result) {
	if (!result.error) {
		NSLog(@"UIImage Obj: %@", result.responseObj);
	}
}];
```

#### Download File
```objc
// manager instantiation
DGConverseAPIOperationManager *manager = [[DGConverseAPIOperationManager alloc] init];

// file download
id<DGConverseAPIGroupBaseOperation> groupOp = [manager downloadFile:@"http://www.test.com/myFile.pdf" inBackground:NO toDocumentsFolder:NO progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
	if (!result.error) {
		NSLog(@"File URL Location: %@", result.responseObj);
	}
}];
```

#### Upload File
```objc
// manager instantiation
DGConverseAPIOperationManager *manager = [[DGConverseAPIOperationManager alloc] init];

// file upload
id<DGConverseAPIGroupBaseOperation> groupOp = [manager uploadFile:@"URL_TO_FILE" to:@"URL_TO_UPLOAD" progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
	if (!result.error) {
		NSLog(@"Upload succeeded!");
	}
}];
```

Please check 'DGConverseAPIOperationManager' to see the complete list of methods.

### Advanced Usage

In case the provided set of methods in [DGConverseAPIOperationManager](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Manager/API/DGConverseAPIOperationManager.h) are not enough for your project's requirements, or if you simply want to include additional functionalities, the framework is prepared to be extended.

At this point it is important to understand that DGConverse has been designed around the idea of having multiple reusable independent NSOperations. Each operation (e.g. fetch data, cache data, parse JSON ...) belongs to a set of _'producer'_ operations. In a few words, what the [DGConverseAPIOperationManager](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Manager/API/DGConverseAPIOperationManager.h) does is convert those _'producer'_ operations into _'consumer'_ operations by picking and connecting the necessary ones in order to get the desired functionality.

For instance, if we want to fetch and parse a JSON, we could follow this basic design:
```
Network Operation (Request) -> Parse Operation (JSON)
```
but we could also add _'Caching'_, so that it could turn out to be like this:
```
Cache Operation (Read) -> Network Operation (Request) -> Cache Operation (Write) -> Parse Operation (JSON)
```

Let's imagine we want to request, fetch, validate and parse a **MyInitialData** object coming from our server as a JSON.
In a few words what we would do is:

* add **getMyInitialData** call our Operation Manager and implement the necessary operations' data flow.
* create a **MyJSONRequest** on which we can define a initialData request (or some other JSON requests if necessary).
* create a **MyInitialDataParseOperation** where we define the object to return and parsing details.
* create a **MyInitialDataModel** where we map JSON attributes with our model.

Now let's dig deeper a little bit more. This is what we would do explained from top (manager request) to bottom:

1. **MyOperationManager**

Extend [DGConverseAPIOperationManager](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Manager/API/DGConverseAPIOperationManager.h) (alternatively create a category) to include our custom API calls.

_MyOperationManager.h_
```objc 
#import "DGConverseAPIOperationManager.h"
#import "MyNetworkHeader.h"
#import "MyOperationsHeader.h"

@interface MyOperationManager : DGConverseAPIOperationManager

- (id<DGConverseAPIGroupBaseOperation>)getInitialDataWithProgress:(nullable DGConverseAPINetworkProgressBlock)progress
													   completion:(MyInitialDataResultBlock)completion;

@end
```

_MyOperationManager.m_
```objc
#import "MyOperationManager.h"

@implementation MyOperationManager

- (id<DGConverseAPIGroupBaseOperation>)getInitialDataWithProgress:(nullable DGConverseAPINetworkProgressBlock)progress
													   completion:(MyInitialDataResultBlock)completion
{
	MyJSONRequest* networkRequest = [[MyJSONRequest alloc] initInitialData];
	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest operationDelegate:self progress:progress completion:nil];
	MyInitialDataParseDataOperation* parseOp = [[MyInitialDataParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp useCache:NO];

	return group;
}
@end
```
------

2. **MyJSONRequest**

Extend [DGConverseAPIJSONDataRequest](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Network/API/DGConverseAPIRequest.h) since our model comes in JSON format.

_MyJSONRequest.h_
```objc
#import "DGConverseAPIRequest.h"

@interface MyJSONRequest : DGConverseAPIJSONDataRequest

#pragma mark - Initial Data

- (instancetype)initInitialData;

@end
```

_MyJSONRequest.m_
```objc
#import "MyJSONRequest.h"

@implementation MyJSONRequest

#pragma mark - Initial Data

- (instancetype)initInitialData
{
	NSString* relativeURLString = @"initialdata";
	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:@"http://my_server_url/"]] absoluteString];

	return [self initWithURLString:url method:GET parameters:nil body:nil];
}

@end
```

Create a _'MyNetworkHeader.h'_ and include this and any other created operation in it to easy import tasks.

------

3. **MyInitialDataParseOperation**

Extend [DGConverseAPIParseBaseOperation](https://gitlab.com/danielgaston/converse/blob/master/DGConverse/Networking/Operations/Operations/Parsing%20Operations/API/DGConverseAPIParseDataOperation.h) to reuse code and define our parsed data model.

_MyInitialDataParseDataOperation.h_
```objc
#import "DGConverseAPIParseDataOperation.h"
#import "MyModelHeader.h"

#pragma mark - Result Objects

@interface MyInitialDataResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) MyInitialDataModel *responseObj;
@end

#pragma mark - Result Block typedefs

typedef void (^MyInitialDataResultBlock)(MyInitialDataResult* result);

#pragma mark - Parse Operations

@interface MyInitialDataParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) MyInitialDataResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(MyInitialDataResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end
```

_MyInitialDataParseDataOperation.m_
```objc
#import "MyInitialDataParseDataOperation.h"

#pragma mark - Result Objects

@implementation MyInitialDataResult
@synthesize responseObj;
@end

#pragma mark - Parse Operations

@interface MyInitialDataParseDataOperation ()

@property (nonatomic, copy) MyInitialDataResultBlock completion;

@end

@implementation MyInitialDataParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(MyInitialDataResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"MyInitialData Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
        } else {
            NSError* serializationError;
            self.result.responseObj = [[MyInitialDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }  else {
                [self.result.responseObj extractModels:self.result.responseObj];
            }
        }
        
        if (self.completion) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.completion(self.result);
            }];
        }
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end
```

Create a _'MyOperationsHeader.h'_ and include this and any other created operation in it to easy import tasks.

------

4. **MyInitialDataModel**

Extend **JSONModel** to map the JSON object with our predefined model.

_MyInitialDataModel.h_
```objc
#import <JSONModel/JSONModel.h>

@interface MyInitialDataModel : JSONModel
@property (nonatomic, strong) NSString<Optional>* id;
@property (nonatomic, strong) NSString<Optional>* title;
@property (nonatomic, strong) NSString<Optional>* subtitle;
@end

```

_MyInitialDataModel.m_
```objc
#import "MyInitialDataModel.h"

@implementation MyInitialDataModel

@end
```

Create a _'MyModelHeader.h'_ and include this and any other created operation in it to easy import tasks.

## Contributing :mailbox_with_mail:
Contributions are welcomed, have a look at the [CONTRIBUTING.md](CONTRIBUTING.md) document for more information.

## Author 🚶

Daniel Gastón, daniel.gaston.iglesias@gmail.com

## License :memo:

DGConverse is available under the MIT license. See the LICENSE file for more info.
