# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- None

## [0.1.3] - 21-09-2018
### Added
- Main Development

### Changed
- Project Settings
- Pod Settings

### Removed
- None

## [0.1.2] - 19-09-2018
### Added
- Main Development
- Test purposes

### Changed
- Project Settings
- Pod Settings

### Removed
- None

## [0.1.1] - 19-09-2018
### Added
- Main Development
- Test purposes

### Changed
- Project Settings
- Pod Settings

### Removed
- None

## [0.1.0] - 19-09-2018
### Added
- Main Development
- Test purposes

### Changed
- Project Settings
- Pod Settings

### Removed
- None


[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
[0.1.3]: https://gitlab.com/danielgaston/converse/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/danielgaston/converse/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/danielgaston/converse/compare/0.1.0...0.1.1
