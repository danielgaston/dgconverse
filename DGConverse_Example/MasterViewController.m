//
//  MasterViewController.m
//  DGConverse_Example
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "MasterViewController.h"
#import "MasterDataSource.h"
#import "DetailViewController.h"

@interface MasterViewController ()

@property NSMutableArray *objects;

@property (nonatomic, strong) id<DGConverseAPIGroupBaseOperation> groupOp;

@end

@implementation MasterViewController

#pragma mark - View Lifecycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    [self initializePreProcessing];
    [self initializeDS_HTTPBasic];
    [self initializeDS_HTTPBasic_DefaultCredential];
    [self initializeDS_DownloadJSON];
    [self initializeDS_DownloadImageWithCache];
    [self initializeDS_DownloadImageWithoutCache];
    [self initializeDS_DownloadFile];
    [self initializeDS_DownloadFileInBg];
    [self initializeDS_DownloadFileToDocuments];
    [self initializeDS_DownloadFileToDocumentsInBg];
    [self initializeDS_UploadFile];
    [self initializeDS_UploadFileInBg];
    [self initializeDS_GET];
    [self initializeDS_HEAD];
    [self initializeDS_POST];
    [self initializeDS_PUT];
    [self initializeDS_PATCH];
    [self initializeDS_DELETE];
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}


#pragma mark - Initialization Methods

- (void)initializePreProcessing
{
    self.title = @"DGConverse Tests";
    self.tableView.accessibilityIdentifier = kAccessibilityId_Master_Table;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.operationManager = [DGConverseAPIOperationManager new];
    self.objects = @[].mutableCopy;
}

- (void)initializeDS_HTTPBasic
{
    //  user:guest password:guest
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"HTTP Basic";
    ds.accessId = kAccessibilityId_HttpBasic_Cell;
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        [self resetToDefault];
        NSLog(@"%@", [[NSURLCredentialStorage sharedCredentialStorage] allCredentials]);
        DGWeakify(self);
        self.groupOp = [self.operationManager data:@"https://jigsaw.w3.org/HTTP/Basic/" progress:^(NSProgress *progress) {
            DGStrongify(self);
            [self progressProcessing:progress];
        } completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
            DGStrongify(self);
            [self completionProcessing:result];
        }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_HTTPBasic_DefaultCredential
{
    //  user:guest password:guest
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"HTTP Basic Default Credential";
    ds.accessId = kAccessibilityId_HttpBasicWithCredential_Cell;
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        [self resetToDefault];
        NSLog(@"%@", [[NSURLCredentialStorage sharedCredentialStorage] allCredentials]);
        
        // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use NSURLCredentialPersistencePermanent)
        NSURLCredential *defaultCredential = [NSURLCredential credentialWithUser:@"guest" password:@"guest" persistence:NSURLCredentialPersistenceForSession];
        // 1. Create Protection Space for Server
        NSURLProtectionSpace *protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:@"jigsaw.w3.org" port:443 protocol:@"https" realm:@"test" authenticationMethod:NSURLAuthenticationMethodHTTPBasic];
        // 2. Set DEFAULT and GENERIC Credential for Protection Space for the system shared credential storage
        [[NSURLCredentialStorage sharedCredentialStorage] setDefaultCredential:defaultCredential forProtectionSpace:protectionSpace];
        
        DGWeakify(self);
        self.groupOp = [self.operationManager data:@"https://jigsaw.w3.org/HTTP/Basic/" progress:^(NSProgress *progress) {
            DGStrongify(self);
            [self progressProcessing:progress];
        } completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
            DGStrongify(self);
            [self completionProcessing:result];
        }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DownloadJSON
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download JSON";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        DGWeakify(self);
        
        self.groupOp = [self.operationManager downloadJSON:@"https://api.ipify.org?format=json"
                                                  useCache:NO
                                                  progress:^(NSProgress *progress) {
                                                        DGStrongify(self);
                                                        [self progressProcessing:progress];
                                                  } completion:^(DGConverseAPIJSONResult *result) {
                                                      DGStrongify(self);
                                                      [self completionProcessing:result];
                                                  }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DownloadImageWithCache
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download Image (cache)";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        self.groupOp = [self.operationManager downloadImage:@"https://images3.alphacoders.com/847/847928.jpg"
                                                   useCache:YES
                                                   progress:^(NSProgress *progress) {
                                                        DGStrongify(self);
                                                        [self progressProcessing:progress];
                                                   } completion:^(DGConverseImageResult *result) {
                                                        DGStrongify(self);
                                                        [self completionProcessing:result];
                                                   }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DownloadImageWithoutCache
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download Image (no cache)";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        self.groupOp = [self.operationManager downloadImage:@"https://images3.alphacoders.com/847/847928.jpg"
                                                   useCache:NO
                                                   progress:^(NSProgress *progress) {
                                                   DGStrongify(self);
                                                   [self progressProcessing:progress];
                                                   } completion:^(DGConverseImageResult *result) {
                                                       DGStrongify(self);
                                                       [self completionProcessing:result];
                                                   }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DownloadFile
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download File";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        self.groupOp = [self.operationManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                              inBackground:NO
                                         toDocumentsFolder:NO
                                                  progress:^(NSProgress *progress) {
                                                  DGStrongify(self);
                                                  [self progressProcessing:progress];
                                                  }
                                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                                    DGStrongify(self);
                                                    [self completionProcessing:result];
                                                }];
    };
    
    [self.objects addObject:ds];
}

-(void)initializeDS_DownloadFileInBg
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download File in Bg";
    ds.accessId = kAccessibilityId_DownloadFileInBg_Cell;
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        self.groupOp = [self.operationManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                              inBackground:YES
                                         toDocumentsFolder:NO
                                                  progress:^(NSProgress *progress) {
                                                  DGStrongify(self);
                                                  [self progressProcessing:progress];
                                                  }
                                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                                    DGStrongify(self);
                                                    [self completionProcessing:result];
                                                }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DownloadFileToDocuments
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download File to Docs";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        self.groupOp = [self.operationManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                              inBackground:NO
                                         toDocumentsFolder:YES
                                                  progress:^(NSProgress *progress) {
                                                  DGStrongify(self);
                                                  [self progressProcessing:progress];
                                                  }
                                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                                    DGStrongify(self);
                                                    [self completionProcessing:result];
                                                }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DownloadFileToDocumentsInBg
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Download File to Docs In Bg";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        self.groupOp = [self.operationManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                              inBackground:YES
                                         toDocumentsFolder:YES
                                                  progress:^(NSProgress *progress) {
                                                  DGStrongify(self);
                                                  [self progressProcessing:progress];
                                                  }
                                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                                    DGStrongify(self);
                                                    [self completionProcessing:result];
                                                }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_UploadFile
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Upload File";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
        
        self.groupOp = [self.operationManager uploadFile:url.absoluteString
                                                      to:@"http://ptsv2.com/t/converse/post"
                                            inBackground:NO
                                                progress:^(NSProgress *progress) {
                                                DGStrongify(self);
                                                [self progressProcessing:progress];
                                                }
                                              completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                                  DGStrongify(self);
                                                  [self completionProcessing:result];
                                              }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_UploadFileInBg
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"Upload File In Bg";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
        
        self.groupOp = [self.operationManager uploadFile:url.absoluteString
                                                      to:@"http://ptsv2.com/t/converse/post"
                                            inBackground:YES
                                                progress:^(NSProgress *progress) {
                                                DGStrongify(self);
                                                [self progressProcessing:progress];
                                                }
                                              completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                                  DGStrongify(self);
                                                  [self completionProcessing:result];
                                              }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_GET
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"GET";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        self.groupOp = [self.operationManager GET:@"https://jsonplaceholder.typicode.com/posts"
                                       parameters:nil
                                       completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                           DGStrongify(self);
                                           [self completionProcessing:result];
                                       }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_HEAD
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"HEAD";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        self.groupOp = [self.operationManager HEAD:@"https://jsonplaceholder.typicode.com/posts"
                                        parameters:nil
                                        completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                            DGStrongify(self);
                                            [self completionProcessing:result];
                                        }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_POST
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"POST";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
        [params addKey:@"title" value:@"foo"];
        [params addKey:@"body" value:@"bar"];
        [params addKey:@"userId" value:@"1"];
        
        self.groupOp = [self.operationManager POST:@"https://jsonplaceholder.typicode.com/posts"
                                        parameters:params
                                        completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                            DGStrongify(self);
                                            [self completionProcessing:result];
                                        }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_PUT
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"PUT";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
        [params addKey:@"id" value:@"1"];
        [params addKey:@"title" value:@"foo"];
        [params addKey:@"body" value:@"bar"];
        [params addKey:@"userId" value:@"1"];
        
        self.groupOp = [self.operationManager PUT:@"https://jsonplaceholder.typicode.com/posts/1"
                                       parameters:params
                                       completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                           DGStrongify(self);
                                           [self completionProcessing:result];
                                       }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_PATCH
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"PATCH";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        
        DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
        [params addKey:@"title" value:@"foo"];

        self.groupOp = [self.operationManager PATCH:@"https://jsonplaceholder.typicode.com/posts/1"
                                         parameters:nil
                                         completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                             DGStrongify(self);
                                             [self completionProcessing:result];
                                         }];
    };
    
    [self.objects addObject:ds];
}

- (void)initializeDS_DELETE
{
    MasterDataSource *ds = [MasterDataSource new];
    ds.name = @"DELETE";
    DGWeakify(self);
    ds.completionBlock = ^(void){
        DGStrongify(self);
        
        DGWeakify(self);
        self.groupOp = [self.operationManager DELETE:@"https://jsonplaceholder.typicode.com/posts/1"
                                          parameters:nil
                                          completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                              DGStrongify(self);
                                              [self completionProcessing:result];
                                          }];
    };
    
    [self.objects addObject:ds];
}


#pragma mark - Helper Methods

- (void)resetToDefault
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] removeCookiesSinceDate:[NSDate distantPast]];

    // Removes all Credentials from the sharedCredentialStorage
    NSDictionary<NSURLProtectionSpace *, NSDictionary<NSString *, NSURLCredential *> *> *allCredentials = [[NSURLCredentialStorage sharedCredentialStorage] allCredentials];

    if (allCredentials.count) {
        NSEnumerator<NSURLProtectionSpace *> *protectionSpaceEnumerator = allCredentials.keyEnumerator;
        NSURLProtectionSpace *protectionSpace;
        
        while (protectionSpace = [protectionSpaceEnumerator nextObject]) {
            
            NSEnumerator<NSString *> *userNameEnumerator = [allCredentials objectForKey:protectionSpace].keyEnumerator;
            NSString *userName;
            
            while (userName = [userNameEnumerator nextObject]) {
            
                NSURLCredential *credential = [[allCredentials objectForKey:protectionSpace] objectForKey:userName];
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:credential forProtectionSpace:protectionSpace];
            }
        }
    }
}

- (void)progressProcessing:(NSProgress*)progress
{
    self.detailViewController.progressLabel.text = [progress localizedDescription];
    self.detailViewController.additionalProgressLabel.text = [progress localizedAdditionalDescription];
    [self.detailViewController.progressView setProgress:progress.fractionCompleted animated:YES];
}

- (void)completionProcessing:(id<DGConverseAPIOperationResult>)result
{
    DGConverseNetworkOperationResultExtraInfo *extraInfo = [result.extraInfo objectForKey:[DGConverseNetworkOperationResultExtraInfo key]];
    
    if (result.error) {
        self.detailViewController.descriptionTextView.textColor = [UIColor colorWithRed:0.44 green:0.1 blue:0.01 alpha:1];
        self.detailViewController.descriptionTextView.text = result.error.localizedDescription;
    } else {
        self.detailViewController.descriptionTextView.textColor = [UIColor colorWithRed:0.01 green:0.44 blue:0.01 alpha:1];
        self.detailViewController.descriptionTextView.text = [NSString stringWithFormat:@"Status: %ld \n\n %@",((NSHTTPURLResponse *)extraInfo.responseURL).statusCode, ((NSHTTPURLResponse *)extraInfo.responseURL).debugDescription];
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {

        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        MasterDataSource *ds = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:ds];
        self.detailViewController = controller;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - UITableView & UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    MasterDataSource *ds = self.objects[indexPath.row];
    cell.textLabel.text = [ds name];
    cell.accessibilityIdentifier = [ds accessId];
    return cell;
}

@end
