//
//  AppDelegate.h
//  DGConverse_Example
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

