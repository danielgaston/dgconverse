//
//  DetailViewController.h
//  DGConverse_Example
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterDataSource.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) MasterDataSource *detailItem;

@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *additionalProgressLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@end

