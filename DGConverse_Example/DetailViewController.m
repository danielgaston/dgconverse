//
//  DetailViewController.m
//  DGConverse_Example
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)configureView
{
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.title = [self.detailItem name];
        self.progressLabel.text = @"";
        self.additionalProgressLabel.text = @"";
        [self.progressView setProgress:0 animated:NO];
        self.descriptionTextView.text = @"Waiting Response...";
        self.descriptionTextView.textColor = [UIColor darkTextColor];
        self.detailItem.completionBlock();
    }
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(MasterDataSource *)newDetailItem
{
    _detailItem = newDetailItem;
    
    // Update the view.
    [self configureView];
}

@end
