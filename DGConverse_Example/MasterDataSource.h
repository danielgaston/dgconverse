//
//  MasterDataSource.h
//  DGConverse_Example
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MasterDataSource : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *accessId;
@property (nonatomic, copy) void (^completionBlock)(void);


@end

NS_ASSUME_NONNULL_END
