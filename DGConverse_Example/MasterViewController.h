//
//  MasterViewController.h
//  DGConverse_Example
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <UIKit/UIKit.h>

@import DGConverse;

#define kAccessibilityId_Master_Table @"MainTableId"
#define kAccessibilityId_HttpBasic_Cell @"HttpBasicId"
#define kAccessibilityId_HttpBasicWithCredential_Cell @"HttpBasicWithCredentialId"
#define kAccessibilityId_DownloadFileInBg_Cell @"DownloadFileInBgId"

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DGConverseAPIOperationManager *operationManager;

@end


