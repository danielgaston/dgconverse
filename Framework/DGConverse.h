//
//  DGConverse.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//! Project version number for converse.
FOUNDATION_EXPORT double DGConverseVersionNumber;

//! Project version string for converse.
FOUNDATION_EXPORT const unsigned char DGConverseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DGConverse/PublicHeader.h>. It is also used by CocoaPods, as Accessing to 'DGConverse-Prefix.pch' and compile sources with it, it is more problematic.

#ifndef _DGCONVERSE_
#define _DGCONVERSE_

#import <DGConverse/DGClangMacro.h>
#import <DGConverse/DGOwnershipMacro.h>
#import <DGConverse/NSData+Base64.h>
#import <DGConverse/NSDate+Additions.h>
#import <DGConverse/NSString+Additions.h>

#import <DGConverse/DGConverseAPIOperationManagerHeader.h>
#import <DGConverse/DGConverseAPIOperationManager.h>
#import <DGConverse/DGConverseAPIQueueManager.h>

#import <DGConverse/DGConverseAPINetworkHeader.h>
#import <DGConverse/DGConverseAPIClient.h>
#import <DGConverse/DGConverseAPIMethod.h>
#import <DGConverse/DGConverseAPIRequest.h>
#import <DGConverse/DGConverseAPIRequestParameters.h>
#import <DGConverse/DGConverseAPIRequestSerialization.h>
#import <DGConverse/DGConverseAPIResponse.h>
#import <DGConverse/DGConverseAPIResponseSerialization.h>

#import <DGConverse/DGConverseAPINetworkOperationClient.h>

#import <DGConverse/DGConverseAPIGroupOperationHeader.h>
#import <DGConverse/DGConverseAPIGroupBaseOperation.h>
#import <DGConverse/DGConverseAPIGroupDataOperation.h>
#import <DGConverse/DGConverseAPIGroupDownloadOperation.h>
#import <DGConverse/DGConverseAPIGroupGeneralOperation.h>
#import <DGConverse/DGConverseAPIGroupImageOperation.h>
#import <DGConverse/DGConverseAPIGroupUploadOperation.h>

#import <DGConverse/DGConverseAPIAdapterOperationHeader.h>
#import <DGConverse/DGConverseAPIAdapterBaseOperation.h>

#import <DGConverse/DGConverseAPIBaseOperationProtocol.h>
#import <DGConverse/DGConverseAPIBaseOperation.h>
#import <DGConverse/DGConverseAPIOperationResponse.h>

#import <DGConverse/DGConverseAPIBlockBaseOperation.h>

#import <DGConverse/DGConverseAPICacheManager.h>

#import <DGConverse/DGConverseAPICacheOperationHeader.h>
#import <DGConverse/DGConverseAPICacheBaseOperation.h>
#import <DGConverse/DGConverseAPICacheFileOperation.h>
#import <DGConverse/DGConverseAPICacheJSONOperation.h>
#import <DGConverse/DGConverseAPICacheImageOperation.h>
#import <DGConverse/DGConverseAPICacheVideoOperation.h>

#import <DGConverse/DGConverseAPIConversionOperationHeader.h>
#import <DGConverse/DGConverseAPIConversionBaseOperation.h>
#import <DGConverse/DGConverseAPIConversionImageOperation.h>

#import <DGConverse/DGConverseAPIMutExBaseOperation.h>
#import <DGConverse/DGConverseAPIExclusivityController.h>
#import <DGConverse/DGConverseAPIMutExAlertData.h>
#import <DGConverse/DGConverseAPIMutExAlertOperation.h>
#import <DGConverse/DGConverseAPIAlertManager.h>
#import <DGConverse/DGConverseAPIOperationQueue.h>

#import <DGConverse/DGConverseAPINetworkOperationHeader.h>
#import <DGConverse/DGConverseAPINetworkBaseOperation.h>
#import <DGConverse/DGConverseAPINetworkDataOperation.h>
#import <DGConverse/DGConverseAPINetworkDownloadOperation.h>
#import <DGConverse/DGConverseAPINetworkUploadOperation.h>

#import <DGConverse/DGConverseAPIParseOperationHeader.h>
#import <DGConverse/DGConverseAPIParseBaseOperation.h>
#import <DGConverse/DGConverseAPIParseDataOperation.h>
#import <DGConverse/DGConverseAPIParseJSONOperation.h>

#import <DGConverse/DGConverseAPIDebugUtils.h>

#import <DGConverse/JSONValueTransformer+CustomDataTransformers.h>

#endif /* _DGCONVERSE_ */

