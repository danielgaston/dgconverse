//
//  DGConverseDateTest.m
//  DGConverseTests
//
//  Copyright © 2019 DG. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDate+Additions.h"

@interface DGConverseDateTest : XCTestCase

@end

static NSDate *jan24th = nil;

@implementation DGConverseDateTest

+ (void)setUp
{
    [super setUp];
    if (!jan24th)
        jan24th = [[NSDate dateWithTimeIntervalSince1970:443750400] startOfDay];
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"************************** START ******************************");
    NSLog(@"---------------------------------------------------------------");
}

- (void)tearDown
{
    NSLog(@"---------------------------------------------------------------");
    NSLog(@"*************************** END *******************************");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark- DATE TESTS
#pragma mark-- Nullification
- (void)test_now
{
    XCTAssertNotNil([NSDate now], "date should not be nil");
}
- (void)test_yesterday
{
    XCTAssertNotNil([NSDate yesterday], "date should not be nil");
}
- (void)test_tomorrow
{
    XCTAssertNotNil([NSDate tomorrow], "date should not be nil");
}
- (void)test_midnight_class
{
    XCTAssertNotNil([NSDate midnight], "date should not be nil");
}
- (void)test_weekStartsWithDay
{
    XCTAssertTrue([NSDate weekStartsWithDay] != -1, "date should not be nil");
}
- (void)test_weekEndsWithDay
{
    XCTAssertTrue([NSDate weekEndsWithDay] != -1, "date should not be nil");
}
- (void)test_dateByAddingYears_class
{
    XCTAssertNotNil([NSDate dateByAddingYears:3], "date should not be nil");
}
- (void)test_dateByAddingMonths_class
{
    XCTAssertNotNil([NSDate dateByAddingMonths:3], "date should not be nil");
}
- (void)test_dateByAddingWeeks_class
{
    XCTAssertNotNil([NSDate dateByAddingWeeks:3], "date should not be nil");
}
- (void)test_dateByAddingDays_class
{
    XCTAssertNotNil([NSDate dateByAddingDays:3], "date should not be nil");
}
- (void)test_dateByAddingHours_class
{
    XCTAssertNotNil([NSDate dateByAddingHours:3], "date should not be nil");
}
- (void)test_dateByAddingMinutes_class
{
    XCTAssertNotNil([NSDate dateByAddingMinutes:3], "date should not be nil");
}
- (void)test_dateByAddingSeconds_class
{
    XCTAssertNotNil([NSDate dateByAddingSeconds:3], "date should not be nil");
}
- (void)test_dateBySubtractingYears_class
{
    XCTAssertNotNil([NSDate dateBySubtractingYears:3], "date should not be nil");
}
- (void)test_dateBySubtractingMonths_class
{
    XCTAssertNotNil([NSDate dateBySubtractingMonths:3], "date should not be nil");
}
- (void)test_dateBySubtractingWeeks_class
{
    XCTAssertNotNil([NSDate dateBySubtractingWeeks:3], "date should not be nil");
}
- (void)test_dateBySubtractingDays_class
{
    XCTAssertNotNil([NSDate dateBySubtractingDays:3], "date should not be nil");
}
- (void)test_dateBySubtractingHours_class
{
    XCTAssertNotNil([NSDate dateBySubtractingHours:3], "date should not be nil");
}
- (void)test_dateBySubtractingMinutes_class
{
    XCTAssertNotNil([NSDate dateBySubtractingMinutes:3], "date should not be nil");
}
- (void)test_dateBySubtractingSeconds_class
{
    XCTAssertNotNil([NSDate dateBySubtractingSeconds:3], "date should not be nil");
}


#pragma mark-- Date Comparison

- (void)test_mediumDateBetweenDate
{
    XCTAssertNotNil([NSDate mediumDateBetweenDate:[NSDate now] andDate:[NSDate now]], "date should not be nil");
}
- (void)test_hoursBetween
{
    XCTAssertTrue([NSDate hoursBetween:[NSDate now] and:[NSDate now]] == 0, "date should not be nil");
}


#pragma mark-- Date Decomposing

- (void)test_year
{
    XCTAssertTrue(jan24th.year == 1984, "wrong year");
}
- (void)test_month
{
    XCTAssertTrue(jan24th.month == 1, "wrong month");
}
- (void)test_week
{
    XCTAssertTrue(jan24th.week == 4, "");
}
- (void)test_weekday
{
    XCTAssertTrue(jan24th.weekday == 3, "wrong weekday");
}
- (void)test_nthWeekday
{
    XCTAssertTrue(jan24th.nthWeekday == 4, "wrong nthWeekday");
}
- (void)test_day
{
    XCTAssertTrue(jan24th.day == 24, "");
}
- (void)test_hour
{
    XCTAssertTrue(jan24th.hour == 0, "");
}
- (void)test_minute
{
    XCTAssertTrue(jan24th.minute == 0, "");
}
- (void)test_seconds
{
    XCTAssertTrue(jan24th.seconds == 0, "");
}


#pragma mark-- Date String

- (void)test_stringWithFormat
{
    XCTAssertNotNil([jan24th stringWithFormat:nil], "string should not be nil");
}
- (void)test_stringWithDateStyle
{
    XCTAssertNotNil([jan24th stringWithDateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle], "string should not be nil");
}
- (void)test_shortDateString
{
    XCTAssertNotNil([jan24th shortDateString], "string should not be nil");
}
- (void)test_shortTimeString
{
    XCTAssertNotNil([jan24th shortTimeString], "string should not be nil");
}
- (void)test_shortDateTimeString
{
    XCTAssertNotNil([jan24th shortDateTimeString], "string should not be nil");
}
- (void)test_mediumDateString
{
    XCTAssertNotNil([jan24th mediumDateString], "string should not be nil");
}
- (void)test_mediumTimeString
{
    XCTAssertNotNil([jan24th mediumTimeString], "string should not be nil");
}
- (void)test_mediumDateTimeString
{
    XCTAssertNotNil([jan24th mediumDateTimeString], "string should not be nil");
}
- (void)test_longDateString
{
    XCTAssertNotNil([jan24th longDateString], "string should not be nil");
}
- (void)test_longTimeString
{
    XCTAssertNotNil([jan24th longTimeString], "string should not be nil");
}
- (void)test_longDateTimeString
{
    XCTAssertNotNil([jan24th longDateTimeString], "string should not be nil");
}
- (void)test_longDateShortTimeString
{
    XCTAssertNotNil([jan24th longDateShortTimeString], "string should not be nil");
}
- (void)test_customShortMonthAndDayString
{
    XCTAssertNotNil([jan24th customShortMonthAndDayString], "string should not be nil");
}
- (void)test_customShortMonthAndYearString
{
    XCTAssertNotNil([jan24th customShortMonthAndYearString], "string should not be nil");
}
- (void)test_customShortWeekdayAndDayString
{
    XCTAssertNotNil([jan24th customShortWeekdayAndDayString], "string should not be nil");
}
- (void)test_customLongMonthAndYearString
{
    XCTAssertNotNil([jan24th customLongMonthAndYearString], "string should not be nil");
}
- (void)test_customLongMonthDayYearString
{
    XCTAssertNotNil([jan24th customLongMonthDayYearString], "string should not be nil");
}
- (void)test_customLongWeekdayString
{
    XCTAssertNotNil([jan24th customLongWeekdayString], "string should not be nil");
}
- (void)test_customLongWeekdayAndDayString
{
    XCTAssertNotNil([jan24th customLongWeekdayAndDayString], "string should not be nil");
}
- (void)test_customLongWeekdayMonthDayYearString
{
    XCTAssertNotNil([jan24th customLongWeekdayMonthDayYearString], "string should not be nil");
}


#pragma mark-- Date Comparisons

- (void)test_isToday
{
    XCTAssertTrue([[NSDate now] isToday], "its not today");
}
- (void)test_isTomorrow
{
    XCTAssertTrue([[[NSDate now] dateByAddingDays:1] isTomorrow], "");
}
- (void)test_isYesterday
{
    XCTAssertTrue([[[NSDate now] dateBySubtractingDays:1] isYesterday], "");
}
- (void)test_isSameWeekAsDate
{
    XCTAssertTrue([jan24th isSameWeekAsDate:[jan24th dateByAddingDays:1]], "");
}
- (void)test_isThisWeek
{
    XCTAssertTrue([[NSDate now] isThisWeek], "");
}
- (void)test_isNextWeek
{
    XCTAssertFalse([[NSDate now] isNextWeek], "");
}
- (void)test_isLastWeek
{
    XCTAssertFalse([[NSDate now] isLastWeek], "");
}
- (void)test_isSameMonthAsDate
{
    XCTAssertTrue([jan24th isSameMonthAsDate:[jan24th dateByAddingDays:1]], "");
}
- (void)test_isSameDayAsDate
{
    XCTAssertTrue([[jan24th startOfDay] isSameDayAsDate:[jan24th endOfDay]], "");
}
- (void)test_isThisMonth
{
    XCTAssertTrue([[NSDate now] isThisMonth], "");
}
- (void)test_isNextMonth
{
    XCTAssertFalse([[NSDate now] isNextMonth], "");
}
- (void)test_isLastMonth
{
    XCTAssertFalse([[NSDate now] isLastMonth], "");
}
- (void)test_isSameYearAsDate
{
    XCTAssertTrue([[jan24th startOfDay] isSameYearAsDate:[jan24th endOfDay]], "");
}
- (void)test_isThisYear
{
    XCTAssertTrue([[NSDate now] isThisYear], "");
}
- (void)test_isNextYear
{
    XCTAssertTrue([[[NSDate now] dateByAddingYears:1] isNextYear], "");
}
- (void)test_isLastYear
{
    XCTAssertTrue([[[NSDate now] dateBySubtractingYears:1] isLastYear], "");
}
- (void)test_isInFuture
{
    XCTAssertTrue([[[NSDate now] dateByAddingYears:1] isInFuture], "");
}
- (void)test_isInPast
{
    XCTAssertTrue([[[NSDate now] dateBySubtractingYears:1] isInPast], "");
}
- (void)test_isTypicallyWorkday
{
    XCTAssertTrue([jan24th isTypicallyWorkday], "");
}
- (void)test_isTypicallyWeekend
{
    XCTAssertTrue([[jan24th endOfWeekend] isTypicallyWeekend], "");
}

#pragma mark -- Date Operations

- (void)test_dateByAddingYears_instance
{
    XCTAssertNotNil([jan24th dateByAddingYears:1], "");
}
- (void)test_dateByAddingMonths_instance
{
    XCTAssertNotNil([jan24th dateByAddingMonths:1], "");
}
- (void)test_dateByAddingWeeks_instance
{
    XCTAssertNotNil([jan24th dateByAddingWeeks:1], "");
}
- (void)test_dateByAddingDays_instance
{
    XCTAssertNotNil([jan24th dateByAddingDays:1], "");
}
- (void)test_dateByAddingHours_instance
{
    XCTAssertNotNil([jan24th dateByAddingHours:1], "");
}
- (void)test_dateByAddingMinutes_instance
{
    XCTAssertNotNil([jan24th dateByAddingMinutes:1], "");
}
- (void)test_dateByAddingSeconds_instance
{
    XCTAssertNotNil([jan24th dateByAddingSeconds:1], "");
}
- (void)test_dateBySubtractingYears_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingYears:1], "");
}
- (void)test_dateBySubtractingMonths_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingMonths:1], "");
}
- (void)test_dateBySubtractingWeeks_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingWeeks:1], "");
}
- (void)test_dateBySubtractingDays_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingDays:1], "");
}
- (void)test_dateBySubtractingHours_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingHours:1], "");
}
- (void)test_dateBySubtractingMinutes_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingMinutes:1], "");
}
- (void)test_dateBySubtractingSeconds_instance
{
    XCTAssertNotNil([jan24th dateBySubtractingSeconds:1], "");
}
- (void)test_dateByAddingCustom
{
    XCTAssertNotNil([jan24th dateByAddingYears:1 months:1 weeks:1 days:1 hours:1 minutes:1 seconds:1], "");
}
- (void)test_dateBySettingHour
{
    XCTAssertNotNil([jan24th dateBySettingHour:1 minute:2 second:3], "");
}
- (void)test_dateByRoundingUp
{
    XCTAssertNotNil([jan24th dateByRoundingUp], "");
}
- (void)test_nextDay
{
    XCTAssertNotNil([jan24th nextDay], "");
}
- (void)test_previousDay
{
    XCTAssertNotNil([jan24th previousDay], "");
}
- (void)test_nextWeek
{
    XCTAssertNotNil([jan24th nextWeek], "");
}
- (void)test_previousWeek
{
    XCTAssertNotNil([jan24th previousWeek], "");
}
- (void)test_nextMonth
{
    XCTAssertNotNil([jan24th nextMonth], "");
}
- (void)test_previousMonth
{
    XCTAssertNotNil([jan24th previousMonth], "");
}
- (void)test_startOfDay
{
    XCTAssertNotNil([jan24th startOfDay], "");
}
- (void)test_endOfDay
{
    XCTAssertNotNil([jan24th endOfDay], "");
}
- (void)test_midnight_instace
{
    XCTAssertNotNil([jan24th midnight], "");
}
- (void)test_firstDayOfWeek
{
    XCTAssertNotNil([jan24th firstDayOfWeek], "");
}
- (void)test_lastDayOfWeek
{
    XCTAssertNotNil([jan24th lastDayOfWeek], "");
}
- (void)test_firstDayOfMonth
{
    XCTAssertNotNil([jan24th firstDayOfMonth], "");
}
- (void)test_lastDayOfMonth
{
    XCTAssertNotNil([jan24th lastDayOfMonth], "");
}
- (void)test_startOfWeek
{
    XCTAssertNotNil([jan24th startOfWeek], "");
}
- (void)test_endOfWeek
{
    XCTAssertNotNil([jan24th endOfWeek], "");
}
- (void)test_startOfMonth
{
    XCTAssertNotNil([jan24th startOfMonth], "");
}
- (void)test_endOfMonth
{
    XCTAssertNotNil([jan24th endOfMonth], "");
}
- (void)test_dateWithDay
{
    XCTAssertNotNil([jan24th dateWithDay:1 direction:NSOrderedAscending includeCurrent:NO], "");
}
- (void)test_startOfWeekend
{
    XCTAssertNotNil([jan24th startOfWeekend], "");
}
- (void)test_endOfWeekend
{
    XCTAssertNotNil([jan24th endOfWeekend], "");
}
- (void)test_nextSunday
{
    XCTAssertNotNil([jan24th nextSunday], "");
}
- (void)test_nextMonday
{
    XCTAssertNotNil([jan24th nextMonday], "");
}
- (void)test_nextTuesday
{
    XCTAssertNotNil([jan24th nextTuesday], "");
}
- (void)test_nextWednesday
{
    XCTAssertNotNil([jan24th nextWednesday], "");
}
- (void)test_nextThursday
{
    XCTAssertNotNil([jan24th nextThursday], "");
}
- (void)test_nextFriday
{
    XCTAssertNotNil([jan24th nextFriday], "");
}
- (void)test_nextSaturday
{
    XCTAssertNotNil([jan24th nextSaturday], "");
}
- (void)test_componentsWithOffsetFromDate
{
    XCTAssertNotNil([jan24th componentsWithOffsetFromDate:[NSDate now]], "");
}
- (void)test_dateAtStartOfWeek
{
    XCTAssertNotNil([jan24th dateAtStartOfWeek], "");
}
- (void)test_dateAtStartOfMonth
{
    XCTAssertNotNil([jan24th dateAtStartOfMonth], "");
}
- (void)test_dateAtNearest5Minute
{
    XCTAssertNotNil([jan24th dateAtNearest5Minute], "");
}
- (void)test_dateWithCurrentTime
{
    XCTAssertNotNil([jan24th dateWithCurrentTime], "");
}


#pragma mark -- Date Interval

- (void)test_daysAfterDate
{
    XCTAssertTrue([[jan24th nextDay] daysAfterDate:jan24th] == 1, "");
}
- (void)test_daysBeforeDate
{
    XCTAssertTrue([[jan24th previousDay] daysBeforeDate:jan24th] == 1, "");
}
- (void)test_hoursAfterDate
{
    XCTAssertTrue([[[jan24th nextDay] startOfDay] hoursAfterDate:[jan24th startOfDay]] == 24, "");
}
- (void)test_hoursBeforeDate
{
    XCTAssertTrue([[[jan24th previousDay] startOfDay] hoursBeforeDate:[jan24th startOfDay]] == 24, "");
}
- (void)test_minutesAfterDate
{
    XCTAssertTrue([[jan24th dateByAddingMinutes:1] minutesAfterDate:jan24th] == 1, "");
}
- (void)test_minutesBeforeDate
{
    XCTAssertTrue([[jan24th dateBySubtractingMinutes:1] minutesBeforeDate:jan24th] == 1, "");
}
- (void)test_secondsAfterDate
{
    XCTAssertTrue([[jan24th dateByAddingSeconds:1] secondsAfterDate:jan24th] == 1, "");
}
- (void)test_secondsBeforeDate
{
    XCTAssertTrue([[jan24th dateBySubtractingSeconds:1] secondsBeforeDate:jan24th] == 1, "");
}
- (void)test_distanceInDaysToDate
{
    XCTAssertTrue([[jan24th previousDay] distanceInDaysToDate:jan24th] == 1, "");
}


#pragma mark -- Date Comparison

- (void)test_isEarlierThanDate
{
    XCTAssertTrue([[jan24th previousDay] isEarlierThanDate:jan24th], "");
}
- (void)test_isLaterThanDate
{
    XCTAssertTrue([[jan24th nextDay] isLaterThanDate:jan24th], "");
}
- (void)test_isEarlierOrEqualToDate
{
    XCTAssertTrue([jan24th isEarlierOrEqualToDate:jan24th], "");
}
- (void)test_isLaterOrEqualToDate
{
    XCTAssertTrue([jan24th isLaterOrEqualToDate:jan24th], "");
}
- (void)test_isBetweenDate
{
    XCTAssertTrue([jan24th isBetweenDate:[jan24th previousDay] andDate:[jan24th nextDay]], "");
}
- (void)test_converseDateNameString
{
    XCTAssertNotNil([jan24th converseDateNameString], "");
}


#pragma mark - DATE FORMATTER

- (void)test_formatterWithDateFormat_class_1
{
    XCTAssertNotNil([NSDateFormatter formatterWithDateFormat:@"MM-dd-yyyy HH:mm"], "");
}
- (void)test_formatterWithDateFormat_class_2
{
    XCTAssertNotNil([NSDateFormatter formatterWithDateFormat:@"MM-dd-yyyy HH:mm" locale:[NSLocale currentLocale]], "");
}
- (void)test_formatterWithDateFormat_class_3
{
    XCTAssertNotNil([NSDateFormatter formatterWithDateFormat:@"MM-dd-yyyy HH:mm" locale:[NSLocale currentLocale] timeZone:[NSTimeZone localTimeZone]], "");
}
- (void)test_shortTimeStringForDate_class
{
    XCTAssertNotNil([NSDateFormatter shortTimeStringForDate:jan24th], "");
}
- (void)test_shortTimeStringForDate_instance
{
    XCTAssertNotNil([[NSDateFormatter formatterWithDateFormat:nil] shortTimeStringForDate:jan24th timeZone:nil], "");
}
- (void)test_longTimeStringForDate_instance
{
    XCTAssertNotNil([[NSDateFormatter formatterWithDateFormat:nil] longTimeStringForDate:jan24th timeZone:nil], "");
}
- (void)test_shortDateStringForDate_class
{
    XCTAssertNotNil([NSDateFormatter shortDateStringForDate:jan24th], "");
}
- (void)test_fullDateStringForDate_class
{
    XCTAssertNotNil([NSDateFormatter fullDateStringForDate:jan24th], "");
}
- (void)test_shortTimeStringForDateInOrbit_class
{
    XCTAssertNotNil([NSDateFormatter shortTimeStringForDateInOrbit:jan24th], "");
}
- (void)test_dateStringForDate_class
{
    XCTAssertNotNil([NSDateFormatter dateStringForDate:jan24th], "");
}
- (void)test_longDateStringForDate_class
{
    XCTAssertNotNil([NSDateFormatter longDateStringForDate:jan24th], "");
}
- (void)test_casualNameForDate_class
{
    XCTAssertNotNil([NSDateFormatter casualNameForDate:jan24th], "");
}
- (void)test_weekDayOrCasualNameForDate_class
{
    XCTAssertNotNil([NSDateFormatter weekDayOrCasualNameForDate:jan24th], "");
}

@end
