//
//  DGConverseNetworkingTest.m
//  DGConverseTests
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DGConverseAPIOperationManager.h"
#import "DGConverseAPIBlockBaseOperation.h"
#import "DGConverseAPIRequestParameters.h"
#import "DGConverseAPICacheManager.h"

#define kExpectation @"expectation"

static DGConverseAPIOperationManager *testManager = nil;

@interface DGConverseGenericTest : XCTestCase

@property (nonatomic, strong) id<DGConverseAPIGroupBaseOperation> groupOp;

@end

@implementation DGConverseGenericTest

+ (void)setUp
{
    [super setUp];
    
    DGConverseLogStart();
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (testManager == nil) {
        
        NSDictionary *launchEnvironment = NSProcessInfo.processInfo.environment;
        NSString *urlSessionMode = launchEnvironment[@"CONVERSE_URL_SESSION"];
        
        if ([urlSessionMode isEqualToString:@"custom_default"]) {
            // custom default session
            testManager = [[DGConverseAPIOperationManager alloc] initWithDefaultConfiguration:[ NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil];
            
        } else if ([urlSessionMode isEqualToString:@"custom_background"]) {
            // custom background session
            testManager = [[DGConverseAPIOperationManager alloc] initWithBackgroundConfiguration:[ NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"CNVBackgroundSession"] delegate:nil];
        } else {
            // default session
            testManager = [DGConverseAPIOperationManager new];
        }
    }
    
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"************************** START ******************************");
    NSLog(@"---------------------------------------------------------------");
}

- (void)tearDown
{
    NSLog(@"---------------------------------------------------------------");
    NSLog(@"*************************** END *******************************");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark- URL TESTS
#pragma mark-- Data

- (void)test_data
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_data"];
    // <1MB
    self.groupOp = [testManager data:@"https://api.ipify.org?format=json" progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_data_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_data_withFailure"];
    // <1MB
    self.groupOp = [testManager data:@"https://nonexistingurl.com" progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}


#pragma mark-- Download JSON

- (void)test_downloadJSON_withCache
{
    [self cleanCaches];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadJSON_withCache"];
    // <1MB
    self.groupOp = [testManager downloadJSON:@"https://api.ipify.org?format=json" useCache:YES progress:nil completion:^(DGConverseAPIJSONResult *result) {
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadJSON_withoutCache
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadJSON_withoutCache"];
    // <1MB
    self.groupOp = [testManager downloadJSON:@"https://api.ipify.org?format=json" useCache:NO progress:nil completion:^(DGConverseAPIJSONResult *result) {
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadJSON_withCache_withFailure
{
    [self cleanCaches];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadJSON_withCache_withFailure"];
    // <1MB
    self.groupOp = [testManager downloadJSON:@"https://wrongURLforJSON.com" useCache:YES progress:nil completion:^(DGConverseAPIJSONResult *result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadJSON_withoutCache_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadJSON_withoutCache_withFailure"];
    // <1MB
    self.groupOp = [testManager downloadJSON:@"https://wrongURLforJSON.com" useCache:NO progress:nil completion:^(DGConverseAPIJSONResult *result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- Download Image

- (void)test_downloadImage_withCache
{
    [self cleanCaches];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadImage_withCache"];
    
    // ~8.3MB
    self.groupOp = [testManager downloadImage:@"https://upload.wikimedia.org/wikipedia/commons/9/9c/Blue_Planet_Aquarium_Copenhagen%29.jpg" useCache:YES progress:nil completion:^(DGConverseImageResult *result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        [self.groupOp cancel];
    }];
}

- (void)test_downloadImage_withoutCache
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadImage_withoutCache"];
    
    // ~8.3MB
    self.groupOp = [testManager downloadImage:@"https://upload.wikimedia.org/wikipedia/commons/9/9c/Blue_Planet_Aquarium_Copenhagen%29.jpg" useCache:NO progress:nil completion:^(DGConverseImageResult *result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        [self.groupOp cancel];
    }];
}

- (void)test_downloadImage_withCache_withFailure
{
    [self cleanCaches];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadImage_withCache_withFailure"];
    
    // ~8.3MB
    self.groupOp = [testManager downloadImage:@"https://thisisawrongurlimage.jpeg" useCache:YES progress:nil completion:^(DGConverseImageResult *result) {
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        [self.groupOp cancel];
    }];
}

- (void)test_downloadImage_withoutCache_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadImage_withoutCache_withFailure"];
    
    // ~8.3MB
    self.groupOp = [testManager downloadImage:@"https://thisisawrongurlimage.jpeg" useCache:NO progress:nil completion:^(DGConverseImageResult *result) {
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        [self.groupOp cancel];
    }];
}

#pragma mark-- Download File

- (void)test_downloadFile
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFile"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                inBackground:NO
                           toDocumentsFolder:NO
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNil(result.error, "error should be nil");
                                      XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
                                      
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadFile_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFile_withFailure"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg"
                                inBackground:NO
                           toDocumentsFolder:NO
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:20 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- Upload File

// Upload Test Server: http://ptsv2.com/t/converse/post
// If it fails, perhaps it is necessary first to create a 'converse' toilet in http://ptsv2.com (typing 'converse')
- (void)test_uploadFile
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_uploadFile"];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    
    self.groupOp = [testManager uploadFile:url.absoluteString
                                        to:@"http://ptsv2.com/t/converse/post"
                                  progress:nil
                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                    XCTAssertNil(result.error, "error should be nil");
                                    XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                    
                                    [expectation fulfill];
                                }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_uploadData
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_uploadData"];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    self.groupOp = [testManager uploadData:data
                                        to:@"http://ptsv2.com/t/converse/post"
                                  progress:nil
                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                    XCTAssertNil(result.error, "error should be nil");
                                    XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                    
                                    [expectation fulfill];
                                }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_uploadFile_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_uploadFile_withFailure"];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    
    self.groupOp = [testManager uploadFile:url.absoluteString
                                        to:@"http://ptsv2.com/t/NON_EXISTING_CONVERSE_FOLDER/post" // 404
                                  progress:nil
                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                    XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                    XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                    
                                    [expectation fulfill];
                                }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_uploadData_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_uploadData_withFailure"];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    self.groupOp = [testManager uploadData:data
                                        to:@"http://ptsv2.com/t/NON_EXISTING_CONVERSE_FOLDER/post" // 404
                                  progress:nil
                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                    XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                    XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                    
                                    [expectation fulfill];
                                }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark- HTTP TESTS
//https://github.com/typicode/jsonplaceholder#how-to

#pragma mark-- GET Request

- (void)test_request_1_GET
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestGET"];
    
    self.groupOp = [testManager GET:@"https://jsonplaceholder.typicode.com/posts" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_request_1_GET_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestGET_withFailure"];
    
    self.groupOp = [testManager GET:@"https://jsonplaceholder.typicode.com/WRONG_PATH" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}


#pragma mark-- HEAD Request

- (void)test_request_2_HEAD
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestHEAD"];
    
    self.groupOp = [testManager HEAD:@"https://jsonplaceholder.typicode.com/posts" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_request_2_HEAD_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestHEAD_withFailure"];
    
    self.groupOp = [testManager HEAD:@"https://jsonplaceholder.typicode.com/WRONG_PATH/" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- POST Request

- (void)test_request_3_POST
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestPOST"];
    
    DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
    [params addKey:@"title" value:@"foo"];
    [params addKey:@"body" value:@"bar"];
    [params addKey:@"userId" value:@"1"];
    
    self.groupOp = [testManager POST:@"https://jsonplaceholder.typicode.com/posts" parameters:params progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        NSLog(@"%@", [NSJSONSerialization JSONObjectWithData:result.data.copy options:0 error:nil]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_request_3_POST_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestPOST_withFailure"];
    
    self.groupOp = [testManager POST:@"https://jsonplaceholder.typicode.com/WRONG_PATH" parameters:nil progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- PUT Request

- (void)test_request_4_PUT
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestPUT"];
    
    DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
    [params addKey:@"id" value:@"1"];
    [params addKey:@"title" value:@"foo"];
    [params addKey:@"body" value:@"bar"];
    [params addKey:@"userId" value:@"1"];
    
    self.groupOp = [testManager PUT:@"https://jsonplaceholder.typicode.com/posts/1" parameters:params completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        NSLog(@"%@", [NSJSONSerialization JSONObjectWithData:result.data.copy options:0 error:nil]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_request_4_PUT_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestPUT_withFailure"];
    
    self.groupOp = [testManager PUT:@"https://jsonplaceholder.typicode.com/WRONG_PATH" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- PATCH Request

- (void)test_request_5_PATCH
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestPATCH"];
    
    DGConverseAPIHTTPRequestParameters *params = [DGConverseAPIHTTPRequestParameters new];
    [params addKey:@"title" value:@"foo"];
    
    self.groupOp = [testManager PATCH:@"https://jsonplaceholder.typicode.com/posts/1" parameters:params completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        NSLog(@"%@", [NSJSONSerialization JSONObjectWithData:result.data.copy options:0 error:nil]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_request_5_PATCH_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestPATCH_withFailure"];
    
    self.groupOp = [testManager PATCH:@"https://jsonplaceholder.typicode.com/WRONG_PATH" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- DELETE Request

- (void)test_request_6_DELETE
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestDELETE"];
    
    self.groupOp = [testManager DELETE:@"https://jsonplaceholder.typicode.com/posts/1" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        NSLog(@"%@", [NSJSONSerialization JSONObjectWithData:result.data.copy options:0 error:nil]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_request_6_DELETE_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_requestDELETE_withFailure"];
    
    self.groupOp = [testManager DELETE:@"https://jsonplaceholder.typicode.com/WRONG_PATH" parameters:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        
        XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}


#pragma mark - Concurrent Operations

- (void)test_concurrentOperations
{
    NSUInteger concurrentOperations = 5;
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_concurrentOperations"];
    // number of times fulfill must be called before the expectation is completely fulfilled
    [expectation setExpectedFulfillmentCount:concurrentOperations];
    NSMutableArray *ops = @[].mutableCopy;
    
    for (NSUInteger i = 0; i < concurrentOperations; i++) {
        [ops addObject:[[DGConverseAPIBlockBaseOperation alloc] initWithBlock:^(DGConverseAPIBlockBaseOperationCompletionBlock completionBlock) {
            NSLog(@"THIS IS A COMPLETION BLOCK");
            [expectation fulfill];
        }]];
    }
    
    self.groupOp = [testManager runConcurrentOperations:ops];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark - Cancel Operations

/*!
 * @brief Cancel Consecutive Download File operations sharing equivalent DGConverseAPIRequest.
 * @discussion Triggers several DownloadFile tasks in a row. When adding the taks to the queue, it cancels any equivalent request that might be running in the system. Last operation should be the only that gets fully executed.
 * @warning Do note rely on cancelling when dealing with Background Queue (Queues defined in the corresponding GroupOperation)
 */
- (void)test_cancelDownloadFileOperations
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_cancelDownloadFileOperations"];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(cancelDownloadFileOperationsHelper:) userInfo:@{kExpectation : expectation} repeats:YES];
    
    double delayInSeconds = 4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [timer invalidate];
    });
    
    [self waitForExpectationsWithTimeout:90 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        [self.groupOp cancel];
    }];
}

- (void)cancelDownloadFileOperationsHelper:(NSTimer *)timer
{
    XCTestExpectation *expectation = timer.userInfo[kExpectation];
    
    self.groupOp = [testManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf" inBackground:NO toDocumentsFolder:NO progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
}


/*!
 * @brief Cancel Consecutive Upload File operations sharing equivalent DGConverseAPIRequest.
 * @discussion Triggers several UploadFile tasks in a row. When adding the taks to the queue, it cancels any equivalent request that might be running in the system. Last operation should be the only that gets fully executed.
 * @warning Do note rely on cancelling when dealing with Background Queue (Queues defined in the corresponding GroupOperation)
 */
- (void)test_cancelUploadFileOperations
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_cancelUploadFileOperations"];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(cancelUploadFileOperationsHelper:) userInfo:@{kExpectation : expectation} repeats:YES];
    
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [timer invalidate];
    });
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        [self.groupOp cancel];
    }];
}

- (void)cancelUploadFileOperationsHelper:(NSTimer *)timer
{
    XCTestExpectation *expectation = timer.userInfo[kExpectation];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    
    self.groupOp = [testManager uploadFile:url.absoluteString to:@"http://ptsv2.com/t/converse/post" progress:nil completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
        
        [expectation fulfill];
    }];
}



#pragma mark - Performance

- (void)test_performance_cleanCaches {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
        [self cleanCaches];
    }];
}

#pragma mark - Helper

- (void)cleanCaches
{
    DGConverseAPICacheManager *cacheManager = [[DGConverseAPICacheManager alloc] init];
    NSArray<DGConverseAPICache *> *caches = [cacheManager cacheForType:kDGConverseAPICacheTypeALL];
    for (DGConverseAPICache *cache in caches) {
        [cache wipeNonLockedFilesWithCallback:nil onQueue:nil];
    }
}

- (BOOL)areOperationsDeallocated
{
    NSUInteger totalOpsInQueues = [testManager currentConcurrentOpsInQueues];
    //1: in completionBlock the last op is still running
    return (totalOpsInQueues <= 1);
}

@end
