//
//  DGConverseSKDefines.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseSKDefines_h
#define DGConverseSKDefines_h

#define kSKApiKey @"ss630745725358065467897349852985"
#define kSKBaseURLString @"http://business.skyscanner.net/" /* @"http://partners.api.skyscanner.net/"*/
#define kSKVersion @"v1.0"
#define kSKCountry @"UK"
#define kSKLocale @"en-GB"
#define kSKCurrency @"EUR"
#define kSKCabinClass @"economy"
#define kSKLocationSchema @"sky"

#endif /* DGConverseSKDefines_h */
