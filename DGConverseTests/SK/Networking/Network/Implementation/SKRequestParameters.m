//
//  SKRequestParameters.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKRequestParameters.h"
#import "DGConverseSKDefines.h"

@implementation SKBasicRequestParameter

@end

#pragma mark -

@implementation SKApikeyRequestParameter

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.apiKey = kSKApiKey;
	}
	return self;
}

@end

#pragma mark -

@implementation SKLiveCreateSessionRequestParameters

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self updateWithUserDefaults];
		self.adults = 1;
		self.children = 0;
		self.infants = 0;
	}
	return self;
}

- (void)updateWithUserDefaults
{
	self.country = kSKCountry;
	self.currency = kSKCurrency;
	self.locale = kSKLocale;
	self.cabinClass = kSKCabinClass;
	self.locationSchema = kSKLocationSchema;
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
	if ([propertyName isEqualToString:@"children"] || [propertyName isEqualToString:@"infants"])
		return YES;
	else
		return NO;
}

@end

#pragma mark -

@implementation SKLivePollSessionRequestParameters

@end

#pragma mark -

@implementation SKLiveCreateBookingDetailsRequestParameters

@end

#pragma mark -

@implementation SKLiveCreateBookingDetailsBodyRequestParameters

@end

#pragma mark -

@implementation SKLivePollBookingDetailsRequestParameters

@end

#pragma mark -

@implementation SKLivePollBookingDetailsBodyRequestParameters

@end

#pragma mark -

@implementation SKLocalesRequestParameters

@end

#pragma mark -

@implementation SKCurrenciesRequestParameters

@end

#pragma mark -

@implementation SKMarketsRequestParameters

@end

#pragma mark -

@implementation SKPlacesRequestParameters

@end

#pragma mark -

@implementation SKPlaceRequestParameters

@end
