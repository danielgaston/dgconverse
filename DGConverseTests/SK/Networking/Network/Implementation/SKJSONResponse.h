//
//  SKJSONResponse.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIResponse.h"

@interface SKJSONResponse : DGConverseAPIJSONDataResponse

- (id)JSON;

@end

typedef void (^SKJSONResponseBlock)(SKJSONResponse* response);
