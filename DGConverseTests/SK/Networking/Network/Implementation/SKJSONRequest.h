//
//  SKJSONRequest.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIRequest.h"
#import "SKRequestParameters.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKJSONRequest : DGConverseAPIJSONDataRequest

- (instancetype)initLivePollSessionWithLocation:(NSString*)url params:(SKLivePollSessionRequestParameters*)params;

- (instancetype)initLivePollBookingDetailsWithKey:(NSString*)sessionKey
									outboundLegId:(NSString*)outboundLegId
									 inboundLegId:(nullable NSString*)inboundLegId
										   params:(SKLivePollBookingDetailsRequestParameters*)params;

- (instancetype)initBrowseQuoteWithOriginPlace:(NSString*)originPlace
							  destinationPlace:(NSString*)destinationPlace
									departDate:(NSString*)departDate
									returnDate:(NSString*)returnDate;

- (instancetype)initBrowseRoutesWithOriginPlace:(NSString*)originPlace
							   destinationPlace:(NSString*)destinationPlace
									 departDate:(NSString*)departDate
									 returnDate:(NSString*)returnDate;

- (instancetype)initBrowseDatesWithOriginPlace:(NSString*)originPlace
							  destinationPlace:(NSString*)destinationPlace
									departDate:(NSString*)departDate
									returnDate:(NSString*)returnDate;

- (instancetype)initBrowseGridWithOriginPlace:(NSString*)originPlace
							 destinationPlace:(NSString*)destinationPlace
								   departDate:(NSString*)departDate
								   returnDate:(NSString*)returnDate;

- (instancetype)initLocales;
- (instancetype)initCurrencies;
- (instancetype)initMarketsWithLocale:(NSString*)locale;

- (instancetype)initPlacesWithCountry:(NSString*)country
							 currency:(NSString*)currency
							   locale:(NSString*)locale
								query:(NSString*)query;

- (instancetype)initPlacesWithQuery:(NSString*)query;

- (instancetype)initPlaceWithCountry:(NSString*)country
							currency:(NSString*)currency
							  locale:(NSString*)locale
							 placeId:(NSString*)placeId;

- (instancetype)initPlaceWithPlaceId:(NSString*)placeId;

@end

NS_ASSUME_NONNULL_END
