//
//  SKURLEncodedResponse.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKURLEncodedResponse.h"

@implementation SKURLEncodedResponse

@end

typedef void (^SKURLEncodedResponseBlock)(SKURLEncodedResponse* response);
