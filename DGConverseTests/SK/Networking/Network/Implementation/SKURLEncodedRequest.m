//
//  SKURLEncodedRequest.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKURLEncodedRequest.h"
#import "SKURLEncodedResponse.h"
#import "DGConverseSKDefines.h"

@implementation SKURLEncodedRequest

- (NSString*)baseURLString
{
	return kSKBaseURLString;
}

- (NSString*)version
{
	return kSKVersion;
}

- (NSString*)apiKey
{
	return kSKApiKey;
}

- (NSString*)country
{
	return kSKCountry;
}

- (NSString*)locale
{
	return kSKLocale;
}

- (NSString*)currency
{
	return kSKCurrency;
}

- (NSString*)locationSchema
{
	return kSKLocationSchema;
}

- (NSString*)cabinClass
{
	return kSKCabinClass;
}

- (instancetype)initCreateLiveSessionWithParams:(SKLiveCreateSessionRequestParameters*)params
{
	// http://partners.api.skyscanner.net/apiservices/pricing/v1.0

	NSString* apiPath = [NSString stringWithFormat:@"apiservices/pricing/%@", self.version];
	NSString* relativeURLString = [NSString stringWithFormat:@"%@", apiPath];

	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	self = [self initWithURLString:url method:POST parameters:params body:nil];
	if (self) {
		self.responseClass = [SKURLEncodedResponse class];
	}

	return self;
}

- (instancetype)initLiveCreateBookingDetailsWithURI:(NSString*)bookingDetailsLinkURI
											   body:(NSString*)bookingDetailsLinkBody
{
	// bookingDetailsLinkURI
	NSString* relativeURLString = [bookingDetailsLinkURI stringByAppendingString:[NSString stringWithFormat:@"?apikey=%@", self.apiKey]];
	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	self = [self initWithURLString:url method:PUT parameters:nil];
	if (self) {
		self.responseClass = [SKURLEncodedResponse class];

		// TODO: Serialize it using HTTPserialization
		[self urlRequest]; // here it is already created
		NSData* putData = [bookingDetailsLinkBody dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		NSString* putLength = [NSString stringWithFormat:@"%lu", (unsigned long)[putData length]];
		[self.urlRequest setValue:putLength forHTTPHeaderField:@"Content-Length"];
		[self.urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
		[self.urlRequest setHTTPBody:putData];
	}

	return self;
}

@end
