//
//  SKURLEncodedResponse.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIResponse.h"

@interface SKURLEncodedResponse : DGConverseAPIDataResponse

@end
