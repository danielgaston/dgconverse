//
//  SKJSONResponse.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKJSONResponse.h"

@implementation SKJSONResponse

- (id)processResponseObject:(NSError**)error;
{
	id json = [NSJSONSerialization JSONObjectWithData:self.responseObject
											  options:NSJSONReadingMutableContainers
												error:error];

	return json;
}

#pragma mark - Getters & Setters

- (id)JSON
{
	return self.processedResponseObject;
}

@end
