//
//  SKJSONRequest.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKJSONRequest.h"
#import "NSString+Additions.h"
#import "SKJSONResponse.h"
#import "DGConverseSKDefines.h"

@implementation SKJSONRequest

- (NSString*)baseURLString
{
	return kSKBaseURLString;
}

- (NSString*)version
{
	return kSKVersion;
}

- (NSString*)apiKey
{
	return kSKApiKey;
}

- (NSString*)country
{
	return kSKCountry;
}

- (NSString*)locale
{
	return kSKLocale;
}

- (NSString*)currency
{
	return kSKCurrency;
}

- (instancetype)initLivePollSessionWithLocation:(NSString*)url params:(SKLivePollSessionRequestParameters*)params
{
	// pricing/v1.0/{SessionKey}

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}

	return self;
}

- (instancetype)initLivePollBookingDetailsWithKey:(NSString*)sessionKey
									outboundLegId:(NSString*)outboundLegId
									 inboundLegId:(nullable NSString*)inboundLegId
										   params:(SKLivePollBookingDetailsRequestParameters*)params
{
	// apiservices/pricing/v1.0/{SessionKey}/booking/{OutboundLegId};{InboundLegId}?apikey=prtl6749387986743898559646983194

	NSString* apiPath = [NSString stringWithFormat:@"apiservices/pricing/%@", self.version];
	NSString* relativeURLString = [NSString stringWithFormat:@"%@/%@/booking/%@;%@", apiPath, sessionKey, outboundLegId, [NSString isNotNilNorEmpty:inboundLegId] ? inboundLegId : @""];

	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}

	return self;
}

- (instancetype)initBrowseWithApiPath:(NSString*)apiPath
						  originPlace:(NSString*)originPlace
					 destinationPlace:(NSString*)destinationPlace
						   departDate:(NSString*)outboundDate
						   returnDate:(NSString*)inboundDate
{
	SKApikeyRequestParameter* params = [SKApikeyRequestParameter new];

	NSString* relativeURLString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@", apiPath, originPlace, destinationPlace, outboundDate, inboundDate];

	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}

	return self;
}

- (instancetype)initBrowseQuoteWithOriginPlace:(NSString*)originPlace
							  destinationPlace:(NSString*)destinationPlace
									departDate:(NSString*)departDate
									returnDate:(NSString*)returnDate
{

	// browsequotes/v1.0/{country}/{currency}/{locale}/
	NSString* apiPath = [NSString stringWithFormat:@"apiservices/browsequotes/%@/%@/%@/%@", self.version, self.country, self.currency, self.locale];

	return [self initBrowseWithApiPath:apiPath originPlace:originPlace destinationPlace:destinationPlace departDate:departDate returnDate:returnDate];
}

- (instancetype)initBrowseRoutesWithOriginPlace:(NSString*)originPlace
							   destinationPlace:(NSString*)destinationPlace
									 departDate:(NSString*)departDate
									 returnDate:(NSString*)returnDate
{
	// browsequotes/v1.0/{country}/{currency}/{locale}/
	NSString* apiPath = [NSString stringWithFormat:@"apiservices/browseroutes/%@/%@/%@/%@", self.version, self.country, self.currency, self.locale];
	return [self initBrowseWithApiPath:apiPath originPlace:originPlace destinationPlace:destinationPlace departDate:departDate returnDate:returnDate];
}

- (instancetype)initBrowseDatesWithOriginPlace:(NSString*)originPlace
							  destinationPlace:(NSString*)destinationPlace
									departDate:(NSString*)departDate
									returnDate:(NSString*)returnDate
{
	// browsequotes/v1.0/{country}/{currency}/{locale}/
	NSString* apiPath = [NSString stringWithFormat:@"apiservices/browsedates/%@/%@/%@/%@", self.version, self.country, self.currency, self.locale];
	return [self initBrowseWithApiPath:apiPath originPlace:originPlace destinationPlace:destinationPlace departDate:departDate returnDate:returnDate];
}

- (instancetype)initBrowseGridWithOriginPlace:(NSString*)originPlace
							 destinationPlace:(NSString*)destinationPlace
								   departDate:(NSString*)departDate
								   returnDate:(NSString*)returnDate
{
	// browsegrid/v1.0/{country}/{currency}/{locale}/
	NSString* apiPath = [NSString stringWithFormat:@"apiservices/browsegrid/%@/%@/%@/%@", self.version, self.country, self.currency, self.locale];
	return [self initBrowseWithApiPath:apiPath originPlace:originPlace destinationPlace:destinationPlace departDate:departDate returnDate:returnDate];
}

- (instancetype)initLocales
{
	// reference/v1.0/locales
	NSString* relativeURLString = [NSString stringWithFormat:@"apiservices/reference/%@/locales", self.version];
	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	SKLocalesRequestParameters* params = [SKLocalesRequestParameters new];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}
	return self;
}

- (instancetype)initCurrencies
{
	// /reference/v1.0/currencies
	NSString* relativeURLString = [NSString stringWithFormat:@"apiservices/reference/%@/currencies", self.version];
	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	SKCurrenciesRequestParameters* params = [SKCurrenciesRequestParameters new];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}
	return self;
}

- (instancetype)initMarketsWithLocale:(NSString*)locale
{
	// /reference/v1.0/countries/{locale}

	NSString* apiPath = [NSString stringWithFormat:@"apiservices/reference/%@/countries", self.version];
	NSString* relativeURLString = [NSString stringWithFormat:@"%@/%@", apiPath, locale];

	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	SKMarketsRequestParameters* params = [SKMarketsRequestParameters new];
	[params setLocale:locale];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}
	return self;
}

- (instancetype)initPlacesWithCountry:(NSString*)country
							 currency:(NSString*)currency
							   locale:(NSString*)locale
								query:(NSString*)query
{
	// /autosuggest/v1.0/{market}/{currency}/{locale}

	NSString* apiPath = [NSString stringWithFormat:@"apiservices/autosuggest/%@", self.version];
	NSString* relativeURLString = [NSString stringWithFormat:@"%@/%@/%@/%@", apiPath, country, currency, locale];

	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	SKPlacesRequestParameters* params = [SKPlacesRequestParameters new];
	[params setQuery:query];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}
	return self;
}

- (instancetype)initPlacesWithQuery:(NSString*)query
{
	return [self initPlacesWithCountry:self.country currency:self.currency locale:self.locale query:query];
}

- (instancetype)initPlaceWithCountry:(NSString*)country
							currency:(NSString*)currency
							  locale:(NSString*)locale
							 placeId:(NSString*)placeId
{
	// /autosuggest/v1.0/{market}/{currency}/{locale}

	NSString* apiPath = [NSString stringWithFormat:@"apiservices/autosuggest/%@", self.version];
	NSString* relativeURLString = [NSString stringWithFormat:@"%@/%@/%@/%@", apiPath, country, currency, locale];

	NSString* url = [[NSURL URLWithString:relativeURLString relativeToURL:[NSURL URLWithString:self.baseURLString]] absoluteString];

	SKPlaceRequestParameters* params = [SKPlaceRequestParameters new];
	[params setId:placeId];

	self = [self initWithURLString:url method:GET parameters:params body:nil];
	if (self) {
		self.responseClass = [SKJSONResponse class];
	}
	return self;
}

- (instancetype)initPlaceWithPlaceId:(NSString*)placeId
{
	return [self initPlaceWithCountry:self.country currency:self.currency locale:self.locale placeId:placeId];
}

@end
