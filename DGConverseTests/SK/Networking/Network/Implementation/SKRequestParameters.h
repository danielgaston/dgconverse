//
//  SKRequestParameters.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIRequestParameters.h"

@protocol SKBasicRequestParameter;

@interface SKBasicRequestParameter : DGConverseAPIJSONRequestParameters

@end

#pragma mark -

@protocol SKApikeyRequestParameter;

@interface SKApikeyRequestParameter : DGConverseAPIJSONRequestParameters

@property (nonatomic, strong) NSString* apiKey;

@end

#pragma mark -

@class SKPlaceDataModel;
@protocol SKLiveCreateSessionRequestParameters;

@interface SKLiveCreateSessionRequestParameters : SKApikeyRequestParameter

@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* currency;
@property (nonatomic, strong) NSString* locale;
@property (nonatomic, strong) NSString* locationSchema; // BUG -- Not described in Documentation --> https://skyscanner.github.io/slate/#creating-the-session
@property (nonatomic, strong) NSString* originPlace;
@property (nonatomic, strong) NSString* destinationPlace;
@property (nonatomic, strong) NSDate* outboundDate;
@property (nonatomic, strong) NSDate<Optional>* inboundDate;
@property (nonatomic, strong) NSString<Optional>* cabinClass;
@property (nonatomic, assign) NSUInteger adults;
@property (nonatomic, assign) NSUInteger children;
@property (nonatomic, assign) NSUInteger infants;
@property (nonatomic, strong) NSString<Optional>* includeCarriers;
@property (nonatomic, strong) NSString<Optional>* excludeCarriers;

- (void)updateWithUserDefaults;

#pragma mark - IGNORED

@property (nonatomic, strong) SKPlaceDataModel<Ignore>* helperOriginPlace;
@property (nonatomic, strong) SKPlaceDataModel<Ignore>* helperDestinationPlace;

@end

#pragma mark -

@protocol SKLivePollSessionRequestParameters;

@interface SKLivePollSessionRequestParameters : SKApikeyRequestParameter

// TODO: Check of ALL TYPE!!

@property (nonatomic, strong) NSString<Optional>* pageIndex; // OPTIONAL: desired Page Number
@property (nonatomic, strong) NSString<Optional>* pageSize; // OPTIONAL: desired Page Size. default 10

@property (nonatomic, strong) NSString<Optional>* sortType;
@property (nonatomic, strong) NSString<Optional>* sortOrder;
@property (nonatomic, strong) NSString<Optional>* duration;
@property (nonatomic, strong) NSString<Optional>* includeCarriers;
@property (nonatomic, strong) NSString<Optional>* excludeCarriers;
@property (nonatomic, strong) NSString<Optional>* originAirports;
@property (nonatomic, strong) NSString<Optional>* destinationAirports;
@property (nonatomic, strong) NSString<Optional>* stops;
@property (nonatomic, strong) NSString<Optional>* outboundDepartTime;
@property (nonatomic, strong) NSString<Optional>* outboundDepartStartTime;
@property (nonatomic, strong) NSString<Optional>* outboundDepartEndTime;
@property (nonatomic, strong) NSString<Optional>* outboundArriveStartTime;
@property (nonatomic, strong) NSString<Optional>* outboundArriveEndTime;
@property (nonatomic, strong) NSString<Optional>* inboundDepartTime;
@property (nonatomic, strong) NSString<Optional>* inboundDepartStartTime;
@property (nonatomic, strong) NSString<Optional>* inboundDepartEndTime;
@property (nonatomic, strong) NSString<Optional>* inboundArriveStartTime;
@property (nonatomic, strong) NSString<Optional>* inboundArriveEndTime;

@end

#pragma mark -

@protocol SKLiveCreateBookingDetailsRequestParameters;

@interface SKLiveCreateBookingDetailsRequestParameters : SKApikeyRequestParameter
@end

#pragma mark -

@protocol SKLiveCreateBookingDetailsBodyRequestParameters;

@interface SKLiveCreateBookingDetailsBodyRequestParameters : SKBasicRequestParameter

@property (nonatomic, strong) NSString* outboundLegId;
@property (nonatomic, strong) NSString<Optional>* inboundLegId;
@property (nonatomic, strong) NSString<Optional>* adults;
@property (nonatomic, strong) NSString<Optional>* children;
@property (nonatomic, strong) NSString<Optional>* infants;

@end

#pragma mark -

@protocol SKLivePollBookingDetailsRequestParameters;

@interface SKLivePollBookingDetailsRequestParameters : SKApikeyRequestParameter
@end

#pragma mark -

@protocol SKLivePollBookingDetailsBodyRequestParameters;

@interface SKLivePollBookingDetailsBodyRequestParameters : SKBasicRequestParameter

@property (nonatomic, strong) NSString* outboundLegId;
@property (nonatomic, strong) NSString<Optional>* inboundLegId;
@property (nonatomic, strong) NSString<Optional>* adults;
@property (nonatomic, strong) NSString<Optional>* children;
@property (nonatomic, strong) NSString<Optional>* infants;

@end

#pragma mark -

@protocol SKLocalesRequestParameters;

@interface SKLocalesRequestParameters : SKApikeyRequestParameter

@end

#pragma mark -

@protocol SKCurrenciesRequestParameters;

@interface SKCurrenciesRequestParameters : SKApikeyRequestParameter

@end

#pragma mark -

@protocol SKMarketsRequestParameters;

@interface SKMarketsRequestParameters : SKApikeyRequestParameter

@property (nonatomic, strong) NSString* locale;

@end

#pragma mark -

@protocol SKPlacesRequestParameters;

@interface SKPlacesRequestParameters : SKApikeyRequestParameter

@property (nonatomic, strong) NSString* query;

@end

#pragma mark -

@protocol SKPlaceRequestParameters;

@interface SKPlaceRequestParameters : SKApikeyRequestParameter

@property (nonatomic, strong) NSString* id;

@end
