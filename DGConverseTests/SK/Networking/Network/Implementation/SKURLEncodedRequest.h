//
//  SKURLEncodedRequest.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIRequest.h"
#import "SKLiveItineraryDataModel.h"
#import "SKRequestParameters.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKURLEncodedRequest : DGConverseAPIDataRequest

- (instancetype)initCreateLiveSessionWithParams:(SKLiveCreateSessionRequestParameters*)params;

/** The url is provided in the response of the live prices:
 "BookingDetailsLink": {
 "Uri": "/apiservices/pricing/v1.0/abb2a69708624a7ca82762ed73493598_ecilpojl_DCE634A426CBDA30CE7EA3E9068CD053/booking",
 "Body": "OutboundLegId=11235-1705301925--32480-0-13554-1705302055&InboundLegId=13554-1706020700--32480-0-11235-1706020820",
 "Method": "PUT" } }
 */
- (instancetype)initLiveCreateBookingDetailsWithURI:(NSString*)bookingDetailsLinkURI
											   body:(NSString*)bookingDetailsLinkBody;

@end

NS_ASSUME_NONNULL_END
