//
//  SKOperationHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef SKOperationHeader_h
#define SKOperationHeader_h

#import "SKOperation.h"
#import "SKParseDataOperation.h"
#import "SKLiveSessionPollGroupOperation.h"
#import "SKLiveBookingDetailsPollGroupOperation.h"

#endif /* SKOperationHeader_h */
