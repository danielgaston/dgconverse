//
//  SKParseDataOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKParseDataOperation.h"

#pragma mark - Result Objects

@implementation SKLiveCreateSessionResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKLivePollSessionResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKLiveCreateBookingDetailsResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKLivePollBookingDetailsResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKLocalesResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKCurrenciesResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKMarketsResult
@synthesize responseObj;
@end

#pragma mark -

@implementation SKPlacesResult
@synthesize responseObj;
@end


#pragma mark -

@implementation SKBrowseQuotesResult
@synthesize responseObj;
@end


#pragma mark -

@implementation SKBrowseRoutesResult
@synthesize responseObj;
@end


#pragma mark -

@implementation SKBrowseDatesResult
@synthesize responseObj;
@end


#pragma mark -

@implementation SKBrowseGridResult
@synthesize responseObj;
@end


#pragma mark - Parse Operations

@interface SKLiveCreateSessionParseDataOperation ()

@property (nonatomic, copy) SKLiveCreateSessionResultBlock completion;

@end

@implementation SKLiveCreateSessionParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLiveCreateSessionResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
        } else {
        
            DGConverseNetworkOperationResultExtraInfo *extraInfoNetworkResult = [self.result.extraInfo objectForKey:[DGConverseNetworkOperationResultExtraInfo key]];
            if (extraInfoNetworkResult.responseURL && [extraInfoNetworkResult.responseURL isKindOfClass:[NSHTTPURLResponse class]]) {
                NSHTTPURLResponse *response = (NSHTTPURLResponse*)extraInfoNetworkResult.responseURL;
                NSString *URLString = [response allHeaderFields][@"location"];
                self.result.responseObj = [[SKLiveCreateSessionDataModel alloc] initWithLocationURLString:URLString];
            } else {
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"URL response does not contain the URL for polling the results in the newly created session");
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKLivePollSessionParseDataOperation ()

@property (nonatomic, copy) SKLivePollSessionResultBlock completion;

@end

@implementation SKLivePollSessionParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLivePollSessionResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKLivePollSessionDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            } else {
                [self.result.responseObj extractModels:self.result.responseObj];
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKLiveCreateBookingDetailsParseDataOperation ()

@property (nonatomic, copy) SKLiveCreateBookingDetailsResultBlock completion;

@end

@implementation SKLiveCreateBookingDetailsParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLiveCreateBookingDetailsResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
        } else {
            DGConverseNetworkOperationResultExtraInfo *extraInfoNetworkResult = [self.result.extraInfo objectForKey:[DGConverseNetworkOperationResultExtraInfo key]];
            if (extraInfoNetworkResult.responseURL && [extraInfoNetworkResult.responseURL isKindOfClass:[NSHTTPURLResponse class]]) {
                NSHTTPURLResponse *response = (NSHTTPURLResponse*)extraInfoNetworkResult.responseURL;
                NSString *URLString = [response allHeaderFields][@"location"];
                self.result.responseObj = [[SKLiveCreateBookingDetailsDataModel alloc] initWithLocationURLString:URLString];
            } else {
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"URL response does not contain the URL for polling the results in the newly created session");
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKLivePollBookingDetailsParseDataOperation ()

@property (nonatomic, copy) SKLivePollBookingDetailsResultBlock completion;

@end

@implementation SKLivePollBookingDetailsParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLivePollBookingDetailsResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKLivePollBookingDetailsDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            } else {
                [self.result.responseObj extractModels:self.result.responseObj];
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end



#pragma mark -

@interface SKLocalesParseDataOperation ()

@property (nonatomic, copy) SKLocalesResultBlock completion;

@end

@implementation SKLocalesParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLocalesResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKLocalesDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKCurrenciesParseDataOperation ()

@property (nonatomic, copy) SKCurrenciesResultBlock completion;

@end

@implementation SKCurrenciesParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKCurrenciesResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKCurrenciesDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKMarketsParseDataOperation ()

@property (nonatomic, copy) SKMarketsResultBlock completion;

@end

@implementation SKMarketsParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKMarketsResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKMarketsDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKPlacesParseDataOperation ()

@property (nonatomic, copy) SKPlacesResultBlock completion;

@end

@implementation SKPlacesParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKPlacesResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKPlacesDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKBrowseQuotesParseDataOperation ()

@property (nonatomic, copy) SKBrowseQuotesResultBlock completion;

@end

@implementation SKBrowseQuotesParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseQuotesResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKBrowseQuotesDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKBrowseRoutesParseDataOperation ()

@property (nonatomic, copy) SKBrowseRoutesResultBlock completion;

@end

@implementation SKBrowseRoutesParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseRoutesResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKBrowseRoutesDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKBrowseDatesParseDataOperation ()

@property (nonatomic, copy) SKBrowseDatesResultBlock completion;

@end

@implementation SKBrowseDatesParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseDatesResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKBrowseDatesDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end


#pragma mark -

@interface SKBrowseGridParseDataOperation ()

@property (nonatomic, copy) SKBrowseGridResultBlock completion;

@end

@implementation SKBrowseGridParseDataOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseGridResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"SK Parse Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [[SKBrowseGridDataModel alloc] initWithData:self.result.data error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
    } else {
        [self cancel];
    }
}

@end
