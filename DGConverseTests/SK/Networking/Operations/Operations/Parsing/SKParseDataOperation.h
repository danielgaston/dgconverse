//
//  SKParseDataOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIParseDataOperation.h"

#import "SKModelHeader.h"

#pragma mark - Result Objects

@interface SKLiveCreateSessionResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKLiveCreateSessionDataModel *responseObj;
@end

#pragma mark -

@interface SKLivePollSessionResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKLivePollSessionDataModel *responseObj;
@end

#pragma mark -

@interface SKLiveCreateBookingDetailsResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKLiveCreateBookingDetailsDataModel *responseObj;
@end

#pragma mark -

@interface SKLivePollBookingDetailsResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKLivePollBookingDetailsDataModel *responseObj;
@end

#pragma mark -

@interface SKLocalesResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKLocalesDataModel *responseObj;
@end

#pragma mark -

@interface SKCurrenciesResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKCurrenciesDataModel *responseObj;
@end

#pragma mark -

@interface SKMarketsResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKMarketsDataModel *responseObj;
@end

#pragma mark -

@interface SKPlacesResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKPlacesDataModel *responseObj;
@end

#pragma mark -

@interface SKBrowseQuotesResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKBrowseQuotesDataModel *responseObj;
@end

#pragma mark -

@interface SKBrowseRoutesResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKBrowseRoutesDataModel *responseObj;
@end

#pragma mark -

@interface SKBrowseDatesResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKBrowseDatesDataModel *responseObj;
@end

#pragma mark -

@interface SKBrowseGridResult : DGConverseExchangeOperationResult
@property (nonatomic, strong) SKBrowseGridDataModel *responseObj;
@end

#pragma mark - Result Block typedefs

typedef void (^SKLiveCreateSessionResultBlock)(SKLiveCreateSessionResult* result);
typedef void (^SKLivePollSessionResultBlock)(SKLivePollSessionResult* result);
typedef void (^SKLiveCreateBookingDetailsResultBlock)(SKLiveCreateBookingDetailsResult* result);
typedef void (^SKLivePollBookingDetailsResultBlock)(SKLivePollBookingDetailsResult* result);
typedef void (^SKLocalesResultBlock)(SKLocalesResult* result);
typedef void (^SKCurrenciesResultBlock)(SKCurrenciesResult* result);
typedef void (^SKMarketsResultBlock)(SKMarketsResult* result);
typedef void (^SKPlacesResultBlock)(SKPlacesResult* result);
typedef void (^SKBrowseQuotesResultBlock)(SKBrowseQuotesResult* result);
typedef void (^SKBrowseRoutesResultBlock)(SKBrowseRoutesResult* result);
typedef void (^SKBrowseDatesResultBlock)(SKBrowseDatesResult* result);
typedef void (^SKBrowseGridResultBlock)(SKBrowseGridResult* result);


#pragma mark - Parse Operations

@interface SKLiveCreateSessionParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKLiveCreateSessionResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLiveCreateSessionResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKLivePollSessionParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKLivePollSessionResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLivePollSessionResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKLiveCreateBookingDetailsParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKLiveCreateBookingDetailsResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLiveCreateBookingDetailsResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKLivePollBookingDetailsParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKLivePollBookingDetailsResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLivePollBookingDetailsResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKLocalesParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKLocalesResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKLocalesResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKCurrenciesParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKCurrenciesResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKCurrenciesResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKMarketsParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKMarketsResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKMarketsResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKPlacesParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKPlacesResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKPlacesResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKBrowseQuotesParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKBrowseQuotesResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseQuotesResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKBrowseRoutesParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKBrowseRoutesResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseRoutesResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKBrowseDatesParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKBrowseDatesResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseDatesResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface SKBrowseGridParseDataOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) SKBrowseGridResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(SKBrowseGridResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end


