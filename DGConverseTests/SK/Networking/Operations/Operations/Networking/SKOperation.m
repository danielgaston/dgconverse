//
//  SKOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKOperation.h"

@implementation SKOperation
#pragma mark - Object LyfeCycle Methods

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super initWithRequest:request operationDelegate:operationDelegate progress:progress completion:completion];
    if (self) {
        self.name = @"SK Network Operation";
    }
    
    return self;
}

@end
