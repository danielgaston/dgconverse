//
//  SKOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkDataOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKOperation : DGConverseAPINetworkDataOperation

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion;

@end

NS_ASSUME_NONNULL_END
