//
//  SKLiveBookingDetailsPollGroupOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"
#import "SKOperation.h"
#import "SKParseDataOperation.h"
#import "DGConverseAPIOperationManagerHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKLiveBookingDetailsPollGroupOperation : DGConverseAPIGroupBaseOperation

- (void)runWithRequest:(id<DGConverseAPIRequest>)request
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
       parseCompletion:(nullable SKLivePollBookingDetailsResultBlock)parseCompletion
       pollingInterval:(NSInteger)pollingInterval;

@end

NS_ASSUME_NONNULL_END
