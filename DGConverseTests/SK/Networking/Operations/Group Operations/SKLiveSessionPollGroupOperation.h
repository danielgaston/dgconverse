//
//  SKLiveSessionPollGroupOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"
#import "DGConverseAPIOperationManagerHeader.h"
#import "SKOperation.h"
#import "SKParseDataOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKLiveSessionPollGroupOperation : DGConverseAPIGroupBaseOperation

- (void)runWithRequest:(id<DGConverseAPIRequest>)request
        progress:(nullable DGConverseAPINetworkProgressBlock)progress
       parseCompletion:(nullable SKLivePollSessionResultBlock)parseCompletion
       pollingInterval:(NSInteger)pollingInterval;

@end

NS_ASSUME_NONNULL_END
