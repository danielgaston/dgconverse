//
//  SKLiveSessionPollGroupOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveSessionPollGroupOperation.h"
#import "DGOwnershipMacro.h"
#import "DGConverseAPICacheJSONOperation.h"
#import "DGConverseAPIAdapterBaseOperation.h"
#import "NSDate+Additions.h"

@interface SKLiveSessionPollGroupOperation ()

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) id<DGConverseAPIRequest> request;
@property (nonatomic, copy) DGConverseAPINetworkProgressBlock progress;
@property (nonatomic, copy) SKLivePollSessionResultBlock parseCompletion;
@property (nonatomic, assign) NSInteger pollingInterval;
@property (nonatomic, strong) SKLivePollSessionResult *result;
@property (nonatomic, weak) SKLivePollSessionParseDataOperation *parseOp;   // weak for not creating a retain cycle

@end

#define kIsFinishedKeyPath @"isFinished"

@implementation SKLiveSessionPollGroupOperation

#pragma mark - Object Lifecycle Methods

- (void)dealloc
{
    [self removeObservers];
}


#pragma mark - Public Methods

- (void)runWithRequest:(id<DGConverseAPIRequest>)request
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
       parseCompletion:(nullable SKLivePollSessionResultBlock)parseCompletion
       pollingInterval:(NSInteger)pollingInterval
{
    self.request = request;
    self.progress = progress;
    self.parseCompletion = parseCompletion;
    self.pollingInterval = pollingInterval;
    
    [self fireTimerWithDelay:0];
}


#pragma mark - Overriden Methods

- (void)cancel
{
    [self removeObservers];
    [self invalidateTimer];
    [super cancel];
}


#pragma mark - Observation Methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    [self removeObservers];
    
    BOOL completePoll = YES;
    BOOL abortPoll = NO;
    
    if (self.result.error || !self.result.data.length)
        abortPoll = YES;
    else
        completePoll = [self.result.responseObj isCompleted];
    
    if (abortPoll) {
        [self invalidateTimer];
    } else if (completePoll) {
        [self invalidateTimer];
        //        [self processCaching];
    } else {
        [self fireTimerWithDelay:self.pollingInterval];
    }
    
    if (self.parseCompletion) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.parseCompletion(self.result);
        }];
    }
}


#pragma mark - Helper Methods

- (void)removeObservers
{
    @try {
        [self.parseOp removeObserver:self forKeyPath:kIsFinishedKeyPath];
    } @catch (NSException * e) {}
}

- (void)fireTimerWithDelay:(NSUInteger)delay
{
    DGWeakify(self);
    // observers requires to work on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        DGStrongify(self);
        self.timer = [NSTimer scheduledTimerWithTimeInterval:delay
                                                      target:self
                                                    selector:@selector(processPolling)
                                                    userInfo:nil
                                                     repeats:NO];
    });
}

- (void)invalidateTimer
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)processPolling
{
    SKOperation* networkOp = [[SKOperation alloc] initWithRequest:self.request operationDelegate:self.manager progress:self.progress completion:nil];
    
    DGWeakify(self);
    SKLivePollSessionParseDataOperation* parseOp = [[SKLivePollSessionParseDataOperation alloc] initWithOperationDelegate:self.manager completion:^(SKLivePollSessionResult *result) {
        DGStrongify(self);
        self.result = result;
    }];
    
    self.parseOp = parseOp;
    [self.parseOp addObserver:self forKeyPath:kIsFinishedKeyPath options:NSKeyValueObservingOptionNew context:nil];
    
    // Network <- Parse
    DGConverseAPIAdapterBaseOperation* NPAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:networkOp toOperation:parseOp];
    
    [NPAdapterOp addDependency:networkOp];
    [parseOp addDependency:NPAdapterOp];
        
    [self storeSubOperation:networkOp];
    [self storeSubOperation:NPAdapterOp];
    [self storeSubOperation:parseOp];
    
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
    [self.manager.queueManager addOperation:NPAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:parseOp inQueue:kDGConverseAPIQueueCompute];
}

- (void)processCaching
{
    if (self.request.method == GET) {
        DGConverseAPICacheJSONOperation *cacheWriteOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:[self.request.URL absoluteString] operationDelegate:self.manager];
        [cacheWriteOp setResult:[self.result deepCopy]];
        
        [self storeSubOperation:cacheWriteOp];
        
        [self.manager.queueManager addOperation:cacheWriteOp inQueue:kDGConverseAPIQueueCompute];
    }
}

@end
