//
//  DGConverseAPIOperationManager+SK.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationManager+SK.h"

@implementation DGConverseAPIOperationManager (SK)

#pragma mark - Public API Methods

#pragma mark - CUSTOM REQUESTS

- (id<DGConverseAPIGroupBaseOperation>)getLocalesWithCompletion:(SKLocalesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initLocales];
	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKLocalesParseDataOperation* parseOp = [[SKLocalesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
    [group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)getCurrenciesWithCompletion:(SKCurrenciesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initCurrencies];
	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKCurrenciesParseDataOperation* parseOp = [[SKCurrenciesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)getMarketsWithLocale:(NSString*)locale
												  completion:(SKMarketsResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initMarketsWithLocale:locale];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKMarketsParseDataOperation* parseOp = [[SKMarketsParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)getPlacesWithCountry:(NSString*)country
													currency:(NSString*)currency
													  locale:(NSString*)locale
													   query:(NSString*)query
												  completion:(SKPlacesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initPlacesWithCountry:country currency:currency locale:locale query:query];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKPlacesParseDataOperation* parseOp = [[SKPlacesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)getPlacesWithQuery:(NSString*)query
												completion:(SKPlacesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initPlacesWithQuery:query];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKPlacesParseDataOperation* parseOp = [[SKPlacesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)getPlaceWithCountry:(NSString*)country
												   currency:(NSString*)currency
													 locale:(NSString*)locale
													placeId:(NSString*)placeId
												 completion:(SKPlacesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initPlaceWithCountry:country currency:currency locale:locale placeId:placeId];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKPlacesParseDataOperation* parseOp = [[SKPlacesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)getPlaceWithPlaceId:(NSString*)placeId
												 completion:(SKPlacesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initPlaceWithPlaceId:placeId];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKPlacesParseDataOperation* parseOp = [[SKPlacesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)createLiveSessionWithParams:(SKLiveCreateSessionRequestParameters*)params
														   progress:(nullable DGConverseAPINetworkProgressBlock)progress
														 completion:(SKLiveCreateSessionResultBlock)completion
{
	SKURLEncodedRequest* networkRequest = [[SKURLEncodedRequest alloc] initCreateLiveSessionWithParams:params];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self progress:progress completion:nil];
	SKLiveCreateSessionParseDataOperation* parseOp = [[SKLiveCreateSessionParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)pollLiveSessionWithLocation:(NSString*)url
															 params:(SKLivePollSessionRequestParameters*)params
														   progress:(nullable DGConverseAPINetworkProgressBlock)progress
														 completion:(nullable SKLivePollSessionResultBlock)completion;
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initLivePollSessionWithLocation:url params:params];

	SKLiveSessionPollGroupOperation* group = [[SKLiveSessionPollGroupOperation alloc] initWithManager:self];
	[group runWithRequest:networkRequest
				 progress:progress
		  parseCompletion:completion
		  pollingInterval:2];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)createLiveBookingDetailsWithURI:(NSString*)bookingDetailsLinkURI
																   body:(NSString*)bookingDetailsLinkBody
															   progress:(nullable DGConverseAPINetworkProgressBlock)progress
															 completion:(nullable SKLiveCreateBookingDetailsResultBlock)completion
{
	SKURLEncodedRequest* networkRequest = [[SKURLEncodedRequest alloc] initLiveCreateBookingDetailsWithURI:bookingDetailsLinkURI body:bookingDetailsLinkBody];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKLiveCreateBookingDetailsParseDataOperation* parseOp = [[SKLiveCreateBookingDetailsParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)pollLiveBookingDetailsWithKey:(NSString*)sessionKey
														outboundLegId:(NSString*)outboundLegId
														 inboundLegId:(nullable NSString*)inboundLegId
															   params:(SKLivePollBookingDetailsRequestParameters*)params
															 progress:(nullable DGConverseAPINetworkProgressBlock)progress
														   completion:(nullable SKLivePollBookingDetailsResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initLivePollBookingDetailsWithKey:sessionKey
																			   outboundLegId:outboundLegId
																				inboundLegId:inboundLegId
																					  params:params];

	SKLiveBookingDetailsPollGroupOperation* group = [[SKLiveBookingDetailsPollGroupOperation alloc] initWithManager:self];
	[group runWithRequest:networkRequest
				 progress:progress
		  parseCompletion:completion
		  pollingInterval:5];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)browseQuoteWithOriginPlace:(NSString*)originPlace
												  destinationPlace:(NSString*)destinationPlace
														departDate:(NSString*)departDate
														returnDate:(NSString*)returnDate
														completion:(SKBrowseQuotesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initBrowseQuoteWithOriginPlace:originPlace
																		 destinationPlace:destinationPlace
																			   departDate:departDate
																			   returnDate:returnDate];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKBrowseQuotesParseDataOperation* parseOp = [[SKBrowseQuotesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)browseRoutesWithOriginPlace:(NSString*)originPlace
												   destinationPlace:(NSString*)destinationPlace
														 departDate:(NSString*)departDate
														 returnDate:(NSString*)returnDate
														 completion:(SKBrowseRoutesResultBlock)completion
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initBrowseRoutesWithOriginPlace:originPlace
																		  destinationPlace:destinationPlace
																				departDate:departDate
																				returnDate:returnDate];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKBrowseRoutesParseDataOperation* parseOp = [[SKBrowseRoutesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)browseDatesWithOriginPlace:(NSString*)originPlace
												  destinationPlace:(NSString*)destinationPlace
														departDate:(NSString*)departDate
														returnDate:(NSString*)returnDate
														completion:(SKBrowseDatesResultBlock)completion;
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initBrowseDatesWithOriginPlace:originPlace
																		 destinationPlace:destinationPlace
																			   departDate:departDate
																			   returnDate:returnDate];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKBrowseDatesParseDataOperation* parseOp = [[SKBrowseDatesParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)browseGridWithOriginPlace:(NSString*)originPlace
												 destinationPlace:(NSString*)destinationPlace
													   departDate:(NSString*)departDate
													   returnDate:(NSString*)returnDate
													   completion:(SKBrowseGridResultBlock)completion;
{
	SKJSONRequest* networkRequest = [[SKJSONRequest alloc] initBrowseGridWithOriginPlace:originPlace
																		destinationPlace:destinationPlace
																			  departDate:departDate
																			  returnDate:returnDate];

	SKOperation* networkOp = [[SKOperation alloc] initWithRequest:networkRequest operationDelegate:self];
	SKBrowseGridParseDataOperation* parseOp = [[SKBrowseGridParseDataOperation alloc] initWithOperationDelegate:self completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp parseOperation:parseOp];

	return group;
}

@end
