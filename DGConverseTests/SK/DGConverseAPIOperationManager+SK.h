//
//  DGConverseAPIOperationManager+SK.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationManager.h"

#import "SKNetworkHeader.h"
#import "SKOperationHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIOperationManager (SK)

#pragma mark - CUSTOM REQUESTS

- (id<DGConverseAPIGroupBaseOperation>)getLocalesWithCompletion:(SKLocalesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)getCurrenciesWithCompletion:(SKCurrenciesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)getMarketsWithLocale:(NSString*)locale
												  completion:(SKMarketsResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)getPlacesWithCountry:(NSString*)country
													currency:(NSString*)currency
													  locale:(NSString*)locale
													   query:(NSString*)query
												  completion:(SKPlacesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)getPlacesWithQuery:(NSString*)query
												completion:(SKPlacesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)getPlaceWithCountry:(NSString*)country
												   currency:(NSString*)currency
													 locale:(NSString*)locale
													placeId:(NSString*)placeId
												 completion:(SKPlacesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)getPlaceWithPlaceId:(NSString*)placeId
												 completion:(SKPlacesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)createLiveSessionWithParams:(SKLiveCreateSessionRequestParameters*)params
														   progress:(nullable DGConverseAPINetworkProgressBlock)progress
														 completion:(SKLiveCreateSessionResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)pollLiveSessionWithLocation:(NSString*)url
															 params:(SKLivePollSessionRequestParameters*)params
														   progress:(nullable DGConverseAPINetworkProgressBlock)progress
														 completion:(nullable SKLivePollSessionResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)createLiveBookingDetailsWithURI:(NSString*)bookingDetailsLinkURI
																   body:(NSString*)bookingDetailsLinkBody
															   progress:(nullable DGConverseAPINetworkProgressBlock)progress
															 completion:(nullable SKLiveCreateBookingDetailsResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)pollLiveBookingDetailsWithKey:(NSString*)sessionKey
														outboundLegId:(NSString*)outboundLegId
														 inboundLegId:(nullable NSString*)inboundLegId
															   params:(SKLivePollBookingDetailsRequestParameters*)params
															 progress:(nullable DGConverseAPINetworkProgressBlock)progress
														   completion:(nullable SKLivePollBookingDetailsResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)browseQuoteWithOriginPlace:(NSString*)originPlace
												  destinationPlace:(NSString*)destinationPlace
														departDate:(NSString*)departDate
														returnDate:(NSString*)returnDate
														completion:(SKBrowseQuotesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)browseRoutesWithOriginPlace:(NSString*)originPlace
												   destinationPlace:(NSString*)destinationPlace
														 departDate:(NSString*)departDate
														 returnDate:(NSString*)returnDate
														 completion:(SKBrowseRoutesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)browseDatesWithOriginPlace:(NSString*)originPlace
												  destinationPlace:(NSString*)destinationPlace
														departDate:(NSString*)departDate
														returnDate:(NSString*)returnDate
														completion:(SKBrowseDatesResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)browseGridWithOriginPlace:(NSString*)originPlace
												 destinationPlace:(NSString*)destinationPlace
													   departDate:(NSString*)departDate
													   returnDate:(NSString*)returnDate
													   completion:(SKBrowseGridResultBlock)completion;

NS_ASSUME_NONNULL_END

@end
