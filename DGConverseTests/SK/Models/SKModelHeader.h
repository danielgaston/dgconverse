//
//  SKModelHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef SKModelHeader_h
#define SKModelHeader_h

#import "SKBrowseCarrierDataModel.h"
#import "SKBrowseDateDataModel.h"
#import "SKBrowseDatesDataModel.h"
#import "SKBrowseGridDataModel.h"
#import "SKBrowseGridDateDataModel.h"
#import "SKBrowseLegDataModel.h"
#import "SKBrowsePlaceDataModel.h"
#import "SKBrowseQuoteDataModel.h"
#import "SKBrowseQuotesDataModel.h"
#import "SKBrowseRouteDataModel.h"
#import "SKBrowseRoutesDataModel.h"

#import "SKLiveAgentDataModel.h"
#import "SKLiveBookingItemDataModel.h"
#import "SKLiveBookingItemsDataModel.h"
#import "SKLiveCarrierDataModel.h"
#import "SKLiveCreateBookingDetailsDataModel.h"
#import "SKLiveCreateSessionDataModel.h"
#import "SKLiveItineraryDataModel.h"
#import "SKLiveLegDataModel.h"
#import "SKLivePlaceDataModel.h"
#import "SKLivePollBookingDetailsDataModel.h"
#import "SKLivePollSessionDataModel.h"
#import "SKLiveQueryDataModel.h"
#import "SKLiveSegmentDataModel.h"

#import "SKCountryDataModel.h"
#import "SKCurrenciesDataModel.h"
#import "SKCurrencyDataModel.h"
#import "SKLocaleDataModel.h"
#import "SKLocalesDataModel.h"
#import "SKMarketsDataModel.h"

#import "SKPlaceDataModel.h"
#import "SKPlacesDataModel.h"

#endif /* SKModelHeader_h */
