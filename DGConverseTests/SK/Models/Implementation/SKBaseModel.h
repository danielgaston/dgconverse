//
//  SKBaseModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

@import JSONModel;

@class SKLiveItineraryDataModel;
@class SKLiveLegDataModel;
@class SKLiveSegmentDataModel;
@class SKLivePlaceDataModel;
@class SKLiveCarrierDataModel;
@class SKLiveBookingItemsDataModel;

@protocol SKLiveItinerariesDelegate
@property (nonatomic, strong) NSArray<SKLiveItineraryDataModel*>* itineraries;
@end
@protocol SKLiveLegsDelegate
@property (nonatomic, strong) NSArray<SKLiveLegDataModel*>* legs;
@end
@protocol SKLiveSegmentsDelegate
@property (nonatomic, strong) NSArray<SKLiveSegmentDataModel*>* segments;
@end
@protocol SKLivePlacesDelegate
@property (nonatomic, strong) NSArray<SKLivePlaceDataModel*>* places;
@end
@protocol SKLiveCarriersDelegate
@property (nonatomic, strong) NSArray<SKLiveCarrierDataModel*>* carriers;
@end
@protocol SKLiveBookingOptionsDelegate
@property (nonatomic, strong) NSArray<SKLiveBookingItemsDataModel*>* bookingOptions;
@end

#pragma mark -

@class SKBrowsePlaceDataModel;
@class SKBrowseCarrierDataModel;
@class SKBrowseQuoteDataModel;
@class SKBrowseDateDataModel;

@protocol SKBrowsePlacesDelegate
@property (nonatomic, strong) NSArray<SKBrowsePlaceDataModel*>* places;
@end
@protocol SKBrowseCarriersDelegate
@property (nonatomic, strong) NSArray<SKBrowseCarrierDataModel*>* carriers;
@end
@protocol SKBrowseQuotesDelegate
@property (nonatomic, strong) NSArray<SKBrowseQuoteDataModel*>* quotes;
@end
@protocol SKBrowseDatesDelegate
@property (nonatomic, strong) SKBrowseDateDataModel* dates;
@end

#pragma mark -

@interface SKBaseModel : JSONModel

- (void)extractModels:(SKBaseModel*)baseModel;
+ (NSDictionary*)attributeMap;

@end
