//
//  SKBaseModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@implementation SKBaseModel

- (void)extractModels:(SKBaseModel*)baseModel
{
	// left blank intentionally
}

+ (NSDictionary*)attributeMap
{
	return @{};
}

@end
