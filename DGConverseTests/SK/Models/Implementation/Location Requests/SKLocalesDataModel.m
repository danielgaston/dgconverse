//
//  SKLocalesDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLocalesDataModel.h"

@implementation SKLocalesDataModel

//+ (JSONKeyMapper*)keyMapper
//{
//    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"locales" : @"Locales" }];
//}

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"locales" : @"Locales" }];
}

@end
