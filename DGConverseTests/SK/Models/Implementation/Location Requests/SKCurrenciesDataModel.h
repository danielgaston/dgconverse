//
//  SKCurrenciesDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKCurrencyDataModel.h"

@protocol SKCurrenciesDataModel;

@interface SKCurrenciesDataModel : SKBaseModel

@property (nonatomic, strong) NSArray<SKCurrencyDataModel> *currencies;

@end

/*
 
 {
 "Currencies": [
 {
 "Code": "USD",
 "Symbol": "$",
 "ThousandsSeparator": ",",
 "DecimalSeparator": ".",
 "SymbolOnLeft": true,
 "SpaceBetweenAmountAndSymbol": false,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 },
 ...
 ]
 }
 
 */
