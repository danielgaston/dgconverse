//
//  SKCountryDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKCountryDataModel.h"

@implementation SKCountryDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:[self attributeMap]];
}

+ (NSDictionary *)attributeMap
{
    return @{ @"code" : @"Code",
              @"name" : @"Name"
              };
}

@end
