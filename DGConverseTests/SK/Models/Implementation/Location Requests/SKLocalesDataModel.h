//
//  SKLocalesDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKLocaleDataModel.h"

@protocol SKLocalesDataModel;

@interface SKLocalesDataModel : SKBaseModel

@property (nonatomic, strong) NSArray<SKLocaleDataModel> *locales;

@end

/*
 {
 "Locales": [
 {
 "Code": "ar-AE",
 "Name": "العربية (الإمارات العربية المتحدة)"
 },
 {
 "Code": "az-AZ",
 "Name": "Azərbaycan­ılı (Azərbaycan)"
 },
 {
 "Code": "bg-BG",
 "Name": "български (България)"
 },
 {
 "Code": "ca-ES",
 "Name": "català (català)"
 },
 */
