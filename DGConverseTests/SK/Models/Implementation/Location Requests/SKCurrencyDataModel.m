//
//  SKCurrencyDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKCurrencyDataModel.h"

@implementation SKCurrencyDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:[self attributeMap]];
}

+ (NSDictionary *)attributeMap
{
    return @{ @"code" : @"Code",
       @"symbol" : @"Symbol",
       @"thousandsSeparator" : @"ThousandsSeparator",
       @"decimalSeparator" : @"DecimalSeparator",
       @"symbolOnLeft" : @"SymbolOnLeft",
       @"spaceBetweenAmountAndSymbol" : @"SpaceBetweenAmountAndSymbol",
       @"roundingCoefficient" : @"RoundingCoefficient",
              @"decimalDigits" : @"DecimalDigits" };
}

@end
