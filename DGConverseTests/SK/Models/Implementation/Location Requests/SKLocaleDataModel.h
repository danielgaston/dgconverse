//
//  SKLocaleDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLocaleDataModel;

@interface SKLocaleDataModel : SKBaseModel

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;

@end
