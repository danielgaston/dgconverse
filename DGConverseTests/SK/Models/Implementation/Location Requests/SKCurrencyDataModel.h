//
//  SKCurrencyDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKCurrencyDataModel;

@interface SKCurrencyDataModel : SKBaseModel

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *symbol;
@property (nonatomic, strong) NSString *thousandsSeparator;
@property (nonatomic, strong) NSString *decimalSeparator;
@property (nonatomic, assign) BOOL symbolOnLeft;
@property (nonatomic, assign) BOOL spaceBetweenAmountAndSymbol;
@property (nonatomic, assign) NSInteger roundingCoefficient;
@property (nonatomic, strong) NSString *decimalDigits;

@end

/*
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 */
