//
//  SKLocaleDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLocaleDataModel.h"

@implementation SKLocaleDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:[self attributeMap]];
}

+ (NSDictionary *)attributeMap
{
    return @{ @"code" : @"Code",
              @"name" : @"Name" };
}

@end
