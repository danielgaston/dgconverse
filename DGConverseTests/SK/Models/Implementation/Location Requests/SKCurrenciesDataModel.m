//
//  SKCurrenciesDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKCurrenciesDataModel.h"

@implementation SKCurrenciesDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"currencies" : @"Currencies" }];
}

@end
