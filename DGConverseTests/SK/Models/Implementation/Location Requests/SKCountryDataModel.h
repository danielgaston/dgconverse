//
//  SKCountryDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKCountryDataModel;

@interface SKCountryDataModel : SKBaseModel

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;

@end

/*
 
 {
 "Countries": [
 {
 "Code": "AD",
 "Name": "Andorra"
 },
 {
 "Code": "AE",
 "Name": "United Arab Emirates"
 },
 {
 "Code": "AF",
 "Name": "Afghanistan"
 },
 ...
 ]
 }
 */
