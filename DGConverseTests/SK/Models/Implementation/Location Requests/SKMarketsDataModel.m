//
//  SKMarketsDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKMarketsDataModel.h"

@implementation SKMarketsDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"countries" : @"Countries" }];
}

@end
