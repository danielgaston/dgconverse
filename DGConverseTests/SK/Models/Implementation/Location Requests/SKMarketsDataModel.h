//
//  SKMarketsDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKCountryDataModel.h"

@protocol SKMarketsDataModel;

@interface SKMarketsDataModel : SKBaseModel

@property (nonatomic, strong) NSArray<SKCountryDataModel> *countries;

@end

/*
 
 {
 "Countries": [
 {
 "Code": "AD",
 "Name": "Andorra"
 },
 {
 "Code": "AE",
 "Name": "United Arab Emirates"
 },
 {
 "Code": "AF",
 "Name": "Afghanistan"
 },
 ...
 ]
 }
 
 */
