//
//  SKLiveBookingItemsDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveBookingItemsDataModel.h"

@implementation SKLiveBookingItemsDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"bookingItems" : @"BookingItems"}];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self.bookingItems enumerateObjectsUsingBlock:^(SKLiveBookingItemDataModel *bookingItem, NSUInteger idx, BOOL * _Nonnull stop) {
        [bookingItem extractModels:self];
    }];
}

@end
