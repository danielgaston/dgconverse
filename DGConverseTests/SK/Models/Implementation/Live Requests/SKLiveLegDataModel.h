//
//  SKLiveLegDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveFlightNumberDataModel;

@interface SKLiveFlightNumberDataModel : SKBaseModel

@property (nonatomic, strong) NSString *flightNumber;
@property (nonatomic, assign) NSInteger carrierId;

@property (nonatomic, strong) SKLiveCarrierDataModel<Optional> *extractedCarrier;

@end

#pragma mark -

@protocol SKLiveLegDataModel;

@interface SKLiveLegDataModel : SKBaseModel

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSArray<NSNumber *> *segmentIds;
@property (nonatomic, assign) NSInteger originStation;
@property (nonatomic, assign) NSInteger destinationStation;
@property (nonatomic, strong) NSDate *departure;
@property (nonatomic, strong) NSDate *arrival;
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, strong) NSString *journeyMode;
@property (nonatomic, strong) NSArray<NSNumber*> *stops;
@property (nonatomic, strong) NSArray<NSNumber*> *carriers;
@property (nonatomic, strong) NSArray<NSNumber*> *operatingCarriers;
@property (nonatomic, strong) NSString *directionality;
@property (nonatomic, strong) NSArray<SKLiveFlightNumberDataModel> *flightNumbers;


@property (nonatomic, strong) SKLivePlaceDataModel<Ignore> *extractedOrigin;
@property (nonatomic, strong) SKLivePlaceDataModel<Ignore> *extractedDestination;
@property (nonatomic, strong) NSArray<SKLivePlaceDataModel*><Ignore> *extractedStops;
@property (nonatomic, strong) NSArray<SKLiveCarrierDataModel*><Ignore> *extractedCarriers;
@property (nonatomic, strong) NSArray<SKLiveCarrierDataModel*><Ignore> *extractedOperatingCarriers;

@end

/*
 
 {
 "Id": "11235-1705300650--32302,-32480-1-13554-1705301100",
 "SegmentIds": [
 0,
 1
 ],
 "OriginStation": 11235,
 "DestinationStation": 13554,
 "Departure": "2017-05-30T06:50:00",
 "Arrival": "2017-05-30T11:00:00",
 "Duration": 250,
 "JourneyMode": "Flight",
 "Stops": [
 13880
 ],
 "Carriers": [
 885,
 881
 ],
 "OperatingCarriers": [
 885,
 881
 ],
 "Directionality": "Outbound",
 "FlightNumbers": [
 {
 "FlightNumber": "290",
 "CarrierId": 885
 },
 {
 "FlightNumber": "1389",
 "CarrierId": 881
 }
 */
