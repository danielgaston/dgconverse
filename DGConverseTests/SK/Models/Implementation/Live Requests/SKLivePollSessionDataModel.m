//
//  SKLivePollSessionDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLivePollSessionDataModel.h"

@implementation SKLivePollSessionDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"sessionKey" : @"SessionKey",
		@"query" : @"Query",
		@"status" : @"Status",
		@"itineraries" : @"Itineraries",
		@"legs" : @"Legs",
		@"segments" : @"Segments",
		@"carriers" : @"Carriers",
		@"agents" : @"Agents",
		@"places" : @"Places",
		@"currencies" : @"Currencies"
	}];
}

- (BOOL)isCompleted;
{
	return [self.status isEqualToString:@"UpdatesComplete"];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    NSMutableArray<NSArray*> *extractionModels = @[].mutableCopy;
    self.places ? [extractionModels addObject:self.places] : nil;
    self.segments ? [extractionModels addObject:self.segments] : nil;
    self.legs ? [extractionModels addObject:self.legs] : nil;
    self.itineraries ? [extractionModels addObject:self.itineraries] : nil;
    
    [extractionModels enumerateObjectsUsingBlock:^(NSArray *models, NSUInteger idx, BOOL * _Nonnull stop) {
        
        for (SKBaseModel *model in models) {
            [model extractModels:self];
        }
    }];
}

@end
