//
//  SKLivePlaceDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLivePlaceDataModel.h"

@implementation SKLivePlaceDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"id" : @"Id",
		@"parentId" : @"ParentId",
		@"code" : @"Code",
		@"type" : @"Type",
		@"name" : @"Name"
	}];
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
	if ([propertyName isEqualToString:@"parentId"]) {
		return YES;
	}
	return NO;
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractParentWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractParentWithBaseModel:baseModel
{
    if (!_extractedParent) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLivePlacesDelegate)]) {
            NSArray<SKLivePlaceDataModel*> *places = ((SKBaseModel<SKLivePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKLivePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.parentId) {
                    self.extractedParent = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedParent extractModels:baseModel];
}

@end
