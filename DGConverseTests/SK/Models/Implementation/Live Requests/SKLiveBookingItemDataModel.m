//
//  SKLiveBookingItemDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveBookingItemDataModel.h"
#import "SKLiveSegmentDataModel.h"

@implementation SKLiveBookingItemDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"agentID" : @"AgentID",
                                                                  @"alternativeCurrency" : @"AlternativeCurrency",
                                                                  @"alternativePrice" : @"AlternativePrice",
                                                                  @"deeplink" : @"Deeplink",
                                                                  @"price" : @"Price",
                                                                  @"segmentIds" : @"SegmentIds",
                                                                  @"status" : @"Status"}];
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
    if ([propertyName isEqualToString:@"alternativePrice"]) {
        return YES;
    } 
    
    return NO;
}


- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractSegmentsWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractSegmentsWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedSegments) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveSegmentsDelegate)]) {
            NSArray<SKLiveSegmentDataModel*> *segments = ((SKBaseModel<SKLiveSegmentsDelegate> *)baseModel).segments;
            
            __block NSMutableArray *extractedSegments = @[].mutableCopy;
            for (NSNumber *segmentId in self.segmentIds){
                
                __block SKLiveSegmentDataModel *segment;
                [segments enumerateObjectsUsingBlock:^(SKLiveSegmentDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.id == segmentId.integerValue) {
                        segment = obj;
                        [extractedSegments addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [segment extractModels:baseModel];
            }
            _extractedSegments = extractedSegments;
        }
    }
}

@end
