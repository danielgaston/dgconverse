//
//  SKLiveCreateSessionDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveCreateSessionDataModel;

@interface SKLiveCreateSessionDataModel : SKBaseModel

@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* sessionKey;

- (instancetype)initWithLocationURLString:(NSString*)URLString;

@end
