//
//  SKLiveCreateBookingDetailsDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveCreateBookingDetailsDataModel.h"

@implementation SKLiveCreateBookingDetailsDataModel

- (instancetype)initWithLocationURLString:(NSString*)URLString
{
	self = [super init];
	if (self) {
		[self processLocation:URLString];
	}
	return self;
}

- (void)processLocation:(NSString*)URLString
{
	//

	NSArray<NSString*>* components = [URLString componentsSeparatedByString:@"/"];
	self.location = [components lastObject];
}

@end
