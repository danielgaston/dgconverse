//
//  SKLiveItineraryDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLivePricingOptionsDataModel;

@interface SKLivePricingOptionsDataModel : SKBaseModel

@property (nonatomic, strong) NSArray <NSNumber *> *agents;
@property (nonatomic, assign) NSInteger quoteAgeInMinutes;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, strong) NSString *deeplinkUrl;

@end;

#pragma mark -

@protocol SKBookingDetailsLinkDataModel;

@interface SKBookingDetailsLinkDataModel : SKBaseModel

@property (nonatomic, strong) NSString *uri;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSString *method;

@end;

#pragma mark -

@protocol SKLiveItineraryDataModel;

@interface SKLiveItineraryDataModel : SKBaseModel

@property (nonatomic, strong) NSString *outboundLegId;
@property (nonatomic, strong) NSString<Optional> *inboundLegId;
@property (nonatomic, strong) NSArray <SKLivePricingOptionsDataModel> *pricingOptions;
@property (nonatomic, strong) SKBookingDetailsLinkDataModel *bookingDetailsLink;


@property (nonatomic, strong) SKLiveLegDataModel<Ignore> *extractedOutboundLeg;
@property (nonatomic, strong) SKLiveLegDataModel<Ignore> *extractedInboundLeg;

- (NSInteger)lowestPrice;

@end

/*
 
 {
 "OutboundLegId": "11235-1705301925--32480-0-13554-1705302055",
 "InboundLegId": "13554-1706020700--32480-0-11235-1706020820",
 "PricingOptions": [
 {
 "Agents": [
 4499211
 ],
 "QuoteAgeInMinutes": 0,
 "Price": 83.41,
 "DeeplinkUrl": "http://partners.api.skyscanner.net/apiservices/deeplink/v2?_cje=jzj5DawL5zJyT%2bnfe1..."
 },
 ...
 ],
 "BookingDetailsLink": {
 "Uri": "/apiservices/pricing/v1.0/ab5b948d616e41fb954a4a2f6b8dde1a_ecilpojl_7CAAD17D0CFC34BFDE68DEBFDFD548C7/booking",
 "Body": "OutboundLegId=11235-1705301925--32480-0-13554-1705302055&InboundLegId=13554-1706020700--32480-0-11235-1706020820",
 "Method": "PUT"
 }
 }
 */
