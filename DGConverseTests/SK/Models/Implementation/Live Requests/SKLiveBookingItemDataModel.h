//
//  SKLiveBookingItemDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//
#import "SKBaseModel.h"

@protocol SKLiveBookingItemDataModel;

@interface SKLiveBookingItemDataModel : SKBaseModel

@property(nonatomic, assign) NSInteger agentID;
@property(nonatomic, strong) NSString <Optional> *alternativeCurrency;
@property(nonatomic, assign) NSInteger alternativePrice;
@property(nonatomic, strong) NSString *deeplink;
@property(nonatomic, assign) NSInteger price;
@property(nonatomic, strong) NSArray<NSNumber *> *segmentIds;
@property(nonatomic, strong) NSString *status;

@property(nonatomic, strong) NSArray<SKLiveSegmentDataModel*><Ignore> *extractedSegments;

@end

/*
 
 {
 "AgentID": 4499219,
 "AlternativeCurrency": "GBP",
 "AlternativePrice": 98.89,
 "Deeplink": "http://partners.api.skyscanner.net/apiservices/deeplink/v2?_cje=I9spmrlz4bVow9vdJX3j0WMrCxxCUhsNO5J1RF40GM%2fKfTXl0DhNzAsMKYVFEgRZ&url=http%3a%2f%2fwww.apideeplink.com%2ftransport_deeplink%2f4.0%2fUS%2fen-gb%2fUSD%2fxpus%2f2%2f4698.10413.2018-04-03%2c10413.4698.2018-04-04%2fair%2ftrava%2fflights%3fitinerary%3dflight%7c-32356%7c8327%7c13542%7c2018-04-03T18%3a00%7c10413%7c2018-04-03T20%3a20%7c80%2cflight%7c-32356%7c2432%7c10413%7c2018-04-04T08%3a55%7c13771%7c2018-04-04T09%3a10%7c75%26carriers%3d-32356%26operators%3d-32356%2c-32356%26passengers%3d1%26channel%3ddataapi%26cabin_class%3deconomy%26facilitated%3dfalse%26ticket_price%3d136.48%26is_npt%3dfalse%26is_multipart%3dfalse%26client_id%3dskyscanner_b2b%26request_id%3da44078a0-32a2-48e1-87c8-63be822b2665%26deeplink_ids%3deu-central-1.prod_f1aee5dc5ae3dda0a81b765d31f988a8%26commercial_filters%3dfalse%26q_datetime_utc%3d2018-03-05T17%3a04%3a53",
 "Price": 136.48,
 "SegmentIds": [
 1,
 2
 ],
 "Status": "Current"
 }
 
 */
