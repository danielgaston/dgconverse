//
//  SKLiveAgentDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveAgentDataModel.h"

@implementation SKLiveAgentDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"id" : @"Id",
		@"name" : @"Name",
		@"imageUrl" : @"ImageUrl",
		@"status" : @"Status",
		@"optimisedForMobile" : @"OptimisedForMobile",
		@"bookingNumber" : @"BookingNumber",
		@"type" : @"Type" }];
}

@end
