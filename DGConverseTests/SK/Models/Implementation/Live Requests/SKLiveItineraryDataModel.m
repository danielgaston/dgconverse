//
//  SKLiveItineraryDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveItineraryDataModel.h"
#import "SKLiveLegDataModel.h"

@implementation SKLivePricingOptionsDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"agents" : @"Agents",
		@"quoteAgeInMinutes" : @"QuoteAgeInMinutes",
		@"price" : @"Price",
		@"deeplinkUrl" : @"DeeplinkUrl"
	}];
}

@end

#pragma mark -

@implementation SKBookingDetailsLinkDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"uri" : @"Uri",
		@"body" : @"Body",
		@"method" : @"Method"
	}];
}

@end

#pragma mark -

@implementation SKLiveItineraryDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"outboundLegId" : @"OutboundLegId",
		@"inboundLegId" : @"InboundLegId",
		@"pricingOptions" : @"PricingOptions",
		@"bookingDetailsLink" : @"BookingDetailsLink"
	}];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractOutboundLegWithBaseModel:baseModel];
    [self extractInboundLegWithBaseModel:baseModel];
}

- (NSInteger)lowestPrice
{
    NSInteger lowestPrice = NSUIntegerMax;
    NSArray *sortedArray = [self.pricingOptions sortedArrayUsingComparator:^NSComparisonResult(SKLivePricingOptionsDataModel *a, SKLivePricingOptionsDataModel *b) {
        NSInteger first = [a price];
        NSInteger second = [b price];
        return [@(first) compare:@(second)];
    }];
    
    if (sortedArray.count)
        lowestPrice = [[sortedArray firstObject] price];

    return lowestPrice;
}

#pragma mark - Helper Methods

- (void)extractOutboundLegWithBaseModel:baseModel
{
    if (!_extractedOutboundLeg) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveLegsDelegate)]) {
            NSArray<SKLiveLegDataModel*> *legs = ((SKBaseModel<SKLiveLegsDelegate> *)baseModel).legs;
            
            [legs enumerateObjectsUsingBlock:^(SKLiveLegDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.id isEqualToString:self.outboundLegId]) {
                    self.extractedOutboundLeg = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedOutboundLeg extractModels:baseModel];
}

- (void)extractInboundLegWithBaseModel:baseModel
{
    if (!_extractedInboundLeg) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveLegsDelegate)]) {
            NSArray<SKLiveLegDataModel*> *legs = ((SKBaseModel<SKLiveLegsDelegate> *)baseModel).legs;
            
            [legs enumerateObjectsUsingBlock:^(SKLiveLegDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.id isEqualToString:self.inboundLegId]) {
                    self.extractedInboundLeg = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedInboundLeg extractModels:baseModel];
}


@end
