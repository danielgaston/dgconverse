//
//  SKLiveCarrierDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveCarrierDataModel;

@interface SKLiveCarrierDataModel : SKBaseModel

@property (nonatomic, assign) NSInteger id;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString <Optional>*displayCode;

@end

/*
 {
 "Id": 885,
 "Code": "BE",
 "Name": "Flybe",
 "ImageUrl": "http://s1.apideeplink.com/images/airlines/BE.png",
 "DisplayCode": "BE"
 }
 */
