//
//  SKLiveLegDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveLegDataModel.h"
#import "SKLivePlaceDataModel.h"
#import "SKLiveCarrierDataModel.h"

@implementation SKLiveFlightNumberDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"flightNumber" : @"FlightNumber",
		@"carrierId" : @"CarrierId" }];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractCarrierWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractCarrierWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedCarrier) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveCarriersDelegate)]) {
            NSArray<SKLiveCarrierDataModel*> *carriers = ((SKBaseModel<SKLiveCarriersDelegate> *)baseModel).carriers;
            
            [carriers enumerateObjectsUsingBlock:^(SKLiveCarrierDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.carrierId) {
                    self.extractedCarrier = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedCarrier extractModels:baseModel];
}

@end

#pragma mark -

@implementation SKLiveLegDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"id" : @"Id",
		@"segmentIds" : @"SegmentIds",
		@"originStation" : @"OriginStation",
		@"destinationStation" : @"DestinationStation",
		@"departure" : @"Departure",
		@"arrival" : @"Arrival",
		@"duration" : @"Duration",
		@"journeyMode" : @"JourneyMode",
		@"stops" : @"Stops",
		@"carriers" : @"Carriers",
		@"operatingCarriers" : @"OperatingCarriers",
		@"directionality" : @"Directionality",
		@"flightNumbers" : @"FlightNumbers"
	}];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractOriginWithBaseModel:baseModel];
    [self extractDestinationWithBaseModel:baseModel];
    [self extractStopsWithBaseModel:baseModel];
    [self extractCarriersWithBaseModel:baseModel];
    [self extractOperatingCarriersWithBaseModel:baseModel];
    [self extractFlightNumbersWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractOriginWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedOrigin) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLivePlacesDelegate)]) {
            NSArray<SKLivePlaceDataModel*> *places = ((SKBaseModel<SKLivePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKLivePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.originStation) {
                    self.extractedOrigin = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedOrigin extractModels:baseModel];
}

- (void)extractDestinationWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedDestination) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLivePlacesDelegate)]) {
            NSArray<SKLivePlaceDataModel*> *places = ((SKBaseModel<SKLivePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKLivePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.destinationStation) {
                    self.extractedDestination = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedDestination extractModels:baseModel];
}

- (void)extractStopsWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedStops) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLivePlacesDelegate)]) {
            NSArray<SKLivePlaceDataModel*> *places = ((SKBaseModel<SKLivePlacesDelegate> *)baseModel).places;
            
            __block NSMutableArray *extractedStops = @[].mutableCopy;
            for (NSNumber *stopId in self.stops){
                
                __block SKLivePlaceDataModel *place;
                [places enumerateObjectsUsingBlock:^(SKLivePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.id == stopId.integerValue) {
                        place = obj;
                        [extractedStops addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [place extractModels:baseModel];
            }
            _extractedStops = extractedStops;
        }
    }
}

- (void)extractCarriersWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedCarriers) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveCarriersDelegate)]) {
            NSArray<SKLiveCarrierDataModel*> *carriers = ((SKBaseModel<SKLiveCarriersDelegate> *)baseModel).carriers;
            
            __block NSMutableArray *extractedCarriers = @[].mutableCopy;
            for (NSNumber *carrierId in self.carriers){
                
                __block SKLiveCarrierDataModel *carrier;
                [carriers enumerateObjectsUsingBlock:^(SKLiveCarrierDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.id == carrierId.integerValue) {
                        carrier = obj;
                        [extractedCarriers addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [carrier extractModels:baseModel];
            }
            _extractedCarriers = extractedCarriers;
        }
    }
}

- (void)extractOperatingCarriersWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedOperatingCarriers) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveCarriersDelegate)]) {
            NSArray<SKLiveCarrierDataModel*> *carriers = ((SKBaseModel<SKLiveCarriersDelegate> *)baseModel).carriers;
            
            __block NSMutableArray *extractedOperatingCarriers = @[].mutableCopy;
            for (NSNumber *carrierId in self.carriers){
                
                __block SKLiveCarrierDataModel *carrier;
                [carriers enumerateObjectsUsingBlock:^(SKLiveCarrierDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.id == carrierId.integerValue) {
                        carrier = obj;
                        [extractedOperatingCarriers addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [carrier extractModels:baseModel];
            }
            _extractedOperatingCarriers = extractedOperatingCarriers;
        }
    }
}

- (void)extractFlightNumbersWithBaseModel:(SKBaseModel *)baseModel
{
    for (SKLiveFlightNumberDataModel *flightNumber in self.flightNumbers)
        [flightNumber extractModels:baseModel];
}

@end
