//
//  SKLivePollBookingDetailsDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLivePollBookingDetailsDataModel.h"

@implementation SKLivePollBookingDetailsDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
        @"bookingOptions" : @"BookingOptions",
        @"carriers" : @"Carriers",
        @"places" : @"Places",
		@"query" : @"Query",
		@"segments" : @"Segments"
	}];
}

- (BOOL)isCompleted;
{
    return YES;
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    NSMutableArray<NSArray*> *extractionModels = @[].mutableCopy;
    self.carriers ? [extractionModels addObject:self.carriers] : nil;
    self.places ? [extractionModels addObject:self.places] : nil;
    self.segments ? [extractionModels addObject:self.segments] : nil;
    self.bookingOptions ? [extractionModels addObject:self.bookingOptions] : nil;

    [extractionModels enumerateObjectsUsingBlock:^(NSArray *models, NSUInteger idx, BOOL * _Nonnull stop) {
        
        for (SKBaseModel *model in models) {
            [model extractModels:self];
        }
    }];
}

@end
