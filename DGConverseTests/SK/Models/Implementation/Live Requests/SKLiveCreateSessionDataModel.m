//
//  SKLiveCreateSessionDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveCreateSessionDataModel.h"

@implementation SKLiveCreateSessionDataModel

- (instancetype)initWithLocationURLString:(NSString*)URLString
{
	self = [super init];
	if (self) {
		[self processLocation:URLString];
	}
	return self;
}

- (void)processLocation:(NSString*)URLString
{
	// "http://partners.api.skyscanner.net/apiservices/pricing/uk2/v1.0/b8ed5fd66e764f0f9b44ff32827ab1c1_rrsqbjcb_cd8283034710069eed7028483edbcb3e"
    self.location = URLString;
	NSArray<NSString*>* components = [URLString componentsSeparatedByString:@"/"];
	self.sessionKey = [components lastObject];
}

@end
