//
//  SKLiveQueryDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveQueryDataModel;

@interface SKLiveQueryDataModel : SKBaseModel

@property(nonatomic, strong) NSString *country;
@property(nonatomic, strong) NSString *currency;
@property(nonatomic, strong) NSString *locale;
@property(nonatomic, strong) NSString *locationSchema;
@property(nonatomic, assign) NSInteger adults;
@property(nonatomic, assign) NSInteger children;
@property(nonatomic, assign) NSInteger infants;
@property(nonatomic, strong) NSString *originPlace;
@property(nonatomic, strong) NSString *destinationPlace;
@property(nonatomic, strong) NSDate *outboundDate;
@property(nonatomic, strong) NSDate <Optional> *inboundDate;
@property(nonatomic, strong) NSString *cabinClass;
@property(nonatomic, assign) BOOL groupPricing;

//@property(nonatomic, strong) NSString <Optional>*includeCarriers;
//@property(nonatomic, strong) NSString <Optional>*excludeCarriers;

@end
