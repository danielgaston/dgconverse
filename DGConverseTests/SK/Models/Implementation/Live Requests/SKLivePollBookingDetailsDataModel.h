//
//  SKLivePollBookingDetailsDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveAgentDataModel.h"
#import "SKCurrencyDataModel.h"
#import "SKLiveCarrierDataModel.h"
#import "SKLiveItineraryDataModel.h"
#import "SKLiveLegDataModel.h"
#import "SKLivePlaceDataModel.h"
#import "SKLiveQueryDataModel.h"
#import "SKLiveSegmentDataModel.h"
#import "SKLiveBookingItemsDataModel.h"
#import "SKBaseModel.h"

@protocol SKLivePollBookingDetailsDataModel;

@interface SKLivePollBookingDetailsDataModel : SKBaseModel <SKLiveSegmentsDelegate, SKLivePlacesDelegate, SKLiveCarriersDelegate, SKLiveBookingOptionsDelegate>

/**
 @warning: https://skyscanner.github.io/slate/#polling-the-booking-details reports many attributes that does not come on the JSON!!
*/

@property (nonatomic, strong) NSArray<SKLiveBookingItemsDataModel, Optional>* bookingOptions;
@property (nonatomic, strong) NSArray<SKLiveCarrierDataModel, Optional>* carriers;
@property (nonatomic, strong) NSArray<SKLivePlaceDataModel, Optional>* places;
@property (nonatomic, strong) SKLiveQueryDataModel* query;
@property (nonatomic, strong) NSArray<SKLiveSegmentDataModel, Optional>* segments;

- (BOOL)isCompleted;

@end

/*
 {
 "Segments": [
 {
 "Id": 1,
 "OriginStation": 11235,
 "DestinationStation": 13554,
 "DepartureDateTime": "2017-05-30T19:25:00",
 "ArrivalDateTime": "2017-05-30T20:55:00",
 "Carrier": 881,
 "OperatingCarrier": 881,
 "Duration": 90,
 "FlightNumber": "1463",
 "JourneyMode": "Flight",
 "Directionality": "Outbound"
 },
 ...
 ],
 "BookingOptions": [
 {
 "BookingItems": [
 {
 "AgentID": 4499211,
 "Status": "Current",
 "Price": 83.41,
 "Deeplink": "http://partners.api.skyscanner.net/apiservices/deeplink/v2?_cje=jzj5DawL5[...]",
 "SegmentIds": [
 1,
 2
 ]
 }
 ]
 },
 ],
 "Places": [
 {
 "Id": 13554,
 "ParentId": 4698,
 "Code": "LHR",
 "Type": "Airport",
 "Name": "London Heathrow"
 },
 ...
 ],
 "Carriers": [
 {
 "Id": 881,
 "Code": "BA",
 "Name": "British Airways",
 "ImageUrl": "http://s1.apideeplink.com/images/airlines/BA.png"
 }
 ],
 "Query": {
 "Country": "GB",
 "Currency": "GBP",
 "Locale": "en-gb",
 "Adults": 1,
 "Children": 0,
 "Infants": 0,
 "OriginPlace": "2343",
 "DestinationPlace": "13554",
 "OutboundDate": "2017-05-30",
 "InboundDate": "2017-06-02",
 "LocationSchema": "Default",
 "CabinClass": "Economy",
 "GroupPricing": false
 }
 }
 */
