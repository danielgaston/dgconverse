//
//  SKLiveQueryDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveQueryDataModel.h"

@implementation SKLiveQueryDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"country" : @"Country",
		@"currency" : @"Currency",
		@"locale" : @"Locale",
		@"locationSchema" : @"LocationSchema",
		@"adults" : @"Adults",
		@"children" : @"Children",
		@"infants" : @"Infants",
		@"originPlace" : @"OriginPlace",
		@"destinationPlace" : @"DestinationPlace",
		@"outboundDate" : @"OutboundDate",
		@"inboundDate" : @"InboundDate",
		@"cabinClass" : @"CabinClass",
		@"groupPricing" : @"GroupPricing" }];
}

@end
