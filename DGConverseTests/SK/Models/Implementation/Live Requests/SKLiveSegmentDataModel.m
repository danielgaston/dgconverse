//
//  SKLiveSegmentDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveSegmentDataModel.h"
#import "SKLivePlaceDataModel.h"
#import "SKLiveCarrierDataModel.h"

@implementation SKLiveSegmentDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"id" : @"Id",
		@"originStation" : @"OriginStation",
		@"destinationStation" : @"DestinationStation",
		@"departureDateTime" : @"DepartureDateTime",
		@"arrivalDateTime" : @"ArrivalDateTime",
		@"carrier" : @"Carrier",
		@"operatingCarrier" : @"OperatingCarrier",
		@"duration" : @"Duration",
		@"flightNumber" : @"FlightNumber",
		@"journeyMode" : @"JourneyMode",
		@"directionality" : @"Directionality" }];
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
	if ([propertyName isEqualToString:@"originStation"]) {
		return YES;
	} else if ([propertyName isEqualToString:@"destinationStation"]) {
		return YES;
	} else if ([propertyName isEqualToString:@"departureDateTime"]) {
		return YES;
	} else if ([propertyName isEqualToString:@"arrivalDateTime"]) {
		return YES;
	} else if ([propertyName isEqualToString:@"duration"]) {
		return YES;
    } else if ([propertyName isEqualToString:@"operatingCarrier"]) {
        return YES;
    }
    
	return NO;
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractOriginWithBaseModel:baseModel];
    [self extractDestinationWithBaseModel:baseModel];
    [self extractCarrierWithBaseModel:baseModel];
    [self extractOperatingCarrierWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractOriginWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedOrigin) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLivePlacesDelegate)]) {
            NSArray<SKLivePlaceDataModel*> *places = ((SKBaseModel<SKLivePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKLivePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.originStation) {
                    self.extractedOrigin = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedOrigin extractModels:baseModel];
}

- (void)extractDestinationWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedDestination) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLivePlacesDelegate)]) {
            NSArray<SKLivePlaceDataModel*> *places = ((SKBaseModel<SKLivePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKLivePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.destinationStation) {
                    self.extractedDestination = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedDestination extractModels:baseModel];
}

- (void)extractCarrierWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedCarrier) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveCarriersDelegate)]) {
            NSArray<SKLiveCarrierDataModel*> *carriers = ((SKBaseModel<SKLiveCarriersDelegate> *)baseModel).carriers;
            
            [carriers enumerateObjectsUsingBlock:^(SKLiveCarrierDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.carrier) {
                    self.extractedCarrier = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedCarrier extractModels:baseModel];
}

- (void)extractOperatingCarrierWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedOperatingCarrier) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKLiveCarriersDelegate)]) {
            NSArray<SKLiveCarrierDataModel*> *carriers = ((SKBaseModel<SKLiveCarriersDelegate> *)baseModel).carriers;
            
            [carriers enumerateObjectsUsingBlock:^(SKLiveCarrierDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.id == self.carrier) {
                    self.extractedOperatingCarrier = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedOperatingCarrier extractModels:baseModel];
}

@end
