//
//  SKLivePlaceDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//
#import "SKBaseModel.h"

@protocol SKLivePlaceDataModel;

@interface SKLivePlaceDataModel : SKBaseModel

@property (nonatomic, assign) NSInteger id;
@property (nonatomic, assign) NSInteger parentId;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) SKLivePlaceDataModel<Ignore> *extractedParent;

@end

/*
 "Id": 11235,
 "ParentId": 2343,
 "Code": "EDI",
 "Type": "Airport",
 "Name": "Edinburgh"
*/
