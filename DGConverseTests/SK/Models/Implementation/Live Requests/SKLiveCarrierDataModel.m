//
//  SKLiveCarrierDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKLiveCarrierDataModel.h"

@implementation SKLiveCarrierDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"id" : @"Id",
		@"code" : @"Code",
		@"name" : @"Name",
		@"imageUrl" : @"ImageUrl",
		@"displayCode" : @"DisplayCode" }];
}

@end
