//
//  SKLiveAgentDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveAgentDataModel;

@interface SKLiveAgentDataModel : SKBaseModel

@property(nonatomic, assign) NSInteger id;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *imageUrl;
@property(nonatomic, strong) NSString<Optional> *status;
@property(nonatomic, assign) BOOL optimisedForMobile;
@property(nonatomic, strong) NSString<Optional> *bookingNumber;
@property(nonatomic, strong) NSString *type;

@end

/*
 {
 "Id": 1963108,
 "Name": "Mytrip",
 "ImageUrl": "http://s1.apideeplink.com/images/websites/at24.png",
 "Status": "UpdatesComplete",
 "OptimisedForMobile": true,
 "BookingNumber": "+448447747881",
 "Type": "TravelAgent"
 }
 */
