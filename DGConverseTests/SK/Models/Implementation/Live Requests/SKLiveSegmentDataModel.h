//
//  SKLiveSegmentDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveSegmentDataModel;

@interface SKLiveSegmentDataModel : SKBaseModel

@property(nonatomic, assign) NSInteger id;
@property(nonatomic, assign) NSInteger originStation;
@property(nonatomic, assign) NSInteger destinationStation;
@property(nonatomic, strong) NSDate *departureDateTime;
@property(nonatomic, strong) NSDate *arrivalDateTime;
@property(nonatomic, assign) NSInteger carrier;
@property(nonatomic, assign) NSInteger operatingCarrier;
@property(nonatomic, assign) NSInteger duration;
@property(nonatomic, strong) NSString *flightNumber;
@property(nonatomic, strong) NSString *journeyMode;
@property(nonatomic, strong) NSString *directionality;

@property (nonatomic, strong) SKLivePlaceDataModel<Optional> *extractedOrigin;
@property (nonatomic, strong) SKLivePlaceDataModel<Optional> *extractedDestination;
@property (nonatomic, strong) SKLiveCarrierDataModel<Optional> *extractedCarrier;
@property (nonatomic, strong) SKLiveCarrierDataModel<Optional> *extractedOperatingCarrier;

@end

/*
 {
 "Id": 0,
 "OriginStation": 11235,
 "DestinationStation": 13880,
 "DepartureDateTime": "2017-05-30T06:50:00",
 "ArrivalDateTime": "2017-05-30T07:55:00",
 "Carrier": 885,
 "OperatingCarrier": 885,
 "Duration": 65,
 "FlightNumber": "290",
 "JourneyMode": "Flight",
 "Directionality": "Outbound"
 },
 */
