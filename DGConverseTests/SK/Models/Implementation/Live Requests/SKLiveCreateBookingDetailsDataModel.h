//
//  SKLiveCreateBookingDetailsDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKLiveCreateBookingDetailsDataModel;

@interface SKLiveCreateBookingDetailsDataModel : SKBaseModel

@property (nonatomic, strong) NSString* location;

- (instancetype)initWithLocationURLString:(NSString*)URLString;

@end
