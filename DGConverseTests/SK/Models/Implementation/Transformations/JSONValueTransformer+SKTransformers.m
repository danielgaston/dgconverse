//
//  JSONValueTransformer+SKTransformers.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "JSONValueTransformer+SKTransformers.h"
#import "NSDate+Additions.h"

@implementation JSONValueTransformer (SKDateTransformer)

- (NSDate*)NSDateFromNSString:(NSString*)string
{
    //RAW: @"2018-02-12T01:04:00"
    
    // Not working properly due to differences on the date format
    //    NSISO8601DateFormatter *ISO8601DateFormatter = [[NSISO8601DateFormatter alloc] init];
    //    ISO8601DateFormatter.formatOptions = NSISO8601DateFormatWithInternetDateTime;
    //    NSDate *capturedDate = [ISO8601DateFormatter dateFromString:string];
    
    NSDateFormatter* skDateFormatter = [NSDateFormatter formatterWithDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* capturedDate = [skDateFormatter dateFromString:string];
    
    return capturedDate;
}

- (NSString*)JSONObjectFromNSDate:(NSDate*)date
{
    //    return [[NSDateFormatter new] stringFromDate:date];
    NSDateFormatter* skDateFormatter = [NSDateFormatter formatterWithDateFormat:@"yyyy-MM-dd"];
    NSString* dateString = [skDateFormatter stringFromDate:date];
    return [NSString stringWithString:dateString];
}

@end
