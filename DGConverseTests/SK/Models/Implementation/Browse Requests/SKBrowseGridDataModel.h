//
//  SKBrowseGridDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

#import "SKBrowseGridDateDataModel.h"
#import "SKBrowsePlaceDataModel.h"
#import "SKBrowseCarrierDataModel.h"
#import "SKCurrencyDataModel.h"

typedef NSArray<SKBrowseGridDateDataModel>* SKBrowseGridDateType;

@protocol SKBrowseGridDataModel;

@interface SKBrowseGridDataModel : SKBaseModel <SKBrowsePlacesDelegate, SKBrowseCarriersDelegate>

@property (nonatomic, strong) NSArray<SKBrowseGridDateType><Optional> *dates;
@property (nonatomic, strong) NSArray<Optional, SKBrowsePlaceDataModel>* places;
@property (nonatomic, strong) NSArray<Optional, SKBrowseCarrierDataModel>* carriers;
@property (nonatomic, strong) NSArray<Optional, SKCurrencyDataModel>* currencies;

@end

/*
 
 {
 "Dates": [
 [
 null,
 {
 "DateString": "2017-01"
 },
 {
 "DateString": "2017-02"
 },
 {
 "DateString": "2017-03"
 },
 ...
 ],
 ...
 ],
 "Places": [
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 },
 ...
 ],
 "Carriers": [
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 },
 {
 "CarrierId": 173,
 "Name": "Silver Airways"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 ]
 }
 
 */

