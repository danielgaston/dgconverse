//
//  SKBrowseQuoteDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseQuoteDataModel.h"

@implementation SKBrowseQuoteDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"quoteId" : @"QuoteId",
                                                                   @"minPrice" : @"MinPrice",
                                                                   @"direct" : @"Direct",
                                                                   @"outboundLeg" : @"OutboundLeg",
                                                                   @"inboundLeg" : @"InboundLeg",
                                                                   @"quoteDateTime" : @"QuoteDateTime" }];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    NSMutableArray<SKBrowseLegDataModel *> *extractionModels = @[].mutableCopy;
    self.outboundLeg ? [extractionModels addObject:self.outboundLeg] : nil;
    self.inboundLeg ? [extractionModels addObject:self.inboundLeg] : nil;
    
    [extractionModels enumerateObjectsUsingBlock:^(SKBrowseLegDataModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        [model extractModels:baseModel];
    }];
}


@end

