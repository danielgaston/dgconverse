//
//  SKBrowseRouteDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKBrowsePlaceDataModel.h"
#import "SKBrowseQuoteDataModel.h"


@protocol SKBrowseRouteDataModel;

@interface SKBrowseRouteDataModel : SKBaseModel

@property (nonatomic, assign) NSInteger originId;
@property (nonatomic, assign) NSInteger destinationId;
@property (nonatomic, strong) NSSet<NSNumber *> *quoteIds;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, strong) NSDate *quoteDateTime;

@property (nonatomic, strong) SKBrowsePlaceDataModel<Ignore> *extractedOrigin;
@property (nonatomic, strong) SKBrowsePlaceDataModel<Ignore> *extractedDestination;
@property (nonatomic, strong) NSSet<SKBrowseQuoteDataModel *><Ignore> *extractedQuotes;

@end
/*
{
    "OriginId": 1811,
    "DestinationId": 1845,
    "QuoteIds": [
                 1,
                 2
                 ],
    "Price": 326,
    "QuoteDateTime": "2016-11-13T01:30:00"
}
*/
