//
//  SKBrowseGridDateDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKBrowseGridDateDataModel;

@interface SKBrowseGridDateDataModel : SKBaseModel

@property (nonatomic, strong) NSDate <Optional> *date;

@end
