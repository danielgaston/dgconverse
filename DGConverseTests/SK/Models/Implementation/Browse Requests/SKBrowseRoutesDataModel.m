//
//  SKBrowseRoutesDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseRoutesDataModel.h"

@implementation SKBrowseRoutesDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"routes" : @"Routes",
		@"quotes" : @"Quotes",
		@"places" : @"Places",
		@"carriers" : @"Carriers",
		@"currencies" : @"Currencies"
	}];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    NSMutableArray<NSArray*> *extractionModels = @[].mutableCopy;
    self.routes ? [extractionModels addObject:self.routes] : nil;
    self.quotes ? [extractionModels addObject:self.quotes] : nil;
    [extractionModels enumerateObjectsUsingBlock:^(NSArray *models, NSUInteger idx, BOOL * _Nonnull stop) {
        
        for (SKBaseModel *model in models) {
            [model extractModels:baseModel];
        }
    }];
}

@end
