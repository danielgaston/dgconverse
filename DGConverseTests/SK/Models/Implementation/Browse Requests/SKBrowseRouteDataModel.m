//
//  SKBrowseRouteDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseRouteDataModel.h"

@implementation SKBrowseRouteDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"originId" : @"OriginId",
		@"destinationId" : @"DestinationId",
		@"quoteIds" : @"QuoteIds",
		@"quoteDateTime" : @"QuoteDateTime"
	}];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractOriginWithBaseModel:baseModel];
    [self extractDestinationWithBaseModel:baseModel];
    [self extractQuotesWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractOriginWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedOrigin) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowsePlacesDelegate)]) {
            NSArray<SKBrowsePlaceDataModel*> *places = ((SKBaseModel<SKBrowsePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKBrowsePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.placeId == self.originId) {
                    self.extractedOrigin = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedOrigin extractModels:baseModel];
}

- (void)extractDestinationWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedDestination) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowsePlacesDelegate)]) {
            NSArray<SKBrowsePlaceDataModel*> *places = ((SKBaseModel<SKBrowsePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKBrowsePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.placeId == self.destinationId) {
                    self.extractedDestination = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedDestination extractModels:baseModel];
}

- (void)extractQuotesWithBaseModel:baseModel
{
    if (!_extractedQuotes) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowseQuotesDelegate)]) {
            NSArray<SKBrowseQuoteDataModel*> *quotes = ((SKBaseModel<SKBrowseQuotesDelegate> *)baseModel).quotes;
            
            __block NSMutableSet *extractedQuotes = @[].mutableCopy;
            for (NSNumber *quoteIdNum in self.quoteIds){
                
                __block SKBrowseQuoteDataModel *quote;
                [quotes enumerateObjectsUsingBlock:^(SKBrowseQuoteDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.quoteId == quoteIdNum.integerValue) {
                        quote = obj;
                        [extractedQuotes addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [quote extractModels:baseModel];
            }
            _extractedQuotes = extractedQuotes;
        }
    }
}

@end
