//
//  SKBrowseDateDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseDateDataModel.h"

@implementation SKBrowseInnerDateDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"partialDate" : @"PartialDate",
                                                                  @"quoteIds" : @"QuoteIds",
                                                                  @"price" : @"Price",
                                                                  @"quoteDateTime" : @"QuoteDateTime" }];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractQuotesWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractQuotesWithBaseModel:baseModel
{
    if (!_extractedQuotes) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowseQuotesDelegate)]) {
            NSArray<SKBrowseQuoteDataModel*> *quotes = ((SKBaseModel<SKBrowseQuotesDelegate> *)baseModel).quotes;
            
            __block NSMutableSet *extractedQuotes = [NSMutableSet new];
            for (NSNumber *quoteIdNum in self.quoteIds){
                
                __block SKBrowseQuoteDataModel *quote;
                [quotes enumerateObjectsUsingBlock:^(SKBrowseQuoteDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.quoteId == quoteIdNum.integerValue) {
                        quote = obj;
                        [extractedQuotes addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [quote extractModels:baseModel];
            }
            _extractedQuotes = extractedQuotes;
        }
    }
}

@end


@implementation SKBrowseDateDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"outboundDates" : @"OutboundDates",
		@"inboundDates" : @"InboundDates" }];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    NSMutableArray<NSArray*> *extractionModels = @[].mutableCopy;
    self.outboundDates ? [extractionModels addObject:self.outboundDates] : nil;
    self.inboundDates ? [extractionModels addObject:self.inboundDates] : nil;
    [extractionModels enumerateObjectsUsingBlock:^(NSArray *models, NSUInteger idx, BOOL * _Nonnull stop) {
        
        for (SKBaseModel *model in models) {
            [model extractModels:baseModel];
        }
    }];
    
}


@end
