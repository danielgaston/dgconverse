//
//  SKBrowseRoutesDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKBrowseRouteDataModel.h"
#import "SKBrowseQuoteDataModel.h"
#import "SKBrowsePlaceDataModel.h"
#import "SKBrowseCarrierDataModel.h"
#import "SKCurrencyDataModel.h"

@protocol SKBrowseRoutesDataModel;

@interface SKBrowseRoutesDataModel : SKBaseModel <SKBrowseQuotesDelegate, SKBrowsePlacesDelegate, SKBrowseCarriersDelegate>

@property (nonatomic, strong) NSArray<Optional, SKBrowseRouteDataModel>* routes;
@property (nonatomic, strong) NSArray<Optional, SKBrowseQuoteDataModel>* quotes;
@property (nonatomic, strong) NSArray<Optional, SKBrowsePlaceDataModel>* places;
@property (nonatomic, strong) NSArray<Optional, SKBrowseCarrierDataModel>* carriers;
@property (nonatomic, strong) NSArray<Optional, SKCurrencyDataModel>* currencies;

@end

/*
 {
 "Routes": [
 {
 "OriginId": 1811,
 "DestinationId": 1845,
 "QuoteIds": [
 1,
 2
 ],
 "Price": 326,
 "QuoteDateTime": "2016-11-13T01:30:00"
 },
 {
 "OriginId": 1811,
 "DestinationId": 929,
 "QuoteIds": [
 3
 ],
 "Price": 150,
 "QuoteDateTime": "2016-11-09T17:44:00"
 },
 ...
 ],
 "Quotes": [
 {
 "QuoteId": 1,
 "MinPrice": 381,
 "Direct": true,
 "OutboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 },
 "InboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 42833,
 "DestinationId": 68033,
 "DepartureDate": "2017-02-06T00:00:00"
 },
 "QuoteDateTime": "2016-11-09T21:20:00"
 },
 ...
 ],
 "Places": [
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 },
 ...
 ],
 "Carriers": [
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 },
 {
 "CarrierId": 173,
 "Name": "Silver Airways"
 },
 ...
 ],
 "Currencies": [
 {
 "Code": "EUR",
 "Symbol": "€",
 "ThousandsSeparator": " ",
 "DecimalSeparator": ",",
 "SymbolOnLeft": false,
 "SpaceBetweenAmountAndSymbol": true,
 "RoundingCoefficient": 0,
 "DecimalDigits": 2
 }
 ]
 }
 */

