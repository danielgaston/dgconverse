//
//  SKBrowsePlaceDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowsePlaceDataModel.h"

@implementation SKBrowsePlaceDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"placeId" : @"PlaceId",
		@"name" : @"Name",
		@"type" : @"Type",
		@"skyscannerCode" : @"SkyscannerCode" }];
}

@end
