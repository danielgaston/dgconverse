//
//  SKBrowsePlaceDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKBrowsePlaceDataModel;

@interface SKBrowsePlaceDataModel : SKBaseModel

@property (nonatomic, assign) NSInteger placeId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *skyscannerCode;

@end

/*
 {
 "PlaceId": 837,
 "Name": "United Arab Emirates",
 "Type": "Country",
 "SkyscannerCode": "AE"
 }
 */
