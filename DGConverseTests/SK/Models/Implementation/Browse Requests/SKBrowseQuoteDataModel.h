//
//  SKBrowseQuoteDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKBrowseLegDataModel.h"

@protocol SKBrowseQuoteDataModel;

@interface SKBrowseQuoteDataModel : SKBaseModel

@property (nonatomic, assign) NSInteger quoteId;
@property (nonatomic, assign) NSInteger minPrice;
@property (nonatomic, assign) BOOL direct;
@property (nonatomic, strong) SKBrowseLegDataModel <Optional> *outboundLeg;
@property (nonatomic, strong) SKBrowseLegDataModel <Optional> *inboundLeg;
@property (nonatomic, strong) NSDate *quoteDateTime;

@end

/*
 {
 "QuoteId": 1,
 "MinPrice": 381,
 "Direct": true,
 "OutboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 },
 "InboundLeg": {
 "CarrierIds": [
 470
 ],
 "OriginId": 42833,
 "DestinationId": 68033,
 "DepartureDate": "2017-02-06T00:00:00"
 },
 "QuoteDateTime": "2016-11-09T21:20:00"
 }
 */

