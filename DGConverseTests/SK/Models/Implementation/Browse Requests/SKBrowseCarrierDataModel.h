//
//  SKBrowseCarrierDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKBrowseCarrierDataModel;

@interface SKBrowseCarrierDataModel : SKBaseModel

@property (nonatomic, assign) NSInteger carrierId;
@property (nonatomic, strong) NSString *name;

@end

/*
 {
 "CarrierId": 29,
 "Name": "Mombasa Air Safari"
 }
 */
