//
//  SKBrowseLegDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKBrowsePlaceDataModel.h"
#import "SKBrowseCarrierDataModel.h"

@protocol SKBrowseLegDataModel;

@interface SKBrowseLegDataModel : SKBaseModel

@property (nonatomic, strong) NSSet <NSNumber*> *carrierIds;
@property (nonatomic, assign) NSInteger originId;
@property (nonatomic, assign) NSInteger destinationId;
@property (nonatomic, strong) NSDate *departureDate;


@property (nonatomic, strong) SKBrowsePlaceDataModel<Optional> *extractedOrigin;
@property (nonatomic, strong) SKBrowsePlaceDataModel<Optional> *extractedDestination;
@property (nonatomic, strong) NSSet<SKBrowseCarrierDataModel *><Optional> *extractedCarriers;

@end

/*
 {
 "CarrierIds": [
 470
 ],
 "OriginId": 68033,
 "DestinationId": 42833,
 "DepartureDate": "2017-02-03T00:00:00"
 }
 */

