//
//  SKBrowseLegDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseLegDataModel.h"

@implementation SKBrowseLegDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"carrierIds" : @"CarrierIds",
                                                                   @"originId" : @"OriginId",
                                                                   @"destinationId" : @"DestinationId",
                                                                   @"departureDate" : @"DepartureDate"
                                                                   }];
}

- (void)extractModels:(SKBaseModel *)baseModel
{
    [self extractOriginWithBaseModel:baseModel];
    [self extractDestinationWithBaseModel:baseModel];
    [self extractCarriersWithBaseModel:baseModel];
}

#pragma mark - Helper Methods

- (void)extractOriginWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedOrigin) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowsePlacesDelegate)]) {
            NSArray<SKBrowsePlaceDataModel*> *places = ((SKBaseModel<SKBrowsePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKBrowsePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.placeId == self.originId) {
                    self.extractedOrigin = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedOrigin extractModels:baseModel];
}

- (void)extractDestinationWithBaseModel:(SKBaseModel *)baseModel
{
    if (!_extractedDestination) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowsePlacesDelegate)]) {
            NSArray<SKBrowsePlaceDataModel*> *places = ((SKBaseModel<SKBrowsePlacesDelegate> *)baseModel).places;
            
            [places enumerateObjectsUsingBlock:^(SKBrowsePlaceDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.placeId == self.destinationId) {
                    self.extractedDestination = obj;
                    *stop = YES;
                }
            }];
        }
    }
    
    [_extractedDestination extractModels:baseModel];
}

- (void)extractCarriersWithBaseModel:baseModel
{
    if (!_extractedCarriers) {
        if (baseModel && [baseModel conformsToProtocol:@protocol(SKBrowseCarriersDelegate)]) {
            NSArray<SKBrowseCarrierDataModel*> *carriers = ((SKBaseModel<SKBrowseCarriersDelegate> *)baseModel).carriers;
            
            __block NSMutableSet *extractedCarriers = [NSMutableSet new];
            for (NSNumber *carrierIdNum in self.carrierIds){
                
                __block SKBrowseCarrierDataModel *carrier;
                [carriers enumerateObjectsUsingBlock:^(SKBrowseCarrierDataModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.carrierId == carrierIdNum.integerValue) {
                        carrier = obj;
                        [extractedCarriers addObject:obj];
                        *stop = YES;
                    }
                }];
                
                [carrier extractModels:baseModel];
            }
            _extractedCarriers = extractedCarriers;
        }
    }
}

@end

