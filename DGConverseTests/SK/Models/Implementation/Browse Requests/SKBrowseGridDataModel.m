//
//  SKBrowseGridDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseGridDataModel.h"

@implementation SKBrowseGridDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"dates" : @"Dates",
                                                                   @"places" : @"Places",
                                                                   @"carriers" : @"Carriers",
                                                                   @"currencies" : @"Currencies"
                                                                   }];
}

//- (void)extractModels:(SKBaseModel *)baseModel
//{
//    if(self.dates)
//        [self.dates extractModels:baseModel];
//
//    NSMutableArray<NSArray*> *extractionModels = @[].mutableCopy;
//    self.quotes ? [extractionModels addObject:self.quotes] : nil;
//    [extractionModels enumerateObjectsUsingBlock:^(NSArray *models, NSUInteger idx, BOOL * _Nonnull stop) {
//
//        for (SKBaseModel *model in models) {
//            [model extractModels:baseModel];
//        }
//    }];
//}

@end

