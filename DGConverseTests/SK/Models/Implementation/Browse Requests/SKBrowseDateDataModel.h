//
//  SKBrowseDateDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKBrowseLegDataModel.h"
#import "SKBrowseQuoteDataModel.h"

@protocol SKBrowseInnerDateDataModel;

@interface SKBrowseInnerDateDataModel : SKBaseModel

@property (nonatomic, strong) NSString<Optional>*partialDate;
@property (nonatomic, strong) NSSet<NSNumber *><Optional>*quoteIds;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, strong) NSDate <Optional>*quoteDateTime;

@property (nonatomic, strong) NSSet<SKBrowseQuoteDataModel *><Optional> *extractedQuotes;

@end


@protocol SKBrowseDateDataModel;

@interface SKBrowseDateDataModel : SKBaseModel

@property (nonatomic, strong) NSArray<SKBrowseInnerDateDataModel, Optional> *outboundDates;
@property (nonatomic, strong) NSArray<SKBrowseInnerDateDataModel, Optional> *inboundDates;

@end

/*
 {
 "OutboundDates": [
 {
 "PartialDate": "2016-11",
 "QuoteIds": [
 1,
 2,
 3,
 4,
 5
 ],
 "Price": 66,
 "QuoteDateTime": "2016-11-08T17:28:00"
 },
 ...
 ],
 "InboundDates": [
 {
 "PartialDate": "2016-11",
 "QuoteIds": [
 1
 ],
 "Price": 93,
 "QuoteDateTime": "2016-11-21T17:19:00"
 },
 ...
 ]
 }
 */
