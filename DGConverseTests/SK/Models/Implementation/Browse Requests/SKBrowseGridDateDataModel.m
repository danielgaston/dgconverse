//
//  SKBrowseGridDateDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseGridDateDataModel.h"

@implementation SKBrowseGridDateDataModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"date" : @"DateString"}];
}

@end
