//
//  SKBrowseCarrierDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBrowseCarrierDataModel.h"

@implementation SKBrowseCarrierDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"carrierId" : @"CarrierId",
		@"name" : @"Name" }];
}

@end
