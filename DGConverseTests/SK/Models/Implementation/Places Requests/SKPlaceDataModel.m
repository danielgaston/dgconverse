//
//  SKPlaceDataModel.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKPlaceDataModel.h"

@implementation SKPlaceDataModel

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"placeId" : @"PlaceId",
		@"placeName" : @"PlaceName",
		@"regionId" : @"RegionId",
		@"cityId" : @"CityId",
		@"countryId" : @"CountryId",
		@"countryName" : @"CountryName",
	}];
}

@end
