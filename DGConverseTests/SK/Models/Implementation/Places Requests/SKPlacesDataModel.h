//
//  SKPlacesDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"
#import "SKPlaceDataModel.h"

@protocol SKPlacesDataModel;

@interface SKPlacesDataModel : SKBaseModel

@property (nonatomic, strong) NSArray<SKPlaceDataModel> *places;

@end

/*
 
 {
 "Places": [
 {
 "PlaceId": "PARI-sky",
 "PlaceName": "Paris",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 },
 {
 "PlaceId": "CDG-sky",
 "PlaceName": "Paris Charles de Gaulle",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 },
 {
 "PlaceId": "ORY-sky",
 "PlaceName": "Paris Orly",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 },
 ...
 ]
 }
 */
