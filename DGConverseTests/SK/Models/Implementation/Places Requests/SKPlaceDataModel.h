//
//  SKPlaceDataModel.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "SKBaseModel.h"

@protocol SKPlaceDataModel;

@interface SKPlaceDataModel : SKBaseModel

@property (nonatomic, strong) NSString <Optional> *placeId;
@property (nonatomic, strong) NSString <Optional> *placeName;
@property (nonatomic, strong) NSString <Optional> *regionId;
@property (nonatomic, strong) NSString <Optional> *cityId;
@property (nonatomic, strong) NSString <Optional> *countryId;
@property (nonatomic, strong) NSString <Optional> *countryName;

@end

/*
 {
 "PlaceId": "PARI-sky",
 "PlaceName": "Paris",
 "CountryId": "FR-sky",
 "RegionId": "",
 "CityId": "PARI-sky",
 "CountryName": "France"
 }
 */
