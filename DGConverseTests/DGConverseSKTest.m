//
//  DGConverseSKTest.m
//  DGConverseTests
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DGConverseAPIOperationManager+SK.h"
#import "NSDate+Additions.h"

@interface DGConverseSKTest : XCTestCase

@property (nonatomic, strong) DGConverseAPIOperationManager *testManager;
@property (nonatomic, strong) id<DGConverseAPIGroupBaseOperation> groupOp;

@end

static NSString *placeIdKey;
static NSString *localeCodeKey;
static NSString *currencyCodeKey;
static NSString *locationKey;
static NSString *sessionKey;
static SKBookingDetailsLinkDataModel *bookingDetails;
static NSString *outboundLegIdKey;
static NSString *inboundLegIdKey;

@implementation DGConverseSKTest

+ (void)setUp
{
    [super setUp];
    
    DGConverseLogStart();
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.testManager = [DGConverseAPIOperationManager new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_SK_PlacesStep1_AllPlaces
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_PlacesStep1_AllPlaces"];
    
    self.groupOp = [self.testManager getPlacesWithCountry:@"AR" currency:@"EUR" locale:@"ES_ar" query:@"bueno" completion:^(SKPlacesResult *result) {
        
        XCTAssertNil(result.error, "ERROR ");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        SKPlaceDataModel *placeDataModel = [result.responseObj.places firstObject];
        placeIdKey = placeDataModel.placeId;
        XCTAssertNotNil(placeIdKey, "Locale Code not obtained");
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}


- (void)test_SK_PlacesStep2_SinglePlace
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_PlacesStep2_SinglePlace"];
    
    self.groupOp = [self.testManager getPlaceWithCountry:@"AR" currency:@"EUR" locale:@"ES_ar" placeId:placeIdKey completion:^(SKPlacesResult *result) {
        
        XCTAssertNil(result.error, "ERROR ");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}



- (void)test_SK_LocalizationStep1_Locales
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LocalizationStep1_Locales"];
    
    self.groupOp = [self.testManager getLocalesWithCompletion:^(SKLocalesResult *result) {
        
        XCTAssertNil(result.error, "ERROR ");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        SKLocaleDataModel *localeDataModel = [result.responseObj.locales firstObject];
        localeCodeKey = localeDataModel.code;
        XCTAssertNotNil(localeCodeKey, "Locale Code not obtained");
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_LocalizationStep2_Currencies
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LocalizationStep2_Currencies"];
    
    self.groupOp = [self.testManager getCurrenciesWithCompletion:^(SKCurrenciesResult *result) {
        
        XCTAssertNil(result.error, "ERROR ");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        SKCurrencyDataModel *currencyDataModel = [result.responseObj.currencies firstObject];
        currencyCodeKey = currencyDataModel.code;
        XCTAssertNotNil(localeCodeKey, "Currency Code not obtained");
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_LocalizationStep3_Markets
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LocalizationStep3_Markets"];
    
    self.groupOp = [self.testManager getMarketsWithLocale:localeCodeKey completion:^(SKMarketsResult *result) {
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        [expectation fulfill];
        
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_LiveStep1_LiveCreateSession
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LiveStep1_LiveCreateSession"];
    
    SKLiveCreateSessionRequestParameters *params = [SKLiveCreateSessionRequestParameters new];
    [params setCabinClass:@"Economy"];
    [params setCountry:@"UK"];
    [params setCurrency:@"GBP"];
    [params setLocale:@"en-GB"];
    [params setLocationSchema:@"iata"];
    [params setOriginPlace:@"EDI"];
    [params setDestinationPlace:@"LHR"];
    [params setOutboundDate:[NSDate dateByAddingDays:30]];
    [params setInboundDate:[NSDate dateByAddingDays:40]];
    
    self.groupOp = [self.testManager createLiveSessionWithParams:params progress:nil completion:^(SKLiveCreateSessionResult *result) {
        XCTAssertNil(result.error, "ERROR ");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        locationKey = result.responseObj.location;
        sessionKey = result.responseObj.sessionKey;
        XCTAssertNotNil(locationKey, "Location Key not obtained");
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_LiveStep2_LivePollSession
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LiveStep2_LivePollSession"];
    
    SKLivePollSessionRequestParameters *params = [SKLivePollSessionRequestParameters new];
    
    if (locationKey) {
        self.groupOp = [self.testManager pollLiveSessionWithLocation:locationKey params:params progress:nil completion:^(SKLivePollSessionResult *result) {
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            
            if (result.error || !result.data)
                [expectation fulfill];
            
            if ([result.responseObj isCompleted]){
                
                SKLiveItineraryDataModel *itinerary = [result.responseObj.itineraries firstObject];
                outboundLegIdKey = itinerary.outboundLegId;
                inboundLegIdKey = itinerary.inboundLegId;
                bookingDetails = itinerary.bookingDetailsLink;
                
                XCTAssertNotNil(outboundLegIdKey, "outboundLegId should not be nil");
                XCTAssertNotNil(inboundLegIdKey, "inboundLegId should not be nil");
                XCTAssertNotNil(bookingDetails, "bookingDetails should not be nil");
                [expectation fulfill];
            }
        }];
        
        [self waitForExpectationsWithTimeout:90 handler:^(NSError *error) {
            if (error != nil)
                NSLog(@"Error: %@", error.localizedDescription);
            
            [self.groupOp cancel];
        }];
        
    } else {
        XCTAssertTrue(false, "'locationKey' coming from 'LiveCreateSession' cannot be nil. Execute first 'LiveCreateSession' request.");
        [expectation fulfill];
    }
}

- (void)test_SK_LiveStep3_LiveCreateBookingDetails
{
    XCTAssertNotNil(bookingDetails, "bookingDetails should not be nil");
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LiveStep3_LiveCreateBookingDetails"];
    
    if (bookingDetails.uri) {
        self.groupOp = [self.testManager createLiveBookingDetailsWithURI:bookingDetails.uri body:bookingDetails.body progress:nil completion:^(SKLiveCreateBookingDetailsResult *result) {
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            
            [expectation fulfill];
            
        }];
        
        [self waitForExpectationsWithTimeout:90 handler:^(NSError *error) {
            if (error != nil)
                NSLog(@"Error: %@", error.localizedDescription);
            
            [self.groupOp cancel];
        }];
    } else {
        XCTAssertTrue(false, "'bookingDetails.uri' coming from 'LivePollSession' cannot be nil. Execute first 'LivePollSession' request.");
        [expectation fulfill];
    }
}

- (void)test_SK_LiveStep4_LivePollBookingDetails
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"SK_LiveStep4_LivePollBookingDetails"];
    
    SKLivePollBookingDetailsRequestParameters *params = [SKLivePollBookingDetailsRequestParameters new];
    
    if (sessionKey) {
        self.groupOp = [self.testManager pollLiveBookingDetailsWithKey:sessionKey outboundLegId:outboundLegIdKey inboundLegId:inboundLegIdKey params:params progress:nil completion:^(SKLivePollBookingDetailsResult *result) {
            
            XCTAssertNil(result.error, "error should be nil");
            XCTAssertNotNil(result.data, "data should not be nil");
            XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
            
            if (result.error || !result.data || !result.responseObj)
                [expectation fulfill];
            
            if ([result.responseObj isCompleted]){
                [expectation fulfill];
            }
        }];
        
        [self waitForExpectationsWithTimeout:90 handler:^(NSError *error) {
            if (error != nil)
                NSLog(@"Error: %@", error.localizedDescription);
            
            [self.groupOp cancel];
        }];
    
    } else {
        XCTAssertTrue(false, "'sessionKey' coming from 'LiveCreateSession' cannot be nil. Execute first 'LiveCreateSession' request.");
        [expectation fulfill];
    }
}

- (void)test_SK_BrowseStep1_BrowseQuotes
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_SK_BrowseStep1_BrowseQuotes"];
    
    self.groupOp = [self.testManager browseQuoteWithOriginPlace:@"VIE-sky" destinationPlace:@"BCN-sky" departDate:[self departDateString] returnDate:[self returnDateString] completion:^(SKBrowseQuotesResult *result) {
        
        [result.responseObj extractModels:result.responseObj];
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_BrowseStep2_BrowseRoutes
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_SK_BrowseStep2_BrowseRoutes"];
    
    self.groupOp = [self.testManager browseRoutesWithOriginPlace:@"VIE-sky" destinationPlace:@"BCN-sky" departDate:[self departDateString] returnDate:[self returnDateString] completion:^(SKBrowseRoutesResult *result) {
        
        [result.responseObj extractModels:result.responseObj];
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_BrowseStep3_BrowseDates
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_SK_BrowseStep3_BrowseDates"];
    
    self.groupOp = [self.testManager browseDatesWithOriginPlace:@"VIE-sky" destinationPlace:@"BCN-sky" departDate:[self departDateString] returnDate:[self returnDateString] completion:^(SKBrowseDatesResult *result) {
        
        [result.responseObj extractModels:result.responseObj];
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_SK_BrowseStep4_BrowseGrid
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_SK_BrowseStep4_BrowseGrid"];
    
    self.groupOp = [self.testManager browseGridWithOriginPlace:@"VIE-sky" destinationPlace:@"BCN-sky" departDate:[self departDateString] returnDate:[self returnDateString] completion:^(SKBrowseGridResult *result) {
        
        [result.responseObj extractModels:result.responseObj];
        
        XCTAssertNil(result.error, "error should be nil");
        XCTAssertNotNil(result.data, "data should not be nil");
        XCTAssertNotNil(result.responseObj, "responseObj should not be nil");
        
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:30 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark - Helper Methods

- (NSString *)departDateString
{
    return [[NSDate dateByAddingDays:30] stringWithFormat:@"yyyy-MM-dd"];
}

- (NSString *)returnDateString
{
    return [[NSDate dateByAddingDays:40] stringWithFormat:@"yyyy-MM-dd"];
}


@end


