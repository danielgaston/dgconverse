//
//  DGConverse_ExampleUITests.m
//  DGConverse_ExampleUITests
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MasterViewController.h"

@import DGConverse;

static DGConverseAPIOperationManager *testManager = nil;

@interface DGConverse_ExampleUITests : XCTestCase

@property (nonatomic, strong) id<DGConverseAPIGroupBaseOperation> groupOp;

@end

@implementation DGConverse_ExampleUITests

+ (void)setUp
{
    [super setUp];
    
    DGConverseLogStart();
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    if (testManager == nil)
        testManager = [DGConverseAPIOperationManager new];
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"************************** START ******************************");
    NSLog(@"---------------------------------------------------------------");
}

- (void)tearDown
{
    NSLog(@"---------------------------------------------------------------");
    NSLog(@"*************************** END *******************************");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// Triggers alertController in NetworkBaseOperation (not fully implemented)
// Have a look at https://jigsaw.w3.org/HTTP/
// to access it, the user is "guest" and the password is also "guest".
- (void)test_1_dataHTTPBasic
{
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [self tapInCellWithId:kAccessibilityId_HttpBasic_Cell];
    sleep(5.0);
    
    XCUIElement *authenticationAlert = app.alerts[@"Authentication"];
    [authenticationAlert.staticTexts[@"Please enter credentials"] tap];
    
    XCUIElementQuery *collectionViewsQuery = authenticationAlert.collectionViews;
    XCUIElement *userTextField = collectionViewsQuery.textFields[@"user"];
    XCUIElement *passwordTextField = collectionViewsQuery.secureTextFields[@"password"];
    XCUIElement *submitButton = app.buttons[@"Submit"];
    XCUIElement *cancelButton = app.buttons[@"Cancel"];

    XCTAssertEqual(userTextField.hittable, true);
    XCTAssertEqual(passwordTextField.hittable, true);
    XCTAssertEqual(submitButton.hittable, true);
    XCTAssertEqual(cancelButton.hittable, true);
    
    [userTextField tap];
    [userTextField typeText:@"guest"];
    
    [passwordTextField tap];
    [passwordTextField typeText:@"guest"];
    
    [submitButton tap];
}

- (void)test_2_dataHTTPBasic_withDefaultProposedCredential
{
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [self tapInCellWithId:kAccessibilityId_HttpBasicWithCredential_Cell];
    sleep(5.0);
    
    // UIAlertController should not appear as we are injecting a default working credential
    XCUIElement *authenticationAlert = app.alerts[@"Authentication"];
    XCTAssertEqual(authenticationAlert.exists, false);
}

- (void)test_3_downloadFileInBg_sendAppToBg
{
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [self tapInCellWithId:kAccessibilityId_DownloadFileInBg_Cell];
    sleep(5.0);
    
    [[XCUIDevice sharedDevice] pressButton:XCUIDeviceButtonHome];
    sleep(15.0);
    [app activate];

    sleep(5.0);
    XCTAssert(true);
}

#pragma mark - Helper Methods

- (void)tapInCellWithId:(NSString *)identifier
{
    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElementQuery *masterTable = [app.tables matchingIdentifier:kAccessibilityId_Master_Table];
    XCUIElement *cell = [masterTable.cells elementMatchingType:XCUIElementTypeCell identifier:identifier];
    [cell tap];
}

@end
