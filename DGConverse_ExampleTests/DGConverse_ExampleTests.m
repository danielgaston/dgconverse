//
//  DGConverse_ExampleTests.m
//  DGConverse_ExampleTests
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <XCTest/XCTest.h>

@import DGConverse;

static DGConverseAPIOperationManager *testManager = nil;

@interface DGConverse_ExampleTests : XCTestCase

@property (nonatomic, strong) id<DGConverseAPIGroupBaseOperation> groupOp;

@end

@implementation DGConverse_ExampleTests

+ (void)setUp
{
    [super setUp];
    
    DGConverseLogStart();
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (testManager == nil)
        testManager = [DGConverseAPIOperationManager new];
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"************************** START ******************************");
    NSLog(@"---------------------------------------------------------------");
}

- (void)tearDown
{
    NSLog(@"---------------------------------------------------------------");
    NSLog(@"*************************** END *******************************");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    NSLog(@"\n");
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark- URL TESTS

#pragma mark-- Authorization

- (void)test_authorization_sharedCredentialStorage_defaultCredential
{
    // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use NSURLCredentialPersistencePermanent)
    NSURLCredential *credential = [NSURLCredential credentialWithUser:@"guest" password:@"guest" persistence:NSURLCredentialPersistenceForSession];
    // 1. Create Protection Space for Server
    NSURLProtectionSpace *protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:@"jigsaw.w3.org" port:443 protocol:@"https" realm:@"test" authenticationMethod:NSURLAuthenticationMethodHTTPBasic];
    // 2. Set DEFAULT Credential for Protection Space for the system shared credential storage
    [[NSURLCredentialStorage sharedCredentialStorage] setDefaultCredential:credential forProtectionSpace:protectionSpace];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_authorization_sharedCredentialStorage_defaultCredential"];
    
    // 3. HTTP Basic authorization request
    self.groupOp = [testManager data:@"https://jigsaw.w3.org/HTTP/Basic/"
                            progress:nil
                          completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                              XCTAssertNil(result.error, "error should be nil");
                              XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                              [expectation fulfill];
                          }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_authorization_sharedCredentialStorage_genericCredential
{
    // 0. Create Credential (Defined for session, if wanted to be stored permanently in keychain use NSURLCredentialPersistencePermanent)
    NSURLCredential *credential = [NSURLCredential credentialWithUser:@"guest" password:@"guest" persistence:NSURLCredentialPersistenceForSession];
    // 1. Create Protection Space for Server
    NSURLProtectionSpace *protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:@"jigsaw.w3.org" port:443 protocol:@"https" realm:@"test" authenticationMethod:NSURLAuthenticationMethodHTTPBasic];
    // 2. Set GENERIC Credential for Protection Space for the system shared credential storage
    [[NSURLCredentialStorage sharedCredentialStorage] setCredential:credential forProtectionSpace:protectionSpace];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_authorization_sharedCredentialStorage_genericCredential"];
    
    // 3. HTTP Basic authorization request
    self.groupOp = [testManager data:@"https://jigsaw.w3.org/HTTP/Basic/"
                            progress:nil
                          completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                              XCTAssertNil(result.error, "error should be nil");
                              XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                              [expectation fulfill];
                          }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_authorization_sharedCredentialStorage_defaultAndGenericCredential
{
    // 0. Create Credentials (Defined for session, if wanted to be stored permanently in keychain use NSURLCredentialPersistencePermanent)
    NSURLCredential *defaultWorkingCredential = [NSURLCredential credentialWithUser:@"guest" password:@"guest" persistence:NSURLCredentialPersistenceForSession];
    NSURLCredential *genericNotWorkingCredential = [NSURLCredential credentialWithUser:@"wrongUser" password:@"wrongPassword" persistence:NSURLCredentialPersistenceForSession];
    // 1. Create Protection Space for Server
    NSURLProtectionSpace *protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:@"jigsaw.w3.org" port:443 protocol:@"https" realm:@"test" authenticationMethod:NSURLAuthenticationMethodHTTPBasic];
    // 2. Set DEFAULT and GENERIC Credential for Protection Space for the system shared credential storage
    [[NSURLCredentialStorage sharedCredentialStorage] setDefaultCredential:defaultWorkingCredential forProtectionSpace:protectionSpace];
    [[NSURLCredentialStorage sharedCredentialStorage] setCredential:genericNotWorkingCredential forProtectionSpace:protectionSpace];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_authorization_sharedCredentialStorage_defaultAndGenericCredential"];
    
    // 4. HTTP Basic authorization request
    self.groupOp = [testManager data:@"https://jigsaw.w3.org/HTTP/Basic/"
                            progress:nil
                          completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                              XCTAssertNil(result.error, "error should be nil");
                              XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                              [expectation fulfill];
                          }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

//- (void)test_authorization_customCredentialStorage_defaultCredential_UNFINISHED
//{
//    // 0. Create Credential (Defined for session, if wanted to be stored permanently  in keychain use NSURLCredentialPersistencePermanent)
//    NSURLCredential *credential = [NSURLCredential credentialWithUser:@"guest" password:@"guest" persistence:NSURLCredentialPersistenceForSession];
//    // 1. Create Protection Space for Server
//    NSURLProtectionSpace *protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:@"jigsaw.w3.org" port:443 protocol:@"https" realm:@"test" authenticationMethod:NSURLAuthenticationMethodHTTPBasic];
//    // 2. Create Credential Storage
//    NSURLCredentialStorage *customCredentialStorage = [NSURLCredentialStorage new];
//    // 3. Set Default Credential for Protection Space
//    [customCredentialStorage setDefaultCredential:credential forProtectionSpace:protectionSpace];
//    [customCredentialStorage setCredential:credential forProtectionSpace:protectionSpace];
//    // 4. Create Session Configuration
//    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    // 5. Set Credential Storage for session
//    sessionConfiguration.URLCredentialStorage = customCredentialStorage;
//    // 6. Initialize Manager with Session Configuration
//    testManager = [[DGConverseAPIOperationManager alloc] initWithDefaultConfiguration:sessionConfiguration delegate:nil];
//
//    XCTestExpectation *expectation = [self expectationWithDescription:@"test_authorization_customCredentialStorage_defaultCredential"];
//
//    // 7. HTTP Basic authorization request
//    self.groupOp = [testManager data:@"https://jigsaw.w3.org/HTTP/Basic/"
//                            progress:nil
//                          completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
//                              XCTAssertNil(result.error, "error should be nil");
//                              XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
//                              [expectation fulfill];
//                          }];
//
//    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
//        if (error != nil)
//            NSLog(@"Error: %@", error.localizedDescription);
//
//        [self.groupOp cancel];
//    }];
//}


#pragma mark-- Bg Download

- (void)test_downloadFileInBackground
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFileInBackground"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                inBackground:YES
                           toDocumentsFolder:NO
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNil(result.error, "error should be nil");
                                      XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadFileInBackground_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFileInBackground_withFailure"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg"
                                inBackground:YES
                           toDocumentsFolder:NO
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadFileToDocuments
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFileToDocuments"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                inBackground:NO
                           toDocumentsFolder:YES
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNil(result.error, "error should be nil");
                                      XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadFileToDocuments_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFileToDocuments_withFailure"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg"
                                inBackground:NO
                           toDocumentsFolder:YES
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadFileToDocumentsInBackground
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFileToDocumentsInBackground"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://www.peoplelikeus.org/piccies/codpaste/codpaste-teachingpack.pdf"
                                inBackground:YES
                           toDocumentsFolder:YES
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNil(result.error, "error should be nil");
                                      XCTAssertNotNil(result.responseObj, "file location URL should not be nil");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_downloadFileToDocumentsInBackground_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_downloadFileToDocumentsInBackground_withFailure"];
    // ~30MB
    self.groupOp = [testManager downloadFile:@"http://ptsv2.com/t/converse/NON_EXISTING_FILE.jpg"
                                inBackground:YES
                           toDocumentsFolder:YES
                                    progress:nil
                                  completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                      XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                      XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                      
                                      [expectation fulfill];
                                  }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- Bg Upload

- (void)test_uploadFileInBackground
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_uploadFile_withFailure"];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    
    self.groupOp = [testManager uploadFile:url.absoluteString
                                        to:@"http://ptsv2.com/t/converse/post"
                              inBackground:YES
                                  progress:nil
                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                    XCTAssertNil(result.error, "error should be nil");
                                    XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                    
                                    [expectation fulfill];
                                }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

- (void)test_uploadFileInBackground_withFailure
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"test_uploadFile_withFailure"];
    
    NSURL *url = [[NSBundle bundleForClass:[self class]] URLForResource:@"dgconverse_test" withExtension:@"txt"];
    
    self.groupOp = [testManager uploadFile:url.absoluteString
                                        to:@"http://ptsv2.com/t/NON_EXISTING_CONVERSE_FOLDER/post" // 404
                              inBackground:YES
                                  progress:nil
                                completion:^(DGConverseNetworkOperationResult * _Nonnull result) {
                                    XCTAssertNotNil(result.error, "error should not be nil. It is a FAILURE Test");
                                    XCTAssertTrue([self areOperationsDeallocated], @"operations not deallocated");
                                    
                                    [expectation fulfill];
                                }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError *error) {
        if (error != nil)
            NSLog(@"Error: %@", error.localizedDescription);
        
        [self.groupOp cancel];
    }];
}

#pragma mark-- Performance

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


#pragma mark - Helper Methods

- (BOOL)areOperationsDeallocated
{
    NSUInteger totalOpsInQueues = [testManager currentConcurrentOpsInQueues];
    //1: in completionBlock the last op is still running
    return (totalOpsInQueues <= 1);
}

@end
