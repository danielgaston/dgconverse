//
//  JSONValueTransformer+CustomDataTransformers.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "JSONValueTransformer+CustomDataTransformers.h"
#import "NSDate+Additions.h"

@implementation JSONValueTransformer (ISO8601DateTransformer)

- (NSDate*)NSDateFromNSString:(NSString*)string
{
	return [[NSISO8601DateFormatter new] dateFromString:string];
}

- (NSString*)JSONObjectFromNSDate:(NSDate*)date
{
	return [[NSISO8601DateFormatter new] stringFromDate:date];
}

@end
