//
//  DGConverseNetworkingDebugUtils.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIDebugUtils.h"

DGConverseLogMode DGConverseLogModeOption = kDGConverseLogDisabled;
DGConverseLogType DGConverseLogTypeOptions = kDGConverseLogTypeQueue | kDGConverseLogTypeCache | kDGConverseLogTypeParser | kDGConverseLogTypeOperations | kDGConverseLogTypeNetwork;
DGConverseLogFlag DGConverseLogFlagOptions = kDGConverseLogFlagInfo | kDGConverseLogFlagWarn | kDGConverseLogFlagError | kDGConverseLogFlagANY;
DGConverseLogLevel DGConverseLogDeepLevel = kDGConverseLogLevel3;

NSString* DGConverseLogTypeString(DGConverseLogType type)
{
	switch (type) {
	case kDGConverseLogTypeQueue:
		return @"Queue";
	case kDGConverseLogTypeCache:
		return @"Cache";
	case kDGConverseLogTypeOperations:
		return @"Operations";
	case kDGConverseLogTypeParser:
		return @"Parser";
	case kDGConverseLogTypeNetwork:
		return @"Network";
	default:
		return @"Unknown";
	}
}

NSString* DGConverseLogFlagString(DGConverseLogFlag flag)
{
    switch (flag) {
        case kDGConverseLogFlagInfo:
            return @"🔷 Info";
        case kDGConverseLogFlagWarn:
            return @"⚠️ Warn";
        case kDGConverseLogFlagError:
            return @"❌ Error";
        case kDGConverseLogFlagANY:
        default:
            return @"❔Unknown";
    }
}

NSString* DGConverseLogLevelString(DGConverseLogLevel level)
{
	switch (level) {
	case kDGConverseLogLevel0:
		return @"";
	case kDGConverseLogLevel1:
		return @"----- ";
	case kDGConverseLogLevel2:
		return @"----- ----- ";
	case kDGConverseLogLevel3:
		return @"----- ----- ----- ";
	default:
		return @"";
	}
}

void DGConverseLog(DGConverseLogType type, DGConverseLogFlag flag, DGConverseLogLevel level, NSString* logMessage)
{
	if (DGConverseLogModeOption == kDGConverseLogEnabled) {
		if ((DGConverseLogTypeOptions & type) == type && (DGConverseLogFlagOptions & flag) == flag && (DGConverseLogDeepLevel & level) == level) {
			NSLog(@"%@ -- %@ ··· %@%@", DGConverseLogTypeString(type), DGConverseLogFlagString(flag), DGConverseLogLevelString(level), logMessage);
		}
	}
}

void DGConverseLogStart(void)
{
    DGConverseLogModeOption = kDGConverseLogEnabled;
    DGConverseLogTypeOptions = kDGConverseLogTypeQueue | kDGConverseLogTypeCache | kDGConverseLogTypeParser | kDGConverseLogTypeOperations | kDGConverseLogTypeNetwork;
    DGConverseLogFlagOptions = kDGConverseLogFlagInfo | kDGConverseLogFlagWarn | kDGConverseLogFlagError | kDGConverseLogFlagANY;
    DGConverseLogDeepLevel = kDGConverseLogLevel3;
}

void DGConverseLogStop(void)
{
    DGConverseLogModeOption = kDGConverseLogDisabled;
}

