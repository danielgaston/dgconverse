//
//  DGConverseNetworkingDebugUtils.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

typedef NS_ENUM(NSUInteger, DGConverseLogMode) {
    kDGConverseLogDisabled,
    kDGConverseLogEnabled
};

typedef NS_OPTIONS(NSUInteger, DGConverseLogType) {
    kDGConverseLogTypeQueue     = (1 << 0),
    kDGConverseLogTypeCache     = (1 << 1),
    kDGConverseLogTypeOperations= (1 << 2),
    kDGConverseLogTypeParser    = (1 << 3),
    kDGConverseLogTypeNetwork   = (1 << 4)
};

typedef NS_OPTIONS(NSUInteger, DGConverseLogFlag) {
    kDGConverseLogFlagInfo     = (1 << 0),
    kDGConverseLogFlagWarn     = (1 << 1),
    kDGConverseLogFlagError    = (1 << 2),
    kDGConverseLogFlagANY      = (1 << 3)
};

typedef NS_ENUM(NSUInteger, DGConverseLogLevel) {
    kDGConverseLogLevel0,
    kDGConverseLogLevel1,
    kDGConverseLogLevel2,
    kDGConverseLogLevel3
};

#pragma mark - console debug flags
// open to be separately modified by any app
extern DGConverseLogMode DGConverseLogModeOption;
extern DGConverseLogType DGConverseLogTypeOptions;
extern DGConverseLogFlag DGConverseLogFlagOptions;
extern DGConverseLogLevel DGConverseLogDeepLevel;

#pragma mark - console debug methods
void DGConverseLog(DGConverseLogType type, DGConverseLogFlag flag, DGConverseLogLevel level, NSString *logMessage);

#pragma mark - console debug settings methods
void DGConverseLogStart(void);
void DGConverseLogStop(void);
