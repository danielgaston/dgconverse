//
//  DGConverseAPINetworkOperationClient.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkOperationClient.h"
#import "NSData+Base64.h"
#import "NSString+Additions.h"

@implementation NSURLSession (DGConverseAPINetworkOperationClient)

#pragma mark - DGConverseDefaultOperationClient Delegate Methods

- (NSURLSessionDataTask*)runSessionDataTaskWithRequest:(id<DGConverseAPIRequest>)request;
{
	NSURLSessionDataTask* task = [self operationDataTaskWithConverseAPIRequest:request];
	return task;
}

- (NSURLSessionDownloadTask*)runSessionDownloadTaskWithRequest:(id<DGConverseAPIRequest>)request
{
	NSURLSessionDownloadTask* task = [self operationDownloadTaskWithConverseAPIRequest:request];
	return task;
}

- (NSURLSessionUploadTask*)runSessionUploadTaskWithRequest:(id<DGConverseAPIRequest>)request fileURL:(NSURL*)fileURL;
{
	NSURLSessionUploadTask* task = [self operationUploadTaskWithConverseAPIRequest:request fileURL:fileURL];
	return task;
}

- (NSURLSessionUploadTask*)runSessionUploadTaskWithRequest:(id<DGConverseAPIRequest>)request bodyData:(NSData*)bodyData
{
	NSURLSessionUploadTask* task = [self operationUploadTaskWithConverseAPIRequest:request bodyData:bodyData];
	return task;
}

@end

#pragma mark -

@implementation DGConverseAPINetworkOperationClient

#pragma mark - Class Methods

+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)clientWithConfiguration:(NSURLSessionConfiguration*)config delegate:(id<NSURLSessionDelegate>)delegate
{
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:delegate delegateQueue:nil];
    session.sessionDescription = @"CONFIGURED URLSession";
    return session;
}

+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)defaultClientWithDelegate:(id<NSURLSessionDelegate>)delegate
{
	/*!
     * @brief Default Session:
     * @discussion:
         timeoutIntervalForRequest: 60s
         timeoutIntervalForResource: 7days
         networkServiceType: NSURLNetworkServiceTypeDefault
         allowsCellularAccess: YES
         HTTPCookieAcceptPolicy: NSHTTPCookieAcceptPolicyOnlyFromMainDocumentDomain
         HTTPShouldSetCookies: YES
         HTTPCookieStorage: [NSHTTPCookieStorage sharedHTTPCookieStorage]
         URLCredentialStorage: [URLCredentialStorage sharedCredentialStorage]
         URLCache: [NSURLCache sharedURLCache]
         requestCachePolicy: NSURLRequestUseProtocolCachePolicy
         configuration.HTTPMaximumConnectionsPerHost: 4
         HTTPShouldUsePipelining: NO
         connectionProxyDictionary: NULL
     */

	NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
	NSURLSession* session = [NSURLSession sessionWithConfiguration:configuration delegate:delegate delegateQueue:nil];
	session.sessionDescription = @"DEFAULT URLSession";
	return session;
}

+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)ephemeralClientWithDelegate:(id<NSURLSessionDelegate>)delegate;
{
	NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
	NSURLSession* session = [NSURLSession sessionWithConfiguration:configuration delegate:delegate delegateQueue:nil];
	session.sessionDescription = @"EPHEMERAL URLSession";
	return session;
}

#define kBackgroundOperationSessionID @"DGConverseBackgroundSession"

+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)backgroundClientWithDelegate:(id<NSURLSessionDelegate>)delegate
{
    NSString* bundleId = [[NSBundle mainBundle] bundleIdentifier];
    NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:bundleId?:kBackgroundOperationSessionID];
	// Indicates whether the app should be resumed or launched in the background when transfers finish.
	// When finished the system calls the app delegate’s application:handleEventsForBackgroundURLSession:completionHandler:
	configuration.sessionSendsLaunchEvents = YES;
	// Determines whether background tasks can be scheduled at the discretion of the system for optimal performance.
	configuration.discretionary = YES;
	NSURLSession* session = [NSURLSession sessionWithConfiguration:configuration delegate:delegate delegateQueue:nil];
	session.sessionDescription = @"BACKGROUND URLSession";
	return session;
}

@end
