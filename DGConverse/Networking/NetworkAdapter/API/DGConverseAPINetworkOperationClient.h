//
//  DGConverseAPINetworkOperationClient.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkHeader.h"

NS_ASSUME_NONNULL_BEGIN

@protocol DGConverseAPINetworkOperationClient <NSObject>

- (NSURLSessionDataTask*)runSessionDataTaskWithRequest:(id<DGConverseAPIRequest>)request;
- (NSURLSessionDownloadTask*)runSessionDownloadTaskWithRequest:(id<DGConverseAPIRequest>)request;
- (NSURLSessionUploadTask*)runSessionUploadTaskWithRequest:(id<DGConverseAPIRequest>)request fileURL:(NSURL*)fileURL;
- (NSURLSessionUploadTask*)runSessionUploadTaskWithRequest:(id<DGConverseAPIRequest>)request bodyData:(NSData*)bodyData;

@end

@interface NSURLSession (DGConverseAPINetworkOperationClient) <DGConverseAPINetworkOperationClient>

@end

@interface DGConverseAPINetworkOperationClient : NSObject

+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)clientWithConfiguration:(NSURLSessionConfiguration*)config delegate:(id<NSURLSessionDelegate>)delegate;
+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)defaultClientWithDelegate:(id<NSURLSessionDelegate>)delegate;
+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)ephemeralClientWithDelegate:(id<NSURLSessionDelegate>)delegate;
+ (NSURLSession<DGConverseAPINetworkOperations, DGConverseAPINetworkOperationClient>*)backgroundClientWithDelegate:(id<NSURLSessionDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
