//
//  DGConverseAPIRequestParameters.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

@import JSONModel;

NS_ASSUME_NONNULL_BEGIN

@protocol DGConverseAPIRequestParameters <NSObject>

- (NSDictionary*)toDictionary;

@end

#pragma mark -

@interface DGConverseAPIHTTPRequestParameters : NSObject <DGConverseAPIRequestParameters>

- (void)addKey:(NSString*)key value:(NSString*)value;

@end

#pragma mark -

/*!
 * @brief Request Parameter default object
 * @discussion We use this object to define the necessary/optional parameters for each particular request. It easy the job of convert all data to an NSDictionary thanks to JSONModel -toDictionary method.
 * @warning All properties must be instances of NSString or NSMutableString
 */
@interface DGConverseAPIJSONRequestParameters : JSONModel <DGConverseAPIRequestParameters>

@end

NS_ASSUME_NONNULL_END
