//
//  DGConverseAPIResponse.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIResponseSerialization.h"

NS_ASSUME_NONNULL_BEGIN

@protocol DGConverseAPIResponse <NSObject>

- (NSURLSessionTask*)task;
- (NSURLResponse*)response;
- (nullable NSError*)error;
- (nullable NSURL*)location;
- (nullable id)responseObject;
// Used by single NSURLSessionTasks
- (nullable id)processedResponseObject;
- (nullable id)processResponseObject:(NSError**)error;
// Used by NSURLSessionTasks embedded in NSOperations
- (nullable id)responseSerializer;

@end

#pragma mark -

@interface DGConverseAPIResponse : NSObject <DGConverseAPIResponse>

@property (nonatomic, copy) NSURLSessionTask* task;
@property (nonatomic, copy) NSURLResponse* response;
@property (nonatomic, copy, nullable) NSError* error;
@property (nonatomic, copy, nullable) NSURL* location;
// Used by single NSURLSessionTasks
@property (nonatomic, strong, nullable) id responseObject;
@property (nonatomic, strong, nullable) id processedResponseObject;
// Used by NSURLSessionTasks embedded in NSOperations
@property (nonatomic, strong, nullable) id responseSerializer;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithTask:(NSURLSessionTask*)task
					response:(NSURLResponse*)response
			  responseObject:(nullable id)responseObject
					   error:(NSError*)error NS_DESIGNATED_INITIALIZER;

@end

#pragma mark - COMPOSED RESPONSES

@interface DGConverseAPIDataResponse : DGConverseAPIResponse

@property (nonatomic, copy) NSURLSessionDataTask* task;
@property (nonatomic, strong) DGConverseAPIHTTPResponseSerializer* responseSerializer;

- (instancetype)init NS_UNAVAILABLE;

@end

typedef void (^DGConverseAPIDataResponseBlock)(DGConverseAPIDataResponse* response);

#pragma mark -

@interface DGConverseAPIJSONDataResponse : DGConverseAPIDataResponse

@property (nonatomic, strong) DGConverseAPIJSONResponseSerializer* responseSerializer;

- (instancetype)init NS_UNAVAILABLE;

@end

typedef void (^DGConverseAPIJSONDataResponseBlock)(DGConverseAPIJSONDataResponse* response);

#pragma mark -

@interface DGConverseAPIUploadResponse : DGConverseAPIResponse

@property (nonatomic, copy) NSURLSessionUploadTask* task;
@property (nonatomic, strong) DGConverseAPIHTTPResponseSerializer* responseSerializer;

- (instancetype)init NS_UNAVAILABLE;

@end

typedef void (^DGConverseAPIUploadResponseBlock)(DGConverseAPIUploadResponse* response);

#pragma mark -

@interface DGConverseAPIDownloadResponse : DGConverseAPIResponse

@property (nonatomic, copy) NSURLSessionDownloadTask* task;
@property (nonatomic, strong) DGConverseAPIHTTPResponseSerializer* responseSerializer;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithDownloadTask:(NSURLSessionDownloadTask*)task
							response:(NSURLResponse*)response
							location:(NSURL*)location
							   error:(NSError*)error;

@end

typedef void (^DGConverseAPIDownloadResponseBlock)(DGConverseAPIDownloadResponse* response);

#pragma mark -

@interface DGConverseAPIDownloadImageResponse : DGConverseAPIDownloadResponse

@end

typedef void (^DGConverseAPIDownloadImageResponseBlock)(DGConverseAPIDownloadImageResponse* response);

NS_ASSUME_NONNULL_END
