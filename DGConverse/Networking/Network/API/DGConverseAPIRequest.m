//
//  DGConverseAPIRequest.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIRequest.h"
#import "NSString+Additions.h"
#import "DGConverseAPIRequestSerialization.h"
#import "DGConverseAPIResponse.h"

@implementation DGConverseAPIRequest

@synthesize method;
@synthesize baseURLString;
@synthesize relativeURLString;
@synthesize URL;
@synthesize parameters;
@synthesize bodyBlock;
@synthesize responseClass;
@dynamic requestSerializer;
@dynamic urlRequest;

#pragma mark-- Object LyfeCycle Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.responseClass = [DGConverseAPIResponse class];
    }
    return self;
}

- (BOOL)isEqualToRequest:(id<DGConverseAPIRequest>)request
{
    if ([self.urlRequest isKindOfClass: [NSURLRequest class]] == NO)
    {
        return NO;
    }
    if (self.urlRequest.URL != request.urlRequest.URL
        && [self.URL isEqual: request.URL] == NO)
    {
        return NO;
    }
    if (self.urlRequest.mainDocumentURL != request.urlRequest.mainDocumentURL
        && [self.urlRequest.mainDocumentURL isEqual: request.urlRequest.mainDocumentURL] == NO)
    {
        return NO;
    }
    if (self.urlRequest.HTTPMethod != request.urlRequest.HTTPMethod
        && [self.urlRequest.HTTPMethod isEqual: request.urlRequest.HTTPMethod] == NO)
    {
        return NO;
    }
    if (self.urlRequest.HTTPBody != request.urlRequest.HTTPBody
        && [self.urlRequest.HTTPBody isEqual: request.urlRequest.HTTPBody] == NO)
    {
        return NO;
    }
    if (self.urlRequest.HTTPBodyStream != request.urlRequest.HTTPBodyStream
        && [self.urlRequest.HTTPBodyStream isEqual: request.urlRequest.HTTPBodyStream] == NO)
    {
        return NO;
    }
    if (self.urlRequest.allHTTPHeaderFields != request.urlRequest.allHTTPHeaderFields
        && [self.urlRequest.allHTTPHeaderFields isEqual: request.urlRequest.allHTTPHeaderFields] == NO)
    {
        return NO;
    }
    return YES;
}

- (void)reset
{
    // left blank to be used by clases that implements urlRequest variable
}

@end

#pragma mark - COMPOSED REQUESTS

@implementation DGConverseAPIDataRequest
@synthesize requestSerializer = _requestSerializer;
@synthesize urlRequest = _urlRequest;

#pragma mark-- Object LyfeCycle Methods

- (instancetype)initWithURLString:(NSString*)URLString
{
    return [self initWithURLString:URLString method:GET parameters:nil];
}

- (instancetype)initWithURLString:(NSString*)URLString
                           method:(DGConverseAPIMethod)method
                       parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
{
    return [self initWithURLString:URLString method:method parameters:parameters body:nil];
}

- (instancetype)initWithURLString:(NSString*)URLString
                           method:(DGConverseAPIMethod)method
                       parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
                             body:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body
{
    self = [super init];
    if (self) {
        self.responseClass = [DGConverseAPIDataResponse class];
        _requestSerializer = [DGConverseAPIHTTPRequestSerializer serializer];
        self.method = method;
        self.URL = [NSURL URLWithString:URLString ?: @""];
        self.parameters = parameters;
        self.bodyBlock = body;
    }
    return self;
}

#pragma mark-- DGConverseAPIRequest Protocol Methods

- (NSMutableURLRequest*)urlRequest
{
    if (!_urlRequest)
        _urlRequest = [self createURLRequest];
    
    return _urlRequest;
}

- (NSMutableURLRequest*)createURLRequest
{
    NSError* serializationError = nil;
    
    NSMutableURLRequest* urlRequest;
    
    if (self.bodyBlock) {
        
        urlRequest = [self.requestSerializer multipartFormRequestWithMethod:HTTPMethodToString(self.method)
                                                                  URLString:[self.URL absoluteString]
                                                                 parameters:[self.parameters toDictionary]
                                                  constructingBodyWithBlock:self.bodyBlock
                                                                      error:&serializationError];
    } else {
        urlRequest = [self.requestSerializer requestWithMethod:HTTPMethodToString(self.method)
                                                     URLString:[self.URL absoluteString]
                                                    parameters:[self.parameters toDictionary]
                                                         error:&serializationError];
    }
    
    if (serializationError) {
        NSAssert(false, @"DEBUG -- Error in serializer, please check it out");
    }
    return [urlRequest copy];
}

- (void)reset
{
    _urlRequest = nil;
}

@end

#pragma mark -

@implementation DGConverseAPIJSONDataRequest
@synthesize requestSerializer;

#pragma mark-- Object LyfeCycle Methods

- (instancetype)initWithURLString:(NSString*)URLString
{
    return [self initWithURLString:URLString method:GET parameters:nil body:nil];
}

- (instancetype)initWithURLString:(NSString*)URLString
                           method:(DGConverseAPIMethod)method
                       parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
{
    return [self initWithURLString:URLString method:method parameters:parameters body:nil];
}

- (instancetype)initWithURLString:(NSString*)URLString
                           method:(DGConverseAPIMethod)method
                       parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
                             body:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body
{
    self = [super initWithURLString:URLString method:method parameters:parameters body:body];
    if (self) {
        self.responseClass = [DGConverseAPIJSONDataResponse class];
        self.requestSerializer = [DGConverseAPIJSONRequestSerializer serializer];
    }
    return self;
}

@end

#pragma mark -

@implementation DGConverseAPIUploadRequest
@synthesize requestSerializer;
@synthesize urlRequest = _urlRequest;

#pragma mark-- Object LyfeCycle Methods

- (instancetype)initWithURLString:(NSString*)URLString
                          fileURL:(NSURL*)fileURL
{
    self = [super init];
    if (self) {
        self.method = POST;
        self.URL = [NSURL URLWithString:URLString];
        self.fileURL = fileURL;
        self.responseClass = [DGConverseAPIUploadResponse class];
        self.requestSerializer = [DGConverseAPIHTTPRequestSerializer serializer];
    }
    return self;
}

- (instancetype)initWithURLString:(NSString*)URLString
                         bodyData:(NSData*)bodyData
{
    self = [super init];
    if (self) {
        self.method = POST;
        self.URL = [NSURL URLWithString:URLString];
        self.bodyData = bodyData;
        self.responseClass = [DGConverseAPIUploadResponse class];
        self.requestSerializer = [DGConverseAPIHTTPRequestSerializer serializer];
    }
    return self;
}

#pragma mark-- DGConverseAPIRequest Protocol Methods

- (NSMutableURLRequest*)urlRequest
{
    if (!_urlRequest)
        _urlRequest = [self createURLRequest];
    
    return _urlRequest;
}

- (NSMutableURLRequest*)createURLRequest
{
    NSError* serializationError = nil;
    
    NSMutableURLRequest* urlRequest;
    
    if (self.bodyBlock) {
        
        urlRequest = [self.requestSerializer multipartFormRequestWithMethod:HTTPMethodToString(self.method)
                                                                  URLString:[self.URL absoluteString]
                                                                 parameters:[self.parameters toDictionary]
                                                  constructingBodyWithBlock:self.bodyBlock
                                                                      error:&serializationError];
    } else {
        urlRequest = [self.requestSerializer requestWithMethod:HTTPMethodToString(self.method)
                                                     URLString:[self.URL absoluteString]
                                                    parameters:[self.parameters toDictionary]
                                                         error:&serializationError];
    }
    
    if (serializationError) {
        NSAssert(false, @"DEBUG -- Error in serializer, please check it out");
    }
    return [urlRequest copy];
}

- (void)reset
{
    _urlRequest = nil;
}

@end

#pragma mark -

@implementation DGConverseAPIDownloadRequest
@synthesize requestSerializer;
@synthesize urlRequest = _urlRequest;

#pragma mark-- Object LyfeCycle Methods

- (instancetype)initWithURLString:(NSString*)URLString
{
    self = [super init];
    if (self) {
        self.URL = [NSURL URLWithString:URLString];
        self.responseClass = [DGConverseAPIDownloadResponse class];
        self.requestSerializer = [DGConverseAPIHTTPRequestSerializer serializer];
    }
    return self;
}

#pragma mark-- DGConverseAPIRequest Protocol Methods

- (NSMutableURLRequest*)urlRequest
{
    if (!_urlRequest)
        _urlRequest = [self createURLRequest];
    
    return _urlRequest;
}

- (NSMutableURLRequest*)createURLRequest
{
    NSError* serializationError = nil;
    
    NSMutableURLRequest* urlRequest = [self.requestSerializer requestWithMethod:HTTPMethodToString(self.method)
                                                                      URLString:[self.URL absoluteString]
                                                                     parameters:[self.parameters toDictionary]
                                                                          error:&serializationError];
    
    if (serializationError) {
        NSAssert(false, @"DEBUG -- Error in serializer, please check it out");
    }
    return [urlRequest copy];
}

- (void)reset
{
    _urlRequest = nil;
}

@end

#pragma mark -

@implementation DGConverseAPIDownloadImageRequest

#pragma mark-- Object LyfeCycle Methods

- (instancetype)initWithURLString:(NSString*)URLString
{
    self = [super initWithURLString:URLString];
    if (self) {
        self.responseClass = [DGConverseAPIDownloadImageResponse class];
    }
    return self;
}

@end
