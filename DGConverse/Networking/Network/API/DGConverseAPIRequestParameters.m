//
//  DGConverseAPIRequestParameters.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIRequestParameters.h"
#import <objc/runtime.h>

@interface DGConverseAPIHTTPRequestParameters ()

@property (nonatomic, strong) NSMutableDictionary* params;

@end

@implementation DGConverseAPIHTTPRequestParameters

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.params = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (NSDictionary*)toDictionary
{
	return self.params;
}

- (void)addKey:(NSString*)key value:(NSString*)value;
{
	[self.params setObject:value forKey:key];
}

@end

#pragma mark -

@implementation DGConverseAPIJSONRequestParameters

- (instancetype)init
{
	self = [super init];
	if (self) {
		//        [self validatePropertyTypes];
	}
	return self;
}

- (void)validatePropertyTypes
{
	unsigned int numberOfProperties = 0;
	objc_property_t* propertyArray = class_copyPropertyList([self class], &numberOfProperties);
	for (NSUInteger i = 0; i < numberOfProperties; i++) {
		objc_property_t property = propertyArray[i];
		NSString* name = [[NSString alloc] initWithUTF8String:property_getName(property)];
		Class class = [self classOfPropertyNamed:name];
        if (!((class == [NSString class]) || (class == [NSMutableString class]))) {
            NSAssert(false, @"DGConverseDefaultRequestParameters - There is at least one property in the parameter object whose type is neither NSString nor NSMutableString. Please correct it");
        }
	}
}

- (Class)classOfPropertyNamed:(NSString*)propertyName
{
	// Get Class of property to be populated.
	Class propertyClass = nil;
	objc_property_t property = class_getProperty([self class], [propertyName UTF8String]);
	NSString* propertyAttributes = [NSString stringWithCString:property_getAttributes(property) encoding:NSUTF8StringEncoding];
	NSArray* splitPropertyAttributes = [propertyAttributes componentsSeparatedByString:@","];
	if (splitPropertyAttributes.count > 0) {
		// xcdoc://ios//library/prerelease/ios/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html
		NSString* encodeType = splitPropertyAttributes[0];
		NSArray* splitEncodeType = [encodeType componentsSeparatedByString:@"\""];
		NSString* className = splitEncodeType[1];
		propertyClass = NSClassFromString(className);
	}
	return propertyClass;
}

@end
