//
//  DGConverseAPIMethod.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIMethod.h"

#define kPostMethodName @"POST"
#define kPutMethodName @"PUT"
#define kGetMethodName @"GET"
#define kHeadMethodName @"HEAD"
#define kPatchMethodName @"PATCH"
#define kDeleteMethodName @"DELETE"

NSString* HTTPMethodToString(DGConverseAPIMethod method)
{
	switch (method) {
	case GET:
		return kGetMethodName;
	case HEAD:
		return kHeadMethodName;
	case POST:
		return kPostMethodName;
	case PUT:
		return kPutMethodName;
	case PATCH:
		return kPatchMethodName;
	case DELETE:
		return kDeleteMethodName;
	default:
		return nil;
	}
}
