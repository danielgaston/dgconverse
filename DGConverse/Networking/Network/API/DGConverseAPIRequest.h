//
//  DGConverseAPIRequest.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

@import Foundation;

#import "DGConverseAPIMethod.h"
#import "DGConverseAPIRequestParameters.h"
#import "DGConverseAPIRequestSerialization.h"

NS_ASSUME_NONNULL_BEGIN

@protocol DGConverseAPIResponse;

@protocol DGConverseAPIRequest <NSObject>

- (DGConverseAPIMethod)method;
- (nullable NSString*)baseURLString;
- (nullable NSString*)relativeURLString;
- (NSURL*)URL;
- (nullable id<DGConverseAPIRequestParameters>)parameters;
- (nullable void (^)(id<DGConverseAPIRequestMultipartFormData>))bodyBlock;
- (Class<DGConverseAPIResponse>)responseClass;
- (NSMutableURLRequest*)urlRequest;

- (nullable id)requestSerializer;
- (BOOL)isEqualToRequest:(id<DGConverseAPIRequest>)request;
- (void)reset;

@end

#pragma mark -

@interface DGConverseAPIRequest : NSObject <DGConverseAPIRequest>

@property (nonatomic, readwrite) DGConverseAPIMethod method;
@property (nonatomic, readwrite, nullable) NSString* baseURLString;
@property (nonatomic, readwrite, nullable) NSString* relativeURLString;
@property (nonatomic, readwrite) NSURL* URL;
@property (nonatomic, readwrite, nullable) id<DGConverseAPIRequestParameters> parameters;
@property (nonatomic, readwrite, nullable) void (^bodyBlock)(id<DGConverseAPIRequestMultipartFormData>);
@property (nonatomic, readwrite) Class<DGConverseAPIResponse> responseClass;
@property (nonatomic, readwrite) NSMutableURLRequest* urlRequest;

@property (nonatomic, readwrite) id requestSerializer;

- (BOOL)isEqualToRequest:(id<DGConverseAPIRequest>)request;

@end

#pragma mark - COMPOSED REQUESTS

@interface DGConverseAPIDataRequest : DGConverseAPIRequest

@property (nonatomic, readwrite) DGConverseAPIHTTPRequestSerializer* requestSerializer;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithURLString:(NSString*)URLString;
- (instancetype)initWithURLString:(NSString*)URLString
                           method:(DGConverseAPIMethod)method
                       parameters:(nullable id<DGConverseAPIRequestParameters>)parameters;

- (instancetype)initWithURLString:(NSString*)URLString
                           method:(DGConverseAPIMethod)method
					parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
						  body:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface DGConverseAPIJSONDataRequest : DGConverseAPIDataRequest

@property (nonatomic, readwrite) DGConverseAPIJSONRequestSerializer* requestSerializer;

@end

#pragma mark -

@interface DGConverseAPIUploadRequest : DGConverseAPIRequest

@property (nonatomic, readwrite) NSURL* fileURL;
@property (nonatomic, readwrite) NSData* bodyData;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithURLString:(NSString*)URLString
						  fileURL:(NSURL*)fileURL NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithURLString:(NSString*)URLString
						 bodyData:(NSData*)bodyData NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface DGConverseAPIDownloadRequest : DGConverseAPIRequest

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithURLString:(NSString*)URLString NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -

@interface DGConverseAPIDownloadImageRequest : DGConverseAPIDownloadRequest

@end

NS_ASSUME_NONNULL_END
