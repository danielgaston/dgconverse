//
//  DGConverseAPIResponse.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIResponse.h"

@implementation DGConverseAPIResponse

@synthesize response;
@synthesize error;
@synthesize location;
@synthesize responseObject;
@synthesize processedResponseObject;
@dynamic task;
@dynamic responseSerializer;

- (instancetype)initWithTask:(NSURLSessionTask*)dataTask
					response:(NSURLResponse*)response
			  responseObject:(nullable id)responseObject
					   error:(NSError*)error
{
	self = [super init];
	if (self) {
		self.task = dataTask;
		self.response = response;
		self.error = error;
		self.responseObject = responseObject;

		if (!error) {
			NSError* processingError;
			self.processedResponseObject = [self processResponseObject:&processingError];
			if (processingError) {
				self.error = processingError;
			}
		}
	}
	return self;
}

#pragma mark-- DGConverseAPIResponse Protocol Delegate Methods

- (id)processResponseObject:(NSError* __autoreleasing*)error
{
	return self.responseObject;
}

@end

#pragma mark - COMPOSED RESPONSES

@implementation DGConverseAPIDataResponse
@synthesize task;
@synthesize responseSerializer = _responseSerializer;

#pragma mark-- DGConverseAPIResponse Protocol Delegate Methods

- (instancetype)initWithTask:(NSURLSessionDataTask*)dataTask
					response:(NSURLResponse*)response
			  responseObject:(id)responseObject
					   error:(NSError*)error
{
	self = [super initWithTask:dataTask response:response responseObject:responseObject error:error];
	if (self) {
		_responseSerializer = [DGConverseAPIHTTPResponseSerializer serializer];
	}
	return self;
}

@end

#pragma mark -

@implementation DGConverseAPIJSONDataResponse
@synthesize responseSerializer;

#pragma mark-- DGConverseAPIResponse Protocol Delegate Methods

- (instancetype)initWithTask:(NSURLSessionDataTask*)dataTask
					response:(NSURLResponse*)response
			  responseObject:(id)responseObject
					   error:(NSError*)error
{
	self = [super initWithTask:dataTask response:response responseObject:responseObject error:error];
	if (self) {
		self.responseSerializer = [DGConverseAPIJSONResponseSerializer serializer];
	}
	return self;
}

- (id)processResponseObject:(NSError* __autoreleasing*)error
{
	return [self.responseSerializer responseObjectForResponse:self.response data:self.responseObject error:error];
}

@end

#pragma mark -

@implementation DGConverseAPIDownloadResponse
@synthesize task;
@synthesize responseSerializer = _responseSerializer;

#pragma mark-- DGConverseAPIResponse Protocol Delegate Methods

- (instancetype)initWithTask:(NSURLSessionDownloadTask*)downloadTask
					response:(NSURLResponse*)response
			  responseObject:(id)responseObject
					   error:(NSError*)error
{
	self = [super initWithTask:downloadTask response:response responseObject:responseObject error:error];
	if (self) {
		_responseSerializer = [DGConverseAPIHTTPResponseSerializer serializer];
	}
	return self;
}

- (instancetype)initWithDownloadTask:(NSURLSessionDownloadTask*)downloadTask
							response:(NSURLResponse*)response
							location:(NSURL*)location
							   error:(NSError*)error
{
	self = [self initWithTask:downloadTask response:response responseObject:nil error:error];
	if (self) {
		self.location = location;
	}
	return self;
}

@end

#pragma mark -

@implementation DGConverseAPIDownloadImageResponse

@end

#pragma mark -

@implementation DGConverseAPIUploadResponse
@synthesize task;
@synthesize responseSerializer = _responseSerializer;

#pragma mark-- DGConverseAPIResponse Protocol Delegate Methods

- (instancetype)initWithTask:(NSURLSessionUploadTask*)uploadTask
					response:(NSURLResponse*)response
			  responseObject:(id)responseObject
					   error:(NSError*)error
{
	self = [super initWithTask:uploadTask response:response responseObject:responseObject error:error];
	if (self) {
		_responseSerializer = [DGConverseAPIHTTPResponseSerializer serializer];
	}
	return self;
}

@end
