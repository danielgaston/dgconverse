//
//  DGConverseAPIClient.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIClient.h"
#import <objc/runtime.h>

#pragma mark -

@interface NSURLRequest (Additions)

+ (NSURLRequest*)urlRequestWithRequest:(id<DGConverseAPIRequest>)request;

@end

@implementation NSURLRequest (Additions)

+ (NSURLRequest*)urlRequestWithRequest:(id<DGConverseAPIRequest>)request;
{
    return request.urlRequest;
}

@end

#pragma mark -

@implementation NSURLSession (DGConverseAPINetworkTasksClient)

- (NSURLSessionDataTask*)dataTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
											 completion:(DGConverseAPIClientCompletionBlock)completion;
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	__block NSURLSessionDataTask* task;
	task = [self dataTaskWithRequest:urlRequest
				   completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {

					   Class responseClass = [request responseClass];
					   id<DGConverseAPIResponse> apiResponse = [[responseClass alloc] initWithTask:task response:response responseObject:data error:error];

					   [[NSOperationQueue mainQueue] addOperationWithBlock:^{
						   if (completion) {
							   completion(apiResponse);
						   }
					   }];
				   }];
	return task;
}

- (NSURLSessionDownloadTask*)downloadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
													 completion:(DGConverseAPIClientCompletionBlock)completion
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	__block NSURLSessionDownloadTask* task;
	task = [self downloadTaskWithRequest:urlRequest
					   completionHandler:^(NSURL* _Nullable location, NSURLResponse* _Nullable response, NSError* _Nullable error) {

						   Class responseClass = [request responseClass];
						   id<DGConverseAPIResponse> apiResponse = [[responseClass alloc] initWithDownloadTask:task response:response location:location error:error];

						   [[NSOperationQueue mainQueue] addOperationWithBlock:^{
							   if (completion) {
								   completion(apiResponse);
							   }
						   }];
					   }];
	return task;
}

- (NSURLSessionUploadTask*)uploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
													fileURL:(NSURL*)fileURL
												 completion:(DGConverseAPIClientCompletionBlock)completion
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	__block NSURLSessionUploadTask* task;
	task = [self uploadTaskWithRequest:urlRequest
							  fromFile:fileURL
					 completionHandler:^(NSData* _Nullable data, NSURLResponse* _Nullable response, NSError* _Nullable error) {

						 Class responseClass = [request responseClass];
						 id<DGConverseAPIResponse> apiResponse = [[responseClass alloc] initWithTask:task response:response responseObject:data error:error];

						 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
							 if (completion) {
								 completion(apiResponse);
							 }
						 }];
					 }];
	return task;
}

- (NSURLSessionUploadTask*)uploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
												   bodyData:(NSData*)bodyData
												 completion:(DGConverseAPIClientCompletionBlock)completion
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	__block NSURLSessionUploadTask* task;
	task = [self uploadTaskWithRequest:urlRequest
							  fromData:bodyData
					 completionHandler:^(NSData* _Nullable data, NSURLResponse* _Nullable response, NSError* _Nullable error) {

						 Class responseClass = [request responseClass];
						 id<DGConverseAPIResponse> apiResponse = [[responseClass alloc] initWithTask:task response:response responseObject:data error:error];

						 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
							 if (completion) {
								 completion(apiResponse);
							 }
						 }];
					 }];
	return task;
}

@end

#pragma mark -

@implementation NSURLSession (DGConverseAPINetworkOperationsClient)

- (NSURLSessionDataTask*)operationDataTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	NSURLSessionDataTask* task = [self dataTaskWithRequest:urlRequest];
	return task;
}

- (NSURLSessionDownloadTask*)operationDownloadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	NSURLSessionDownloadTask* task = [self downloadTaskWithRequest:urlRequest];
	return task;
}

- (NSURLSessionUploadTask*)operationUploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request fileURL:(NSURL*)fileURL
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	NSURLSessionUploadTask* task = [self uploadTaskWithRequest:urlRequest fromFile:fileURL];
	return task;
}

- (NSURLSessionUploadTask*)operationUploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request bodyData:(NSData*)bodyData
{
	NSURLRequest* urlRequest = [NSURLRequest urlRequestWithRequest:request];
	NSURLSessionUploadTask* task = [self uploadTaskWithRequest:urlRequest fromData:bodyData];
	return task;
}

@end
