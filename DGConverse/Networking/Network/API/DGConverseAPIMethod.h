//
//  DGConverseAPIMethod.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//


#ifndef DGCONVERSEAPIMETHOD_H_
#define DGCONVERSEAPIMETHOD_H_

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DGConverseAPIMethod){
    GET,
    HEAD,
    POST,
    PUT,
    PATCH,
    DELETE,
    UNDEFINED
};

extern NSString *HTTPMethodToString(DGConverseAPIMethod method);

#endif

