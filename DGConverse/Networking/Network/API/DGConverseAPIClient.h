//
//  DGConverseAPIClient.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

@import Foundation;

#import "DGConverseAPIRequest.h"
#import "DGConverseAPIResponse.h"

typedef void (^DGConverseAPIClientCompletionBlock)(id<DGConverseAPIResponse> response);

/*!
 * @brief Defined to use an NSURLSessionDataTask directly on an NSURLSession
 */
@protocol DGConverseAPINetworkTasks <NSObject>

- (NSURLSessionDataTask*)dataTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
											 completion:(DGConverseAPIClientCompletionBlock)completion;
- (NSURLSessionDownloadTask*)downloadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
													 completion:(DGConverseAPIClientCompletionBlock)completion;
- (NSURLSessionUploadTask*)uploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
													fileURL:(NSURL*)fileURL
												 completion:(DGConverseAPIClientCompletionBlock)completion;
- (NSURLSessionUploadTask*)uploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request
												   bodyData:(NSData*)bodyData
												 completion:(DGConverseAPIClientCompletionBlock)completion;

@end

#pragma mark -

/*!
 * @brief Defined to use an NSURLSessionDataTask directly on an NSURLSession inside an NSOperation
 */
@protocol DGConverseAPINetworkOperations <NSObject>

- (NSURLSessionDataTask*)operationDataTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request;
- (NSURLSessionDownloadTask*)operationDownloadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request;
- (NSURLSessionUploadTask*)operationUploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request fileURL:(NSURL*)fileURL;
- (NSURLSessionUploadTask*)operationUploadTaskWithConverseAPIRequest:(id<DGConverseAPIRequest>)request bodyData:(NSData*)bodyData;

@end

#pragma mark -

/*!
 * @brief Category defined to be used as an NSURLSession call
 */
@interface NSURLSession (DGConverseAPINetworkTasksClient) <DGConverseAPINetworkTasks>
@end

#pragma mark -

/*!
 * @brief Category defined to be used as an NSURLSession call inside an NSOperation
 */
@interface NSURLSession (DGConverseAPINetworkOperationsClient) <DGConverseAPINetworkOperations>
@end
