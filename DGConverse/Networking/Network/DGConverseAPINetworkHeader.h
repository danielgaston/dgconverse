//
//  DGConverseAPINetworkHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ConverseAPINetwork.
FOUNDATION_EXPORT double ConverseAPINetworkVersionNumber;

//! Project version string for ConverseAPINetwork.
FOUNDATION_EXPORT const unsigned char ConverseAPINetworkVersionString[];

// In this header, we should import all the public headers of your framework using statements

#define kDGConverseNetworkDomain @"kDGConverseNetworkDomain"

#import "DGConverseAPIClient.h"


