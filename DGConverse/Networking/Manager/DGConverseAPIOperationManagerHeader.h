//
//  DGConverseAPIOperationManagerHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPIOperationManagerHeader_h
#define DGConverseAPIOperationManagerHeader_h

#import "DGConverseAPIBaseOperationProtocol.h"
#import "DGConverseAPIQueueManager.h"

/**
 * The principal protocol that define what any Operation Manager must implement.
 */
@protocol DGConverseAPIOperationNetworkManager <NSObject, DGConverseAPINetworkBaseOperationDelegate, DGConverseAPIParseBaseOperationDelegate, DGConverseAPICacheBaseOperationDelegate, DGConverseAPIConversionBaseOperationDelegate>

@property (nonatomic, readonly) DGConverseAPIQueueManager* queueManager;

@end

#endif
