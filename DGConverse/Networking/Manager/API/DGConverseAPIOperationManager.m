//
//  DGConverseAPIOperationManager.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationManager.h"
#import "DGConverseAPICacheOperationHeader.h"
#import "DGConverseAPIDebugUtils.h"
#import "DGConverseAPIGroupDataOperation.h"
#import "DGConverseAPIGroupDownloadOperation.h"
#import "DGConverseAPIGroupGeneralOperation.h"
#import "DGConverseAPIGroupImageOperation.h"
#import "DGConverseAPIGroupUploadOperation.h"
#import "DGConverseAPINetworkDownloadOperation.h"
#import "DGConverseAPINetworkUploadOperation.h"

@interface DGConverseAPIOperationManager ()

@property (nonatomic, readwrite) DGConverseAPIQueueManager* queueManager;

@property (nonatomic, strong) NSMapTable<NSURLSessionTask*, DGConverseAPINetworkBaseOperation*>* weakTaskToStrongOpMapTable;
@property (nonatomic, strong) NSMapTable<NSURLSessionTask*, void (^)(id<DGConverseAPIOperationResult>)>* weakCompletionsWeakMapTable; // Completion handlers for background NSURLSession download tasks

@property dispatch_queue_t mapTableThreadSafeQueue;

@end

@implementation DGConverseAPIOperationManager

#pragma mark - Object LifeCycle Methods

- (instancetype)init
{
	self = [super init];
	if (self) {
		_queueManager = [DGConverseAPIQueueManager new];
        
        // In general the use of weak-to-strong map tables is not recommended. The strong values for weak keys which get zeroed out continue to be maintained until the map table RESIZES itself (adding or removing elements). The reason of strongify values is that some network ops were dealloc before reaching '- (void)URLSession:task:didCompleteWithError:' in this class, so that the client never was receiving the operation result. Thats why all stored tasks & operations are cleaned in dealloc, to release the operation (value) that otherwise would never be released because an NSURLSession holds all created NSURLSessionTasks (finished/unfinished) until it gets invalidated (- (void)invalidateAndCancel or - (void)finishTasksAndInvalidate).
        _weakTaskToStrongOpMapTable = [NSMapTable weakToStrongObjectsMapTable];
        _weakCompletionsWeakMapTable = [NSMapTable weakToWeakObjectsMapTable];
        
        _mapTableThreadSafeQueue = dispatch_queue_create("com.danielgaston.converse.operation_manager_serial_queue", NULL);
    }
	return self;
}

- (instancetype)initWithDefaultConfiguration:(NSURLSessionConfiguration *)defaultConfiguration
                                    delegate:(nullable id<DGConverseAPIOperationManagerDelegate>)delegate
{
    return [self initWithDefaultConfiguration:defaultConfiguration backgroundConfiguration:nil delegate:delegate];
}

- (instancetype)initWithBackgroundConfiguration:(NSURLSessionConfiguration *)backgroundConfiguration
                                       delegate:(nullable id<DGConverseAPIOperationManagerDelegate>)delegate
{
    return [self initWithDefaultConfiguration:nil backgroundConfiguration:backgroundConfiguration delegate:delegate];
}

- (instancetype)initWithDefaultConfiguration:(nullable NSURLSessionConfiguration *)defaultConfiguration
                     backgroundConfiguration:(nullable NSURLSessionConfiguration *)backgroundConfiguration
                                    delegate:(nullable id<DGConverseAPIOperationManagerDelegate>)delegate
{
    self = [self init];
    if (self) {
        _defaultSession = defaultConfiguration ? [DGConverseAPINetworkOperationClient clientWithConfiguration:defaultConfiguration delegate:self] : nil;
        
        _backgroundSession = backgroundConfiguration ? [DGConverseAPINetworkOperationClient clientWithConfiguration:backgroundConfiguration delegate:self] : nil;
        
        _delegate = delegate;
    }
    
    return self;
}

- (void)dealloc
{
    // In general the use of weak-to-strong map tables is not recommended. The strong values for weak keys which get zeroed out continue to be maintained until the map table RESIZES itself (adding or removing elements). The reason of strongify values is that some network ops were dealloc before reaching '- (void)URLSession:task:didCompleteWithError:' in this class, so that the client never was receiving the operation result. Thats why all stored tasks & operations are cleaned in dealloc, to release the operation (value) that otherwise would never be released because an NSURLSession holds all created NSURLSessionTasks (finished/unfinished) until it gets invalidated (- (void)invalidateAndCancel or - (void)finishTasksAndInvalidate).
    [self removeAllOperations];
    
	// @warning, it also cancels executing and queued BACKGROUND operations
//    [self cancelAllRequests];
	// Adding this line fixes possible memory management issues
	[self.defaultSession finishTasksAndInvalidate];
	[self.backgroundSession finishTasksAndInvalidate];
    
    if (self.delegate)
        [self.delegate operationManagerDidDealloc:self];
}

#pragma mark - Helper Methods (Thread Safe - Operations Table)

- (void)storeOperation:(DGConverseAPINetworkBaseOperation*)op forTask:(NSURLSessionTask*)task
{
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        [self.weakTaskToStrongOpMapTable setObject:op forKey:task];
    });
}

- (DGConverseAPINetworkBaseOperation *)getOperationForTask:(NSURLSessionTask *)task
{
    __block DGConverseAPINetworkBaseOperation *op;
    
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        op = [self.weakTaskToStrongOpMapTable objectForKey:task];
    });
    
    return op;
}

- (void)removeOperationForTask:(NSURLSessionTask *)task
{
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        [self.weakTaskToStrongOpMapTable removeObjectForKey:task];
    });
}

- (void)removeAllOperations
{
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        [self.weakTaskToStrongOpMapTable removeAllObjects];
    });
}

#pragma mark - Helper Methods (Thread Safe - Completions Table)

- (void)storeCompletion:(void (^)(id<DGConverseAPIOperationResult>))completion forTask:(NSURLSessionTask*)task
{
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        [self.weakCompletionsWeakMapTable setObject:completion forKey:task];
    });
}

- (void (^)(id<DGConverseAPIOperationResult>))getCompletionForTask:(NSURLSessionTask *)task
{
    __block void (^completion)(id<DGConverseAPIOperationResult>);
    
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        completion = [self.weakCompletionsWeakMapTable objectForKey:task];
    });
    
    return completion;
}

- (void)removeCompletionForTask:(NSURLSessionTask *)task
{
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        [self.weakCompletionsWeakMapTable removeObjectForKey:task];
    });
}

- (void)removeAllCompletions
{
    dispatch_sync(self.mapTableThreadSafeQueue, ^{
        [self.weakCompletionsWeakMapTable removeAllObjects];
    });
}


#pragma mark - URL BASE REQUESTS

#pragma mark-- DATA

- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self data:URLString progress:nil completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
									progress:(nullable DGConverseAPINetworkProgressBlock)progress
								  completion:(DGConverseAPINetworkResultBlock)completion
{
    return [self data:URLString useCache:NO progress:nil completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
                                   useCache:(BOOL)useCache
                                   progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                 completion:(DGConverseAPINetworkResultBlock)completion
{
    return [self data:URLString useCache:useCache locked:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
                                   useCache:(BOOL)useCache
                                     locked:(BOOL)locked
                                   progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                 completion:(DGConverseAPINetworkResultBlock)completion
{
    DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString];
    DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
                                                                                            operationDelegate:self
                                                                                                     progress:progress
                                                                                                   completion:completion];
    
    DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
    
    if (useCache) {
        DGConverseAPICacheJSONOperation *readCacheOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:URLString locked:locked operationDelegate:self];
        DGConverseAPICacheJSONOperation *writeCacheOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:URLString locked:locked operationDelegate:self];
        [group runWithReadCacheOperation:readCacheOp networkOperation:networkOp writeCacheOperation:writeCacheOp];
    } else {
        [group runWithNetworkOperation:networkOp];
    }
    
    return group;
}


#pragma mark-- UPLOAD

- (id<DGConverseAPIGroupBaseOperation>)uploadFile:(NSString*)fileUrlString
												to:(NSString*)toUrlString
										  progress:(nullable DGConverseAPINetworkProgressBlock)progress
										completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self uploadFile:fileUrlString to:toUrlString inBackground:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)uploadFile:(NSString*)fileUrlString
												to:(NSString*)toUrlString
									  inBackground:(BOOL)background
										  progress:(nullable DGConverseAPINetworkProgressBlock)progress
										completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIGroupUploadOperation* group = [[DGConverseAPIGroupUploadOperation alloc] initWithManager:self];

	if (background) {
		[group runInBackgroundWithFileURL:fileUrlString toURL:toUrlString progress:progress completion:completion];
	} else {
		[group runWithFileURL:fileUrlString toURL:toUrlString progress:progress completion:completion];
	}

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)uploadData:(NSData*)fileData
												to:(NSString*)toUrlString
										  progress:(nullable DGConverseAPINetworkProgressBlock)progress
										completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIGroupUploadOperation* group = [[DGConverseAPIGroupUploadOperation alloc] initWithManager:self];
	[group runWithFileData:fileData toURL:toUrlString progress:progress completion:completion];

	return group;
}

#pragma mark-- DOWNLOAD

- (id<DGConverseAPIGroupBaseOperation>)downloadJSON:(NSString*)URLString
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPIJSONResultBlock)completion
{
	return [self downloadJSON:URLString useCache:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)downloadJSON:(NSString*)URLString
											useCache:(BOOL)useCache
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPIJSONResultBlock)completion
{
    return [self downloadJSON:URLString useCache:useCache locked:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)downloadJSON:(NSString*)URLString
                                           useCache:(BOOL)useCache
                                             locked:(BOOL)locked
                                           progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                         completion:(DGConverseAPIJSONResultBlock)completion
{
    DGConverseAPIJSONDataRequest *networkRequest = [[DGConverseAPIJSONDataRequest alloc] initWithURLString:URLString];
    DGConverseAPINetworkDataOperation *networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest operationDelegate:self progress:progress completion:nil];
    DGConverseAPIParseJSONOperation *parseOp = [[DGConverseAPIParseJSONOperation alloc] initWithOperationDelegate:self completion:completion];
    DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
    
    if (useCache) {
        DGConverseAPICacheJSONOperation *readCacheOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:URLString locked:locked operationDelegate:self];
        DGConverseAPICacheJSONOperation *writeCacheOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:URLString locked:locked operationDelegate:self];
        [group runWithReadCacheOperation:readCacheOp networkOperation:networkOp writeCacheOperation:writeCacheOp parseOperation:parseOp];
    } else {
        [group runWithNetworkOperation:networkOp parseOperation:parseOp];
    }
    
    return group;
}

- (id<DGConverseAPIGroupBaseOperation>)downloadImage:(NSString*)URLString
											 progress:(nullable DGConverseAPINetworkProgressBlock)progress
										   completion:(DGConverseImageResultBlock)completion
{
	return [self downloadImage:URLString useCache:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)downloadImage:(NSString*)URLString
											 useCache:(BOOL)useCache
											 progress:(nullable DGConverseAPINetworkProgressBlock)progress
										   completion:(DGConverseImageResultBlock)completion
{
    return [self downloadImage:URLString useCache:useCache locked:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)downloadImage:(NSString*)URLString
                                            useCache:(BOOL)useCache
                                              locked:(BOOL)locked
                                            progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                          completion:(DGConverseImageResultBlock)completion
{
    DGConverseAPIGroupImageOperation* group = [[DGConverseAPIGroupImageOperation alloc] initWithManager:self];
    [group runWithURLString:URLString useCache:useCache locked:locked progress:progress completion:completion];
    
    return group;
}


- (id<DGConverseAPIGroupBaseOperation>)downloadFile:(NSString*)URLString
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self downloadFile:URLString inBackground:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)downloadFile:(NSString*)URLString
										inBackground:(BOOL)background
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self downloadFile:URLString inBackground:NO toDocumentsFolder:NO progress:progress completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)downloadFile:(NSString*)URLString
										inBackground:(BOOL)background
								   toDocumentsFolder:(BOOL)toDocuments
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIGroupDownloadOperation* group = [[DGConverseAPIGroupDownloadOperation alloc] initWithManager:self];

	if (background) {
		[group runInBackgroundWithURLString:URLString destinationFolder:toDocuments ? NSDocumentDirectory : 0 progress:progress completion:completion];
	} else {
		[group runWithURLString:URLString destinationFolder:toDocuments ? NSDocumentDirectory : 0 progress:progress completion:completion];
	}

	return group;
}

#pragma mark - CONCURRENT REQUESTS

- (id<DGConverseAPIGroupBaseOperation>)runConcurrentOperations:(NSArray<DGConverseAPIBaseOperation*>*)operations
{
	DGConverseAPIGroupGeneralOperation* group = [[DGConverseAPIGroupGeneralOperation alloc] initWithManager:self];
	[group runWithOperations:operations];

	return group;
}

#pragma mark - HTTP BASE REQUESTS

#pragma mark-- GET

- (id<DGConverseAPIGroupBaseOperation>)GET:(NSString*)URLString
								 parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								 completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self GET:URLString parameters:parameters progress:nil completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)GET:(NSString*)URLString
								 parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								   progress:(nullable DGConverseAPINetworkProgressBlock)progress
								 completion:(DGConverseAPINetworkResultBlock)completion
{
    return [self GET:URLString parameters:parameters useCache:NO progress:nil completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)GET:(NSString*)URLString
                                parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
                                  useCache:(BOOL)useCache
                                  progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                completion:(DGConverseAPINetworkResultBlock)completion
{
    DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:GET
                                                                                        parameters:parameters];
    
    DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
                                                                                            operationDelegate:self
                                                                                                     progress:progress
                                                                                                   completion:completion];
    
    DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
    if (useCache) {
        DGConverseAPICacheJSONOperation *readCacheOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:URLString locked:NO operationDelegate:self];
        DGConverseAPICacheJSONOperation *writeCacheOp = [[DGConverseAPICacheJSONOperation alloc] initWithURLString:URLString locked:NO operationDelegate:self];
        [group runWithReadCacheOperation:readCacheOp networkOperation:networkOp writeCacheOperation:writeCacheOp];
    } else {
        [group runWithNetworkOperation:networkOp];
    }
    
    return group;
}

#pragma mark-- HEAD

- (id<DGConverseAPIGroupBaseOperation>)HEAD:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								  completion:(DGConverseAPINetworkResultBlock)completion
{
    DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:HEAD
                                                                                        parameters:parameters];

	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
																							  operationDelegate:self
																									 completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp];

	return group;
}

#pragma mark-- POST

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								  completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self POST:URLString parameters:parameters progress:nil completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
									progress:(nullable DGConverseAPINetworkProgressBlock)progress
								  completion:(DGConverseAPINetworkResultBlock)completion
{
    DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:POST
                                                                                        parameters:parameters];

	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
																							  operationDelegate:self
																									   progress:progress
																									 completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp];

	return group;
}

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
				   constructingBodyWithBlock:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body
								  completion:(DGConverseAPINetworkResultBlock)completion
{
	return [self POST:URLString parameters:parameters constructingBodyWithBlock:body progress:nil completion:completion];
}

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
				   constructingBodyWithBlock:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body
									progress:(nullable DGConverseAPINetworkProgressBlock)progress
								  completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:POST
                                                                                        parameters:parameters
                                                                                              body:body];

	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
                                                                                            operationDelegate:self
                                                                                                     progress:progress
                                                                                                   completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp];

	return group;
}

#pragma mark-- PUT

- (id<DGConverseAPIGroupBaseOperation>)PUT:(NSString*)URLString
								 parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								 completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:PUT
                                                                                        parameters:parameters];

	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
																							  operationDelegate:self
																									 completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp];

	return group;
}

#pragma mark-- PATCH

- (id<DGConverseAPIGroupBaseOperation>)PATCH:(NSString*)URLString
								   parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								   completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:PATCH
                                                                                        parameters:parameters];

	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
																							  operationDelegate:self
																									 completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp];

	return group;
}

#pragma mark-- DELETE

- (id<DGConverseAPIGroupBaseOperation>)DELETE:(NSString*)URLString
									parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
									completion:(DGConverseAPINetworkResultBlock)completion
{
	DGConverseAPIDataRequest* networkRequest = [[DGConverseAPIDataRequest alloc] initWithURLString:URLString
                                                                                            method:DELETE
                                                                                        parameters:parameters];

	DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest
																							  operationDelegate:self
																									 completion:completion];

	DGConverseAPIGroupDataOperation* group = [[DGConverseAPIGroupDataOperation alloc] initWithManager:self];
	[group runWithNetworkOperation:networkOp];

	return group;
}

#pragma mark-- BACKGROUND TASK Helper

- (void)handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)(void))completionHandler
{
    DGWeakify(self);
    // In case of having multiple background sessions, make use of 'identifier'
    [self.backgroundSession getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks) {
        DGStrongify(self);
        
        [@[dataTasks, downloadTasks, uploadTasks] enumerateObjectsUsingBlock:^(NSArray<NSURLSessionTask *> *sessionTasks, NSUInteger idx, BOOL * _Nonnull stop) {
            
            [sessionTasks enumerateObjectsUsingBlock:^(NSURLSessionTask * _Nonnull sessionTask, NSUInteger idx, BOOL * _Nonnull stop) {
               
                DGConverseAPINetworkBaseOperation *op = [self getOperationForTask:sessionTask];
                void (^sessionCompletionHandler)(id<DGConverseAPIOperationResult>) = [self getCompletionForTask:sessionTask];
                [self removeCompletionForTask:sessionTask];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    sessionCompletionHandler(op.result);
                    completionHandler();
                });
            }];
        }];
    }];
}


#pragma mark - QUEUE Helper

- (void)cancelAllRequests
{
	[self.queueManager cancelAllOperationsInQueue:kDGConverseAPIQueueALL];
}

- (NSUInteger)currentConcurrentOpsInQueues
{
    NSUInteger dataOps = [self.queueManager currentConcurrentOpsInQueue:kDGConverseAPIQueueData];
    NSUInteger computeOps = [self.queueManager currentConcurrentOpsInQueue:kDGConverseAPIQueueCompute];
    NSUInteger bgOps = [self.queueManager currentConcurrentOpsInQueue:kDGConverseAPIQueueBackground];
    
    return dataOps + computeOps + bgOps;
}

#pragma mark - DGConverseAPINetworkBaseOperationDelegate

- (NSURLSession*)converseAPINetworkBaseOperationRequestDefaultSession:(DGConverseAPINetworkBaseOperation*)op
{
	if (!self.defaultSession)
		self.defaultSession = [DGConverseAPINetworkOperationClient defaultClientWithDelegate:self];

	return self.defaultSession;
}

/*!
 * @warning There must be only one background session instance in the App, independently from where it was instanced! As long as we use single NetworkManager via dependency injection there wont be any problem, however, in case the app requires two or more NetworkManager (undesired), background downloads/uploads wont work as it would necessarily require to create a new instance of backgroundSession showing the next error: A background URLSession with identifier ConverseBackgroundOperationSession already exists!
 */
- (NSURLSession*)converseAPINetworkBaseOperationRequestBackgroundSession:(DGConverseAPINetworkBaseOperation*)op
{
	if (!self.backgroundSession)
		self.backgroundSession = [DGConverseAPINetworkOperationClient backgroundClientWithDelegate:self];

	return self.backgroundSession;
}

- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op didStartWithTask:(NSURLSessionTask*)task
{
	[self storeOperation:op forTask:task];
}

- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op didDiscardTask:(NSURLSessionTask *)task
{
    [self removeOperationForTask:task];
}

- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op requiresStoreCompletion:(void (^)(id<DGConverseAPIOperationResult>))completionBlock forURLSessionTask:(NSURLSessionTask *)sessionTask
{
    [self storeCompletion:completionBlock forTask:sessionTask];
}

- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op reportsKBperSecond:(CGFloat)KBperSec
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel2, [NSString stringWithFormat:@"%@ -- TRANSFER SPEEDx: KB/s: %f", op.name, KBperSec]);
}

#pragma mark - DGConverseAPIParseBaseOperationDelegate

- (void)converseAPIParseBaseOperation:(DGConverseAPIParseBaseOperation*)op reportsDuration:(NSTimeInterval)interval
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel2, [NSString stringWithFormat:@"%@ -- PARSE DURATION: %f s.", op.name, interval]);
}

#pragma mark - DGConverseAPICacheBaseOperationDelegate

- (void)converseAPICacheBaseOperation:(DGConverseAPICacheBaseOperation*)op reportsDuration:(NSTimeInterval)interval
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel2, [NSString stringWithFormat:@"%@ -- CACHE DURATION: %f s.", op.name, interval]);
}

#pragma mark - DGConverseAPIConversionBaseOperationDelegate

- (void)converseAPIConversionBaseOperation:(DGConverseAPIConversionBaseOperation*)op reportsDuration:(NSTimeInterval)interval
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel2, [NSString stringWithFormat:@"%@ -- CONVERSION DURATION: %f s.", op.name, interval]);
}

#pragma mark - NSURLSessionDelegate Methods

/*!
 * @brief The last message a session receives.  A session will only become invalid because of a systemic error or when it has been explicitly invalidated, in which case the error parameter will be nil.
 */
- (void)URLSession:(NSURLSession*)session didBecomeInvalidWithError:(NSError*)error
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, [NSString stringWithFormat:@"URLSession: %@ has been INVALIDATED.", session.sessionDescription]);
}

/*!
 * @brief If an application has received an -application:handleEventsForBackgroundURLSession:completionHandler: message, the session delegate will receive this message to indicate that all messages previously enqueued for this session have been delivered.  At this time it is safe to invoke the previously stored completion handler, or to begin any internal updates that will result in invoking the completion handler.
 * @discussion called after finishing a bg task in bg (while app was not in foreground).
 */
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession*)session
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"URLSession: %@ did FINISH EVENTS.", session.sessionDescription]);
}



/*!
 * @brief If implemented, when a connection level authentication challenge has occurred, this delegate will be given the opportunity to provide authentication credentials to the underlying connection. Some types of authentication will apply to more than one request on a given connection to a server (SSL Server Trust challenges).  If this delegate message is not implemented, the behavior will be to use the default handling, which may involve user interaction.
 */
// TODO Proper handling with UI (alert) involved. Check difference with similar NSURLSessionTaskDelegate method
//- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
//{
//    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagWarn, kDGConverseLogLevel1, [NSString stringWithFormat:@"URLSession: %@ did RECEIVE CREDENTIAL CHALLENGE.", session.sessionDescription]);
//}


#pragma mark - NSURLSessionTaskDelegate Methods

/*!
 * @brief Sent as the last message related to a specific task.  Error may be nil, which implies that no error occurred and this task is complete.
 */
- (void)URLSession:(NSURLSession*)session task:(NSURLSessionTask*)task didCompleteWithError:(NSError*)error
{
	DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    // In general the use of weak-to-strong map tables is not recommended. The strong values for weak keys which get zeroed out continue to be maintained until the map table RESIZES itself (adding or removing elements). The reason of strongify values is that some network ops were dealloc before reaching '- (void)URLSession:task:didCompleteWithError:' in this class, so that the client never was receiving the operation result. Thats why remove the task (key) at this point, to release the operation (value) that otherwise would never be released because an NSURLSession holds all created NSURLSessionTasks (finished/unfinished) until it gets invalidated (- (void)invalidateAndCancel or - (void)finishTasksAndInvalidate).
    [self removeOperationForTask:task];
	if ([op respondsToSelector:_cmd]) {
		[op URLSession:session task:task didCompleteWithError:error];
    }
}

/*!
 * @brief This method is called only for tasks in default and ephemeral sessions. Tasks in background sessions automatically follow redirects.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willPerformHTTPRedirection:(NSHTTPURLResponse *)response newRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLRequest *))completionHandler
{
    DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session task:task willPerformHTTPRedirection:response newRequest:request completionHandler:completionHandler];
    }
}

/*!
 * @brief Sent periodically to notify the delegate of upload progress. This information is also available as properties of the task.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSen totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session task:task didSendBodyData:bytesSent totalBytesSent:totalBytesSen totalBytesExpectedToSend:totalBytesExpectedToSend];
    }
}

/*!
 * @brief Sent if a task requires a new, unopened body stream.  This may be necessary when authentication has failed for any request that involves a body stream.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task needNewBodyStream:(void (^)(NSInputStream *bodyStream))completionHandler
{
    DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session task:task needNewBodyStream:completionHandler];
    }
}

/*!
 * @brief The task has received a request specific authentication challenge. If this delegate is not implemented, the session specific authentication challenge will *NOT* be called and the behavior will be the same as using the default handling disposition.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session task:task didReceiveChallenge:challenge completionHandler:completionHandler];
    }
}

/*!
 * @brief This method is called when a background session task with a delayed start time (as set with the earliestBeginDate property) is ready to start. This delegate method should only be implemented if the request might become stale while waiting for the network load and needs to be replaced by a new request. For loading to continue, the delegate must call the completion handler, passing in a disposition that indicates how the task should proceed. Passing the NSURLSessionDelayedRequestCancel disposition is equivalent to calling cancel on the task directly.
 */
/**
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willBeginDelayedRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLSessionDelayedRequestDisposition disposition, NSURLRequest *newRequest))completionHandler
{
    DGConverseAPINetworkBaseOperation* op = [self.weakTaskToOpMapTable objectForKey:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session task:task willBeginDelayedRequest:request completionHandler:completionHandler];
    } else {
        completionHandler(NSURLSessionDelayedRequestContinueLoading, nil);
    }
}
*/

/*!
 * @brief Sent when a task cannot start the network loading process because the current network connectivity is not available or sufficient for the task's request. This delegate will be called at most one time per task, and is only called if the waitsForConnectivity property in the NSURLSessionConfiguration has been set to YES. This delegate callback will never be called for background sessions, because the waitForConnectivity property is ignored by those sessions.
 */
- (void)URLSession:(NSURLSession *)session taskIsWaitingForConnectivity:(NSURLSessionTask *)task
{
    DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session taskIsWaitingForConnectivity:task];
    }
}


/*!
 * @brief Sent when complete statistics information has been collected for the task.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics
{
    DGConverseAPINetworkBaseOperation* op = [self getOperationForTask:task];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session task:task didFinishCollectingMetrics:metrics];
    }
}

#pragma mark - NSURLSessionDataDelegate Methods
/*!
 * @brief The task has received a response and no further messages will be received until the completion block is called. The disposition allows you to cancel a request or to turn a data task into a download task. This delegate message is optional - if you do not implement it, you can get the response as a property of the task. This method will not be called for background upload tasks (which cannot be converted to download tasks).
 */
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didReceiveResponse:(NSURLResponse*)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
	DGConverseAPINetworkDataOperation* op = (DGConverseAPINetworkDataOperation*)[self getOperationForTask:dataTask];

	if ([op respondsToSelector:_cmd]) {
		[op URLSession:session dataTask:dataTask didReceiveResponse:response completionHandler:completionHandler];
	}
}

/*!
 * @brief Notification that a data task has become a download task. No future messages will be sent to the data task.
 */
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didBecomeDownloadTask:(NSURLSessionDownloadTask*)downloadTask
{
    DGConverseAPINetworkDataOperation* op = (DGConverseAPINetworkDataOperation*)[self getOperationForTask:dataTask];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session dataTask:dataTask didBecomeDownloadTask:downloadTask];
    }
}

/*!
 * @brief Notification that a data task has become a bidirectional stream task.  No future messages will be sent to the data task.  The newly created streamTask will carry the original request and response as properties. For requests that were pipelined, the stream object will only allow reading, and the object will immediately issue a -URLSession:writeClosedForStream:.  Pipelining can be disabled for all requests in a session, or by the NSURLRequest HTTPShouldUsePipelining property. The underlying connection is no longer considered part of the HTTP connection cache and won't count against the total number of connections per host.
 */
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didBecomeStreamTask:(NSURLSessionStreamTask*)streamTask
{
    DGConverseAPINetworkDataOperation* op = (DGConverseAPINetworkDataOperation*)[self getOperationForTask:dataTask];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session dataTask:dataTask didBecomeStreamTask:streamTask];
    }
}

/*!
 * @brief Sent when data is available for the delegate to consume.  It is assumed that the delegate will retain and not copy the data.  As the data may be discontiguous, you should use [NSData enumerateByteRangesUsingBlock:] to access it.
 */
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didReceiveData:(NSData*)data
{
	DGConverseAPINetworkDataOperation* op = (DGConverseAPINetworkDataOperation*)[self getOperationForTask:dataTask];

	if ([op respondsToSelector:_cmd]) {
		[op URLSession:session dataTask:dataTask didReceiveData:data];
	}
}

/*!
 * @brief Invoke the completion routine with a valid NSCachedURLResponse to allow the resulting data to be cached, or pass nil to prevent caching. Note that there is no guarantee that caching will be attempted for a given resource, and you should not rely on this message to receive the resource data.
 */
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask willCacheResponse:(NSCachedURLResponse*)proposedResponse completionHandler:(void (^)(NSCachedURLResponse* cachedResponse))completionHandler
{
	DGConverseAPINetworkDataOperation* op = (DGConverseAPINetworkDataOperation*)[self getOperationForTask:dataTask];

	if ([op respondsToSelector:_cmd]) {
		[op URLSession:session dataTask:dataTask willCacheResponse:proposedResponse completionHandler:completionHandler];
	}
}


#pragma mark - NSURLSessionDownloadDelegate Methods

/*!
 * @brief Sent when a download task that has completed a download.  The delegate should copy or move the file at the given location to a new location as it will be removed when the delegate message returns. URLSession:task:didCompleteWithError: will still be called.
 */
- (void)URLSession:(nonnull NSURLSession*)session downloadTask:(nonnull NSURLSessionDownloadTask*)downloadTask didFinishDownloadingToURL:(nonnull NSURL*)location
{
	DGConverseAPINetworkDownloadOperation* op = (DGConverseAPINetworkDownloadOperation*)[self getOperationForTask:downloadTask];
	if ([op respondsToSelector:_cmd]) {
		[op URLSession:session downloadTask:downloadTask didFinishDownloadingToURL:location];
	}
}

/*!
 * @brief Sent when a download has been resumed. If a download failed with an error, the -userInfo dictionary of the error will contain an NSURLSessionDownloadTaskResumeData key, whose value is the resume data.
 */
- (void)URLSession:(NSURLSession*)session
      downloadTask:(NSURLSessionDownloadTask*)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes
{
    DGConverseAPINetworkDownloadOperation* op = (DGConverseAPINetworkDownloadOperation*)[self getOperationForTask:downloadTask];
    
    if ([op respondsToSelector:_cmd]) {
        [op URLSession:session downloadTask:downloadTask didResumeAtOffset:fileOffset expectedTotalBytes:expectedTotalBytes];
    }
}

/*!
 * @brief Sent periodically to notify the delegate of download progress.
 */
- (void)URLSession:(NSURLSession*)session
				 downloadTask:(NSURLSessionDownloadTask*)downloadTask
				 didWriteData:(int64_t)bytesWritten
			totalBytesWritten:(int64_t)totalBytesWritten
	totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
	DGConverseAPINetworkDownloadOperation* op = (DGConverseAPINetworkDownloadOperation*)[self getOperationForTask:downloadTask];

	if ([op respondsToSelector:_cmd]) {
		[op URLSession:session downloadTask:downloadTask didWriteData:bytesWritten totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
	}
}

@end
