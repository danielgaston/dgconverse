//
//  DGConverseAPIOperationManager.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationManagerHeader.h"

#import "DGConverseAPIConversionImageOperation.h"
#import "DGConverseAPIGroupOperationHeader.h"
#import "DGConverseAPINetworkBaseOperation.h"
#import "DGConverseAPIParseJSONOperation.h"

NS_ASSUME_NONNULL_BEGIN

@class DGConverseAPIOperationManager;
@protocol DGConverseAPIOperationManagerDelegate <NSObject>

- (void)operationManagerDidDealloc:(DGConverseAPIOperationManager *)operationManager;

@end

#pragma mark-

@interface DGConverseAPIOperationManager : NSObject <  DGConverseAPIOperationNetworkManager,
                                                       NSURLSessionDelegate,
                                                       NSURLSessionTaskDelegate,
                                                       NSURLSessionDataDelegate,
                                                       NSURLSessionDownloadDelegate>

@property (nonatomic, weak, nullable) id<DGConverseAPIOperationManagerDelegate> delegate;
@property (nonatomic) NSURLSession* defaultSession;
@property (nonatomic) NSURLSession* backgroundSession;

#pragma mark - CONSTRUCTORS

/*!
 * @brief Initializes Manager with configuration for the default NSURLSession. This configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration.
 * @discussion Default sessions behave much like the shared session (unless you customize them further), but let you obtain data incrementally using a delegate. Ephemeral sessions are similar to default sessions, but they don’t write caches, cookies, or credentials to disk. Once configured, the session object ignores any changes you make to the NSURLSessionConfiguration object. If you need to modify your transfer policies, you must update the session configuration object and use it to create a new NSURLSession object.
 * @warning The configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration.
 */
- (instancetype)initWithDefaultConfiguration:(NSURLSessionConfiguration *)defaultConfiguration
                                    delegate:(nullable id<DGConverseAPIOperationManagerDelegate>)delegate;

/*!
 * @brief Initializes Manager with configuration for the background NSURLSession. This configuration must be a 'background' NSURLSessionConfiguration.
 * @discussion Background sessions let you perform uploads and downloads of content in the background while your app isn’t running. Once configured, the session object ignores any changes you make to the NSURLSessionConfiguration object. If you need to modify your transfer policies, you must update the session configuration object and use it to create a new NSURLSession object.
 * @warning The configuration must be a 'background' NSURLSessionConfiguration.
 */
- (instancetype)initWithBackgroundConfiguration:(NSURLSessionConfiguration *)backgroundConfiguration
                                       delegate:(nullable id<DGConverseAPIOperationManagerDelegate>)delegate;

/*!
 * @brief Initializes Manager with configurations for the default and background NSURLSessions. Default configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration. Background configuration must be a 'background' NSURLSessionConfiguration.
 * @discussion Default sessions behave much like the shared session (unless you customize them further), but let you obtain data incrementally using a delegate. Ephemeral sessions are similar to default sessions, but they don’t write caches, cookies, or credentials to disk. Background sessions let you perform uploads and downloads of content in the background while your app isn’t running. Once configured, the session object ignores any changes you make to the NSURLSessionConfiguration object. If you need to modify your transfer policies, you must update the session configuration object and use it to create a new NSURLSession object.
 * @warning Default configuration must be either 'default' or 'ephemeral' NSURLSessionConfiguration. Background configuration must be a 'background' NSURLSessionConfiguration.
 */
- (instancetype)initWithDefaultConfiguration:(nullable NSURLSessionConfiguration *)defaultConfiguration
                     backgroundConfiguration:(nullable NSURLSessionConfiguration *)backgroundConfiguration
                                    delegate:(nullable id<DGConverseAPIOperationManagerDelegate>)delegate;


#pragma mark - URL BASE REQUESTS

#pragma mark-- DATA

/*!
 * @brief Default RAW Data Fetch. NSData output in "result.data"
 */
- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
								  completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default RAW Data Fetch. NSData output in "result.data"
 */
- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
									progress:(nullable DGConverseAPINetworkProgressBlock)progress
								  completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default RAW Data Fetch. NSData output in "result.data"
 */
- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
                                   useCache:(BOOL)useCache
                                   progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                 completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default RAW Data Fetch. NSData output in "result.data"
 */
- (id<DGConverseAPIGroupBaseOperation>)data:(NSString*)URLString
                                   useCache:(BOOL)useCache
                                     locked:(BOOL)locked
                                   progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                 completion:(DGConverseAPINetworkResultBlock)completion;


#pragma mark-- UPLOAD

/*!
 * @brief Default URLSession Upload
 * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. Upload File must exist in some device path. There is No caching involved in this upload.
 */
- (id<DGConverseAPIGroupBaseOperation>)uploadFile:(NSString*)fileUrlString
												to:(NSString*)toUrlString
										  progress:(nullable DGConverseAPINetworkProgressBlock)progress
										completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default/Background URLSession Upload
 * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. If 'inBackground' Upload will be executing even if the app is suspended. Upload File must exist in some device path. There is No caching involved in this upload.
 * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
 */
- (id<DGConverseAPIGroupBaseOperation>)uploadFile:(NSString*)fileUrlString
												to:(NSString*)toUrlString
									  inBackground:(BOOL)background
										  progress:(nullable DGConverseAPINetworkProgressBlock)progress
										completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default URLSession Upload
 * @discussion Upload only will execute while the app is running, if it goes to background, the upload will be paused. Upload Data must exist in some device path. There is No caching involved in this upload.
 * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
 */
- (id<DGConverseAPIGroupBaseOperation>)uploadData:(NSData*)fileData
												to:(NSString*)toUrlString
										  progress:(nullable DGConverseAPINetworkProgressBlock)progress
										completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- DOWNLOAD

/*!
 * @brief Default JSON Download without Cache
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadJSON:(NSString*)URLString
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPIJSONResultBlock)completion;

/*!
 * @brief Default JSON Download with/without Cache
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadJSON:(NSString*)URLString
											useCache:(BOOL)useCache
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPIJSONResultBlock)completion;

/*!
 * @brief Default JSON Download with/without Cache and permanence
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadJSON:(NSString*)URLString
                                           useCache:(BOOL)useCache
                                             locked:(BOOL)locked
                                           progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                         completion:(DGConverseAPIJSONResultBlock)completion;

/*!
 * @brief Default Image Download with Cache
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadImage:(NSString*)URLString
											 progress:(nullable DGConverseAPINetworkProgressBlock)progress
										   completion:(DGConverseImageResultBlock)completion;

/*!
 * @brief Default Image Download with/without Cache
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadImage:(NSString*)URLString
											 useCache:(BOOL)useCache
											 progress:(nullable DGConverseAPINetworkProgressBlock)progress
										   completion:(DGConverseImageResultBlock)completion;

/*!
 * @brief Default Image Download with/without Cache and permanence
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadImage:(NSString*)URLString
                                            useCache:(BOOL)useCache
                                              locked:(BOOL)locked
                                            progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                          completion:(DGConverseImageResultBlock)completion;

/*!
 * @brief Default URLSession Download and Stored in Temp Folder
 * @discussion Download only will execute while the app is running, if it goes to background, the download will be paused. Downloaded file will be stored in 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadFile:(NSString*)URLString
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default/Background URLSession Download and Stored in Temp Folder.
 * @discussion If not 'inBackground' Download only will execute while the app is running, if it goes to background, the download will be paused. If 'inBackground' Download will be executing even if the app is suspended. Downloaded file will be stored in 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadFile:(NSString*)URLString
										inBackground:(BOOL)background
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Downloaded and Stored in Temp or Documents Folder
 * @discussion If not 'inBackground' Download only will execute while the app is running, if it goes to background, the download will be paused. If 'inBackground' Download will be executing even if the app is suspended. Downloaded file will be stored in 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (id<DGConverseAPIGroupBaseOperation>)downloadFile:(NSString*)URLString
										inBackground:(BOOL)background
								   toDocumentsFolder:(BOOL)toDocuments
											progress:(nullable DGConverseAPINetworkProgressBlock)progress
										  completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark - CONCURRENT REQUESTS

/*!
 * @brief Runs multiple defined operations concurrently
 */
- (id<DGConverseAPIGroupBaseOperation>)runConcurrentOperations:(NSArray<DGConverseAPIBaseOperation*>*)operations;

#pragma mark - HTTP BASE REQUESTS

#pragma mark-- GET

- (id<DGConverseAPIGroupBaseOperation>)GET:(NSString*)URLString
								 parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								 completion:(DGConverseAPINetworkResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)GET:(NSString*)URLString
								 parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								   progress:(nullable DGConverseAPINetworkProgressBlock)progress
								 completion:(DGConverseAPINetworkResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)GET:(NSString*)URLString
                                parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
                                  useCache:(BOOL)useCache
                                  progress:(nullable DGConverseAPINetworkProgressBlock)progress
                                completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- HEAD

- (id<DGConverseAPIGroupBaseOperation>)HEAD:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								  completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- POST

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								  completion:(DGConverseAPINetworkResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
									progress:(nullable DGConverseAPINetworkProgressBlock)progress
								  completion:(DGConverseAPINetworkResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
				   constructingBodyWithBlock:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body
								  completion:(DGConverseAPINetworkResultBlock)completion;

- (id<DGConverseAPIGroupBaseOperation>)POST:(NSString*)URLString
								  parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
				   constructingBodyWithBlock:(nullable void (^)(id<DGConverseAPIRequestMultipartFormData> formData))body
									progress:(nullable DGConverseAPINetworkProgressBlock)progress
								  completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- PUT

- (id<DGConverseAPIGroupBaseOperation>)PUT:(NSString*)URLString
								 parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								 completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- PATCH

- (id<DGConverseAPIGroupBaseOperation>)PATCH:(NSString*)URLString
								   parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
								   completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- DELETE

- (id<DGConverseAPIGroupBaseOperation>)DELETE:(NSString*)URLString
									parameters:(nullable id<DGConverseAPIRequestParameters>)parameters
									completion:(DGConverseAPINetworkResultBlock)completion;

#pragma mark-- BACKGROUND TASK Helper

- (void)handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)(void))completionHandler;

#pragma mark-- QUEUE Helper

- (void)cancelAllRequests;
- (NSUInteger)currentConcurrentOpsInQueues;

NS_ASSUME_NONNULL_END

@end
