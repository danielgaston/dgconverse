//
//  DGConverseAPIQueueManager.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIQueueManager.h"
#import "DGConverseAPIDebugUtils.h"
#import "DGConverseAPIGroupBaseOperation.h"
#import "DGConverseAPIRequest.h"

#define kOperationCountKeyPath @"operationCount"
#define kMaxConcurrentOperationCountKeyPath @"maxConcurrentOperationCount"
#define kSuspendedKeyPath @"suspended"

/**
 @warning: reducing MAX concurrent operations (i.e. 2), make NETWORK Operations (i.e.Download) no to be finished, properly cancelled actually. And this BLOCKS the whole queue as the operations dont get released!! Leave this values to SystemMax for now. The problem needs to be addressed on operations, either APINetworkBase or APIBase to fix how to FINISH an operation which is not READY nor EXECUTING. Seems that cancelling an op
 */
#define kDataQueueMaxConcurrentOperationDefaultMode NSOperationQueueDefaultMaxConcurrentOperationCount
#define kBackgroundQueueMaxConcurrentOperationDefaultMode NSOperationQueueDefaultMaxConcurrentOperationCount

@interface DGConverseAPIQueueManager ()

@property (nonatomic, strong) NSOperationQueue* dataQueue;
@property (nonatomic, strong) NSOperationQueue* backgroundQueue;
@property (nonatomic, strong) NSOperationQueue* computeQueue;

@property (nonatomic, strong) NSHashTable<id<DGConverseAPIGroupBaseOperation>> *weakGroupBaseOpHashTable;

@property (nonatomic, strong) dispatch_queue_t hashTableThreadSafeQueue;

@end

@implementation DGConverseAPIQueueManager

#pragma mark - View LifeCycle Methods

- (instancetype)init
{
	self = [super init];

	if (self) {
        [self initializePreProcessing];

		[self initializeDataOperationQueue];
		[self initializeBackgroundOperationQueue];
		[self initializeComputeOperationQueue];

		[self initializeObservers];
	}

	return self;
}

- (void)dealloc
{
	[self deinitializeObservers];
}

#pragma mark - Public Methods (Thread Safe - GroupBaseOperation Table)

- (void)storeGroupOperation:(id<DGConverseAPIGroupBaseOperation>)groupOp
{
    dispatch_sync(self.hashTableThreadSafeQueue, ^{
        if (groupOp)
            [self.weakGroupBaseOpHashTable addObject:groupOp];
    });
}

- (void)cancelGroupOperationIfMatchesRequest:(id<DGConverseAPIRequest>)request
{
    dispatch_sync(self.hashTableThreadSafeQueue, ^{
        for (id<DGConverseAPIGroupBaseOperation> groupOp in [self.weakGroupBaseOpHashTable.allObjects reverseObjectEnumerator]) {
            if ([groupOp containsRequest:request]) {
                [groupOp cancelAndDiscard];
                NSString *queueLogString = [NSString stringWithFormat:@"Duplicated Running Request Discarded: %@", request.URL];
                DGConverseLog(kDGConverseLogTypeQueue, kDGConverseLogFlagInfo, kDGConverseLogLevel0, queueLogString);
            }
        }
    });
}

#pragma mark - Public Methods

- (NSUInteger)currentConcurrentOpsInQueue:(DGConverseAPIQueueType)type
{
    NSAssert(type != kDGConverseAPIQueueALL, @"Please do not use ALL type for a getter");
    
    NSOperationQueue* queue = [[self queuesForType:type] firstObject];
    return queue.operationCount;
}

- (NSInteger)maxConcurrentOpsInQueue:(DGConverseAPIQueueType)type
{
	NSAssert(type != kDGConverseAPIQueueALL, @"Please do not use ALL type for a getter");

	NSOperationQueue* queue = [[self queuesForType:type] firstObject];
	return queue.maxConcurrentOperationCount;
}

- (void)setMaxConcurrentOps:(NSInteger)maxConcurrentOps inQueue:(DGConverseAPIQueueType)type
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue setMaxConcurrentOperationCount:maxConcurrentOps];
}

- (NSQualityOfService)qualityOfServiceInQueue:(DGConverseAPIQueueType)type
{
	NSAssert(type != kDGConverseAPIQueueALL, @"Please do not use ALL type for a getter");

	NSOperationQueue* queue = [[self queuesForType:type] firstObject];
	return queue.qualityOfService;
}

- (void)setQualityOfService:(NSQualityOfService)qos inQueue:(DGConverseAPIQueueType)type
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue setQualityOfService:qos];
}

- (void)addOperation:(NSOperation*)op inQueue:(DGConverseAPIQueueType)type;
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue addOperation:op];
}

- (void)pauseAllOperationsInQueue:(DGConverseAPIQueueType)type
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue setSuspended:YES];
}

- (void)resumeAllOperationsInQueue:(DGConverseAPIQueueType)type
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue setSuspended:NO];
}

- (void)cancelAllOperationsInQueue:(DGConverseAPIQueueType)type
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue cancelAllOperations];
}

- (void)waitUntilAllDataOpsAreFinishedInQueue:(DGConverseAPIQueueType)type
{
	for (NSOperationQueue* queue in [self queuesForType:type])
		[queue waitUntilAllOperationsAreFinished];
}

#pragma mark - Initialialization Methods

- (void)initializePreProcessing
{
    self.weakGroupBaseOpHashTable = [NSHashTable weakObjectsHashTable];
    self.hashTableThreadSafeQueue = dispatch_queue_create("com.danielgaston.converse.queue_manager_serial_queue", DISPATCH_QUEUE_SERIAL);
}

- (void)initializeDataOperationQueue
{
	self.dataQueue = [[NSOperationQueue alloc] init];
	[self.dataQueue setName:@"Data Queue"];
	// https://developer.apple.com/documentation/foundation/nsqualityofservice
	[self.dataQueue setQualityOfService:NSQualityOfServiceUserInitiated];
	[self.dataQueue setMaxConcurrentOperationCount:kDataQueueMaxConcurrentOperationDefaultMode];
}

- (void)initializeBackgroundOperationQueue
{
	self.backgroundQueue = [[NSOperationQueue alloc] init];
	[self.backgroundQueue setName:@"Background Queue"];
	// https://developer.apple.com/documentation/foundation/nsqualityofservice
	[self.backgroundQueue setQualityOfService:NSQualityOfServiceUtility];
	[self.backgroundQueue setMaxConcurrentOperationCount:kBackgroundQueueMaxConcurrentOperationDefaultMode];
}

- (void)initializeComputeOperationQueue
{
	self.computeQueue = [[NSOperationQueue alloc] init];
	[self.computeQueue setName:@"Compute Queue"];
	// https://developer.apple.com/documentation/foundation/nsqualityofservice
	[self.computeQueue setQualityOfService:NSQualityOfServiceDefault];
	[self.computeQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
}

- (void)initializeObservers
{
	for (NSOperationQueue* queue in [self queuesForType:kDGConverseAPIQueueALL]) {
		[queue addObserver:self forKeyPath:kOperationCountKeyPath options:NSKeyValueObservingOptionNew context:nil];
		[queue addObserver:self forKeyPath:kMaxConcurrentOperationCountKeyPath options:NSKeyValueObservingOptionNew context:nil];
		[queue addObserver:self forKeyPath:kSuspendedKeyPath options:NSKeyValueObservingOptionNew context:nil];
	}
}

- (void)deinitializeObservers
{
	for (NSOperationQueue* queue in [self queuesForType:kDGConverseAPIQueueALL]) {
		[queue removeObserver:self forKeyPath:kOperationCountKeyPath];
		[queue removeObserver:self forKeyPath:kMaxConcurrentOperationCountKeyPath];
		[queue removeObserver:self forKeyPath:kSuspendedKeyPath];
	}
}

#pragma mark - Observation Methods

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void*)context
{
	NSOperationQueue* queue = (NSOperationQueue*)object;
	NSMutableString* queueLogString = @"".mutableCopy;
	[queueLogString appendString:[NSString stringWithFormat:@"\n\n<<<<---- %@ ----->>>>", queue.name]];
	[queueLogString appendString:[NSString stringWithFormat:@"\n%@ Max Concurrent Ops: %@", queue.name, [self verboseMaxConcurrentOps:queue.maxConcurrentOperationCount]]];
	[queueLogString appendString:[NSString stringWithFormat:@"\n%@ Cur Concurrent Ops: %lu", queue.name, (unsigned long)queue.operationCount]];
	[queueLogString appendString:[NSString stringWithFormat:@"\n%@ Quality of Service: %@", queue.name, [self verboseQualityOfService:queue.qualityOfService]]];
	[queueLogString appendString:[NSString stringWithFormat:@"\n%@ Suspended: %@", queue.name, queue.isSuspended ? @"YES" : @"NO"]];
	[queueLogString appendString:[NSString stringWithFormat:@"\n%@ Operations: %@\n", queue.name, queue.operations]];

	DGConverseLog(kDGConverseLogTypeQueue, kDGConverseLogFlagInfo, kDGConverseLogLevel0, queueLogString);

	[self performSelectorOnMainThread:@selector(updateNetworkIndicator) withObject:nil waitUntilDone:NO];
}

#pragma mark - Helper

- (void)updateNetworkIndicator
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = (self.dataQueue.operationCount || self.backgroundQueue.operationCount);
}

- (NSArray<NSOperationQueue*>*)queuesForType:(DGConverseAPIQueueType)type
{
	NSMutableArray<NSOperationQueue*>* queues = @[].mutableCopy;
	switch (type) {
	case kDGConverseAPIQueueData: {
		[queues addObject:self.dataQueue];
		break;
	}
	case kDGConverseAPIQueueBackground: {
		[queues addObject:self.backgroundQueue];
		break;
	}
	case kDGConverseAPIQueueCompute: {
		[queues addObject:self.computeQueue];
		break;
	}
	case kDGConverseAPIQueueALL: {
		[queues addObject:self.dataQueue];
		[queues addObject:self.backgroundQueue];
		[queues addObject:self.computeQueue];
		break;
	}
	}

	return queues;
}

- (NSString*)verboseMaxConcurrentOps:(NSInteger)maxOps
{
	switch (maxOps) {
	case NSOperationQueueDefaultMaxConcurrentOperationCount:
		return @"System Max";
	default:
		return [[NSNumber numberWithInteger:maxOps] stringValue];
	}
}

- (NSString*)verboseQualityOfService:(NSQualityOfService)quality
{
	switch (quality) {
	case NSQualityOfServiceUserInteractive:
		return @"User Interactive";
	case NSQualityOfServiceUserInitiated:
		return @"User Initiated";
		;
	case NSQualityOfServiceUtility:
		return @"Utility";
	case NSQualityOfServiceBackground:
		return @"Background";
		;
	case NSQualityOfServiceDefault:
		return @"Default";
	default:
		return nil;
	}
}

@end
