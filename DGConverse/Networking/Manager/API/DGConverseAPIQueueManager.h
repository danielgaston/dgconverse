//
//  DGConverseAPIQueueManager.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DGConverseAPIGroupBaseOperation;
@protocol DGConverseAPIRequest;

typedef NS_ENUM(NSInteger, DGConverseAPIQueueType) {
	kDGConverseAPIQueueData,
	kDGConverseAPIQueueBackground,
	kDGConverseAPIQueueCompute,
	kDGConverseAPIQueueALL
};

@interface DGConverseAPIQueueManager : NSObject

- (void)storeGroupOperation:(id<DGConverseAPIGroupBaseOperation>)groupOp;

/*!
 * @brief Cancels all suboperations of a group operation, if any network suboperation shares the SAME request (URLRequest), and makes sure the cancelled ones does not execute its completion block.
 * @discussion check NSOperation (Discardable) for furher details
 */
- (void)cancelGroupOperationIfMatchesRequest:(id<DGConverseAPIRequest>)request;

- (NSUInteger)currentConcurrentOpsInQueue:(DGConverseAPIQueueType)type;

- (NSInteger)maxConcurrentOpsInQueue:(DGConverseAPIQueueType)type;
- (void)setMaxConcurrentOps:(NSInteger)maxConcurrentOps inQueue:(DGConverseAPIQueueType)type;

- (NSQualityOfService)qualityOfServiceInQueue:(DGConverseAPIQueueType)type;
- (void)setQualityOfService:(NSQualityOfService)qos inQueue:(DGConverseAPIQueueType)type;

- (void)addOperation:(NSOperation*)op inQueue:(DGConverseAPIQueueType)type;

/*!
 * @brief Prevents the queue from starting any queued operations
 * @warning already executing operations continue to execute. You may continue to add operations to a queue that is suspended but those operations are not scheduled for execution until the queue is resumed
 */
- (void)pauseAllOperationsInQueue:(DGConverseAPIQueueType)type;

/*!
 * @brief Prevents the queue from starting any queued operations
 * @warning already executing operations continue to execute. You may continue to add operations to a queue that is suspended but those operations are not scheduled for execution until the queue is resumed
 */
- (void)resumeAllOperationsInQueue:(DGConverseAPIQueueType)type;

/*!
 * @brief Cancels all queued and executing operations.
 */
- (void)cancelAllOperationsInQueue:(DGConverseAPIQueueType)type;

/*!
 * @brief Blocks the queue thread until all pending and executing operations are finished
 * @warning During this time, add operations is not permitted.
 */
- (void)waitUntilAllDataOpsAreFinishedInQueue:(DGConverseAPIQueueType)type;

@end

NS_ASSUME_NONNULL_END
