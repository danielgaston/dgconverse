//
//  DGConverseAPIGroupOperationHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPIGroupOperationHeader_h
#define DGConverseAPIGroupOperationHeader_h

#import "DGConverseAPIGroupBaseOperation.h"
#import "DGConverseAPIGroupGeneralOperation.h"
#import "DGConverseAPIGroupDataOperation.h"
#import "DGConverseAPIGroupImageOperation.h"

#endif /* DGConverseAPIGroupOperationHeader_h */
