//
//  DGConverseAPIGroupDownloadOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"

#import "DGConverseAPIOperationManagerHeader.h"
#import "DGConverseAPINetworkDownloadOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIGroupDownloadOperation : DGConverseAPIGroupBaseOperation

/*!
 * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
 * @discussion Download only will execute while the app is running, if it goes to background, the download will be paused. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download.
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (void)runWithURLString:(NSString *)URLString
     destinationFolder:(NSSearchPathDirectory)directory
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
            completion:(DGConverseAPINetworkResultBlock)completion;


/*!
 * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
 * @discussion Download will be executing in background even if the app is suspended. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (void)runInBackgroundWithURLString:(NSString *)URLString
                 destinationFolder:(NSSearchPathDirectory)directory
                          progress:(nullable DGConverseAPINetworkProgressBlock)progress
                        completion:(DGConverseAPINetworkResultBlock)completion;

@end

NS_ASSUME_NONNULL_END
