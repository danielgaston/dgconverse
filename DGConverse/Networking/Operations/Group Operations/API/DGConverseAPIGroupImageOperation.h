//
//  DGConverseAPIGroupImageOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"

#import "DGConverseAPIOperationManagerHeader.h"
#import "DGConverseAPINetworkDataOperation.h"
#import "DGConverseAPIConversionImageOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIGroupImageOperation : DGConverseAPIGroupBaseOperation

- (void)runWithURLString:(NSString *)URLString
                useCache:(BOOL)useCache
                  locked:(BOOL)locked
                progress:(nullable DGConverseAPINetworkProgressBlock)progress
              completion:(DGConverseImageResultBlock)completion;

@end

NS_ASSUME_NONNULL_END
