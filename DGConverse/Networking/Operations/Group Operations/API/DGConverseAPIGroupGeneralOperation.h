//
//  DGConverseAPIGroupGeneralOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"

#import "DGConverseAPIOperationManager.h"
#import "DGConverseAPIOperationQueue.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIGroupGeneralOperation : DGConverseAPIGroupBaseOperation

/*!
 * @brief Runs concurrent operations on an internal queue (AdHoc)
 */
- (void)runAdHocWithOperations:(NSArray<NSOperation*>*)operations;

/*!
 * @brief Runs concurrent operations on a app manager queue (Shared)
 */
- (void)runWithOperations:(NSArray<DGConverseAPIBaseOperation*>*)operations;

@end

NS_ASSUME_NONNULL_END
