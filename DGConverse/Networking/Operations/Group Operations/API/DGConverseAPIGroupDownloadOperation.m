//
//  DGConverseAPIGroupDownloadOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupDownloadOperation.h"

@implementation DGConverseAPIGroupDownloadOperation

- (void)runWithURLString:(NSString *)URLString
     destinationFolder:(NSSearchPathDirectory)directory
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
            completion:(DGConverseAPINetworkResultBlock)completion
{
    // No need to make use of cache ops
    DGConverseAPIDownloadRequest* networkRequest = [[DGConverseAPIDownloadRequest alloc] initWithURLString:URLString];
    DGConverseAPINetworkDownloadOperation* networkOp = [[DGConverseAPINetworkDownloadOperation alloc] initWithRequest:networkRequest
                                                                                                      operationDelegate:self.manager
                                                                                                      destinationFolder:directory
                                                                                                               progress:progress
                                                                                                             completion:completion];
    
    [self processRequest:networkRequest];
    
    [self storeSubOperation:networkOp];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
}

- (void)runInBackgroundWithURLString:(NSString *)URLString
                 destinationFolder:(NSSearchPathDirectory)directory
                          progress:(nullable DGConverseAPINetworkProgressBlock)progress
                        completion:(DGConverseAPINetworkResultBlock)completion
{
    // No need to make use of cache ops
    DGConverseAPIDownloadRequest* networkRequest = [[DGConverseAPIDownloadRequest alloc] initWithURLString:URLString];
    DGConverseAPINetworkDownloadOperation* networkOp = [[DGConverseAPINetworkDownloadOperation alloc] initWithBackgroundRequest:networkRequest
                                                                                                                operationDelegate:self.manager
                                                                                                                destinationFolder:directory
                                                                                                                         progress:progress
                                                                                                                       completion:completion];
    [self processRequest:networkRequest];
    
    [self storeSubOperation:networkOp];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
}

@end
