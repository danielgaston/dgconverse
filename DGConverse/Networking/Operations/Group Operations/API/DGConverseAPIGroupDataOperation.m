//
//  DGConverseAPIGroupDataOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupDataOperation.h"
#import "DGConverseAPICacheJSONOperation.h"
#import "DGConverseAPIAdapterBaseOperation.h"

@implementation DGConverseAPIGroupDataOperation

- (void)runWithNetworkOperation:(DGConverseAPINetworkDataOperation *)networkOp
{
    // Network
    [self storeSubOperation:networkOp];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
}

- (void)runWithNetworkOperation:(DGConverseAPINetworkDataOperation *)networkOp
                 parseOperation:(DGConverseAPIParseDataOperation *)parseOp
{
    // Network <- Parse
    DGConverseAPIAdapterBaseOperation* NPAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:networkOp toOperation:parseOp];
    
    [NPAdapterOp addDependency:networkOp];
    [parseOp addDependency:NPAdapterOp];
    
    [self storeSubOperation:networkOp];
    [self storeSubOperation:NPAdapterOp];
    [self storeSubOperation:parseOp];
    
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
    [self.manager.queueManager addOperation:NPAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:parseOp inQueue:kDGConverseAPIQueueCompute];
}

- (void)runWithReadCacheOperation:(DGConverseAPICacheBaseOperation *)readCacheOp
                 networkOperation:(DGConverseAPINetworkDataOperation *)networkOp
              writeCacheOperation:(DGConverseAPICacheBaseOperation *)writeCacheOp
{
    // Cache (read) <- Network
    DGConverseAPIAdapterBaseOperation *CNAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:readCacheOp toOperation:networkOp];
    
    // Network <- Cache (write)
    DGConverseAPIAdapterBaseOperation* NCAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:networkOp toOperation:writeCacheOp];
    
    [CNAdapterOp addDependency:readCacheOp];
    [networkOp addDependency:CNAdapterOp];
    [NCAdapterOp addDependency:networkOp];
    [writeCacheOp addDependency:NCAdapterOp];
    
    [self storeSubOperation:readCacheOp];
    [self storeSubOperation:CNAdapterOp];
    [self storeSubOperation:networkOp];
    [self storeSubOperation:NCAdapterOp];
    [self storeSubOperation:writeCacheOp];
    
    [self.manager.queueManager addOperation:readCacheOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:CNAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
    [self.manager.queueManager addOperation:NCAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:writeCacheOp inQueue:kDGConverseAPIQueueCompute];
}

- (void)runWithReadCacheOperation:(DGConverseAPICacheBaseOperation *)readCacheOp
                 networkOperation:(DGConverseAPINetworkDataOperation *)networkOp
              writeCacheOperation:(DGConverseAPICacheBaseOperation *)writeCacheOp
                   parseOperation:(DGConverseAPIParseDataOperation *)parseOp
{
    // Cache (read) <- Network
    DGConverseAPIAdapterBaseOperation* CNAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:readCacheOp toOperation:networkOp];
    
    // Network <- Cache (write)
    DGConverseAPIAdapterBaseOperation* NCAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:networkOp toOperation:writeCacheOp];
    
    // Cache (write) <- Parse
    DGConverseAPIAdapterBaseOperation* CPAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:writeCacheOp toOperation:parseOp];
    
    [CNAdapterOp addDependency:readCacheOp];
    [networkOp addDependency:CNAdapterOp];
    [NCAdapterOp addDependency:networkOp];
    [writeCacheOp addDependency:NCAdapterOp];
    [CPAdapterOp addDependency:writeCacheOp];
    [parseOp addDependency:CPAdapterOp];
    
    [self storeSubOperation:readCacheOp];
    [self storeSubOperation:CNAdapterOp];
    [self storeSubOperation:networkOp];
    [self storeSubOperation:NCAdapterOp];
    [self storeSubOperation:writeCacheOp];
    [self storeSubOperation:CPAdapterOp];
    [self storeSubOperation:parseOp];
    
    [self.manager.queueManager addOperation:readCacheOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:CNAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
    [self.manager.queueManager addOperation:NCAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:writeCacheOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:CPAdapterOp inQueue:kDGConverseAPIQueueCompute];
    [self.manager.queueManager addOperation:parseOp inQueue:kDGConverseAPIQueueCompute];
}

@end
