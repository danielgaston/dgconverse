//
//  DGConverseAPIGroupBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"
#import "DGConverseAPIBaseOperation.h"
#import "DGConverseAPIOperationManagerHeader.h"
#import "DGConverseAPIRequest.h"

@interface DGConverseAPIGroupBaseOperation ()

@property (nonatomic) NSUUID *groupUUID;
@property (nonatomic, strong) NSHashTable<NSOperation *> *weakOpTable;
@property (nonatomic, strong) NSHashTable<id<DGConverseAPIRequest>> *weakOpRequestTable;

@end

#pragma mark -

@implementation DGConverseAPIGroupBaseOperation

#pragma mark - Object Life cycle Methods

- (instancetype)initWithManager:(id<DGConverseAPIOperationNetworkManager>)manager
{
    self = [super init];
    if (self) {
        self.groupUUID = [NSUUID UUID];
        self.weakOpTable = [NSHashTable weakObjectsHashTable];
        self.weakOpRequestTable = [NSHashTable weakObjectsHashTable];
        self.manager = manager;
        [self.manager.queueManager storeGroupOperation:self];
    }
    return self;
}


#pragma mark - DGConverseAPIGroupBaseOperation Delegate Methods

- (NSUUID *)groupUUID
{
    return self.groupUUID;
}

- (void)storeSubOperation:(nullable NSOperation *)op
{
    [self.weakOpTable addObject:op];
}

- (void)processRequest:(id<DGConverseAPIRequest>)request
{
    if (request) {
        [self.manager.queueManager cancelGroupOperationIfMatchesRequest:request];
        [self storeRequest:request];
    }
}

- (void)storeRequest:(id<DGConverseAPIRequest>)request
{
    if (request)
        [self.weakOpRequestTable addObject:request];
}

- (BOOL)containsRequest:(id<DGConverseAPIRequest>)request
{
    for (id<DGConverseAPIRequest> storedRequest in [self.weakOpRequestTable.allObjects reverseObjectEnumerator]) {
        if ([storedRequest isEqualToRequest:request])
            return YES;
    }
    return NO;
}

- (void)cancel
{
    for (NSOperation *op in [self.weakOpTable.allObjects reverseObjectEnumerator]) {
        [op cancel];
    }
}

- (void)cancelAndDiscard
{
    for (DGConverseAPIBaseOperation *op in [self.weakOpTable.allObjects reverseObjectEnumerator]) {
        [op setIsDiscarded:YES];
        [op cancel];
    }
}

@end
