//
//  DGConverseAPIGroupBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DGConverseAPIRequest;
@protocol DGConverseAPIOperationNetworkManager;
@protocol DGConverseAPIGroupBaseOperation <NSObject>

- (NSUUID *)groupUUID;
- (void)cancel;
- (void)cancelAndDiscard;
- (void)storeSubOperation:(nullable NSOperation *)op;
- (void)processRequest:(id<DGConverseAPIRequest>)request;
- (void)storeRequest:(id<DGConverseAPIRequest>)request;
- (BOOL)containsRequest:(id<DGConverseAPIRequest>)request;

@end

#pragma mark -

@interface DGConverseAPIGroupBaseOperation : NSObject <DGConverseAPIGroupBaseOperation>

@property (nonatomic, weak) id<DGConverseAPIOperationNetworkManager> manager;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithManager:(id<DGConverseAPIOperationNetworkManager>)manager NS_DESIGNATED_INITIALIZER;

- (NSUUID *)groupUUID;
- (void)cancel;
- (void)cancelAndDiscard;
- (void)storeSubOperation:(nullable NSOperation *)op;
- (void)processRequest:(id<DGConverseAPIRequest>)request;
- (void)storeRequest:(id<DGConverseAPIRequest>)request;
- (BOOL)containsRequest:(id<DGConverseAPIRequest>)request;

@end

NS_ASSUME_NONNULL_END
