//
//  DGConverseAPIGroupImageOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupImageOperation.h"

#import "DGConverseAPICacheImageOperation.h"
#import "DGConverseAPIAdapterBaseOperation.h"

@implementation DGConverseAPIGroupImageOperation

- (void)runWithURLString:(NSString *)URLString
                useCache:(BOOL)useCache
                  locked:(BOOL)locked
                progress:(nullable DGConverseAPINetworkProgressBlock)progress
              completion:(DGConverseImageResultBlock)completion
{
    if (useCache) {
        
        // Cache <- Network
        
        DGConverseAPICacheImageOperation *cacheReadOp = [[DGConverseAPICacheImageOperation alloc] initWithURLString:URLString locked:locked operationDelegate:self.manager];
        
        DGConverseAPIDownloadImageRequest* networkRequest = [[DGConverseAPIDownloadImageRequest alloc] initWithURLString:URLString];
        DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest operationDelegate:self.manager progress:progress completion:nil];
        
        
        DGConverseAPIAdapterBaseOperation* CNAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:cacheReadOp toOperation:networkOp];
        
        // Network <- Cache
        
        DGConverseAPICacheImageOperation *cacheWriteOp = [[DGConverseAPICacheImageOperation alloc] initWithURLString:URLString locked:locked operationDelegate:self.manager];
        DGConverseAPIAdapterBaseOperation* NCAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:networkOp toOperation:cacheWriteOp];
        
        // Cache <- Conversion
        
        DGConverseAPIConversionImageOperation *conversionOp = [[DGConverseAPIConversionImageOperation alloc] initWithOperationDelegate:self.manager completion:completion];
        DGConverseAPIAdapterBaseOperation* CCAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:cacheWriteOp toOperation:conversionOp];
        
        // Dependencies
        
        [CNAdapterOp addDependency:cacheReadOp];
        [networkOp addDependency:CNAdapterOp];
        
        [NCAdapterOp addDependency:networkOp];
        [cacheWriteOp addDependency:NCAdapterOp];
        
        [CCAdapterOp addDependency:cacheWriteOp];
        [conversionOp addDependency:CCAdapterOp];
        
        [self storeSubOperation:cacheReadOp];
        [self storeSubOperation:CNAdapterOp];
        [self storeSubOperation:networkOp];
        [self storeSubOperation:NCAdapterOp];
        [self storeSubOperation:cacheWriteOp];
        [self storeSubOperation:CCAdapterOp];
        [self storeSubOperation:conversionOp];
        
        [self.manager.queueManager addOperation:cacheReadOp inQueue:kDGConverseAPIQueueCompute];
        [self.manager.queueManager addOperation:CNAdapterOp inQueue:kDGConverseAPIQueueCompute];
        [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
        [self.manager.queueManager addOperation:NCAdapterOp inQueue:kDGConverseAPIQueueCompute];
        [self.manager.queueManager addOperation:cacheWriteOp inQueue:kDGConverseAPIQueueCompute];
        [self.manager.queueManager addOperation:CCAdapterOp inQueue:kDGConverseAPIQueueCompute];
        [self.manager.queueManager addOperation:conversionOp inQueue:kDGConverseAPIQueueCompute];
    } else {
        
        // Network <- Conversion
        
        DGConverseAPIDownloadImageRequest* networkRequest = [[DGConverseAPIDownloadImageRequest alloc] initWithURLString:URLString];
        DGConverseAPINetworkDataOperation* networkOp = [[DGConverseAPINetworkDataOperation alloc] initWithRequest:networkRequest operationDelegate:self.manager progress:progress completion:nil];
        
        DGConverseAPIConversionImageOperation *conversionOp = [[DGConverseAPIConversionImageOperation alloc] initWithOperationDelegate:self.manager completion:completion];
        DGConverseAPIAdapterBaseOperation* NCAdapterOp = [[DGConverseAPIAdapterBaseOperation alloc] initWithFromOperation:networkOp toOperation:conversionOp];
        
        [NCAdapterOp addDependency:networkOp];
        [conversionOp addDependency:NCAdapterOp];
        
        [self storeSubOperation:networkOp];
        [self storeSubOperation:NCAdapterOp];
        [self storeSubOperation:conversionOp];
        
        [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
        [self.manager.queueManager addOperation:NCAdapterOp inQueue:kDGConverseAPIQueueCompute];
        [self.manager.queueManager addOperation:conversionOp inQueue:kDGConverseAPIQueueCompute];
    }
}

@end
