//
//  DGConverseAPIGroupGeneralOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupGeneralOperation.h"

@interface DGConverseAPIGroupGeneralOperation ()

@property (nonatomic, strong) DGConverseAPIOperationQueue* internalQueue;

@end

@implementation DGConverseAPIGroupGeneralOperation

#pragma mark - Public Methods

- (void)runAdHocWithOperations:(NSArray<NSOperation*>*)operations
{
    _internalQueue = [[DGConverseAPIOperationQueue alloc] init];
    _internalQueue.qualityOfService = NSQualityOfServiceUserInitiated;
    _internalQueue.suspended = YES;
    _internalQueue.name = @"Internal Generic Purpose Queue";

    for (NSOperation* operation in operations) {
        [_internalQueue addOperation:operation];
    }
    
    self.internalQueue.suspended = NO;
}

- (void)runWithOperations:(NSArray<DGConverseAPIBaseOperation*>*)operations
{
    for (DGConverseAPIBaseOperation* operation in operations) {
        [self storeSubOperation:operation];
        [self.manager.queueManager addOperation:operation inQueue:kDGConverseAPIQueueCompute];
    }
}

#pragma mark - Overriden Methods

- (void)cancel
{
    // affects to internal queue (AdHoc) coming from 'runWithOperations'
    [self.internalQueue cancelAllOperations];
    
    // affects to computational queue (Shared) coming from 'runWithManager:operations'
    [super cancel];
}

@end

