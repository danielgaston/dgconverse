//
//  DGConverseAPIGroupUploadOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"

#import "DGConverseAPIOperationManagerHeader.h"
#import "DGConverseAPINetworkUploadOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIGroupUploadOperation : DGConverseAPIGroupBaseOperation

/*!
 * @brief Default Upload Task
 * @discussion Upload only will execute while the app is running, if it goes to background, it will be paused. There is No caching involved in this upload.
 */
- (void)runWithFileURL:(NSString *)fileURLString
                 toURL:(NSString *)toURLString
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
            completion:(DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Default Upload Task
 * @discussion Upload only will execute while the app is running, if it goes to background, it will be paused. There is No caching involved in this upload.
 */
- (void)runWithFileData:(NSData *)fileData
                 toURL:(NSString *)toURLString
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
            completion:(DGConverseAPINetworkResultBlock)completion;


/*!
 * @brief Background Upload Task
 * @discussion Upload only will execute while the app is running, if it goes to background, it will be paused. There is No caching involved in this upload.
 * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
 */
- (void)runInBackgroundWithFileURL:(NSString *)fileURLString
                             toURL:(NSString *)toURLString
                          progress:(nullable DGConverseAPINetworkProgressBlock)progress
                        completion:(DGConverseAPINetworkResultBlock)completion;

@end

NS_ASSUME_NONNULL_END
