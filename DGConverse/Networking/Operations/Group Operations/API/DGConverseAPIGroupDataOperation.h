//
//  DGConverseAPIGroupDataOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupBaseOperation.h"

#import "DGConverseAPIOperationManager.h"
#import "DGConverseAPINetworkDataOperation.h"
#import "DGConverseAPIParseJSONOperation.h"
#import "DGConverseAPICacheBaseOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIGroupDataOperation : DGConverseAPIGroupBaseOperation

- (void)runWithNetworkOperation:(DGConverseAPINetworkDataOperation *)networkOp;

- (void)runWithNetworkOperation:(DGConverseAPINetworkDataOperation *)networkOp
                 parseOperation:(DGConverseAPIParseDataOperation *)parseOp;

- (void)runWithReadCacheOperation:(DGConverseAPICacheBaseOperation *)readCacheOp
                 networkOperation:(DGConverseAPINetworkDataOperation *)networkOp
              writeCacheOperation:(DGConverseAPICacheBaseOperation *)writeCacheOp;

- (void)runWithReadCacheOperation:(DGConverseAPICacheBaseOperation *)readCacheOp
                 networkOperation:(DGConverseAPINetworkDataOperation *)networkOp
              writeCacheOperation:(DGConverseAPICacheBaseOperation *)writeCacheOp
                   parseOperation:(DGConverseAPIParseDataOperation *)parseOp;
@end

NS_ASSUME_NONNULL_END
