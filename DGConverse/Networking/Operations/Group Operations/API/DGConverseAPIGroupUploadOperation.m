//
//  DGConverseAPIGroupUploadOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIGroupUploadOperation.h"

@implementation DGConverseAPIGroupUploadOperation

- (void)runWithFileURL:(NSString *)fileURLString
                 toURL:(NSString *)toURLString
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
            completion:(DGConverseAPINetworkResultBlock)completion
{
    // No need to make use of cache ops
    DGConverseAPIUploadRequest* networkRequest = [[DGConverseAPIUploadRequest alloc] initWithURLString:toURLString fileURL:[NSURL URLWithString:fileURLString]];
    DGConverseAPINetworkUploadOperation* networkOp = [[DGConverseAPINetworkUploadOperation alloc] initWithRequest:networkRequest
                                                                                                  operationDelegate:self.manager
                                                                                                           progress:progress
                                                                                                         completion:completion];
    [self processRequest:networkRequest];
    
    [self storeSubOperation:networkOp];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
}

- (void)runWithFileData:(NSData *)fileData
                 toURL:(NSString *)toURLString
              progress:(nullable DGConverseAPINetworkProgressBlock)progress
            completion:(DGConverseAPINetworkResultBlock)completion
{
    // No need to make use of cache ops
    DGConverseAPIUploadRequest* networkRequest = [[DGConverseAPIUploadRequest alloc] initWithURLString:toURLString bodyData:fileData];
    DGConverseAPINetworkUploadOperation* networkOp = [[DGConverseAPINetworkUploadOperation alloc] initWithRequest:networkRequest
                                                                                                  operationDelegate:self.manager
                                                                                                           progress:progress
                                                                                                         completion:completion];
    [self processRequest:networkRequest];
    
    [self storeSubOperation:networkOp];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
}

- (void)runInBackgroundWithFileURL:(NSString *)fileURLString
                             toURL:(NSString *)toURLString
                          progress:(nullable DGConverseAPINetworkProgressBlock)progress
                        completion:(DGConverseAPINetworkResultBlock)completion
{
    // No need to make use of cache ops
    DGConverseAPIUploadRequest* networkRequest = [[DGConverseAPIUploadRequest alloc] initWithURLString:toURLString fileURL:[NSURL URLWithString:fileURLString]];
    DGConverseAPINetworkUploadOperation* networkOp = [[DGConverseAPINetworkUploadOperation alloc] initWithBackgroundRequest:networkRequest
                                                                                                            operationDelegate:self.manager
                                                                                                                     progress:progress
                                                                                                                   completion:completion];
    [self processRequest:networkRequest];
    
    [self storeSubOperation:networkOp];
    [self.manager.queueManager addOperation:networkOp inQueue:kDGConverseAPIQueueData];
}

@end
