//
//  DGConverseAPINetworkBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkBaseOperation.h"
#import "DGConverseAPIAlertManager.h"

@interface DGConverseAPINetworkBaseOperation()

@property (nonatomic) NSURLCredential *proposedCredential;
@property (nonatomic) NSDictionary<NSString*,NSURLCredential*> *sharedCredentials;

@end

@implementation DGConverseAPINetworkBaseOperation

#pragma mark - Operation LifeCycle Methods

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
{
    return [self initWithRequest:request operationDelegate:operationDelegate progress:nil completion:nil];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithRequest:request operationDelegate:operationDelegate progress:nil completion:completion];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"API Network Base Operation";
        self.qualityOfService = NSQualityOfServiceUserInitiated;
        self.queuePriority = NSOperationQueuePriorityNormal;
        
        self.result.data = [NSMutableData new];
        
        self.taskRequest = request;
        self.operationMode = kOperationModeDefault;
        self.operationDelegate = operationDelegate;
        self.progress = progress;
        self.completion = completion;
    }
    return self;
}

#pragma mark - Public Methods

- (BOOL)isValid
{
    BOOL isValid = [super isValid];
    if (!isValid)
        [self.task cancel];
    
    return isValid;
}

- (void)cancel
{
    [super cancel];
    
    [self.task cancel];
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            if (self.result.data.length)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation injected data, no more processing needed");
            // Preceding operation end up with either ERROR or DATA result. No need to process this operation anymore.
            [self finish];
            return;
        } else {
            [self prepareStart];
            [self processStart];
        }
    }
}

- (void)repeatRequest
{
    // discard current task
    [self discardCurrentTask];
    // reset request result
    [self prepareStart];
    // perform request
    [self processStart];
}

- (void)reportResult
{
    DGConverseNetworkOperationResult *networkResult = [DGConverseNetworkOperationResult new];
    networkResult.data = self.result.data;
    networkResult.responseObj = self.result.responseObj;
    networkResult.error = self.result.error;
    
    [self processExtraInfo:networkResult];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
         if (!self.isDiscarded && self.completion) {
             self.completion(networkResult);
         }
    }];
}

#pragma mark - DGConverseAPICommonNetworkProcessing Delegate Methods

- (void)prepareStart
{
    self.result.error = nil;
    self.result.data = [NSMutableData new];
}

- (void)processStart
{
    [self finish];
}

- (void)processExtraInfo:(id<DGConverseAPIOperationResult>)networkResult
{
    DGConverseNetworkOperationResultExtraInfo *extraInfoNetworkResult;
    if ([networkResult.extraInfo objectForKey:[DGConverseNetworkOperationResultExtraInfo key]])
        extraInfoNetworkResult = [networkResult.extraInfo objectForKey:[DGConverseNetworkOperationResultExtraInfo key]];
    else
        extraInfoNetworkResult = [DGConverseNetworkOperationResultExtraInfo new];
    
    extraInfoNetworkResult.task = self.task;
    extraInfoNetworkResult.responseURL = self.responseURL;
    [networkResult.extraInfo setObject:extraInfoNetworkResult forKey:[DGConverseNetworkOperationResultExtraInfo key]];
}

- (BOOL)processResponseValidity:(NSURLResponse *)response
{
    self.responseURL = response;
    Class responseClass = [self.taskRequest responseClass];
    id<DGConverseAPIResponse> apiResponse = [[responseClass alloc] initWithTask:self.task response:response responseObject:self.result.data  error:self.result.error];
    
    NSError *error;
    BOOL isValid = [apiResponse.responseSerializer validateResponse:(NSHTTPURLResponse *)self.responseURL data:self.result.data error:&error];
    self.result.error = self.result.error ?: error;
    
    return isValid;
}

#pragma mark - Helper Methods

- (void)discardCurrentTask
{
    [self.operationDelegate converseAPINetworkBaseOperation:self didDiscardTask:self.task];
    [self.task cancel];
    [self.taskRequest reset];
}

- (void)showAuthenticationAlertControllerWithCompletion:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Authentication"
                                                                             message:@"Please enter credentials"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    if (self.sharedCredentials.count) {
        // TODO: Show List of matching sharedCredentials
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
                                                   }];
    DGWeakify(self);
    UIAlertAction *submit = [UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       UITextField *userTextField = alertController.textFields[0];
                                                       UITextField *passTextField = alertController.textFields[1];
                                                       
                                                       NSURLCredential *newCredential = [NSURLCredential credentialWithUser:userTextField.text
                                                                                                                   password:passTextField.text   persistence:NSURLCredentialPersistenceNone];
                                                       
                                                       completionHandler(NSURLSessionAuthChallengeUseCredential, newCredential);
                                                   }];
    
    [alertController addAction:cancel];
    [alertController addAction:submit];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        DGStrongify(self);
        textField.placeholder = @"user";
        textField.text = self.proposedCredential.user;
    }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"password";
        textField.secureTextEntry = YES;
    }];
    
    if ([[[UIApplication sharedApplication] keyWindow] rootViewController]) {
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController animated:YES completion:nil];
    } else {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }
}


#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession*)session task:(NSURLSessionTask*)task didCompleteWithError:(NSError*)error
{
    [self processResponseValidity:self.task.response];
    
    self.endTime = [NSDate date];
    self.result.error = self.result.error ?: error;
    
    if (NSThread.isMainThread)
        DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, [NSString stringWithFormat:@"Failed to receive response: %@", self.result.error.localizedDescription]);
    
    if ([self isValid]) {
        
        if (self.result.error) {
            DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, [NSString stringWithFormat:@"Failed to receive response: %@", self.result.error.localizedDescription]);
            if (error != self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, [NSString stringWithFormat:@"Extended Failure Description: %@", error.localizedDescription]);
            
            [self cancel];
        }
        
        if (self.completion) {
            [self reportResult];
        }
        
        if (self.startTime && self.endTime && self.result.data) {
            NSTimeInterval interval = [self.endTime timeIntervalSinceDate:self.startTime];
            // number of KiloBytes contained in the mutable data object
            NSUInteger bytes = self.result.data.length;
            CGFloat KB = bytes / 1024.0f;
            CGFloat KBPerSec = KB / interval;
            
            if (self.operationDelegate)
                [self.operationDelegate converseAPINetworkBaseOperation:self reportsKBperSecond:KBPerSec];
        }
        
        /// Data already appended at this point
        [self finish];
    }
}


/*!
 * @brief https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/URLLoadingSystem/Articles/AuthenticationChallenges.html
 * @discussion Important: The URL loading system classes do not call their delegates to handle request challenges unless the server response contains a WWW-Authenticate header. Other authentication types, such as proxy authentication and TLS trust validation do not require this header.
 
 NSURLSessionAuthChallengeDisposition:
 -------------------------------------
 - NSURLSessionAuthChallengeUseCredential: Use the specified credential, which may be nil.
 - NSURLSessionAuthChallengePerformDefaultHandling: Use the default handling for the challenge as though this delegate method were not implemented. The provided credential parameter is ignored.
 - NSURLSessionAuthChallengeCancelAuthenticationChallenge: Cancel the entire request. The provided credential parameter is ignored.
 - NSURLSessionAuthChallengeRejectProtectionSpace: Reject this challenge, and call the authentication delegate method again with the next authentication protection space. The provided credential parameter is ignored.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    NSString *authMethod = challenge.protectionSpace.authenticationMethod;
    
    if ([authMethod isEqualToString:NSURLAuthenticationMethodHTTPBasic]) {
        
        // Same as [[NSURLCredentialStorage sharedCredentialStorage] defaultCredentialForProtectionSpace:challenge.protectionSpace];
        self.proposedCredential = challenge.proposedCredential;
        // All credentials (including not default) that match the protection space.
        self.sharedCredentials = [[NSURLCredentialStorage sharedCredentialStorage] credentialsForProtectionSpace:challenge.protectionSpace];
        
        if (challenge.previousFailureCount == 0 && self.proposedCredential) {
            completionHandler(NSURLSessionAuthChallengeUseCredential, self.proposedCredential);
            
        } else {
            
            [self performSelectorOnMainThread:@selector(showAuthenticationAlertControllerWithCompletion:) withObject:completionHandler waitUntilDone:YES];
        }
    } else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}


/*!
 * @brief https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/URLLoadingSystem/Articles/RequestChanges.html
 * @warning This method is called only for tasks in default and ephemeral sessions. Tasks in background sessions automatically follow redirects.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willPerformHTTPRedirection:(NSHTTPURLResponse *)response newRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLRequest *))completionHandler
{
    NSURLRequest *newRequest = request;
    if (response)
        newRequest = nil;
    
    completionHandler(newRequest);
}

@end


