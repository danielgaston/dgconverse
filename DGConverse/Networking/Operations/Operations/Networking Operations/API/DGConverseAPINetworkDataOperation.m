//
//  DGConverseAPINetworkDataOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkDataOperation.h"
#import "NSDate+Additions.h"

static void * DGConverseAPINetworkDataOperationContext = &DGConverseAPINetworkDataOperationContext;

@implementation DGConverseAPINetworkDataOperation
@synthesize task, responseURL;

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super initWithRequest:request operationDelegate:operationDelegate progress:progress completion:completion];
    if (self) {
        self.name = @"API Network Data Operation";
        self.qualityOfService = NSQualityOfServiceUserInitiated;
        self.queuePriority = NSOperationQueuePriorityNormal;
    }
    return self;
}

- (void)initObservers
{
    // Necessary for 'repeatRequest'
    [self deinitObservers];
    
    if (self.task) {
        [self.task.progress addObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) options:NSKeyValueObservingOptionNew context:DGConverseAPINetworkDataOperationContext];
    }
}

- (void)deinitObservers
{
    @try {
        if (self.task.progress) {
            [self.task.progress removeObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) context:DGConverseAPINetworkDataOperationContext];
        }
    }
    @catch (NSException * __unused exception) {}
}

#pragma mark - Helpers Methods

- (NSURLSession *)sessionForCurrentOperationMode
{
    NSURLSession *session;
    switch (self.operationMode) {
        case kOperationModeBackground:
        {
            session = [self.operationDelegate converseAPINetworkBaseOperationRequestBackgroundSession:self];
        }
            break;
        default:
            session = [self.operationDelegate converseAPINetworkBaseOperationRequestDefaultSession:self];
            break;
    }
    return session;
}

#pragma mark - NSKeyValueObserving

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(__unused NSDictionary *)change context:(void *)context
{
    if (context == DGConverseAPINetworkDataOperationContext) {
        
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(fractionCompleted))]) {
            if (self.progress) {
                DGWeakify(self);
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    DGStrongify(self);
                    self.progress(object);
                }];
            }
        }
    }
}

#pragma mark - DGConverseAPICommonNetworkProcessing Delegate Methods

- (void)processStart
{
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"NSURLSessionDataTask start for: %@", [[self.taskRequest.urlRequest URL] absoluteString]]);
    NSURLSession *session = [self sessionForCurrentOperationMode];
    self.task = [session runSessionDataTaskWithRequest:self.taskRequest];
    
    [self initObservers];
    
    if (self.operationMode == kOperationModeBackground) {
        [self.operationDelegate converseAPINetworkBaseOperation:self requiresStoreCompletion:(void (^ )(id<DGConverseAPIOperationResult>))self.completion forURLSessionTask:self.task];
    }
    
    [self.task resume];
        
    [self.operationDelegate converseAPINetworkBaseOperation:self didStartWithTask:self.task];
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession*)session task:(NSURLSessionTask*)task didCompleteWithError:(NSError*)error
{
    [self deinitObservers];
    
    [super URLSession:session task:task didCompleteWithError:error];
}

#pragma mark - NSURLSessionDataTaskDelegate

// Tells the delegate that the data task received the initial reply (headers) from the server.
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didReceiveResponse:(NSURLResponse*)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    BOOL valid = [self processResponseValidity:response];
    [self processExtraInfo:self.result];
    
    completionHandler(valid ? NSURLSessionResponseAllow : NSURLSessionResponseCancel);
}

// Tells the delegate that the data task has received some of the expected data.
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didReceiveData:(NSData*)data
{
    if ([self isValid])
        [self.result.data appendData:data];
}

// This delegate method is called only for Data and Upload tasks. The caching policy for download tasks is determined by the specified cache policy exclusively. usePrococolCachePolicy caches HTTPS responses to disk, which may be undesirable for securing user data. this method allows to change this behavior by manually handling caching behavior.
// https://developer.apple.com/documentation/foundation/url_loading_system/accessing_cached_data?language=objc#2923215?language=objc
- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask willCacheResponse:(NSCachedURLResponse*)proposedResponse completionHandler:(void (^)(NSCachedURLResponse* cachedResponse))completionHandler
{
    [self isValid];
    
    NSCachedURLResponse *newCachedResponse = proposedResponse;
    NSDictionary *newUserInfo = @{@"Cached Date" : [NSDate now]};
    
    if ([proposedResponse.response.URL.scheme isEqualToString:@"https"]) {
        
        newCachedResponse = [[NSCachedURLResponse alloc] initWithResponse:proposedResponse.response
                                                                     data:proposedResponse.data
                                                                 userInfo:newUserInfo
                                                            storagePolicy:NSURLCacheStorageAllowedInMemoryOnly];
    } else {
        
        newCachedResponse = [[NSCachedURLResponse alloc] initWithResponse:[proposedResponse response]
                                                                     data:[proposedResponse data]
                                                                 userInfo:newUserInfo
                                                            storagePolicy:[proposedResponse storagePolicy]];
    }
    
    completionHandler(newCachedResponse);
}

- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didBecomeDownloadTask:(NSURLSessionDownloadTask*)downloadTask
{
    // Left blank intentionally
}

- (void)URLSession:(NSURLSession*)session dataTask:(NSURLSessionDataTask*)dataTask didBecomeStreamTask:(NSURLSessionStreamTask*)streamTask
{
    // Left blank intentionally
}

@end


