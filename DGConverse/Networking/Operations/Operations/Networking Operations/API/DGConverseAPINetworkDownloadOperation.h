//
//  DGConverseAPINetworkDownloadOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkBaseOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPINetworkDownloadOperation : DGConverseAPINetworkBaseOperation <NSURLSessionDownloadDelegate>

@property (nonatomic) NSURLSessionDownloadTask* task;

/*!
 * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
 * @discussion Download only will execute while the app is running, if it goes to background, the download will be paused. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download.
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
              destinationFolder:(NSSearchPathDirectory)directory
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion;


/*
 * Comply with Background Transfer Limitations
 * With background sessions, the actual transfer is performed by a process that is separate from your app’s process. Because restarting your app’s process is fairly expensive, a few features are unavailable, resulting in the following limitations:
 *
 * ·The session must provide a delegate for event delivery. (For uploads and downloads, the delegates behave the same as for in-process transfers.)
 * ·Only HTTP and HTTPS protocols are supported (no custom protocols).
 * ·Redirects are always followed. As a result, even if you have implemented URLSession:task:willPerformHTTPRedirection:newRequest:completionHandler:, it is not called.
 *
 * ·Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
 */

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate;

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                               completion:(nullable DGConverseAPINetworkResultBlock)completion;

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                                 progress:(nullable DGConverseAPINetworkProgressBlock)progress
                               completion:(nullable DGConverseAPINetworkResultBlock)completion;

/*!
 * @brief Downloaded and Stored in selected 'directory' or 'Tmp' (by default or none is set)
 * @discussion Download will be executing in background even if the app is suspended. Please consider that 'Tmp' directory, being cleaned each time the app is started. There is No caching involved in this download. Use 'inBackground' only if you really need large downloads to continue in the background after the app is suspended/terminated.
 * @warning File location URL is returned as NSData in DGConverseNetworkOperationResult.responseObj. Use [NSURL URLWithDataRepresentation:relativeToURL:]. It does not return any content NSData, it is up to the callback to work or not with the provided file URL.
 */
- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                        destinationFolder:(NSSearchPathDirectory)directory
                                 progress:(nullable DGConverseAPINetworkProgressBlock)progress
                               completion:(nullable DGConverseAPINetworkResultBlock)completion;

@end

NS_ASSUME_NONNULL_END
