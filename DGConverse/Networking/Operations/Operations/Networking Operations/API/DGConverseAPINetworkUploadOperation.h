//
//  DGConverseAPINetworkUploadOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkDataOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPINetworkUploadOperation : DGConverseAPINetworkDataOperation

@property (nonatomic) NSURLSessionUploadTask* task;
@property (nonatomic) DGConverseAPIUploadRequest *taskRequest;


/************************************************************************
 * Comply with Background Transfer Limitations
 * With background sessions, the actual transfer is performed by a process that is separate from your app’s process. Because restarting your app’s process is fairly expensive, a few features are unavailable, resulting in the following limitations:
 *
 * ·The session must provide a delegate for event delivery. (For uploads and downloads, the delegates behave the same as for in-process transfers.)
 * ·Only HTTP and HTTPS protocols are supported (no custom protocols).
 * ·Redirects are always followed. As a result, even if you have implemented URLSession:task:willPerformHTTPRedirection:newRequest:completionHandler:, it is not called.
 *
 * ·Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
 *************************************************************************/

/*!
 * @brief Upload Operation
 * @discussion Upload will be executing in background even if the app is suspended. There is No caching involved in this download. Use 'inBackground' only if you really need large uploads to continue in the background after the app is suspended/terminated.
 * @warning Background Transfer Limitations: Only upload tasks from a file are supported (uploads from data instances or a stream fail after the app exits).
 */
- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate;

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                               completion:(nullable DGConverseAPINetworkResultBlock)completion;

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                                 progress:(nullable DGConverseAPINetworkProgressBlock)progress
                               completion:(nullable DGConverseAPINetworkResultBlock)completion;

@end

NS_ASSUME_NONNULL_END
