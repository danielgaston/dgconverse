//
//  DGConverseAPINetworkDownloadOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkDownloadOperation.h"
#import "NSDate+Additions.h"
#import "NSString+Additions.h"

static void * DGConverseAPINetworkDownloadOperationContext = &DGConverseAPINetworkDownloadOperationContext;

@interface DGConverseAPINetworkDownloadOperation ()

@property (nonatomic, assign) NSSearchPathDirectory directory;

@end

@implementation DGConverseAPINetworkDownloadOperation
@synthesize task, taskRequest;

#pragma mark - Public Methods

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
{
    return [self initWithRequest:request operationDelegate:operationDelegate completion:nil];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithRequest:request operationDelegate:operationDelegate progress:nil completion:completion];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithRequest:request operationDelegate:operationDelegate destinationFolder:0 progress:progress completion:completion];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
              destinationFolder:(NSSearchPathDirectory)directory
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super initWithRequest:request operationDelegate:operationDelegate progress:progress completion:completion];
    if (self) {
        self.name = @"API Network Download Operation";
        self.qualityOfService = NSQualityOfServiceBackground;
        self.queuePriority = NSOperationQueuePriorityLow;
        self.directory = directory;
    }
    
    return self;
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
{
    return [self initWithBackgroundRequest:request operationDelegate:operationDelegate completion:nil];
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                               completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithBackgroundRequest:request operationDelegate:operationDelegate progress:nil completion:completion];
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                                 progress:(nullable DGConverseAPINetworkProgressBlock)progress
                               completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithBackgroundRequest:request operationDelegate:operationDelegate destinationFolder:0 progress:progress completion:completion];
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                        destinationFolder:(NSSearchPathDirectory)directory
                                 progress:(nullable DGConverseAPINetworkProgressBlock)progress
                               completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super initWithRequest:request operationDelegate:operationDelegate progress:progress completion:completion];
    if (self) {
        self.name = @"API Network Background Download Operation";
        self.qualityOfService = NSQualityOfServiceBackground;
        self.queuePriority = NSOperationQueuePriorityLow;
        self.operationMode = kOperationModeBackground;
        self.directory = directory;
    }
    
    return self;
}

- (void)initObservers
{
    // Necessary for 'repeatRequest'
    [self deinitObservers];
    
    if (self.task.progress) {
         [self.task.progress addObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) options:NSKeyValueObservingOptionNew context:DGConverseAPINetworkDownloadOperationContext];
    }
}

- (void)deinitObservers
{
    @try {
        if (self.task.progress) {
            [self.task.progress removeObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) context:DGConverseAPINetworkDownloadOperationContext];
        }
    }
    @catch (NSException * __unused exception) {}
}


#pragma mark - Helpers Methods

- (NSURLSession *)sessionForCurrentOperationMode
{
    NSURLSession *session;
    switch (self.operationMode) {
        case kOperationModeBackground:
        {
            session = [self.operationDelegate converseAPINetworkBaseOperationRequestBackgroundSession:self];
        }
            break;
        default:
            session = [self.operationDelegate converseAPINetworkBaseOperationRequestDefaultSession:self];
            break;
    }
    return session;
}

#pragma mark - NSKeyValueObserving

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(__unused NSDictionary *)change context:(void *)context
{
    if (context == DGConverseAPINetworkDownloadOperationContext) {
        
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(fractionCompleted))]) {
            if (self.progress) {
                DGWeakify(self);
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    DGStrongify(self);
                    self.progress(object);
                }];
            }
        }
    }
}

#pragma mark - DGConverseAPICommonNetworkProcessing Delegate Methods

- (void)processStart
{
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"NSURLSessionDownloadTask start for: %@", [[self.taskRequest.urlRequest URL] absoluteString]]);
    
    NSURLSession *session = [self sessionForCurrentOperationMode];
    self.task = [session runSessionDownloadTaskWithRequest:self.taskRequest];
    
    [self initObservers];
    
    if (self.operationMode == kOperationModeBackground) {
        [self.operationDelegate converseAPINetworkBaseOperation:self requiresStoreCompletion:(void (^ )(id<DGConverseAPIOperationResult>))self.completion forURLSessionTask:self.task];
    }
    
    [self.task resume];
        
    [self.operationDelegate converseAPINetworkBaseOperation:self didStartWithTask:self.task];
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession*)session task:(NSURLSessionTask*)task didCompleteWithError:(NSError*)error
{
    [self deinitObservers];
    
    [super URLSession:session task:task didCompleteWithError:error];
}

#pragma mark - NSURLSessionDownload Delegate Methods

/// Handling Download Life Cycle Changes
/// Pipeline:
/// 1st.- (void)URLSession:downloadTask:didFinishDownloadingToURL:
/// 2nd.- (void)URLSession:task:didCompleteWithError:
- (void)URLSession:(nonnull NSURLSession*)session
      downloadTask:(nonnull NSURLSessionDownloadTask*)downloadTask
didFinishDownloadingToURL:(nonnull NSURL*)location
{
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"NSURLSessionDownloadTask has completed");

    BOOL isValidResponse = [self processResponseValidity:downloadTask.response];
    if (isValidResponse && location) {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:[location path]]) {
            
            NSURL *directoryURL;
            if (self.directory) {
                directoryURL = [fileManager URLsForDirectory:self.directory inDomains:NSUserDomainMask].firstObject;
            } else {
                directoryURL = [NSURL fileURLWithPath:NSTemporaryDirectory()];
            }
            
            NSURL *destinationURL = [directoryURL URLByAppendingPathComponent:self.taskRequest.URL.lastPathComponent]; // myPhoto.jpg
            NSError *error;
            
            if (![fileManager fileExistsAtPath:destinationURL.path]) {
                // Move item directly
                [fileManager moveItemAtURL:location toURL:destinationURL error:&error];
            } else {
                // Rename item first to avoid name collision, and move it afterwards
                NSString *extension = self.taskRequest.URL.pathExtension;   // jpg
                if([NSString isNotNilNorEmpty:extension])
                    extension = [@"." stringByAppendingString:extension];   // .jpg
                NSString *fileName = self.taskRequest.URL.URLByDeletingPathExtension.lastPathComponent;         // myPhoto
                
                destinationURL = [directoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@_%f%@",fileName, [[NSDate now] timeIntervalSinceReferenceDate], extension]];
                
                [fileManager moveItemAtURL:location toURL:destinationURL error:&error];
            }
            
            if (error) {
                self.result.error = error;
            } else {
                // save file URL location
                self.result.responseObj = destinationURL;
            }
        } else {
            // Default 'tmp' folder
            // save file URL location
            self.result.responseObj = location;
        }
    }
}

// Receiving Progress Updates
- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
   // Left blank intentionally
}

// Resuming Paused Downloads
- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes
{
    CGFloat percentDone = ((double)fileOffset/(double)expectedTotalBytes)*100;
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"NSURLSessionDownloadTask Did RESUME at: %.2f %%", percentDone]);
}

@end
