//
//  DGConverseAPINetworkBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

/**
-------------------------------------------------------------------------------------------------------------------------------------------
|                    |    OK                                          | OK-RESULT |            FORCED FAILURE                 | FF-RESULT |
-------------------------------------------------------------------------------------------------------------------------------------------
|Data Task           |                                                |           |                                           |           |
|                    |                                                |           |                                           |           |
|    Default         |    1.NetworkDataOp - didReceiveResponse        |           |1.NetworkDataOp - didCompleteWithError     |           |
|                    |    2.NetworkDataOp - willCacheResponse         |           |2.NetworkBaseOp - didCompleteWithError     |     ✔     |
|                    |    3.NetworkDataOp - didCompleteWithError      |           |                                           |           |
|                    |    4.NetworkBaseOp - didCompleteWithError      |     ✔     |                                           |           |
|                    |                                                |           |                                           |           |
|    Background      |    Bg DataTasks are Unavailable in iOS         |     -     |                                           |     -     |
-------------------------------------------------------------------------------------------------------------------------------------------
|Download Task       |                                                |           |                                           |           |
|                    |                                                |           |                                           |           |
|    Default         |    1.NetworkDownOp - didFinishDownloadingToURL |           |1.NetworkBaseOp - didCompleteWithError     |     ✔     |
|                    |    2.NetworkBaseOp - didCompleteWithError      |     ✔     |                                           |           |
|                    |                                                |           |                                           |           |
|    Background      |    1.NetworkDownOp - didFinishDownloadingToURL |           |1.NetworkDownOp - didFinishDownloadingToURL|           |
|                    |    2.NetworkBaseOp - didCompleteWithError      |     ✔     |2.NetworkBaseOp - didCompleteWithError     |     ✔     |
-------------------------------------------------------------------------------------------------------------------------------------------
|Upload Task         |                                                |           |                                           |           |
|                    |                                                |           |                                           |           |
|    Default         |    1.NetworkDataOp - didReceiveResponse        |           |1.NetworkDataOp - didReceiveResponse       |           |
|    (File/Data)     |    2.NetworkDataOp - willCacheResponse         |           |2.NetworkDataOp - didCompleteWithError     |           |
|                    |    3.NetworkDataOp - didCompleteWithError      |           |3.NetworkBaseOp - didCompleteWithError     |           |
|                    |    4.NetworkBaseOp - didCompleteWithError      |     ✔     |4.NetworkDataOp - willCacheResponse        |     ✔     |
|                    |                                                |           |                                           |           |
|    Background      |    1.NetworkDataOp - didCompleteWithError      |           |1.NetworkDataOp - didCompleteWithError     |           |
|    (File only)     |    2.NetworkBaseOp - didCompleteWithError      |     ✔     |2.NetworkBaseOp - didCompleteWithError     |     ✔     |
-------------------------------------------------------------------------------------------------------------------------------------------

 BG Processig: https://developer.apple.com/documentation/foundation/url_loading_system/downloading_files_in_the_background?language=objc
*/

#import "DGConverseAPIBaseOperation.h"
#import "DGConverseAPINetworkOperationClient.h"


typedef NS_ENUM(NSUInteger, DGConverseAPINetworkOperationMode)
{
    kOperationModeDefault,
    kOperationModeBackground
};

NS_ASSUME_NONNULL_BEGIN

typedef void (^DGConverseAPINetworkResultBlock)(DGConverseNetworkOperationResult* result);
typedef void (^DGConverseAPINetworkProgressBlock)(NSProgress *progress);

#pragma mark -

@protocol DGConverseAPICommonNetworkProcessing <NSObject>

- (void)prepareStart;
- (void)processStart;
- (void)processExtraInfo:(id<DGConverseAPIOperationResult>)networkResult;
- (BOOL)processResponseValidity:(NSURLResponse *)response;

@end

#pragma mark -

@interface DGConverseAPINetworkBaseOperation : DGConverseAPIBaseOperation <NSURLSessionTaskDelegate, DGConverseAPICommonNetworkProcessing, DGConverseAPIOperationDiscardable>

/*!
 * @brief Delegate used to report network speed, and adapt the queue accordingly
 * @warning DGConverseOperationNetworkManager variable must be retained in a ViewController, otherwise operationDelegate is deallocated before the request finishes
 */
@property (nonatomic, weak) id<DGConverseAPINetworkBaseOperationDelegate> operationDelegate;

@property (nonatomic) NSURLSessionTask* task;
@property (nonatomic) NSURLResponse* responseURL;

@property (nonatomic) id<DGConverseAPIRequest> taskRequest;

@property (nonatomic, assign) DGConverseAPINetworkOperationMode operationMode;
@property (nonatomic, copy) DGConverseAPINetworkProgressBlock progress;
@property (nonatomic, copy) DGConverseAPINetworkResultBlock completion;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate;

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                     completion:(nullable DGConverseAPINetworkResultBlock)completion;

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion NS_DESIGNATED_INITIALIZER;

/*!
 * @brief Creates a new networkTask and performs a networkRequest again.
 * @discussion This method helps with recreating the necessary environment within the NSOperation to safely perform the networkTask, using the same request that the NSOperation has been initialized with. This method should be called internally, controlling all the NSURLSessionTask-related delegate methods, usually by creating the corresponding categories overriding the default delegate methods logic.
 * @warning This shouldn’t be called externally as this is used internally by subclasses/categories if necessary (i.e. 401 related relogin without 'WWW-Authenticate: Basic' headers, that would enter through 'URLSession:task:didReceiveChallenge:completionHandler:' otherwise).
 */
- (void)repeatRequest;

/// Checks the operation has not been discarded and executes the completion block
- (void)reportResult;

@end

NS_ASSUME_NONNULL_END
