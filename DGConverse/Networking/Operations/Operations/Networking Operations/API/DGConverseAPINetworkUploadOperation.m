//
//  DGConverseAPINetworkUploadOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkUploadOperation.h"

static void * DGConverseAPINetworkUploadOperationContext = &DGConverseAPINetworkUploadOperationContext;

@implementation DGConverseAPINetworkUploadOperation
@synthesize task, taskRequest;

#pragma mark - Public Methods

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
{
    return [self initWithRequest:request operationDelegate:operationDelegate completion:nil];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithRequest:request operationDelegate:operationDelegate progress:nil completion:completion];
}

- (instancetype)initWithRequest:(id<DGConverseAPIRequest>)request
              operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                       progress:(nullable DGConverseAPINetworkProgressBlock)progress
                     completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super initWithRequest:request operationDelegate:operationDelegate progress:progress completion:completion];
    if (self) {
        self.name = @"API Network Upload Operation";
        self.qualityOfService = NSQualityOfServiceBackground;
        self.queuePriority = NSOperationQueuePriorityLow;
    }
    
    return self;
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
{
    return [self initWithBackgroundRequest:request operationDelegate:operationDelegate completion:nil];
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                               completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    return [self initWithBackgroundRequest:request operationDelegate:operationDelegate progress:nil completion:completion];
}

- (instancetype)initWithBackgroundRequest:(id<DGConverseAPIRequest>)request
                        operationDelegate:(nullable id<DGConverseAPINetworkBaseOperationDelegate>)operationDelegate
                                 progress:(nullable DGConverseAPINetworkProgressBlock)progress
                               completion:(nullable DGConverseAPINetworkResultBlock)completion
{
    self = [super initWithRequest:request operationDelegate:operationDelegate progress:progress completion:completion];
    if (self) {
        self.name = @"API Network Background Upload Operation";
        self.qualityOfService = NSQualityOfServiceBackground;
        self.queuePriority = NSOperationQueuePriorityLow;
        self.operationMode = kOperationModeBackground;
    }
    
    return self;
}

- (void)initObservers
{
    // Necessary for 'repeatRequest'
    [self deinitObservers];
    
    if (self.task.progress) {
         [self.task.progress addObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) options:NSKeyValueObservingOptionNew context:DGConverseAPINetworkUploadOperationContext];
    }
}

- (void)deinitObservers
{
    @try {
        if (self.task.progress) {
            [self.task.progress removeObserver:self forKeyPath:NSStringFromSelector(@selector(fractionCompleted)) context:DGConverseAPINetworkUploadOperationContext];
        }
    }
    @catch (NSException * __unused exception) {}
}

#pragma mark - Helpers Methods

- (NSURLSession *)sessionForCurrentOperationMode
{
    NSURLSession *session;
    switch (self.operationMode) {
        case kOperationModeBackground:
            session = [self.operationDelegate converseAPINetworkBaseOperationRequestBackgroundSession:self];
            break;
        default:
            session = [self.operationDelegate converseAPINetworkBaseOperationRequestDefaultSession:self];
            break;
    }
    return session;
}


#pragma mark - NSKeyValueObserving

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(__unused NSDictionary *)change context:(void *)context
{
    if (context == DGConverseAPINetworkUploadOperationContext) {
        
        if ([keyPath isEqualToString:NSStringFromSelector(@selector(fractionCompleted))]) {
            if (self.progress) {
                DGWeakify(self);
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    DGStrongify(self);
                    self.progress(object);
                }];
            }
        }
    }
}


#pragma mark - DGConverseAPICommonNetworkProcessing Delegate Methods

- (void)processStart
{
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"NSURLSessionUploadTask start for: %@", [[self.taskRequest.urlRequest URL] absoluteString]]);
    
    NSURLSession *session = [self sessionForCurrentOperationMode];
    
    if (self.taskRequest.fileURL)
        self.task = [session runSessionUploadTaskWithRequest:self.taskRequest fileURL:self.taskRequest.fileURL];
    else
        self.task = [session runSessionUploadTaskWithRequest:self.taskRequest bodyData:self.taskRequest.bodyData];
    
    [self initObservers];
    
    
    if (self.operationMode == kOperationModeBackground) {
        [self.operationDelegate converseAPINetworkBaseOperation:self requiresStoreCompletion:(void (^ )(id<DGConverseAPIOperationResult>))self.completion forURLSessionTask:self.task];
    }
        
    [self.task resume];
    
    [self.operationDelegate converseAPINetworkBaseOperation:self didStartWithTask:self.task];
}


#pragma mark - NSURLSessionUpload Delegate Methods

// @dgaston: NSURLSessionUploadDelegate does not exist. It is inheriting DGConverseAPINetworkDataOperation Delegate Methods as NSURLSessionUploadTask inherits from NSURLSessionDataTask

@end


