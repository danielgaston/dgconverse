//
//  DGConverseAPINetworkDataOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPINetworkBaseOperation.h"
#import "DGConverseAPINetworkOperationClient.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPINetworkDataOperation : DGConverseAPINetworkBaseOperation <NSURLSessionDataDelegate>

@property (nonatomic) NSHTTPURLResponse* responseURL;
@property (nonatomic) NSURLSessionDataTask* task;

@end

NS_ASSUME_NONNULL_END
