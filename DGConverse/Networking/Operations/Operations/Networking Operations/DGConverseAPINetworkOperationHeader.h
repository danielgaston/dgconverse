//
//  DGConverseAPINetworkOperationHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPINetworkOperationHeader_h
#define DGConverseAPINetworkOperationHeader_h

#import "DGConverseAPINetworkBaseOperation.h"
#import "DGConverseAPINetworkDataOperation.h"
#import "DGConverseAPINetworkDownloadOperation.h"
#import "DGConverseAPINetworkUploadOperation.h"

#endif /* DGConverseAPINetworkOperationHeader_h */
