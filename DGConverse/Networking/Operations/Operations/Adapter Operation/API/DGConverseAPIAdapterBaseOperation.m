//
//  DGConverseAPIAdapterBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIAdapterBaseOperation.h"

@implementation DGConverseAPIAdapterBaseOperation

- (instancetype)initWithFromOperation:(DGConverseAPIBaseOperation*)fromOp toOperation:(DGConverseAPIBaseOperation*)toOp
{
	self = [super init];
	if (self) {
        
        self.opUUID = [NSUUID UUID];
		self.name = [NSString stringWithFormat:@"Adapter Operation: %@ -> %@", fromOp.name, toOp.name];

        DGWeakify(fromOp);
        DGWeakify(toOp);
		[self addExecutionBlock:^{
            DGStrongify(fromOp);
            DGStrongify(toOp);
			toOp.result = fromOp.result;
		}];
	}
	return self;
}

- (void)dealloc
{
	DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel0, [NSString stringWithFormat:@"DEALLOC %@ %@", self.name, self.opUUID]);
}

@end
