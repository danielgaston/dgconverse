//
//  DGConverseAPIAdapterBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"
#import "DGConverseAPIDebugUtils.h"

@interface DGConverseAPIAdapterBaseOperation : NSBlockOperation

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithFromOperation:(DGConverseAPIBaseOperation*)fromOp toOperation:(DGConverseAPIBaseOperation*)toOp NS_DESIGNATED_INITIALIZER;

@end
