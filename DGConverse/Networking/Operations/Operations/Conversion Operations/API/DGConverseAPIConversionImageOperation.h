//
//  DGConverseAPIConversionImageOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIConversionBaseOperation.h"

@interface DGConverseImageResult : DGConverseExchangeOperationResult

@property (nonatomic, strong) UIImage *responseObj;

@end

typedef void (^DGConverseImageResultBlock)(DGConverseImageResult* result);

#pragma mark -

@interface DGConverseAPIConversionImageOperation : DGConverseAPIConversionBaseOperation

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIConversionBaseOperationDelegate>)operationDelegate
                               completion:(DGConverseImageResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end
