//
//  DGConverseAPIConversionBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"

@interface DGConverseAPIConversionBaseOperation : DGConverseAPIBaseOperation <DGConverseAPIOperationDiscardable>

@property (nonatomic, weak) id<DGConverseAPIConversionBaseOperationDelegate> operationDelegate;

/*!
 * @brief Generic completion block
 * @warning Must be synthesized with a custom operation block on each child operation
 */
@property (nonatomic, copy) void (^completion)(id);

/*!
 * @brief Checks the opearation has not been discarded and executes the completion block
 */
- (void)reportResult;

@end
