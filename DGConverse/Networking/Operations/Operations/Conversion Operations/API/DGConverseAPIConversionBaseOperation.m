//
//  DGConverseAPIConversionBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIConversionBaseOperation.h"

@implementation DGConverseAPIConversionBaseOperation

#pragma mark - Public Methods

- (void)finish
{
    [super finish];
    
    if ([self isValid]) {
        
        if (self.result.error) {
            DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, [NSString stringWithFormat:@"Failed to convert response object: %@", self.result.error.localizedDescription]);
        }
        
        if (self.startTime && self.endTime) {
            NSTimeInterval interval = [self.endTime timeIntervalSinceDate:self.startTime];
            
            if (self.operationDelegate)
                [self.operationDelegate converseAPIConversionBaseOperation:self reportsDuration:interval];
        }
    }
}

- (void)reportResult
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (!self.isDiscarded && self.completion) {
            self.completion(self.result);
        }
    }];
}

@end

