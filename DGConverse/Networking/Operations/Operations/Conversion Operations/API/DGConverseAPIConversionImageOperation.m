//
//  DGConverseAPIConversionImageOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIConversionImageOperation.h"

@implementation DGConverseImageResult
@synthesize responseObj;
@end

#pragma mark -

@implementation DGConverseAPIConversionImageOperation
@synthesize completion;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIConversionBaseOperationDelegate>)operationDelegate
                               completion:(DGConverseImageResultBlock)completion
{
    self = [super init];
    if (self) {
        self.operationDelegate = operationDelegate;
        self.completion = completion;
        self.name = @"Image Conversion Operation";
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        
        if (self.result.error || !(self.result.data.length || self.result.responseObj)) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            
            DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"NSData -> UIImage conversion started");
            if (self.result.data.length) {
                // comes from cache
                self.result.responseObj = [UIImage imageWithData:self.result.data];
                
            } else if (self.result.responseObj && [self.result.responseObj isKindOfClass:[NSURL class]]) {
                // FIXME: NSURL check should be avoided...
                NSData *data = [NSData dataWithContentsOfURL:self.result.responseObj];
                UIImage *image = [UIImage imageWithData:data];
                self.result.responseObj = image;
            }
            DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"NSData -> UIImage conversion ended");
        }
        
        [self reportResult];
        [self finish];
        
    } else {
        [self cancel];
    }
}

@end


