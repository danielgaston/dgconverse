//
//  DGConverseAPIConversionOperationHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPIConversionOperationHeader_h
#define DGConverseAPIConversionOperationHeader_h

#import "DGConverseAPIConversionBaseOperation.h"
#import "DGConverseAPIConversionImageOperation.h"

#endif /* DGConverseAPIConversionOperationHeader_h */
