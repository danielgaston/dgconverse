//
//  DGConverseAPIBlockOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"

typedef void (^DGConverseAPIBlockBaseOperationCompletionBlock)(void);
typedef void (^DGConverseAPIBlockBaseOperationBlock)(DGConverseAPIBlockBaseOperationCompletionBlock completionBlock);


@interface DGConverseAPIBlockBaseOperation : DGConverseAPIBaseOperation

/**
 *  Initialized with a block to be executed when the operation starts
 *
 *  @param block the block to run when the operation starts.
 *
 */
- (instancetype)initWithBlock:(DGConverseAPIBlockBaseOperationBlock)block;

@end
