//
//  DGConverseAPIBlockOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBlockBaseOperation.h"

@interface DGConverseAPIBlockBaseOperation ()

@property (nonatomic, copy) DGConverseAPIBlockBaseOperationBlock block;

@end

@implementation DGConverseAPIBlockBaseOperation

- (instancetype)initWithBlock:(DGConverseAPIBlockBaseOperationBlock)block
{
    self = [super init];
    if (self) {
        _block = block;
        self.name = @"API Block Operation Operation";
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        
        if (self.block) {
            
            self.block(^{
                [self finish];
            });
            
        } else {
            [self finish];
        }
    }
}

@end
