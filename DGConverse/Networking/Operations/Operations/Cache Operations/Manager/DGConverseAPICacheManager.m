//
//  DGConverseAPICacheManager.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheManager.h"
#import "NSString+Additions.h"

#define kJSONCacheId @"/com.danielgaston.converse.json.cache"
#define kImageCacheId @"/com.danielgaston.converse.image.cache"
#define kVideoCacheId @"/com.danielgaston.converse.video.cache"
#define kFileCacheId @"/com.danielgaston.converse.file.cache"

@interface DGConverseAPICacheManager ()

@property (nonatomic) DGConverseAPICache* jsonCache;
@property (nonatomic) DGConverseAPICache* imageCache;
@property (nonatomic) DGConverseAPICache* videoCache;
@property (nonatomic) DGConverseAPICache* fileCache;

@end

@implementation DGConverseAPICacheManager

#pragma mark - Public Methods

- (NSArray<DGConverseAPICache*> *)cacheForType:(DGConverseAPICacheType)type
{
    NSMutableArray<DGConverseAPICache*>* caches = @[].mutableCopy;
    switch (type) {
        case kDGConverseAPICacheTypeALL:{
            [caches addObject:self.jsonCache];
            [caches addObject:self.imageCache];
            [caches addObject:self.videoCache];
            [caches addObject:self.fileCache];
            break;
        }
        case kDGConverseAPICacheTypeJSON:{
            [caches addObject:self.jsonCache];
            break;
        }
        case kDGConverseAPICacheTypeImage:{
            [caches addObject:self.imageCache];
            break;
        }
        case kDGConverseAPICacheTypeVideo:{
            [caches addObject:self.videoCache];
            break;
        }
        case kDGConverseAPICacheTypeFile:{
            [caches addObject:self.fileCache];
            break;
        }
    }
    return caches;
}

- (void)scheduleGarbageCollectionForType:(DGConverseAPICacheType)type
{
    NSArray<DGConverseAPICache*> *caches = [self cacheForType:type];
    for (DGConverseAPICache *cache in caches)
        [cache scheduleGarbageCollector];
}

- (void)unscheduleGarbageCollectionForType:(DGConverseAPICacheType)type
{
    NSArray<DGConverseAPICache*> *caches = [self cacheForType:type];
    for (DGConverseAPICache *cache in caches)
        [cache unscheduleGarbageCollector];
}

#pragma mark - Getters & Setters

- (DGConverseAPICache*)jsonCache
{
    if (!_jsonCache)
        [self initializeJSONCache];
    return _jsonCache;
}

- (DGConverseAPICache*)imageCache
{
    if (!_imageCache)
        [self initializeImageCache];
    return _imageCache;
}

- (DGConverseAPICache*)videoCache
{
    if (!_videoCache)
        [self initializeVideoCache];
    return _videoCache;
}

- (DGConverseAPICache*)fileCache
{
    if (!_fileCache)
        [self initializeFileCache];
    return _fileCache;
}

#pragma mark - Initialization Methods

- (void)initializeJSONCache
{
    NSString* cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject stringByAppendingString:kJSONCacheId];
    
    DGConverseAPICacheOptions* options = [DGConverseAPICacheOptions new];
    options.cachePath = cachePath;
    options.cacheIdentifier = kJSONCacheId;
    options.defaultExpirationPeriod = 60 * 60 * 24 * 1; // 1 days
    options.garbageCollectionInterval = (NSUInteger)(1.5 * SPTPersistentCacheDefaultGCIntervalSec);
    options.sizeConstraintBytes = 1024 * 1024 * 5; // 5 MiB
    options.debugOutput = ^(NSString* string) {
        NSLog(@"%@", string);
    };
    
    self.jsonCache = [[DGConverseAPICache alloc] initWithOptions:options];
}

- (void)initializeImageCache
{
    NSString* cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject stringByAppendingString:kImageCacheId];
    
    DGConverseAPICacheOptions* options = [DGConverseAPICacheOptions new];
    options.cachePath = cachePath;
    options.cacheIdentifier = kImageCacheId;
    options.defaultExpirationPeriod = 60 * 60 * 24 * 30; // 30 days
    options.garbageCollectionInterval = (NSUInteger)(1.5 * SPTPersistentCacheDefaultGCIntervalSec);
    options.sizeConstraintBytes = 1024 * 1024 * 10; // 10 MiB
    options.debugOutput = ^(NSString* string) {
        NSLog(@"%@", string);
    };
    
    self.imageCache = [[DGConverseAPICache alloc] initWithOptions:options];
}

- (void)initializeVideoCache
{
    NSString* cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject stringByAppendingString:kVideoCacheId];
    
    DGConverseAPICacheOptions* options = [DGConverseAPICacheOptions new];
    options.cachePath = cachePath;
    options.cacheIdentifier = kVideoCacheId;
    options.defaultExpirationPeriod = 60 * 60 * 24 * 30; // 30 days
    options.garbageCollectionInterval = (NSUInteger)(1.5 * SPTPersistentCacheDefaultGCIntervalSec);
    options.sizeConstraintBytes = 1024 * 1024 * 10; // 10 MiB
    options.debugOutput = ^(NSString* string) {
        NSLog(@"%@", string);
    };
    
    self.videoCache = [[DGConverseAPICache alloc] initWithOptions:options];
}

- (void)initializeFileCache
{
    NSString* cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject stringByAppendingString:kFileCacheId];
    
    DGConverseAPICacheOptions* options = [DGConverseAPICacheOptions new];
    options.cachePath = cachePath;
    options.cacheIdentifier = kFileCacheId;
    options.defaultExpirationPeriod = 60 * 60 * 24 * 30; // 30 days
    options.garbageCollectionInterval = (NSUInteger)(1.5 * SPTPersistentCacheDefaultGCIntervalSec);
    options.sizeConstraintBytes = 1024 * 1024 * 10; // 10 MiB
    options.debugOutput = ^(NSString* string) {
        NSLog(@"%@", string);
    };
    
    self.fileCache = [[DGConverseAPICache alloc] initWithOptions:options];
}

@end


@implementation DGConverseAPICacheResponse
@end

@implementation DGConverseAPICacheOptions
@end

@implementation DGConverseAPICache

- (BOOL)loadDataForURL:(NSURL*)url
          withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
               onQueue:(dispatch_queue_t _Nullable)queue
{
    return [self loadDataForKey:[[url absoluteString] MD5Digest] withCallback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (BOOL)loadDataForURLString:(NSString*)URLString
                withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
                     onQueue:(dispatch_queue_t _Nullable)queue
{
    return [self loadDataForKey:[URLString MD5Digest] withCallback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (BOOL)storeData:(NSData*)data
           forURL:(NSURL*)url
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue
{
    return [self storeData:data forKey:[[url absoluteString] MD5Digest] locked:locked withCallback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (BOOL)storeData:(NSData*)data
     forURLString:(NSString*)URLString
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue
{
    return [self storeData:data forKey:[URLString MD5Digest] locked:locked withCallback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (BOOL)storeData:(NSData*)data
           forURL:(NSURL*)url
              ttl:(NSUInteger)ttl
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue
{
    return [self storeData:data forKey:[[url absoluteString] MD5Digest] ttl:ttl locked:locked withCallback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (BOOL)storeData:(NSData*)data
     forURLString:(NSString*)URLString
              ttl:(NSUInteger)ttl
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue
{
    return [self storeData:data forKey:[URLString MD5Digest] ttl:ttl locked:locked withCallback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (void)touchDataForURL:(NSURL*)url
               callback:(DGConverseAPICacheResponseCallback _Nullable)callback
                onQueue:(dispatch_queue_t _Nullable)queue
{
    [self touchDataForKey:[[url absoluteString] MD5Digest] callback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

- (void)touchDataForURLString:(NSString*)URLString
                     callback:(DGConverseAPICacheResponseCallback _Nullable)callback
                      onQueue:(dispatch_queue_t _Nullable)queue
{
    [self touchDataForKey:[URLString MD5Digest] callback:(SPTPersistentCacheResponseCallback)callback onQueue:queue];
}

@end


