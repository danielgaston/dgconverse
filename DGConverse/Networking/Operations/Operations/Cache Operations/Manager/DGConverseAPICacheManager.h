//
//  DGConverseAPICacheManager.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>
@import SPTPersistentCache;

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, DGConverseAPICacheType) {
    kDGConverseAPICacheTypeALL,
    kDGConverseAPICacheTypeJSON,
    kDGConverseAPICacheTypeImage,
    kDGConverseAPICacheTypeVideo,
    kDGConverseAPICacheTypeFile
};

@class DGConverseAPICache;

@interface DGConverseAPICacheManager : NSObject

- (NSArray<DGConverseAPICache*> *)cacheForType:(DGConverseAPICacheType)type;
- (void)scheduleGarbageCollectionForType:(DGConverseAPICacheType)type;
- (void)unscheduleGarbageCollectionForType:(DGConverseAPICacheType)type;

@end



/**
 *  Type off callback for load/store calls
 */



@interface DGConverseAPICacheResponse : SPTPersistentCacheResponse
@end

@interface DGConverseAPICacheOptions : SPTPersistentCacheOptions
@end

typedef void (^DGConverseAPICacheResponseCallback)(DGConverseAPICacheResponse *response);

@interface DGConverseAPICache : SPTPersistentCache

- (BOOL)loadDataForURL:(NSURL*)url
          withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
               onQueue:(dispatch_queue_t _Nullable)queue;

- (BOOL)loadDataForURLString:(NSString*)URLString
                withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
                     onQueue:(dispatch_queue_t _Nullable)queue;

- (BOOL)storeData:(NSData*)data
           forURL:(NSURL*)url
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue;

- (BOOL)storeData:(NSData*)data
     forURLString:(NSString*)URLString
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue;

- (BOOL)storeData:(NSData*)data
           forURL:(NSURL*)url
              ttl:(NSUInteger)ttl
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue;

- (BOOL)storeData:(NSData*)data
     forURLString:(NSString*)URLString
              ttl:(NSUInteger)ttl
           locked:(BOOL)locked
     withCallback:(DGConverseAPICacheResponseCallback _Nullable)callback
          onQueue:(dispatch_queue_t _Nullable)queue;

- (void)touchDataForURL:(NSURL*)url
               callback:(DGConverseAPICacheResponseCallback _Nullable)callback
                onQueue:(dispatch_queue_t _Nullable)queue;

- (void)touchDataForURLString:(NSString*)URLString
                     callback:(DGConverseAPICacheResponseCallback _Nullable)callback
                      onQueue:(dispatch_queue_t _Nullable)queue;

@end

NS_ASSUME_NONNULL_END


