//
//  DGConverseAPICacheOperationHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPICacheOperationHeader_h
#define DGConverseAPICacheOperationHeader_h

#import "DGConverseAPICacheBaseOperation.h"
#import "DGConverseAPICacheImageOperation.h"
#import "DGConverseAPICacheFileOperation.h"
#import "DGConverseAPICacheVideoOperation.h"
#import "DGConverseAPICacheJSONOperation.h"

#endif /* DGConverseAPICacheOperationHeader_h */
