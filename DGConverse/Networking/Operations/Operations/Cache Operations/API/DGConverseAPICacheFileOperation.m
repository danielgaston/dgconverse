//
//  DGConverseAPICacheFileOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheFileOperation.h"

@implementation DGConverseAPICacheFileOperation

#pragma mark - Overriden Methods

- (instancetype)initWithURLString:(NSString*)URLString locked:(BOOL)locked operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate
{
    self = [super initWithURLString:URLString locked:locked operationDelegate:operationDelegate];
    if (self) {
        self.cache = [[self.cacheManager cacheForType:kDGConverseAPICacheTypeFile] firstObject];
        self.name = @"File Cache Operation";
    }
    return self;
}

@end
