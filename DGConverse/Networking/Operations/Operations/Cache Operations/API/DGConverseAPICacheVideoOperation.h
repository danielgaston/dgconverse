//
//  DGConverseAPICacheVideoOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheBaseOperation.h"

@interface DGConverseAPICacheVideoOperation : DGConverseAPICacheBaseOperation

@end
