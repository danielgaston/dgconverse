//
//  DGConverseAPICacheBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"
#import "DGConverseAPICacheManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPICacheBaseOperation : DGConverseAPIBaseOperation

@property (nonatomic, weak) id<DGConverseAPICacheBaseOperationDelegate> operationDelegate;

@property (nonatomic, strong) NSString *URLString;
@property (nonatomic, assign) BOOL locked;
@property (nonatomic, strong) DGConverseAPICacheManager *cacheManager;
@property (nonatomic, strong) DGConverseAPICache *cache;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithURLString:(NSString*)URLString
                operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate;

- (instancetype)initWithURLString:(NSString*)URLString
                           locked:(BOOL)locked
                operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate NS_DESIGNATED_INITIALIZER;

- (void)readData;
- (void)storeData;

@end

NS_ASSUME_NONNULL_END
