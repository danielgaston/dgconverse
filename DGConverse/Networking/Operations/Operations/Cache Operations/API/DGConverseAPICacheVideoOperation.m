//
//  DGConverseAPICacheVideoOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheVideoOperation.h"

@implementation DGConverseAPICacheVideoOperation

#pragma mark - Overriden Methods

- (instancetype)initWithURLString:(NSString*)URLString locked:(BOOL)locked operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate
{
    self = [super initWithURLString:URLString locked:locked operationDelegate:operationDelegate];
    if (self) {
        self.cache = [[self.cacheManager cacheForType:kDGConverseAPICacheTypeVideo] firstObject];
        self.name = @"Video Cache Operation";
    }
    return self;
}

@end
