//
//  DGConverseAPICacheJSONOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheJSONOperation.h"

@implementation DGConverseAPICacheJSONOperation

#pragma mark - Overriden Methods

- (instancetype)initWithURLString:(NSString*)URLString locked:(BOOL)locked operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate
{
    self = [super initWithURLString:URLString locked:locked operationDelegate:operationDelegate];
    if (self) {
        self.cache = [[self.cacheManager cacheForType:kDGConverseAPICacheTypeJSON] firstObject];
        self.name = @"JSON Cache Operation";
    }
    return self;
}

@end
