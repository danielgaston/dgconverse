//
//  DGConverseAPICacheBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheBaseOperation.h"

@implementation DGConverseAPICacheBaseOperation

#pragma mark - Operation LifeCycle Methods


- (instancetype)initWithURLString:(NSString*)URLString
                operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate
{
    return [self initWithURLString:URLString locked:NO operationDelegate:operationDelegate];
}

- (instancetype)initWithURLString:(NSString*)URLString
                           locked:(BOOL)locked
                operationDelegate:(id<DGConverseAPICacheBaseOperationDelegate>)operationDelegate
{
    self = [super init];
    if (self) {
        _URLString = URLString;
        _locked = locked;
        _operationDelegate = operationDelegate;
        _cacheManager = [DGConverseAPICacheManager new];
    }
    return self;
}


#pragma mark - Public Methods

- (void)finish
{
    [super finish];
    
    if ([self isValid]) {
        
        if (self.startTime && self.endTime) {
            NSTimeInterval interval = [self.endTime timeIntervalSinceDate:self.startTime];
            
            if (self.operationDelegate)
                [self.operationDelegate converseAPICacheBaseOperation:self reportsDuration:interval];
        }
    }
}

- (void)readData
{
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"Cache Read for: %@", self.URLString]);
    DGWeakify(self);
    [self.cache loadDataForURLString:self.URLString withCallback:^(DGConverseAPICacheResponse * _Nonnull response) {
        DGStrongify(self);
        
        self.result.error = response.error ? : self.result.error;
        if (response.record.data)
            self.result.data = [NSMutableData dataWithData:response.record.data];
        
        DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"Cache Read Result: %@", response.description]);
        
        [self finish];
        
    } onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
}

- (void)storeData
{
    // stores new value or overwrittes the existing one
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"Cache Write for: %@", self.URLString]);
    DGWeakify(self);
    
    NSInteger ttl = [self processTTLStorage];
    NSData *data = [self processDataStorage];
    
    [self.cache storeData:data forURLString:self.URLString ttl:ttl locked:self.locked withCallback:^(DGConverseAPICacheResponse * _Nonnull response) {
        DGStrongify(self);
        self.result.error = response.error ? : self.result.error;
        
        DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"Cache Write Result: %@", response.description]);
        /*!
         * @brief Reads recently Stored data to make it available it in the operation exchange response
         */
        [self readData];
        
    } onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
}

- (void)checkAndStoreData
{
    // reads for an existing value. It does not overwrite any value, just stores new one if it does not exist
    DGWeakify(self);
    [self.cache touchDataForURLString:self.URLString callback:^(DGConverseAPICacheResponse * _Nonnull response) {
        DGStrongify(self);
        
        self.result.error = response.error ? : self.result.error;
        
        switch(response.result){
                
            case SPTPersistentCacheResponseCodeNotFound:
            {
                [self storeData];
            }
                break;
            case SPTPersistentCacheResponseCodeOperationError:
            case SPTPersistentCacheResponseCodeOperationSucceeded:
            {
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, [NSString stringWithFormat:@"Cache Existing Data for: %@", self.URLString]);
                [self finish];
            }
                break;
        }
    } onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
}

#pragma mark - Overriding Methods

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        if (self.result.error || !self.URLString) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected URL, no more processing needed");
            
            [self finish];
        } else {
            if ([self hasDataToProcess]) {
                [self checkAndStoreData];
            } else {
                [self readData];
            }
        }
    }
}

#pragma mark - Helper Methods

- (BOOL)hasDataToProcess
{
    return (self.result.data.length || self.result.responseObj);
}

- (NSInteger)processTTLStorage
{
    NSInteger ttl = 0;
    
    DGConverseNetworkOperationResultExtraInfo *extraInfoNetworkResult = [self.result.extraInfo objectForKey:[DGConverseNetworkOperationResultExtraInfo key]];
    
    if (extraInfoNetworkResult.responseURL) {
//        NSArray<NSHTTPCookie *> *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:self.URLString]];
//        if(cookies.count) {
//            NSHTTPCookie *cookie = cookies.firstObject;
//            NSDate *expirationDate = cookie.expiresDate;
//            ttl = [expirationDate timeIntervalSinceNow] > 0 ? [expirationDate timeIntervalSinceNow] : 0;
//        }
//        NSHTTPURLResponse *response = (NSHTTPURLResponse *)extraInfoNetworkResult.responseURL;
//        NSString *cacheControlStr = response.allHeaderFields[@"Cache-Control"];
//        
    }
    
    return ttl;
}

- (NSData *)processDataStorage
{
    NSData *data;
    // FIXME: NSURL check should be avoided...
    if (self.result.responseObj && [self.result.responseObj isKindOfClass:[NSURL class]])
        data = [NSData dataWithContentsOfURL:self.result.responseObj];
    else if (self.result.data.length)
        data = self.result.data;
    
    return data;
}

@end

