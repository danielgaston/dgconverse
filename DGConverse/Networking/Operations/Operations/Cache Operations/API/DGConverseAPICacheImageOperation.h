//
//  DGConverseAPICacheImageOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPICacheBaseOperation.h"

@interface DGConverseAPICacheImageOperation : DGConverseAPICacheBaseOperation

@end
