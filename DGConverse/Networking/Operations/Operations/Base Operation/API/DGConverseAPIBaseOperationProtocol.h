//
//  DGConverseAPIBaseOperationProtocol.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPIBaseOperationProtocol_h
#define DGConverseAPIBaseOperationProtocol_h

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class DGConverseAPICacheBaseOperation;
@protocol DGConverseAPICacheBaseOperationDelegate
@optional
- (void)converseAPICacheBaseOperation:(DGConverseAPICacheBaseOperation*)op reportsDuration:(NSTimeInterval)interval;

@end

#pragma mark -

@class DGConverseAPINetworkBaseOperation;
@protocol DGConverseAPIOperationResult;
@protocol DGConverseAPINetworkBaseOperationDelegate

- (NSURLSession *)converseAPINetworkBaseOperationRequestDefaultSession:(DGConverseAPINetworkBaseOperation*)op;
- (NSURLSession *)converseAPINetworkBaseOperationRequestBackgroundSession:(DGConverseAPINetworkBaseOperation*)op;
- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op didStartWithTask:(NSURLSessionTask *)task;
- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op didDiscardTask:(NSURLSessionTask *)task;
- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op requiresStoreCompletion:(void (^)(id<DGConverseAPIOperationResult>))completionBlock forURLSessionTask:(NSURLSessionTask *)sessionTask;

@optional
- (void)converseAPINetworkBaseOperation:(DGConverseAPINetworkBaseOperation*)op reportsKBperSecond:(CGFloat)KBperSec;

@end

#pragma mark -


@class DGConverseAPIParseBaseOperation;
@protocol DGConverseAPIParseBaseOperationDelegate
@optional
- (void)converseAPIParseBaseOperation:(DGConverseAPIParseBaseOperation*)op reportsDuration:(NSTimeInterval)interval;

@end

#pragma mark -


@class DGConverseAPIConversionBaseOperation;
@protocol DGConverseAPIConversionBaseOperationDelegate
@optional
- (void)converseAPIConversionBaseOperation:(DGConverseAPIConversionBaseOperation*)op reportsDuration:(NSTimeInterval)interval;

@end

#pragma mark -

@protocol DGConverseAPIOperationDiscardable

- (void)reportResult;

@end

#endif /* DGConverseAPIBaseOperationProtocol_h */


NS_ASSUME_NONNULL_END
