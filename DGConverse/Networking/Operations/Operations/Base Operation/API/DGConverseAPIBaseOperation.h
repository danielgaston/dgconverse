//
//  DGConverseAPIBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationResponse.h"
#import "DGConverseAPIDebugUtils.h"
#import "DGConverseAPIBaseOperationProtocol.h"

@interface NSOperation (UUID)

@property (nonatomic, readwrite) NSUUID *opUUID;

@end

#pragma mark-

/*!
 * @brief Discardable category helps to cancel concurrently running network operations sharing the SAME request (URLRequest).
 * @discussion Category used by both NSOperations (All) and NSBlockOperations (Adapter). Imagine the case where two consecutive network requests are triggered i.e. for an image download. First is launched and running, when the second one is launched, detects if there is any other network operation sharing the same URLRequest. If so, the first one is cancelled (and completed since is an NSOperation) and the second one is completed. IsDiscarded attribute makes the cancelled first operation does not execute its completion block, so that the client will never receive two callbacks (cancelled and completed), but just one (completed).
 */
@interface NSOperation (Discardable)

@property (nonatomic, readwrite, assign) BOOL isDiscarded;

@end

#pragma mark-

@interface DGConverseAPIBaseOperation : NSOperation

@property (nonatomic, strong) id<DGConverseAPIOperationResult> result;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

/*!
 * @brief Finishes the execution of the operation
 * @warning This shouldn’t be called externally as this is used internally by subclasses. To cancel an operation use cancel instead.
 */
- (void)finish;

/*!
 * @brief Checks current operation state to cancel it as soon as possible if necessary
 */
- (BOOL)isValid;

@end


