//
//  DGConverseAPIOperationResponse.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DGConverseAPIOperationResult <NSObject, NSCopying>

@property (nonatomic, strong)   NSMutableData       *data;
@property (nonatomic, strong)   id                  responseObj;
@property (nonatomic, strong)   NSError             *error;
@property (nonatomic, strong)   NSMutableDictionary *extraInfo;

- (instancetype)deepCopy;

@end

@protocol DGConverseAPIOperationResultExtraInfo <NSObject>

+ (NSString *)key;

@end

#pragma mark -

@interface DGConverseExchangeOperationResult : NSObject <DGConverseAPIOperationResult>

@property (nonatomic, strong)   NSMutableData       *data;
@property (nonatomic, strong)   id                  responseObj;
@property (nonatomic, strong)   NSError             *error;
@property (nonatomic, strong)   NSMutableDictionary *extraInfo;

- (instancetype)deepCopy;

@end

#pragma mark -

@interface DGConverseNetworkOperationResult : NSObject <DGConverseAPIOperationResult>

@property (nonatomic, strong)   NSMutableData       *data;
@property (nonatomic, strong)   NSURL               *responseObj;
@property (nonatomic, strong)   NSError             *error;
@property (nonatomic, strong)   NSMutableDictionary *extraInfo;

- (instancetype)deepCopy;

@end

#pragma mark -

@interface DGConverseNetworkOperationResultExtraInfo : NSObject

+ (NSString *)key;

@property (nonatomic, copy)     NSURLSessionTask    *task;
@property (nonatomic, strong)   NSURLResponse       *responseURL;

@end

