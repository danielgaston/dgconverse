//
//  DGConverseAPIBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"
#import <objc/runtime.h>

@implementation NSOperation (UUID)

- (NSUUID *)opUUID
{
    return objc_getAssociatedObject(self, @selector(opUUID));
}

- (void)setOpUUID:(NSUUID *)opUUIDValue
{
    objc_setAssociatedObject(self, @selector(opUUID), opUUIDValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

#pragma mark -

@implementation NSOperation (Discardable)

- (BOOL)isDiscarded
{
    NSNumber *number = objc_getAssociatedObject(self, @selector(isDiscarded));
    return [number boolValue];
}

- (void)setIsDiscarded:(BOOL)discardableValue
{
    NSNumber *number = [NSNumber numberWithBool:discardableValue];
    objc_setAssociatedObject(self, @selector(isDiscarded), number, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

#pragma mark -

@implementation DGConverseAPIBaseOperation

/*
 We need to do old school synthesizing as the compiler has trouble creating the internal ivars.
 */
@synthesize executing = _executing;
@synthesize finished = _finished;

#pragma mark - Object Life Cycle Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.opUUID = [NSUUID UUID];
        self.result = [DGConverseExchangeOperationResult new];
    }
    return self;
}

- (void)dealloc
{
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel0, [NSString stringWithFormat:@"DEALLOC %@ %@", self.name, self.opUUID]);
}

#pragma mark - Public Methods

- (void)finish
{
    self.endTime = [NSDate date];
    if (self.executing) {
        // DG: makes the operation be removed from the queue. The queue monitors those two values using key-value observing.
        self.executing = NO;
        self.finished = YES;
        DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel0, [NSString stringWithFormat:@"%@ Operation Finished. %@", self.name, self.opUUID]);
    }
}

- (BOOL)isValid
{
    if (self.cancelled) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - Overriding Methods

- (void)start
{
    self.startTime = [NSDate date];
    
    if (![self isValid]){
        self.finished = YES;
    } else if (!self.isExecuting) {
        self.executing = YES;
        self.finished = NO;
        self.startTime = [NSDate date];
        DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel0, [NSString stringWithFormat:@"%@ Operation Started. %@", self.name, self.opUUID]);
    }
}

#pragma mark - Getters & Setters

- (void)setExecuting:(BOOL)executing
{
    if (_executing != executing) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
        _executing = executing;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
    }
}

- (BOOL)isExecuting
{
    return _executing;
}

- (void)setFinished:(BOOL)finished
{
    if (_finished != finished) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
        _finished = finished;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
    }
}

- (BOOL)isFinished
{
    return _finished;
}

- (BOOL)isAsynchronous
{
    return YES;
}

#pragma mark - Helper Methods

- (void)cancel
{
    [super cancel];
    
    DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagWarn, kDGConverseLogLevel0, [NSString stringWithFormat:@"%@ Operation Cancelled. %@", self.name, self.opUUID]);
    [self finish];
}

@end
