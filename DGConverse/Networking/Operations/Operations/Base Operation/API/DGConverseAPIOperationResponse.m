//
//  DGConverseAPIOperationResponse.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationResponse.h"

@implementation DGConverseExchangeOperationResult

- (instancetype)init
{
    self = [super init];
    if (self) {
        _extraInfo = @{}.mutableCopy;
    }
    return self;
}

#pragma mark - NSCopying Protocol Methods

- (instancetype)deepCopy
{
    return [self copy];
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    DGConverseExchangeOperationResult *copy = [[[self class] allocWithZone:zone] init];
    [copy setData:[NSMutableData dataWithData:[self data]]];
    [copy setResponseObj:[self responseObj]];
    [copy setError:[self error]];
    [copy setExtraInfo:[self extraInfo]];
    
    return copy;
}

@end

#pragma mark -

@implementation DGConverseNetworkOperationResult

- (instancetype)init
{
    self = [super init];
    if (self) {
        _extraInfo = @{}.mutableCopy;
    }
    return self;
}

#pragma mark - NSCopying Protocol Methods

- (instancetype)deepCopy
{
    return [self copy];
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    DGConverseNetworkOperationResult *copy = [[[self class] allocWithZone:zone] init];
    [copy setData:[NSMutableData dataWithData:[self data]]];
    [copy setResponseObj:[self responseObj]];
    [copy setError:[self error]];
    [copy setExtraInfo:[self extraInfo]];

    return copy;
}

@end

#pragma mark -

@implementation DGConverseNetworkOperationResultExtraInfo

+ (NSString *)key
{
    return @"DGConverseNetworkOperationResultExtraInfo";
}

@end
