//
//  DGConverseAPIParseBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"

NS_ASSUME_NONNULL_BEGIN

@interface DGConverseAPIParseBaseOperation : DGConverseAPIBaseOperation

@property (nonatomic, weak) id<DGConverseAPIParseBaseOperationDelegate> operationDelegate;

@end

NS_ASSUME_NONNULL_END
