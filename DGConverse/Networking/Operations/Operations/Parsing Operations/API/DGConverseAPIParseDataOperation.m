//
//  DGConverseAPIParseDataOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIParseDataOperation.h"

@interface DGConverseAPIParseDataOperation ()
/*!
 * @brief Generic completion block
 * @warning Must be synthesized with a custom operation block on each child operation
 */
@property (nonatomic, copy) void (^completion)(id);

@end

@implementation DGConverseAPIParseDataOperation

- (void)reportResult
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (!self.isDiscarded && self.completion) {
            self.completion(self.result);
        }
    }];
}

@end
