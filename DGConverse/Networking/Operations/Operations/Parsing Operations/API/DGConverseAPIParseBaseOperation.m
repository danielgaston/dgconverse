//
//  DGConverseAPIParseBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIParseBaseOperation.h"

@implementation DGConverseAPIParseBaseOperation

#pragma mark - Public Methods

- (void)finish
{
    [super finish];
    
    if ([self isValid]) {
        
        if (self.result.error) {
            DGConverseLog(kDGConverseLogTypeParser, kDGConverseLogFlagError, kDGConverseLogLevel1, [NSString stringWithFormat:@"Failed to parse response object: %@", self.result.error.localizedDescription]);
        }
        
        if (self.startTime && self.endTime) {
            NSTimeInterval interval = [self.endTime timeIntervalSinceDate:self.startTime];
            
            if (self.operationDelegate)
                [self.operationDelegate converseAPIParseBaseOperation:self reportsDuration:interval];
        }
    }
}

@end


