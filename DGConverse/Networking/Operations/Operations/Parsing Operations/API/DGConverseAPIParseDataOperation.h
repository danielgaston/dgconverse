//
//  DGConverseAPIParseDataOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIParseBaseOperation.h"

@interface DGConverseAPIParseDataOperation : DGConverseAPIParseBaseOperation <DGConverseAPIOperationDiscardable>

/*!
 * @brief Checks the opearation has not been discarded and executes the completion block
 */
- (void)reportResult;

@end
