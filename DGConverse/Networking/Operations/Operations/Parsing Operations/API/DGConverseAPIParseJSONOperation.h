//
//  DGConverseAPIParseJSONOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIParseDataOperation.h"

#pragma mark - Result Objects

@interface DGConverseAPIJSONResult : DGConverseExchangeOperationResult
@end

#pragma mark -

#pragma mark - Result Block typedefs

typedef void (^DGConverseAPIJSONResultBlock)(DGConverseAPIJSONResult* result);

#pragma mark -

#pragma mark - Parse Operations

@interface DGConverseAPIParseJSONOperation : DGConverseAPIParseDataOperation

@property (nonatomic, strong) DGConverseAPIJSONResult *result;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(DGConverseAPIJSONResultBlock)completion NS_DESIGNATED_INITIALIZER;

@end

