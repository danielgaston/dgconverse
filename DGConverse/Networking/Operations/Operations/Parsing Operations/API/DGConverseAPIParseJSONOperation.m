//
//  DGConverseAPIParseJSONOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIParseJSONOperation.h"

#pragma mark - Result Objects

@implementation DGConverseAPIJSONResult
@synthesize responseObj;
@end

#pragma mark -


#pragma mark - Parse Operations

@interface DGConverseAPIParseJSONOperation ()

@property (nonatomic, copy) DGConverseAPIJSONResultBlock completion;

@end

@implementation DGConverseAPIParseJSONOperation
@synthesize completion;
@synthesize result;

- (instancetype)initWithOperationDelegate:(id<DGConverseAPIParseBaseOperationDelegate>)operationDelegate
                               completion:(DGConverseAPIJSONResultBlock)completion
{
    self = [super init];
    if (self) {
        self.name = @"Parse JSON Operation";
        self.operationDelegate = operationDelegate;
        self.completion = completion;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        
        if (self.result.error || !self.result.data.length) {
            if (self.result.error)
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagError, kDGConverseLogLevel1, @"Dependent operation failed, no more processing needed");
            else
                DGConverseLog(kDGConverseLogTypeOperations, kDGConverseLogFlagInfo, kDGConverseLogLevel1, @"Dependent operation DID NOT injected data, no more processing needed");
            
        } else {
            NSError* serializationError;
            self.result.responseObj = [NSJSONSerialization JSONObjectWithData:self.result.data
                                                                      options:NSJSONReadingMutableContainers
                                                                        error:&serializationError];
            if (serializationError) {
                self.result.error = serializationError;
            }
        }
        
        [self reportResult];
        
        [self finish];
        
    } else {
        [self cancel];
    }
}

@end

#pragma mark -

