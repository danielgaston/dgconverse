//
//  DGConverseAPIParseOperationHeader.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#ifndef DGConverseAPIParseOperationHeader_h
#define DGConverseAPIParseOperationHeader_h

#import "DGConverseAPIParseBaseOperation.h"
#import "DGConverseAPIParseDataOperation.h"
#import "DGConverseAPIParseJSONOperation.h"

#endif /* DGConverseAPIParseOperationHeader_h */
