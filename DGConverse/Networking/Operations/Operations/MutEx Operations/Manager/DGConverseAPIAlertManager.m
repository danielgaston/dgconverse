//
//  DGConverseAPIAlertManager.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIAlertManager.h"
#import "DGConverseAPIMutExAlertOperation.h"
#import "DGConverseAPIOperationQueue.h"

@interface DGConverseAPIAlertManager ()

@property (nonatomic) DGConverseAPIOperationQueue *internalQueue;

@end

@implementation DGConverseAPIAlertManager

#pragma mark - Object Lifecycle Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        _internalQueue = [[DGConverseAPIOperationQueue alloc] init];
        _internalQueue.qualityOfService = NSQualityOfServiceUserInitiated;
        _internalQueue.name = @"Internal Generic Purpose Queue";
        _internalQueue.suspended = NO;
    }
    return self;
}

- (void)dealloc
{
    [self.internalQueue cancelAllOperations];
}


#pragma mark - Public Methods

- (void)alertWithData:(DGConverseAPIMutExAlertData *)alertData
{
    DGConverseAPIMutExAlertOperation *alertOp = [[DGConverseAPIMutExAlertOperation alloc] initAlertWithData:alertData];
    [self.internalQueue addOperation:alertOp];
}

- (void)alertsWithData:(NSArray<DGConverseAPIMutExAlertData*> *)alertDataArray
{
    for (DGConverseAPIMutExAlertData* alertData in alertDataArray) {
        [self alertWithData:alertData];
    }
}

- (void)alertSingleInstanceWithData:(DGConverseAPIMutExAlertData *)alertData
{
    DGConverseAPIMutExAlertOperation *alertOp = [[DGConverseAPIMutExAlertOperation alloc] initAlertWithData:alertData asSingleInstanceExclusive:YES];
    [self.internalQueue addOperation:alertOp];
}

- (void)sheetWithData:(DGConverseAPIMutExAlertData *)alertData
{
    DGConverseAPIMutExAlertOperation *sheetOp = [[DGConverseAPIMutExAlertOperation alloc] initSheetWithData:alertData];
    [self.internalQueue addOperation:sheetOp];
}

- (void)sheetsWithData:(NSArray<DGConverseAPIMutExAlertData*> *)alertDataArray
{
    for (DGConverseAPIMutExAlertData* alertData in alertDataArray) {
        [self sheetWithData:alertData];
    }
}

- (void)sheetSingleInstanceWithData:(DGConverseAPIMutExAlertData *)alertData
{
    DGConverseAPIMutExAlertOperation *sheetOp = [[DGConverseAPIMutExAlertOperation alloc] initSheetWithData:alertData asSingleInstanceExclusive:YES];
    [self.internalQueue addOperation:sheetOp];
}

@end
