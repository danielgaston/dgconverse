//
//  DGConverseAPIAlertManager.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DGConverseAPIMutExAlertData.h"

@interface DGConverseAPIAlertManager : NSObject

- (void)alertWithData:(DGConverseAPIMutExAlertData *)alertData;
- (void)alertsWithData:(NSArray<DGConverseAPIMutExAlertData*> *)alertDataArray;
- (void)alertSingleInstanceWithData:(DGConverseAPIMutExAlertData *)alertData;

- (void)sheetWithData:(DGConverseAPIMutExAlertData *)alertData;
- (void)sheetsWithData:(NSArray<DGConverseAPIMutExAlertData*> *)alertDataArray;
- (void)sheetSingleInstanceWithData:(DGConverseAPIMutExAlertData *)alertData;

@end
