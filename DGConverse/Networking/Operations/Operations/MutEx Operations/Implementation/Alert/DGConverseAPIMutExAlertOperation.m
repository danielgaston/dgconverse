//
//  DGConverseAPIMutExAlertOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIMutExAlertOperation.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@interface DGConverseAPIMutExAlertOperation ()

@property (nonatomic, strong) UIAlertController* alertController;
@property (nonatomic, strong) DGConverseAPIMutExAlertData *alertData;

@end

@implementation DGConverseAPIMutExAlertOperation

#pragma mark - Operation LifeCycle Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"Alert MutEx Operation";
        self.isSingleInstanceExclusive = YES;
    }
    return self;
}

- (void)start
{
    [super start];
    
    if ([self isValid]) {
        
        [self performSelectorOnMainThread:@selector(presentViewControllerAnimated) withObject:nil waitUntilDone:NO];
        
    } else {
        [self cancel];
    }
}

#pragma mark - Public Methods

- (instancetype)initAlertWithData:(DGConverseAPIMutExAlertData *)alertData
{
    return [self initAlertWithData:alertData asSingleInstanceExclusive:NO];
}

- (instancetype)initSheetWithData:(DGConverseAPIMutExAlertData *)alertData
{
    return [self initSheetWithData:alertData asSingleInstanceExclusive:NO];
}

- (instancetype)initAlertWithData:(DGConverseAPIMutExAlertData *)alertData asSingleInstanceExclusive:(BOOL)singleExclusive
{
    self = [self init];
    if (self) {
        _alertData = alertData;
        self.isSingleInstanceExclusive = singleExclusive;
        [self initializeAlert];
    }
    return self;
}

- (instancetype)initSheetWithData:(DGConverseAPIMutExAlertData *)alertData asSingleInstanceExclusive:(BOOL)singleExclusive
{
    self = [self init];
    if (self) {
        _alertData = alertData;
        self.isSingleInstanceExclusive = singleExclusive;
        [self initializeSheet];
    }
    return self;
}

#pragma mark - Initialization Methods

- (void)initializeAlert
{
    self.alertController = [UIAlertController alertControllerWithTitle:self.alertData.title
                                                               message:self.alertData.message
                                                        preferredStyle:UIAlertControllerStyleAlert];
    
    if (self.alertData.actionsArray.count > 0) {
        // action buttons
        [self.alertData.actionsArray enumerateObjectsUsingBlock:^(DGConverseAPIMutExAlertAction *_Nonnull buttonAction, NSUInteger idx, BOOL* _Nonnull stop) {
          
            DGWeakify(self);
            [self.alertController addAction:[UIAlertAction actionWithTitle:buttonAction.title
                                                                     style:buttonAction.style
                                                                   handler:^(UIAlertAction* _Nonnull action) {
                                                                       DGStrongify(self);
                                                                       buttonAction.handler();
                                                                       [self finish];
                                                                   }]];
        }];
    } else {
        // default cancel button
        DGWeakify(self);
        [self.alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", @"Ok")
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * _Nonnull action) {
                                                                   DGStrongify(self);
                                                                   [self finish];
                                                               }]];
    }
}

- (void)initializeSheet
{
    UIAlertControllerStyle alertStyle = IS_IPAD ? UIAlertControllerStyleAlert : UIAlertControllerStyleActionSheet;
    
    self.alertController = [UIAlertController alertControllerWithTitle:self.alertData.title
                                                               message:self.alertData.message
                                                        preferredStyle:alertStyle];
    
    if (self.alertData.actionsArray.count > 0) {
        // action buttons
        [self.alertData.actionsArray enumerateObjectsUsingBlock:^(DGConverseAPIMutExAlertAction *_Nonnull buttonAction, NSUInteger idx, BOOL* _Nonnull stop) {
            
            DGWeakify(self);
            [self.alertController addAction:[UIAlertAction actionWithTitle:buttonAction.title
                                                      style:buttonAction.style
                                                    handler:^(UIAlertAction* action) {
                                                        DGStrongify(self);
                                                        buttonAction.handler();
                                                        [self finish];
                                                    }]];
            
        }];
    } else {
        // default cancel button
        DGWeakify(self);
        [self.alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * _Nonnull action) {
                                                                   DGStrongify(self);
                                                                   [self finish];
                                                               }]];
    }
}

#pragma mark - Helper Methods

- (void)presentViewControllerAnimated
{
     if (self.alertController) {
         [[[[[UIApplication sharedApplication] keyWindow] rootViewController] navigationController] presentViewController:self.alertController animated:YES completion:nil];
     }
}

@end

