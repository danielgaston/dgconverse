//
//  DGConverseAPIMutExAlertData.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^__nullable DGConverseAPIAlertHandlerBlock)(void);

@interface DGConverseAPIMutExAlertAction : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic, copy) DGConverseAPIAlertHandlerBlock handler;
@property (nonatomic, assign) UIAlertActionStyle style;

- (instancetype)initWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(DGConverseAPIAlertHandlerBlock)handler;

@end

#pragma mark -

@interface DGConverseAPIMutExAlertData : NSObject

@property (nonatomic) NSString* _Nullable title;
@property (nonatomic) NSString* _Nullable message;
@property (nonatomic) NSArray<DGConverseAPIMutExAlertAction *>* _Nullable actionsArray;

- (void)addButtonWithTitle:(NSString* _Nonnull)title andAction:(DGConverseAPIAlertHandlerBlock)handler;
- (void)addButtonsWithTitles:(NSArray* _Nonnull)titles andActions:(NSArray<DGConverseAPIAlertHandlerBlock>* _Nonnull)actions;

- (void)addCancelButtonWithTitle:(NSString* _Nonnull)title andAction:(DGConverseAPIAlertHandlerBlock)handler;
- (void)addCancelButtonWithAction:(DGConverseAPIAlertHandlerBlock)handler;

- (void)addDestructiveButtonWithTitle:(NSString* _Nonnull)title andAction:(DGConverseAPIAlertHandlerBlock)handler;

@end

NS_ASSUME_NONNULL_END
