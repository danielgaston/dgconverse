//
//  DGConverseAPIMutExAlertData.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIMutExAlertData.h"

@implementation DGConverseAPIMutExAlertAction

- (instancetype)initWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(DGConverseAPIAlertHandlerBlock)handler
{
    self = [super init];
    if (self) {
        _title = title;
        _style = style;
        _handler = handler ?: ^{};
    }
    return self;
}

@end

#pragma mark -

@implementation DGConverseAPIMutExAlertData

- (instancetype)init
{
    self = [super init];
    if (self) {
        _title = nil;
        _message = nil;
    }
    return self;
}

- (void)addButtonWithTitle:(NSString* _Nonnull)title style:(UIAlertActionStyle)style handler:(DGConverseAPIAlertHandlerBlock)handler
{
    DGConverseAPIMutExAlertAction *action = [[DGConverseAPIMutExAlertAction alloc] initWithTitle:title style:style handler:handler];

    if (self.actionsArray.count > 0) {
        
        NSMutableArray *tempMutableArray = [self.actionsArray mutableCopy];
        [tempMutableArray addObject:action];
        _actionsArray = tempMutableArray;
    } else {
        _actionsArray = @[ action ];
    }
}

- (void)addButtonWithTitle:(NSString* _Nonnull)title andAction:(DGConverseAPIAlertHandlerBlock)handler
{
    [self addButtonWithTitle:title style:UIAlertActionStyleDefault handler:handler];
}

- (void)addDestructiveButtonWithTitle:(NSString* _Nonnull)title andAction:(DGConverseAPIAlertHandlerBlock)handler
{
    [self addButtonWithTitle:title style:UIAlertActionStyleDestructive handler:handler];
}

- (void)addCancelButtonWithTitle:(NSString* _Nonnull)title andAction:(DGConverseAPIAlertHandlerBlock)handler
{
    [self addButtonWithTitle:title style:UIAlertActionStyleCancel handler:handler];
}

- (void)addCancelButtonWithAction:(DGConverseAPIAlertHandlerBlock)handler
{
    [self addButtonWithTitle:NSLocalizedString(@"Dismiss", @"Dismiss") style:UIAlertActionStyleCancel handler:handler];
}

- (void)addButtonsWithTitles:(NSArray<NSString *>*)titles andActions:(NSArray<DGConverseAPIAlertHandlerBlock>*)actions
{
    NSUInteger minIterator = MIN(titles.count, actions.count);
    
    for (int i = 0; i < minIterator; i++) {
        [self addButtonWithTitle:titles[i] andAction:actions[i]];
    }
}

@end
