//
//  DGConverseAPIMutExAlertOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIMutExBaseOperation.h"
#import "DGConverseAPIMutExAlertData.h"

@interface DGConverseAPIMutExAlertOperation : DGConverseAPIMutExBaseOperation

/*!
 * @brief Consecutive Alert Operations are added with serial dependency.
 * @discussion 'isMutuallyExclusive' is Not editable, belongs to each MutExBaseOperation subclass (YES by default). Assures consecutive operations are serial queued. 'isSingleInstanceExclusive' is editable and assures only one operation instance is either queued or running
 * @warning if 'isMutuallyExclusive' is YES (default), a second operation will not be executed until a first one finishes. If 'isSingleInstanceExclusive' is YES, a second operation will be discarded if a first one is queued or running. Only one instance can be present at a time.
 */
- (instancetype)initAlertWithData:(DGConverseAPIMutExAlertData *)alertData;
- (instancetype)initSheetWithData:(DGConverseAPIMutExAlertData *)alertData;

- (instancetype)initAlertWithData:(DGConverseAPIMutExAlertData *)alertData asSingleInstanceExclusive:(BOOL)singleExclusive;
- (instancetype)initSheetWithData:(DGConverseAPIMutExAlertData *)alertData asSingleInstanceExclusive:(BOOL)singleExclusive;

@end
