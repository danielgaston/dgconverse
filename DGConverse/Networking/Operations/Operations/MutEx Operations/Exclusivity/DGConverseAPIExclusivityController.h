//
//  DGConverseAPIExclusivityController.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DGConverseAPIMutExBaseOperation.h"

/**
 * The exclusivity controller will guaranty that the condition that mutually 
 * excludes each other will _always_ run one after the other and never in 
 * parallel.
 * This class is a singleton to keep track of all mutex. However it is OK to 
 * instanciate it for a local mutal exclusion
 */
@interface DGConverseAPIExclusivityController : NSObject

- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)sharedInstance;
- (instancetype)initLocalInstance NS_DESIGNATED_INITIALIZER;

- (BOOL)validateOperation:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation withCategories:(NSArray*)categories;
- (void)addOperation:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation withCategories:(NSArray*)categories;
- (void)removeOperation:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation withCategories:(NSArray*)categories;

@end
