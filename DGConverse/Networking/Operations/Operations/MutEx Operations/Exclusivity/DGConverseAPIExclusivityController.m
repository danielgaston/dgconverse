//
//  DGConverseAPIExclusivityController.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <dispatch/dispatch.h>

#import "DGConverseAPIBaseOperation.h"
#import "DGConverseAPIExclusivityController.h"

static DGConverseAPIExclusivityController* sharedExclusivityController;

@interface DGConverseAPIExclusivityController ()

@property (nonatomic, strong) NSMapTable<NSString *, NSMutableArray<DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *>*>* operationsLock;
@property (nonatomic) dispatch_queue_t dispatchQueue;

@end

@implementation DGConverseAPIExclusivityController

#pragma mark - Class Methods

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedExclusivityController = [[self alloc] init];
        [sharedExclusivityController initialize];
    });
    return sharedExclusivityController;
}

#pragma mark - Public Methods

- (instancetype)initLocalInstance;
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

#pragma mark - Initialize Methods

- (void)initialize
{
    _operationsLock = [NSMapTable strongToStrongObjectsMapTable];
    _dispatchQueue = dispatch_queue_create("DGConverseOperationMutexList", DISPATCH_QUEUE_SERIAL);
}

#pragma mark - Public Methods

- (BOOL)validateOperation:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation withCategories:(NSArray*)categories
{
    __block BOOL isValid = YES;
    
    // Critical Synchronous section -> https://developer.apple.com/library/content/documentation/General/Conceptual/ConcurrencyProgrammingGuide/ThreadMigration/ThreadMigration.html#//apple_ref/doc/uid/TP40008091-CH105-SW1
    dispatch_sync(self.dispatchQueue, ^{
        
        [categories enumerateObjectsUsingBlock:^(NSString *_Nonnull category, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSMutableArray *ops = [self.operationsLock objectForKey:category];
            
            // isValid = YES if no operation is queued
            if (ops.count) {
                DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *op = [ops lastObject];
                // isValid = YES if there is one or more queued operations, but last one is not 'singleInstanceExclusive'
                if (op.isSingleInstanceExclusive) {
                    // isValid = NO if last queued operation is 'singleInstanceExclusive', making any other coming operation (with same class) to be discarded until this one has finished.
                    isValid = NO;
                    *stop = YES;
                }
            }
        }];
    });
    
    // please note it is a SYNC dispatch
    return isValid;
}

- (void)addOperation:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation withCategories:(NSArray*)categories
{
    // Critical Synchronous section -> https://developer.apple.com/library/content/documentation/General/Conceptual/ConcurrencyProgrammingGuide/ThreadMigration/ThreadMigration.html#//apple_ref/doc/uid/TP40008091-CH105-SW1
    dispatch_sync(self.dispatchQueue, ^{
        for (NSString* category in categories) {
            [self addMutexOn:operation toCategory:category];
        }
    });
}

- (void)removeOperation:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation withCategories:(NSArray*)categories
{
    // Critical Synchronous section -> https://developer.apple.com/library/content/documentation/General/Conceptual/ConcurrencyProgrammingGuide/ThreadMigration/ThreadMigration.html#//apple_ref/doc/uid/TP40008091-CH105-SW1
    dispatch_sync(self.dispatchQueue, ^{
        for (NSString* category in categories) {
            [self removeMutexOn:operation toCategory:category];
        }
    });
}

#pragma mark - Helper Methods

- (void)addMutexOn:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation toCategory:(NSString*)category
{
    if (![self.operationsLock objectForKey:category]) {
        [self.operationsLock setObject:[NSMutableArray new] forKey:category];
    }
    
    DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *previousOperation = [[self.operationsLock objectForKey:category] lastObject];
    if (previousOperation) {
        [operation addDependency:previousOperation];
    }
    
    [[self.operationsLock objectForKey:category] addObject:operation];
}

- (void)removeMutexOn:(DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation toCategory:(NSString*)category
{
    [[self.operationsLock objectForKey:category] removeObject:operation];
}

@end
