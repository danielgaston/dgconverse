//
//  DGConverseAPIMutExBaseOperation.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIBaseOperation.h"

@class DGConverseAPIMutExBaseOperation;

@protocol DGConverseAPIMutExObserver <NSObject>

- (void)operationDidFinish:(DGConverseAPIMutExBaseOperation*)mutexOperation;

@end

#pragma mark -

@interface DGConverseAPIMutExOperationBlockObserver : NSObject <DGConverseAPIMutExObserver>

- (instancetype)initWithFinishBlock:(void(^)(DGConverseAPIMutExBaseOperation *mutexOperation))finishBlock;

@end


#pragma mark - Mutual Exclusivity Operation

@protocol DGConverseAPIMutExOperation <NSObject>

@property (nonatomic, readonly) NSArray *observers;
@property (nonatomic, readonly) BOOL isMutuallyExclusive;
@property (nonatomic, readwrite) BOOL isSingleInstanceExclusive;

- (void)addObserver:(id<DGConverseAPIMutExObserver>)observer;
- (void)removeObserver:(id<DGConverseAPIMutExObserver>)observer;

@end


@interface DGConverseAPIMutExBaseOperation : DGConverseAPIBaseOperation <DGConverseAPIMutExOperation>

@property (nonatomic, readonly, strong) NSArray *observers;

/*!
 * @brief Consecutive MutEx Operations are added with serial dependency.
 * @discussion 'isMutuallyExclusive' is Not editable, belongs to each MutExBaseOperation subclass (YES by default). Assures consecutive operations are serial queued. 'isSingleInstanceExclusive' is editable and assures only one operation instance is either queued or running.
 * @warning if 'isMutuallyExclusive' is YES (default), a second operation will not be executed until a first one finishes. If 'isSingleInstanceExclusive' is YES, a second operation will be discarded if a first one is queued or running. Only one instance can be present at a time.
 */
@property (nonatomic, readonly) BOOL isMutuallyExclusive;
@property (nonatomic, readwrite) BOOL isSingleInstanceExclusive;

- (void)addObserver:(id<DGConverseAPIMutExObserver>)observer;
- (void)removeObserver:(id<DGConverseAPIMutExObserver>)observer;

@end
