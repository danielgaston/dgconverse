//
//  DGConverseAPIMutExBaseOperation.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIMutExBaseOperation.h"

@interface DGConverseAPIMutExOperationBlockObserver ()

@property (nonatomic, copy) void(^finishBlock)(DGConverseAPIMutExBaseOperation*);

@end

@implementation DGConverseAPIMutExOperationBlockObserver

#pragma mark -- Public Methods

- (instancetype)initWithFinishBlock:(void(^)(DGConverseAPIMutExBaseOperation *mutexOperation))finishBlock
{
    self = [super init];
    if (self) {
        _finishBlock = finishBlock;
    }
    return self;
}

- (void)operationDidFinish:(DGConverseAPIMutExBaseOperation *)mutexOperation
{
    if (self.finishBlock) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.finishBlock(mutexOperation);
        });
    }
}

@end

#pragma mark -

@interface DGConverseAPIMutExBaseOperation ()

@property (nonatomic, strong) NSMutableArray *_observers;

@end


@implementation DGConverseAPIMutExBaseOperation

#pragma mark - Operation LifeCycle Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"Mutual Exclusivity Operation";
        self._observers = @[].mutableCopy;
        _isMutuallyExclusive = YES;
        _isSingleInstanceExclusive = NO;
    }
    return self;
}


#pragma mark - Getters & Setters

- (NSArray*)observers
{
    return [NSArray arrayWithArray:self._observers];
}

#pragma mark - Overriden Methods

- (void)finish
{
    DGWeakify(self);
    for (id<DGConverseAPIMutExObserver> observer in self._observers) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DGStrongify(self);
            [self removeObserver:observer];
            [observer operationDidFinish:self];
        });
    }
    
    [super finish];
}

#pragma mark - DGConverseAPIMutExOperation Delegate Methods

- (void)addObserver:(id<DGConverseAPIMutExObserver>)observer
{
    [self._observers addObject:observer];
}

- (void)removeObserver:(id<DGConverseAPIMutExObserver>)observer
{
    [self._observers removeObject:observer];
}

@end




