//
//  DGConverseAPIOperationQueue.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "DGConverseAPIOperationQueue.h"
#import "DGConverseAPIMutExBaseOperation.h"
#import "DGConverseAPIExclusivityController.h"

@implementation DGConverseAPIOperationQueue

#pragma mark - Overriden Methods

- (void)addOperation:(nonnull NSOperation*)operation
{
	if ([operation conformsToProtocol:@protocol(DGConverseAPIMutExOperation)]) {

		DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> * mutexOp = (DGConverseAPIBaseOperation<DGConverseAPIMutExOperation> *)operation;
        NSMutableArray* mutexClasses = [NSMutableArray arrayWithObject:NSStringFromClass(mutexOp.class)];
        
        DGConverseAPIExclusivityController* mutexController = [DGConverseAPIExclusivityController sharedInstance];
        
        if ([mutexController validateOperation:mutexOp withCategories:mutexClasses]) {
            [mutexController addOperation:mutexOp withCategories:mutexClasses];
            
            DGConverseAPIMutExOperationBlockObserver *mutexObserver = [[DGConverseAPIMutExOperationBlockObserver alloc] initWithFinishBlock:^(DGConverseAPIMutExBaseOperation *mutexOperation) {
                [mutexController removeOperation:mutexOp withCategories:mutexClasses];
            }];
            
            [mutexOp addObserver:mutexObserver];

            [super addOperation:mutexOp];
        }

	} else {

		[super addOperation:operation];
	}
}

- (void)addOperations:(NSArray<NSOperation *> *)ops waitUntilFinished:(BOOL)wait
{
    for (NSOperation *operation in ops) {
        [self addOperation:operation];
    }
    
    if (wait) {
        for (NSOperation *operation in ops) {
            [operation waitUntilFinished];
        }
    }
}

@end
