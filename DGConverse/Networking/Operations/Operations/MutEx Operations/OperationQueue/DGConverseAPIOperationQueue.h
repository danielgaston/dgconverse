//
//  DGConverseAPIOperationQueue.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DGConverseAPIOperationQueue : NSOperationQueue

@end
