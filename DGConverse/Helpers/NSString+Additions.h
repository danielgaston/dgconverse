//
//  NSString+Additions.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

+ (NSString*)GetUUID;

- (BOOL)isValidEmail;
- (BOOL)isValidPassword;
- (BOOL)isValidIP;

+ (BOOL)isNilOrEmpty:(NSString*)string;
+ (BOOL)isNotNilNorEmpty:(NSString*)string;
- (BOOL)isEmpty;
- (BOOL)isNotEmpty;
- (NSArray*)arrayOfLinks;
- (NSArray*)locationsOfLinks;

+ (NSString*)randomAlphanumericStringWithLength:(NSInteger)length;
+ (NSString*)secureRandom;

- (NSString*)MD5Digest;
- (NSString*)sha1Hash;

// base 32 is filename safe
- (NSString*)base32String;

- (NSString*)trim;

- (NSString*)normalizeString;
- (NSString*)removeAllWhitespaces;
- (NSString*)removeEmoji;

- (NSString*)addHttpPrefix;

- (NSString*)cutSubstringFromParser:(NSString*)substring canBecomeEmpty:(BOOL)canBecomeEmpty;

- (NSString*)toSnake;

- (NSString*)cutSubstringFromParser:(NSString*)substring;

- (NSString*)appendStringIfNeeded:(NSString*)stringToAppend;

@end
