//
//  DGClangMacro.h
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#define DGClangDiagnosticPush _Pragma("clang diagnostic push")
#define DGClangDiagnosticPop _Pragma("clang diagnostic pop")

#define DGClangDiagnosticPushOption(option) \
    DGClangDiagnosticPush \
    _Pragma(option)

#define DGClangDiagnosticPopOption DGClangDiagnosticPop
