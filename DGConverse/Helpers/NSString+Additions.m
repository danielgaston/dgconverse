//
//  NSString+Additions.m
//  DGConverse
//
//  Copyright © 2019 Daniel Gastón. All rights reserved.
//

#import "NSString+Additions.h"
#import <CommonCrypto/CommonDigest.h>
#import <arpa/inet.h>
#import <netinet/in.h>

@implementation NSString (Additions)

#pragma mark - Cryptor

- (BOOL)isValidEmail
{
	NSString* emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidPassword
{
	return [self length] > 0;
}

- (BOOL)isValidIP
{
	const char* utf8 = [self UTF8String];

	// Check valid IPv4.
	struct in_addr dst;
	int success = inet_pton(AF_INET, utf8, &(dst.s_addr));
	if (success != 1) {
		// Check valid IPv6.
		struct in6_addr dst6;
		success = inet_pton(AF_INET6, utf8, &dst6);
	}
	return (success == 1);
}

+ (BOOL)isNilOrEmpty:(NSString*)string
{
	BOOL isNilOrEmpty = NO;

	if ((nil == string) || ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])) {
		isNilOrEmpty = YES;
	}

	return isNilOrEmpty;
}

+ (BOOL)isNotNilNorEmpty:(NSString*)string
{
	return ![self isNilOrEmpty:string];
}

- (BOOL)isEmpty
{
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0;
}

- (BOOL)isNotEmpty
{
	return ![self isEmpty];
}

+ (NSString*)randomAlphanumericStringWithLength:(NSInteger)length
{
	NSString* letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	NSMutableString* randomString = [NSMutableString stringWithCapacity:length];

	for (int i = 0; i < length; i++) {
		[randomString appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
	}

	return randomString;
}

+ (NSString*)secureRandom
{
	uint8_t bytes[50] = { 0 };
	int status = SecRandomCopyBytes(kSecRandomDefault, 50, bytes);
	if (status == -1) {
		return nil;
	}

	NSString* salt = [[NSString stringWithFormat:@"%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x",
								bytes[0], bytes[1], bytes[2], bytes[3],
								bytes[4], bytes[5], bytes[6], bytes[7],
								bytes[8], bytes[9], bytes[10], bytes[11],
								bytes[12], bytes[13], bytes[14], bytes[15],
								bytes[16], bytes[17], bytes[18], bytes[19],
								bytes[20], bytes[21], bytes[22], bytes[23],
								bytes[24], bytes[25], bytes[26], bytes[27],
								bytes[28], bytes[29], bytes[30], bytes[31],
								bytes[32], bytes[33], bytes[34], bytes[35],
								bytes[36], bytes[37], bytes[38], bytes[39],
								bytes[40], bytes[41], bytes[42], bytes[43],
								bytes[44], bytes[45], bytes[46], bytes[47],
								bytes[48], bytes[49]] stringByReplacingOccurrencesOfString:@" "
																				withString:@""];

	return salt;
}

- (NSString*)MD5Digest
{
	const char* input = [self UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(input, (CC_LONG)strlen(input), result);

	NSMutableString* digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
	for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
		[digest appendFormat:@"%02x", result[i]];
	}

	return digest;
}

- (NSString*)sha1Hash
{
	const char* cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
	NSData* data = [NSData dataWithBytes:cstr length:self.length];

	uint8_t digest[CC_SHA1_DIGEST_LENGTH];

	CC_SHA1(data.bytes, (int)data.length, digest);

	NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];

	for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
		[output appendFormat:@"%02x", digest[i]];

	return output;
}

- (NSArray*)arrayOfLinks
{
	NSString* regexToReplaceRawLinks = @"(\\b(https?):\\/\\/[-A-Z0-9+&@#\\/%?=~_|!:,.;]*[-A-Z0-9+&@#\\/%=~_|])";

	NSError* error = NULL;
	NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:regexToReplaceRawLinks
																		   options:NSRegularExpressionCaseInsensitive
																			 error:&error];

	NSArray* results = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];

	NSMutableArray* links = [NSMutableArray array];

	for (id result in results) {
		[links addObject:[self substringWithRange:[result range]]];
	}

	return links;
}

- (NSArray*)locationsOfLinks
{
	NSString* regexToReplaceRawLinks = @"(\\b(https?):\\/\\/[-A-Z0-9+&@#\\/%?=~_|!:,.;]*[-A-Z0-9+&@#\\/%=~_|])";

	NSError* error = NULL;
	NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:regexToReplaceRawLinks
																		   options:NSRegularExpressionCaseInsensitive
																			 error:&error];

	NSArray* results = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
	return results;
}

+ (NSString*)GetUUID
{
	return [[[NSUUID UUID] UUIDString] lowercaseString];
}

static const char _base32Alphabet[32] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

- (NSString*)base32String
{
	NSMutableString* string = [NSMutableString stringWithCapacity:(([self length] / 5) * 8)];

	const char* currentByte = [self cStringUsingEncoding:NSUTF8StringEncoding];
	NSUInteger byteOffset = 0;

	while (byteOffset < [self length]) {
		// Note: every 40 bits evaluates to 8 characters
		char characters[8] = "========";

		do {
			// Note: the first five bits depend on the first byte
			{
				uint8_t bits = ((*currentByte & /* 0b11111000 */ 248) >> 3);
				characters[0] = _base32Alphabet[bits];
			}

			// Note: the second five bits depend on the first byte
			{
				uint8_t bits = ((*currentByte & /* 0b00000111 */ 7) << 2);
				bits = bits | (((byteOffset + 1) > [self length]) ? 0 : ((*(currentByte + 1) & /* 0b11000000 */ 192) >> 6));
				characters[1] = _base32Alphabet[bits];
			}

			// Note: the third five bits depend on the second byte
			if ((byteOffset + 2) > [self length])
				break;
			{
				uint8_t bits = ((*(currentByte + 1) & /* 0b00111110 */ 62) >> 1);
				characters[2] = _base32Alphabet[bits];
			}

			// Note: the fourth five bits depend on the second byte
			{
				uint8_t bits = ((*(currentByte + 1) & /* 0b00000001 */ 1) << 4);
				bits = bits | ((byteOffset + 2 > [self length]) ? 0 : (((*(currentByte + 2)) & /* 0b11110000 */ 240) >> 4));
				characters[3] = _base32Alphabet[bits];
			}

			// Note: the fifth five bits depend on the third byte
			if ((byteOffset + 3) > [self length])
				break;
			{
				uint8_t bits = ((*(currentByte + 2) & /* 0b00001111 */ 15) << 1);
				bits = bits | ((byteOffset + 3 > [self length]) ? 0 : ((*(currentByte + 3) & /* 0b1000000 */ 128) >> 7));
				characters[4] = _base32Alphabet[bits];
			}

			// Note: the sixth five bits depend on the fourth byte
			if ((byteOffset + 4) > [self length])
				break;
			{
				uint8_t bits = ((*(currentByte + 3) & /* 0b01111100 */ 124) >> 2);
				characters[5] = _base32Alphabet[bits];
			}

			// Note: the seventh five bits depend on the fourth byte
			{
				uint8_t bits = ((*(currentByte + 3) & /* 0b00000011 */ 3) << 3);
				bits = bits | ((byteOffset + 4 > [self length]) ? 0 : ((*(currentByte + 4) & /* 0b11100000 */ 224) >> 5));
				characters[6] = _base32Alphabet[bits];
			}

			// Note: the eighth five bits depend on the fifth byte
			if ((byteOffset + 5) > [self length])
				break;
			{
				uint8_t bits = *(currentByte + 4) & /* 0b00011111 */ 31;
				characters[7] = _base32Alphabet[bits];
			}
		} while (NO);

		[string appendString:[[NSString alloc] initWithBytes:characters length:8 encoding:NSASCIIStringEncoding]];

		byteOffset += 5;
		currentByte += 5;
	}

	return string;
}

- (NSString*)removeAllWhitespaces
{
	return [self stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (NSString*)trim
{
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString*)addHttpPrefix
{
	if (![self hasPrefix:@"http://"] && ![self hasPrefix:@"https://"]) {
		return [NSString stringWithFormat:@"http://%@", self];
	}

	return self;
}

- (NSString*)cutSubstringFromParser:(NSString*)substring canBecomeEmpty:(BOOL)canBecomeEmpty
{
	if ([NSString isNilOrEmpty:substring])
		return self;

	NSRange range = [self rangeOfString:substring];
	if (range.location == NSNotFound)
		return self;

	if (range.location > 0 && range.location + range.length < self.length) {
		unichar charBefore = [self characterAtIndex:range.location - 1];
		unichar charAfter = [self characterAtIndex:range.location + range.length];
		NSCharacterSet* whitespaces = [NSCharacterSet whitespaceCharacterSet];
		if ([whitespaces characterIsMember:charBefore] && [whitespaces characterIsMember:charAfter])
			range.length++;
	}

	NSString* cuttedStr = [self stringByReplacingCharactersInRange:range withString:@""];
	NSString* trimmedStr = [cuttedStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if (canBecomeEmpty)
		return trimmedStr;
	else
		return trimmedStr.length ? trimmedStr : self;
}

- (NSString*)toSnake
{
	NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=[a-z])([A-Z])|([A-Z])(?=[a-z])" options:0 error:nil];
	NSString* underscoreString = [[regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, self.length) withTemplate:@"_$1$2"] lowercaseString];

	return underscoreString;
}

- (NSString*)cutSubstringFromParser:(NSString*)substring
{
	if ([NSString isNilOrEmpty:substring])
		return self;

	NSRange range = [self rangeOfString:substring];
	if (range.location == NSNotFound)
		return self;

	if (range.location > 0 && range.location + range.length < self.length) {
		unichar charBefore = [self characterAtIndex:range.location - 1];
		unichar charAfter = [self characterAtIndex:range.location + range.length];
		NSCharacterSet* whitespaces = [NSCharacterSet whitespaceCharacterSet];
		if ([whitespaces characterIsMember:charBefore] && [whitespaces characterIsMember:charAfter])
			range.length++;
	}

	NSString* cuttedStr = [self stringByReplacingCharactersInRange:range withString:@""];
	NSString* trimmedStr = [cuttedStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	return trimmedStr.length ? trimmedStr : self;
}

- (NSString*)normalizeString
{
	NSString* normalized = [self removeEmoji];
	normalized = [normalized uppercaseFirstChar];
	return normalized;
}

- (NSString*)removeEmoji
{
	__block NSMutableString* temp = [NSMutableString string];

	[self enumerateSubstringsInRange:NSMakeRange(0, [self length])
							 options:NSStringEnumerationByComposedCharacterSequences
						  usingBlock:
							  ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {

								  const unichar hs = [substring characterAtIndex:0];

								  // surrogate pair
								  if (0xd800 <= hs && hs <= 0xdbff) {
									  const unichar ls = [substring characterAtIndex:1];
									  const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;

									  [temp appendString:(0x1d000 <= uc && uc <= 0x1f77f) ? @"" : substring]; // U+1D000-1F77F

									  // non surrogate
								  } else {
									  [temp appendString:(0x2100 <= hs && hs <= 0x26ff) ? @"" : substring]; // U+2100-26FF
								  }
							  }];

	return temp;
}

- (NSString*)uppercaseFirstChar
{
	// [NSString capitalized] is not useful here since does not allow to have inner capital letters on the name

	NSString* temp = self;
	if (self.length) {
		NSString* firstCharUpper = [[self substringToIndex:1] uppercaseString];
		temp = [self stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstCharUpper];
	}
	return temp;
}

- (NSString*)appendStringIfNeeded:(NSString*)stringToAppend
{
	if (![[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] hasSuffix:stringToAppend]) {
		return [self stringByAppendingString:stringToAppend];
	}
	return self;
}

@end
